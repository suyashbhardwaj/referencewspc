/*THE DEFAULT EXPRESS SERVER*/
const defaultValues = {
  "theme": {
    "headerTitle": "Botgo",
    "headerAvatar": "",
    "headerAvatarStyle": {
      "content": "url(https://chatbot-project.s3.ap-south-1.amazonaws.com/chatbot/Images/icons8-female-profile-96.png)",
      "height": "30px",
      "width": "30px",
      "borderRadius": "50%",
      "background": "white",
      "marginRight": "20px"
    },
    "initiallyShow": false,
    "iconHref": "https://chatbot-project.s3.ap-south-1.amazonaws.com/botgo/avatars/avatar1.png",
    "botAvatar": "",
    "botAvatarStyle": {
      "content": "url(https://chatbot-project.s3.ap-south-1.amazonaws.com/chatbot/Images/icons8-online-support-96.png)",
      "height": "30px",
      "width": "30px",
      "borderRadius": "50%",
      "background": "blue"
    },
    "inputStyle": {
      "paddingLeft": "5px",
      "paddingRight": "5px",
      "paddingTop": "10px",
      "paddingBottom": "10px",
      "borderRadius": "4px",
      "backgroundColor": "#fff",
      "textTransform": "capitalize"
    },
    "placeholder": "Type here",
    "submitButtonStyle": {
      "color": "#888",
      "position": "absolute",
      "right": "20px",
      "bottom": "24px",
      "z-index": "1"
    },
    "width": "400px",
    "height": "450px",
    "titleBarStyle": {
      "background": "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFZZPgWO2-Tc5FMjcNi7VXWy-ZMkBomfCEpw&usqp=CAU)",
      "borderRadius": "2%"
    },
    "chatBackgroundStyle": {
      "backgroundColor": "#fff"
    },
    "calendarStyle": {
      "width": "300px"
    }
  },
  "webhook": {
    "isEnabled": false,
    "endpoint": "",
    "auth": {
      "username": "",
      "password": ""
    }
  }
}

import express from 'express';
import cors from 'cors'

const app = express();
// CORS is now enabled for all origins 
app.use(cors()); 
app.use(express.json());

app.get('/ping', (_req, res) => {
  res.send('pong');
});

app.get('/get-config', (_req, res) => {
  res.json(defaultValues);
});

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});


/*
libraries to be used: express, jsonwebtoken dotenv (to contain our secret tokens for JWT), nodemon, REST Client extension in VScode to make POST requests
01 SERVERSIDE: set up secret token & refresh tokens to a random letters string in process.env
02 SERVERSIDE: require('dotenv').config
03 SERVERSIDE: const accessToken = jwt.sign(payload, secret token from process.env); res.json({ accessToken: accessToken })
04 SERVERSIDE: app.get('/posts', authenticateToken, (req, res)) => { res.json(posts) }
05 SERVERSIDE: function authenticateToken(req,res,next) {const authHeader = req.headers['authorization']; const token = authHeader && authHeader.split(' ')[1]}
06 SERVERSIDE: if(token === null) return res.sendStatus(401) else jwt.verify(token,process.env.ACCESS_TOKEN_SECRET, (err,user)=>{if(err) return res.sendStatus(403)})
07 SERVERSIDE: else req.user = user; next()
08 CLIENTSIDE: Authorization: Bearer <token_received> 14:33
*/