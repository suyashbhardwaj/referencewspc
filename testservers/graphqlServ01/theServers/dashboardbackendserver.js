// This one is the explainer code
const { v1: uuid } = require('uuid')
const { GraphQLError } = require('graphql')

const { ApolloServer } = require('@apollo/server')
const { startStandaloneServer } = require('@apollo/server/standalone')

// THE DATA: in json form
const stockOfForms = [
            {
                bgAccountId: 1,
                formUuid: "5a0c996e7a469089c68af00ec4369c09",
                templateID: "8",
                name: "test check",
                description: "check",
                formData: [
                    {
                        size: "h1",
                        type: "heading",
                        color: "#000000",
                        alignment: "left",
                        formHeading: "Tour Reservation Form"
                    },
                    {
                        type: "subheading",
                        formSubHeading: "Let's know what you are interested to see!"
                    },
                    {
                        type: "text",
                        label: "Full Name",
                        placeholder: "Full name"
                    },
                    {
                        type: "text",
                        label: "Email:",
                        placeholder: "Enter you email"
                    },
                    {
                        type: "text",
                        label: "Phone number:",
                        placeholder: "Enter you mobile number"
                    },
                    {
                        type: "date",
                        color: "#000000",
                        label: "When are you planning to visit?"
                    },
                    {
                        type: "text",
                        label: "How many people are in your group?",
                        placeholder: "0"
                    },
                    {
                        name: "checkbox",
                        type: "checkbox",
                        color: "#000000",
                        label: "Which tours or events are you most interested in?",
                        options: [
                            {
                                text: "Revolution was just the beginning",
                                value: "Revolution was just the beginning"
                            },
                            {
                                text: "Transcendentalism",
                                value: "Transcendentalism"
                            },
                            {
                                text: "Custom",
                                value: "Custom"
                            },
                            {
                                text: "Ghosts in the Gloaming",
                                value: "Ghosts in the Gloaming",
                                selected: false
                            },
                            {
                                text: "Gateposts, Grapes & Graveyards",
                                value: "Gateposts, Grapes & Graveyards",
                                selected: false
                            },
                            {
                                text: "Tavern Life",
                                value: "Tavern Life",
                                selected: false
                            }
                        ]
                    },
                    {
                        type: "divider",
                        color: "#000000",
                        thickness: 1
                    },
                    {
                        id: "kt_sign_up_submit",
                        text: "Submit",
                        type: "button",
                        btnBgColor: "#18bd5b",
                        buttonType: "submit"
                    },
                    {
                        name: "checkbox",
                        type: "checkbox",
                        color: "#000000",
                        label: "What is the best way to contact you?",
                        options: [
                            {
                                text: "Email",
                                value: "null"
                            },
                            {
                                text: "Phone",
                                value: "null"
                            },
                            {
                                text: "Either",
                                value: "null"
                            }
                        ]
                    },
                    {
                        name: "radio",
                        type: "radio",
                        color: "#000000",
                        label: "If phone, when is the best time of day for a call-back?",
                        options: [
                            {
                                text: "8-11 am",
                                value: "null"
                            },
                            {
                                text: "12-4 pm",
                                value: "null"
                            },
                            {
                                text: "6-10 pm",
                                value: "null"
                            }
                        ]
                    },
                    {
                        rows: 4,
                        type: "textarea",
                        label: "Anything else we should know?",
                        placeholder: "Type here..."
                    },
                    {
                        rows: "5",
                        type: "textarea",
                        label: "And last, how did you hear about us?",
                        placeholder: "Type here...."
                    }
                ],
                cardstyle: "",
                cardBackground: ""
            }
        ]

/* THE MAPPING SCHEMATICS: OF DATA, OF QUERY, OF QUERY WITH ENUMS, OF MUTATIONS (ADD, UPDATE): i.e how the data is 
formatted, what all can be inquired, what all updations in data can be made ~ basically, API contracts*/
const typeDefs = `
    type Options {
        text: String!
        value: String!
        selected: String
    }

    type FormData {
        size: String
        type: String!
        color: String
        alignment: String
        formHeading: String
        formSubHeading: String
        label: String
        placeholder: String
        name: String
        options: [Options!]
        thickness: Int
        id: String
        text: String
        btnBgColor: String
        buttonType: String
        rows: Int
    }

    type Form {
        bgAccountId: Int!
        formUuid: String!
        templateID: String!
        name: String!
        description: String!
        formData: [FormData!]!
        cardstyle: String
        cardBackground: String
    }

    type Query {
        getFormBuilderDetailsByFormUuid(formuuid: String!): Form
    }
`

// EXECUTIONERS OF QUERY: how are we gonna go about each query, mutation, data accesses specified in the schemas
const resolvers = {
    Query: {
        getFormBuilderDetailsByFormUuid: (root, args) => {
            const theForm = stockOfForms.find(f => f.formUuid === args.formId)
            /*return {message: "Data found successfully", form: theForm}*/
            return stockOfForms[0];
        }
    },
  
    /*default resolvers for each field of the Person object are automatically made by graphQL internally, however, for user defined
    field like Address, we've got to define our own resolver for the same.*/
    FormData: {
        options: (root) => {
            return { text: root.text,
                value: root.value,
                selected: root.selected
                }
            },
    },

    /*Form: {
        formData: (parent) => {
            return parent.formData.map(field => {
                const { options, ...rest } = field;
                return {
                    options: options || [],
                    ...rest
                };
            });
        }
    }*/
    
    Form: {
        formData: (root) => {
            return { size: root.size,
                type: root.type,
                color: root.color,
                alignment: root.alignment,
                formHeading: root.formHeading,
                formSubHeading: root.formSubHeading,
                label: root.label,
                placeholder: root.placeholder,
                name: root.name,
                options: root.options,
                thickness: root.thickness,
                id: root.id,
                text: root.text,
                btnBgColor: root.btnBgColor,
                buttonType: root.buttonType,
                rows: root.rows 
                }
            }
    },
}

// THE QUERY ITSELF: in an agreed to format to trigger executioners (~resolvers) both for enquiring & mutating the data
/*
    { 
        "variables": {"<vkey01>":"<vVal01>"},
        "query": "query <queryAlias>($aliasvKey01: <typespec>) { <queryName>(vKey01: $aliasvKey01) {<retfield01> <retfield02> ..} }"
    }
*/

/*prepare a reference to a server script with an endpoint aimed to receive graphQl POST requests that are intended to be catered to by the 
resolvers being provided to you here alongwith the needed typeDefs to be assisted meanwhile*/
const server = new ApolloServer({
   typeDefs,
  resolvers
})

/*"start a standalone server (the one from the @apollo/server library) with listening parameters - port needs 2b 4000.
And, make sure that the server waiting for requests sitting over there understands the typeDefs & their corrosponding
resolvers (as is being described in the 'server' reference). now if this promise gets fulfilled then just console log 
that the server is ready at whatever url the Promise gives you back"*/
startStandaloneServer(server, {
  listen: { port: 4000 },
}).then(({ url }) => {
  console.log(`Server ready at ${url}`)
})