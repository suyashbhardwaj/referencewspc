const { startStandaloneServer } = require('@apollo/server/standalone')
const { ApolloServer } = require('@apollo/server')
const { v1: uuid } = require('uuid')
const jwt = require('jsonwebtoken');
require('dotenv').config

// THE DATA: in json form
let users = [
	{name: "YashBhardwaj", emailid:"yash@yahoo.com", password:'123456', mailOTP: 'fastdo', profileData:{id:'9c83c650-fc7c-11ee-bb0b-798a39fb61a1', status:'', contact:'9049257736', designation:'', department:'home ministry', emailVerified:false, onboardingDone:true, userRole:{roleId:1, userId:800}, franchiseId:null}},
	{name: "Aiyash Bhardwaj", emailid:"aiyash@gmail.com", password:'654321', mailOTP: 'slowdo', profileData:{id:'9c83c650-fc7c-11ee-bb0b-798a39fb61a2', status:'', contact:'9818787137', designation:'', department:'HRD ministry', emailVerified:false, onboardingDone:true, userRole:{roleId:2, userId:801}, franchiseId:null}},
	{name: "Suyash Bhardwaj", emailid:"suyash.inpersonaluse@gmail.com", password:'yoyohornysingh', mailOTP: 'undo', profileData:{id:'9c83c650-fc7c-11ee-bb0b-798a39fb61a3', status:'', contact:'9319661081', designation:'', department:'AYUSH Mantralaya', emailVerified:false, onboardingDone:false, userRole:{roleId:3, userId:802}, franchiseId:null}},
	{name: "Suyash Bhardwaj", emailid:"suyash.inprofessionaluse@gmail.com", password:'banadezindagi', mailOTP: 'redo', profileData:{id:'9c83c650-fc7c-11ee-bb0b-798a39fb61a4', status:'9319661081', contact:'', designation:'', department:'Corporate Affairs', emailVerified:false, onboardingDone:true, userRole:{roleId:4, userId:803}, franchiseId:null}},
	];

const blankrec = {name: "", emailid:"", password:'', mailOTP:'', profileData:{id:'', status:'', contact:'', designation:'', department:'', emailVerified:false, onboardingDone:null, userRole:{roleId:-1, userId:-1}, franchiseId:null}}

/* THE MAPPING SCHEMATICS: OF DATA, OF QUERY, OF QUERY WITH ENUMS, OF MUTATIONS (ADD, UPDATE): i.e how the data is 
formatted, what all can be inquired, what all updations in data can be made ~ basically, API contracts*/
const typeDefs = `
    type UserRole {
        roleId: Int
        userId: Int
    }

    type ProfileInfo {
        id: String!
        status: String
        contact: String
        designation: String
        department: String
        emailVerified: Boolean
        onboardingDone: Boolean
        userRole: UserRole
        franchiseId: String
    }

	type User {
        name: String
        emailid: String
        password: String
        mailOTP: String
        profileData: ProfileInfo
    }

	type Response {
		message: String!
		status: Int!
		token: String
		data: User
	}

    type SignUpResponse {
        token: String
    }

    type VerifyMailRes {
        status: String
        message: String
        data: String
    }

	type Query {
        allUsers: [User!]!
        findUserById(id: String!): User
        signInQuery(emailid: String! password: String!): Response
    }

    type Mutation {
    	signInmutation(emailid: String! password: String!): Response
        signUp(name: String! emailid: String! password: String!): Response
        verifyEmail(emailid: String! otp: String!): VerifyMailRes
    }
    `;

// EXECUTIONERS OF QUERY: how are we gonna go about each query, mutation, data accesses specified in the schemas
const resolvers = {
    Query: {
        allUsers: (root, args) => users.map(user=>({...user , password:'********'})),
        findUserById: (root, args) => users.find(u => u.profileData.id === args.id),
        signInQuery: (root, args) => {
            let mailidfound = users.some(user=>user.emailid === args.emailid);
            const userFound = users.find(user=> user.emailid === args.emailid && user.password === args.password);
            const accessToken = jwt.sign({data:{...userFound}}, 'd1e659cc8b26b89e42f25cbed8ecbabfef63cbe4e0bf5341d83c63c43aa1bfe8b7159ca3f37c2f17c2f3136dacdead1f7a0e790dac7748fac2e9b2d57af96c70');
            return userFound 
                ? { message: "user found in database", status: 200, token: accessToken, data: {...userFound}}
                : (mailidfound ? { message: "incorrect password", status: 201, token: '', data: {}} : null);
        },
    },
    Response: {
        data: (root2) => {
        	console.log(root2)
            return { name:root2.data.name, emailid:root2.data.emailid, password:root2.data.password, profileData:root2.data.profileData }
            }
    },
    User: {
        profileData: (root) => {
            return { 
                id:root.profileData.id, 
                status:root.profileData.status, 
                contact:root.profileData.contact,
                designation:root.profileData.designation,
                department:root.profileData.department,
                emailVerified:root.profileData.emailVerified,
                onboardingDone:root.profileData.onboardingDone,
                userRole:root.profileData.userRole,
                franchiseId:root.profileData.franchiseId
                }
            }
    },
    ProfileInfo: {
        userRole: (root) => {
            return {
                roleId:root.userRole.roleId,
                userId: root.userRole.userId
            }
        }
    },

    Mutation: {
		signInmutation: (root, args) => {
            let mailidfound = users.some(user=>user.emailid === args.emailid);
    		const userFound = users.find(user=> user.emailid === args.emailid && user.password === args.password);
            const accessToken = jwt.sign({data:{...userFound}}, 'd1e659cc8b26b89e42f25cbed8ecbabfef63cbe4e0bf5341d83c63c43aa1bfe8b7159ca3f37c2f17c2f3136dacdead1f7a0e790dac7748fac2e9b2d57af96c70');
    		return userFound 
                ? { message: "user found in database", status: 200, token: accessToken, data: {...userFound}}
                : (mailidfound ? { message: "incorrect password", status: 201, token: '', data: {}} : null);
    	},
        
        signUp: (root, args) => {
            const accessToken = jwt.sign({data:{name: args.name, emailid:args.emailid, password:args.password}}, 'd1e659cc8b26b89e42f25cbed8ecbabfef63cbe4e0bf5341d83c63c43aa1bfe8b7159ca3f37c2f17c2f3136dacdead1f7a0e790dac7748fac2e9b2d57af96c70');
            const newuserid = uuid();
            if(users.find(user=>user.emailid===args.emailid) === undefined)
            {users.push(
                {name: args.name, 
                emailid:args.emailid, 
                password:args.password, 
                mailOTP:uuid(),
                profileData:
                    {id:newuserid, status:'', 
                    contact:'', designation:'', 
                    department:'', 
                    emailVerified:false, 
                    onboardingDone:null, 
                    userRole:
                        {roleId:1, 
                        userId:804
                        }, 
                    franchiseId:null
                    }
                }
            )} else return { message: "mailid not available", status: 201, token: '', data: {...blankrec}}
            return { message: "successfully registered", status: 201, token: accessToken, data: {...blankrec}}
        },

        verifyEmail: (root, args) => {
            /*const otpMatched = users.find(user=>user.emailid === args.emailid).mailOTP === args.otp ? true: false*/
            const theMatch = users.find(user=>user.emailid === args.emailid)
            const otpMatched = theMatch.mailOTP === args.otp ? true: false
            if(otpMatched) users = users.map((user)=>{if(user.emailid === args.emailid) user.profileData.emailVerified = true; return user})
            return otpMatched ? {status:'200', message:'OTP verification success', data:''}: {status:'400', message:'OTP verification failed', data:''}
        }
    }
}

/*prepare a reference to a server script with an endpoint aimed to receive graphQl POST requests that are intended to be catered to by the 
resolvers being provided to you here alongwith the needed typeDefs to be assisted meanwhile*/
const server = new ApolloServer({
    typeDefs,
    resolvers
})

/*"start a standalone server (the one from the @apollo/server library) with listening parameters - port needs 2b 4000.
And, make sure that the server waiting for requests sitting over there understands the typeDefs & their corrosponding
resolvers (as is being described in the 'server' reference). now if this promise gets fulfilled then just console log 
that the server is ready at whatever url the Promise gives you back"*/
startStandaloneServer(server, {
  listen: { port: 4000 },
}).then(({ url }) => {
  console.log(`Server ready at ${url}`)
})