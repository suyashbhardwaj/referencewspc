export const mutations = `#graphql
    createMyUser(name:String!, email:String!, password:String!): String
    verifyMail(theotp: String!): Boolean
    onboardUser(status: String, contact: String, designation: String, department: String): StandardResponse
`;