import { CreateMyUserPayload, UserService } from "../../services/user";

const queries = {
    getMyUserToken: async(_: any,payload: {email: string, password: string}) => {
        const token = await UserService.getMyUserToken({
            email: payload.email,
            password: payload.password
        });
        return token;
    },
    getMyCurrentLoggedInUser: async(_:any, parameters:any, context:any) => {
        if (context && context.user) {
            const id = context.user.id;
            const user = await UserService.getMyUserById(id);
            return user;
          }
          throw new Error("I dont know who are you");
    },
}

const mutations = {
    createMyUser: async(_: any,payload:CreateMyUserPayload) => {
        console.log(payload);
        const res = await UserService.createMyUser(payload);
        console.log("response to user creation is ",res);
        return res.id;
    },
    verifyMail: async(_:any, payload: {theotp: string}, context:any) => {
        if (context && context.user) {
            const id = context.user.id;
            const user = await UserService.getMyUserById(id);
            console.log('id from context is ', id);
            console.log('user found as ', user);
            if(user) {
                if(user.mailOTP === payload.theotp) {
                    UserService.emailVerifyMyUser({uid: user.id}).then(res=>console.log('response from flag set ',res));
                    return true;
                } 
                else {return false};
            }
          }
          throw new Error("I dont know who are you");
    },
    onboardUser: async(_:any, payload: {status: string, contact: string, designation: string, department: string}, context:any) => {
        if (context && context.user) {
            const id = context.user.id;
            const user = await UserService.getMyUserById(id);
            if(user) {
                const res = await UserService.updateMyUser({...payload, uid: user.id});
                console.log("response to user creation is ",res);
                return {status: "200", message:"updated successfully", data: res};
            }
          }
          throw new Error("I dont know who are you");
    }
};

export const resolvers = { queries, mutations }