export const queries = `#graphql
    getMyUserToken(email: String!, password: String!): String
    getMyCurrentLoggedInUser: MyUser
`;