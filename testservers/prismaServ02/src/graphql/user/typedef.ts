export const typeDefs = `#graphql
    type MyUser {
        name: String
        emailid: String
        password: String
        mailOTP: String
        id: String!
        status: String
        contact: String
        designation: String
        department: String
        emailVerified: Boolean
        onboardingDone: Boolean
        roleId: Int
        userId: Int
        franchiseId: String
    }

    type StandardResponse {
        status: String
        message: String
        data: MyUser
    }
`;