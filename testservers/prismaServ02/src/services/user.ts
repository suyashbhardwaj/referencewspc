import {createHmac, randomBytes} from 'node:crypto'
import { prismaClient } from "../lib/db"
import JWT from 'jsonwebtoken'

const JWT_SECRET= '$uperM@n@123'

export interface CreateMyUserPayload {
    name:string
    email:string
    password:string
    salt:string
    mailOTP?:string
    id:string
    status:string
    contact?:string
    designation?:string
    department?:string
    emailVerified:boolean
    onboardingDone:boolean
    roleId:number
    userId?:number
    franchiseId?:string
}

export interface GetUserTokenPayload {
    email: string;
    password: string;
  }

interface UpdateMyUserPayload {
    status: string; 
    contact: string; 
    designation: string; 
    department: string;
    uid: string;
}

interface emailVerifyPayload {
    uid: string;
}

export class UserService {
    public static createMyUser(payload: CreateMyUserPayload) {
        const {name, email, password} = payload
        const salt = randomBytes(32).toString("hex");
        const hashedPassword = createHmac('sha256', salt).update(password).digest('hex')
        const temporaryOtp = Math.floor(100000 + Math.random() * 900000);
        return prismaClient.myUser.create({
            data: {
                name,
                email,
                mailOTP: String(temporaryOtp),
                status: "active",
                emailVerified:false,
                onboardingDone:false,
                roleId:1,
                salt,
                password: hashedPassword
            }
        })
    }

    public static updateMyUser(payload: UpdateMyUserPayload) {
        const {status, contact, designation, department, uid} = payload
        return prismaClient.myUser.update({
            where:{id:uid},
            data:{
                status: status,
                contact: contact,
                designation: designation,
                department: department,
                emailVerified:true,
                onboardingDone:true,
            }
        })
    }

    public static emailVerifyMyUser(payload: emailVerifyPayload) {
        const {uid} = payload
        console.log('received uid for mail verification as ', uid);
        return prismaClient.myUser.update({
            where:{id:uid},
            data:{
                emailVerified: true
            }
        })
    }
    
    private static generateHash(salt: string, password: string) {
        const hashedPassword = createHmac("sha256", salt)
          .update(password)
          .digest("hex");
        return hashedPassword;
      }

    public static getMyUserByEmail(email: string) {
        return prismaClient.myUser.findUnique({ where: { email } });
      }
    
    public static async getMyUserToken(payload: GetUserTokenPayload) {
        const { email, password } = payload;
        const user = await UserService.getMyUserByEmail(email);
        if (!user) throw new Error("user not found");

        const userSalt = user.salt;
        const usersHashPassword = UserService.generateHash(userSalt, password);
        if (usersHashPassword !== user.password)    
            throw new Error("Incorrect Password");

        // Gen Token
        const token = JWT.sign({ id: user.id, email: user.email }, JWT_SECRET)
        return token;
    }

    public static decodeJWTToken(token:string) {
        return JWT.verify(token, JWT_SECRET);
    }

    public static getMyUserById(id: string) {
        return prismaClient.myUser.findUnique({ where: { id } });
      }
}