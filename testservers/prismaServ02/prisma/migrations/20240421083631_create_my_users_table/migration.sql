-- CreateTable
CREATE TABLE `myusers` (
    `full_name` VARCHAR(191) NOT NULL,
    `email` VARCHAR(191) NOT NULL,
    `password` VARCHAR(191) NOT NULL,
    `salt` VARCHAR(191) NOT NULL,
    `mailOTP` VARCHAR(191) NULL,
    `id` VARCHAR(191) NOT NULL,
    `status` VARCHAR(191) NOT NULL,
    `contact` VARCHAR(191) NULL,
    `designation` VARCHAR(191) NULL,
    `department` VARCHAR(191) NULL,
    `emailVerified` BOOLEAN NOT NULL,
    `onboardingDone` BOOLEAN NOT NULL,
    `roleId` INTEGER NOT NULL,
    `userId` INTEGER NULL,
    `franchiseId` VARCHAR(191) NULL,

    UNIQUE INDEX `myusers_email_key`(`email`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
