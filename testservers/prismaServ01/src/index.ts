import express from 'express'
import { expressMiddleware } from '@apollo/server/express4';
import createApolloGraphQLserver from './graphql';
import { UserService } from './services/user';

async function init() {
    const app = express()
    const PORT = Number(process.env.PORT) || 8000

    app.use(express.json());
    
    app.get('/', (req, res)=> { res.json({message:'server is up & running'}) })

    app.use("/graphql", expressMiddleware(await createApolloGraphQLserver(),{
        context: async({req}) => {
            //@ts-ignore
            const token = req.headers["token"]
            // console.log(token);
            try {
                const user = UserService.decodeJWTToken(token as string);
                // console.log(user);
                return { user };
            } catch(error) {
                return {};
            }
        },
    }))

    app.listen(PORT, ()=> console.log("server started at port", PORT));
}

init();