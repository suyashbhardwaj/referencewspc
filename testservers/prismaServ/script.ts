import { PrismaClient } from '@prisma/client'
// const prisma = new PrismaClient({ log: ["query"]})
const prisma = new PrismaClient()

async function main() {
	// await prisma.user.deleteMany();
	// const user = await prisma.user.create({
	// const users = await prisma.user.createMany({
		// 	data: {
		// 		age:67,
		// 		name: "aiyash",
		// 		email: "aiyash@yahoo.com"
		// 	},
		// const user = await prisma.user.create({data: {name:'shubham joshi'}})
		// const users = await prisma.user.findMany()
		// await prisma.user.deleteMany()
		/* include: {
			userPreference:true,
		} */
		/* select: {
			name: true,
			userPreference: true
		} */
		/* data:[
				{
				name:'Gullu',
				email:'gullu@yoyo.com',
				age:34
				},
			]
			}) */

		/* const user = await prisma.user.findUnique({
			where: {
				age_name: {
					age:27,
					name:'Sally'
				}
			}
		}) */
		/* const user = await prisma.user.findFirst({
			where: {
				email:"sally@yoyo.com"
			}
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: 'Gullu'
			}
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: 'Gullu'
			},
			distinct: ["name"]
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: 'Gullu'
			},
			distinct: ["name", "age"]
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: 'Gullu'
			},
			take:2,
			skip:1,
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: 'Gullu'
			},
			orderBy: {
				age:"asc"
			},
			take:2,
			skip:1,
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: {equals:"Gullu"}
			},
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: {not:"Gullu"}
			},
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: {in:["Gullu","Tullu","Kyle"]}
			},
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: {notIn:["Gullu","Tullu","Kyle"]}
			},
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				name: "Gullu",
				age: {lt:33}
			},
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				AND: [{ email:{contains: "@yoyo.com"}}, {name:"Tullu"}]
			},
		}) */
		/* const users = await prisma.user.findMany({
			where: {
				userPreference: {
					emailUpdates: true
				}
			},
		}) */
		const users = await prisma.user.findMany({
			where: {
				writtenPosts: { 
					every: {
						title: "Test"
					}
				}
			},
		})
		/* const users = await prisma.post.findMany({
			where: {
				author: {
					is: {
						age:27,
					},
					isNot: {
						name:"Sally"
					}
				}
			},
		}) */
		/* const users = await prisma.user.update({
			where:{
				name:"Gullu",
			},
			data:{
				name:"Tullu"
			}
		}) */
		/* const users = await prisma.user.update({
			where:{
				name:"Gullu",
			},
			data:{
				userPreference: {
					connect:{
						id:"322sfsf-2343f"
					}
				}
			}
		}) */
		/* const users = await prisma.user.update({
			where:{
				name:"Gullu",
			},
			data:{
				userPreference: {
					disconnect:true
				}
			}
		}) */
	console.log(users)
	}

main()
	.catch(e=>{console.error(e.message)})
	.finally(async()=>{await prisma.$disconnect()})