const io = require('socket.io')(3001, { cors: { origin: ['http://localhost:5173']} });

io.on('connection', socket => {
    console.log("connection established with id ",socket.id);
    // socket.on('myevent',(num, str, obj) => console.log('an event has been raised with ', num, str, obj));
    // socket.on('sendmsgEvt', (themessage)=> console.log(themessage));
    socket.on('sendMsgToServerEvt', (themessage)=> socket.emit('receiveMsgFromServerEvt', themessage)); //echoes the received message to all the connections except only the sender
    // socket.on('sendMsgToServerEvt', (themessage)=> socket.broadcast.emit('receiveMsgFromServerEvt',themessage)); //echoes the received message to all the connections except only the sender
    // socket.on('sendMsgToServerEvt', (themessage, room)=> {
    //     if(room === "") socket.broadcast.emit('receiveMsgFromServerEvt',themessage); //echoes the received message to all the connections except only the sender
    //     else socket.to(room).emit('receiveMsgFromServerEvt',themessage); //echoes the received message to all the connections within the specified room except only the sender
    // })

    // socket.on('joinroom', theroom => {
    //     socket.join(theroom);
    // })

    socket.on('joinroom', (theroom, callback) => {
        // socket.join(theroom);
        callback('I am the server, you have joined the room');
    })
});