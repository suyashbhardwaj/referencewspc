import './App.css'
import SocketIOImpl from './components/SocketIOImpl'

function App() {

  return (
    <>
      <SocketIOImpl/>
      {/* <div>
        <h1>websockets implementation</h1>
      </div>
      <p className="read-the-docs">
        An implementation of websockets using socket.io & socket.io-client npm libraries
      </p> */}
    </>
  )
}

export default App
