/* import SideMenu from '../sections/aside/SideMenu' */
import React, { Suspense } from 'react'
import { Outlet } from 'react-router-dom'
import Footer from '../sections/Footer'
import HeaderWrapper from '../sections/header/HeaderWrapper'
import LoadingSpinner from '../components/LoadingSpinner'
import { PropsWithChildren } from 'react'
import SideBarWrapper from '../sections/SideBar/SideBarWrapper'

interface IProps {
  showSideBar: any
  toggleSidebar: any
}

const MasterLayout: React.FC<PropsWithChildren<IProps>> = ({showSideBar, toggleSidebar}) => {
  return (
      <div className={`page d-flex flex-row flex-column-fluid ${showSideBar?'':'toggle-sidebar'}`}>
        <SideBarWrapper showSideBar = {showSideBar}/>
        <div className='wrapper d-flex flex-column flex-row-fluid px-0' id='kt_wrapper'>  
          <HeaderWrapper toggleSidebar = {toggleSidebar}/>
          <div id='kt_content' className='d-flex flex-column flex-column-fluid mt-n12 '>
            {/* <Toolbar /> */}
            <div className='post d-flex flex-column-fluid' id='kt_post'>
              <div style={{width:'100vw'}}>
                <Suspense fallback={<LoadingSpinner />}>
                  <Outlet />
                </Suspense>
              </div>
            </div>
          </div>
        </div>
      <Footer />
    </div>
  )
}

export { MasterLayout }