import Linechart from "../utils/Charts/Linechart";
import Apexchart from "../utils/Charts/Apexchart";
import { PropsWithChildren } from 'react'

interface IProps {
  showSideBar: any
}

const Dashboard: React.FC<PropsWithChildren<IProps>> = ({showSideBar}) => {

	return (
			<div>
  				{/*main starts*/}
  				<main id="main" className="main">
				    {/*<!-- Start Page Title -->*/}
				    <div className="pagetitle">
				      <h1>Dashboard</h1>
				      <nav>
				        <ol className="breadcrumb">
				          <li className="breadcrumb-item"><a href="index.html">Home</a></li>
				          <li className="breadcrumb-item active">Dashboard</li>
				        </ol>
				      </nav>
				    </div>
				    {/*<!-- End Page Title -->*/}

				    <section className="section dashboard">
					    <div className="row">

					        {/*<!-- Start Left side columns -->*/}
					        <div className="col-lg-8">
						        <div className="row">

						            {/*<!-- Sales Card -->*/}
						            <div className="col-xxl-4 col-md-6">
						              <div className="card info-card sales-card">

						                <div className="filter">
						                  <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
						                  <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
						                    <li className="dropdown-header text-start">
						                      <h6>Filter</h6>
						                    </li>

						                    <li><a className="dropdown-item" href="#">Today</a></li>
						                    <li><a className="dropdown-item" href="#">This Month</a></li>
						                    <li><a className="dropdown-item" href="#">This Year</a></li>
						                  </ul>
						                </div>

						                <div className="card-body">
						                  <h5 className="card-title">Sales <span>| Today</span></h5>

						                  <div className="d-flex align-items-center">
						                    <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
						                      <i className="bi bi-cart"></i>
						                    </div>
						                    <div className="ps-3">
						                      <h6>145</h6>
						                      <span className="text-success small pt-1 fw-bold">12%</span> <span className="text-muted small pt-2 ps-1">increase</span>

						                    </div>
						                  </div>
						                </div>

						              </div>
						            </div>{/*<!-- End Sales Card -->*/}

						            {/*<!-- Revenue Card -->*/}
						            <div className="col-xxl-4 col-md-6">
						              <div className="card info-card revenue-card">

						                <div className="filter">
						                  <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
						                  <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
						                    <li className="dropdown-header text-start">
						                      <h6>Filter</h6>
						                    </li>

						                    <li><a className="dropdown-item" href="#">Today</a></li>
						                    <li><a className="dropdown-item" href="#">This Month</a></li>
						                    <li><a className="dropdown-item" href="#">This Year</a></li>
						                  </ul>
						                </div>

						                <div className="card-body">
						                  <h5 className="card-title">Revenue <span>| This Month</span></h5>

						                  <div className="d-flex align-items-center">
						                    <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
						                      <i className="bi bi-currency-dollar"></i>
						                    </div>
						                    <div className="ps-3">
						                      <h6>$3,264</h6>
						                      <span className="text-success small pt-1 fw-bold">8%</span> <span className="text-muted small pt-2 ps-1">increase</span>

						                    </div>
						                  </div>
						                </div>

						              </div>
						            </div>{/*<!-- End Revenue Card -->*/}

						            {/*<!-- Customers Card -->*/}
						            <div className="col-xxl-4 col-xl-12">

						              <div className="card info-card customers-card">

						                <div className="filter">
						                  <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
						                  <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
						                    <li className="dropdown-header text-start">
						                      <h6>Filter</h6>
						                    </li>

						                    <li><a className="dropdown-item" href="#">Today</a></li>
						                    <li><a className="dropdown-item" href="#">This Month</a></li>
						                    <li><a className="dropdown-item" href="#">This Year</a></li>
						                  </ul>
						                </div>

						                <div className="card-body">
						                  <h5 className="card-title">Customers <span>| This Year</span></h5>

						                  <div className="d-flex align-items-center">
						                    <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
						                      <i className="bi bi-people"></i>
						                    </div>
						                    <div className="ps-3">
						                      <h6>1244</h6>
						                      <span className="text-danger small pt-1 fw-bold">12%</span> <span className="text-muted small pt-2 ps-1">decrease</span>

						                    </div>
						                  </div>

						                </div>
						              </div>
						            </div>
						            {/*<!-- End Customers Card -->*/}

						            {/*<!-- Reports -->*/}
						            <div className="col-12">
						              <div className="card">

						                <div className="filter">
						                  <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
						                  <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
						                    <li className="dropdown-header text-start">
						                      <h6>Filter</h6>
						                    </li>

						                    <li><a className="dropdown-item" href="#">Today</a></li>
						                    <li><a className="dropdown-item" href="#">This Month</a></li>
						                    <li><a className="dropdown-item" href="#">This Year</a></li>
						                  </ul>
						                </div>

						                <div className="card-body">
						                  <h5 className="card-title">Reports <span>/Today</span></h5>
						                  <Linechart/>
						                </div>

						              </div>
						            </div>{/*<!-- End Reports -->*/}

						            {/*<!-- Recent Sales -->*/}
						            <div className="col-12">
						              <div className="card recent-sales overflow-auto">

						                <div className="filter">
						                  <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
						                  <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
						                    <li className="dropdown-header text-start">
						                      <h6>Filter</h6>
						                    </li>

						                    <li><a className="dropdown-item" href="#">Today</a></li>
						                    <li><a className="dropdown-item" href="#">This Month</a></li>
						                    <li><a className="dropdown-item" href="#">This Year</a></li>
						                  </ul>
						                </div>

						                <div className="card-body">
						                  <h5 className="card-title">Recent Sales <span>| Today</span></h5>
						                  <div className="datatable-wrapper datatable-loading no-footer sortable searchable fixed-columns">
						                    <div className="datatable-top">
						                      <div className="datatable-dropdown">
						                        <label>
						                            <select className="datatable-selector">
						                              <option value="5">5</option>
						                              <option value="10" selected>10</option>
						                              <option value="15">15</option>
						                              <option value="-1">All</option>
						                            </select> entries per page
						                        </label>
						                      </div>
						                      <div className="datatable-search">
						                        <input className="datatable-input" placeholder="Search..." type="search" title="Search within table"/>
						                      </div>
						                    </div>
						                    <div className="datatable-container">
						                      <table className="table table-borderless datatable datatable-table">
						                        <thead>
						                          <tr>
						                            <th scope="col" data-sortable="true" style={{width: '10.754414125200643%'}}>
						                              <button className="btn btn-link datatable-sorter">#</button>
						                            </th>
						                            <th scope="col" data-sortable="true" style={{width: '23.595505617977526%'}}>
						                              <button className="btn btn-link datatable-sorter">Customer</button>
						                            </th>
						                            <th scope="col" data-sortable="true" style={{width:' 39.64686998394863%'}}>
						                              <button className="btn btn-link datatable-sorter">Product</button>
						                            </th>
						                            <th scope="col" data-sortable="true" style={{width: '11.075441412520064%'}}>
						                              <button className="btn btn-link datatable-sorter">Price</button>
						                            </th>
						                            <th scope="col" data-sortable="true" className="red" style={{width:' 14.92776886035313%'}}>
						                              <button className="btn btn-link datatable-sorter">Status</button>
						                            </th>
						                          </tr>
						                        </thead>
						                        <tbody>
						                          <tr data-index="0">
						                            <td scope="row">
						                              <a href="#">#2457</a>
						                            </td>
						                            <td>Brandon Jacob</td>
						                            <td>
						                              <a href="#" className="text-primary">At praesentium minu</a>
						                            </td>
						                            <td>$64</td>
						                            <td className="green">
						                              <span className="badge bg-success">Approved</span>
						                            </td>
						                          </tr>
						                          <tr data-index="1"><td scope="row"><a href="#">#2147</a></td><td>Bridie Kessler</td><td><a href="#" className="text-primary">Blanditiis dolor omnis similique</a></td><td>$47</td><td className="green"><span className="badge bg-warning">Pending</span></td></tr>
						                          <tr data-index="2"><td scope="row"><a href="#">#2049</a></td><td>Ashleigh Langosh</td><td><a href="#" className="text-primary">At recusandae consectetur</a></td><td>$147</td><td className="green"><span className="badge bg-success">Approved</span></td></tr>
						                          <tr data-index="3"><td scope="row"><a href="#">#2644</a></td><td>Angus Grady</td><td><a href="#" className="text-primar">Ut voluptatem id earum et</a></td><td>$67</td><td className="green"><span className="badge bg-danger">Rejected</span></td></tr>
						                          <tr data-index="4"><td scope="row"><a href="#">#2644</a></td><td>Raheem Lehner</td><td><a href="#" className="text-primary">Sunt similique distinctio</a></td><td>$165</td><td className="green"><span className="badge bg-success">Approved</span></td></tr>
						                        </tbody>
						                      </table>
						                    </div>
						                    <div className="datatable-bottom">
						                      <div className="datatable-info">Showing 1 to 5 of 5 entries</div>
						                        <nav className="datatable-pagination">
						                          <ul className="datatable-pagination-list">
						                          </ul>
						                        </nav>
						                    </div>
						                  </div>
						                </div>
						              </div>
						            </div>
						            {/*<!-- End Recent Sales -->*/}
								</div>
							</div>
							{/*<!-- End Left side columns -->*/}

							{/*<!-- Start Right side columns -->*/}
					        <div className="col-lg-4">

					          {/*<!-- Recent Activity -->*/}
					          <div className="card">
					            <div className="filter">
					              <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
					              <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
					                <li className="dropdown-header text-start">
					                  <h6>Filter</h6>
					                </li>

					                <li><a className="dropdown-item" href="#">Today</a></li>
					                <li><a className="dropdown-item" href="#">This Month</a></li>
					                <li><a className="dropdown-item" href="#">This Year</a></li>
					              </ul>
					            </div>

					            <div className="card-body">
					              <h5 className="card-title">Recent Activity <span>| Today</span></h5>

					              <div className="activity">

					                <div className="activity-item d-flex">
					                  <div className="activite-label">32 min</div>
					                  <i className="bi bi-circle-fill activity-badge text-success align-self-start"></i>
					                  <div className="activity-content">
					                    Quia quae rerum <a href="#" className="fw-bold text-dark">explicabo officiis</a> beatae
					                  </div>
					                </div>{/*<!-- End activity item-->*/}

					                <div className="activity-item d-flex">
					                  <div className="activite-label">56 min</div>
					                  <i className="bi bi-circle-fill activity-badge text-danger align-self-start"></i>
					                  <div className="activity-content">
					                    Voluptatem blanditiis blanditiis eveniet
					                  </div>
					                </div>{/*<!-- End activity item-->*/}

					                <div className="activity-item d-flex">
					                  <div className="activite-label">2 hrs</div>
					                  <i className="bi bi-circle-fill activity-badge text-primary align-self-start"></i>
					                  <div className="activity-content">
					                    Voluptates corrupti molestias voluptatem
					                  </div>
					                </div>{/*<!-- End activity item-->*/}

					                <div className="activity-item d-flex">
					                  <div className="activite-label">1 day</div>
					                  <i className="bi bi-circle-fill activity-badge text-info align-self-start"></i>
					                  <div className="activity-content">
					                    Tempore autem saepe <a href="#" className="fw-bold text-dark">occaecati voluptatem</a> tempore
					                  </div>
					                </div>{/*<!-- End activity item-->*/}

					                <div className="activity-item d-flex">
					                  <div className="activite-label">2 days</div>
					                  <i className="bi bi-circle-fill activity-badge text-warning align-self-start"></i>
					                  <div className="activity-content">
					                    Est sit eum reiciendis exercitationem
					                  </div>
					                </div>{/*<!-- End activity item-->*/}

					                <div className="activity-item d-flex">
					                  <div className="activite-label">4 weeks</div>
					                  <i className="bi bi-circle-fill activity-badge text-muted align-self-start"></i>
					                  <div className="activity-content">
					                    Dicta dolorem harum nulla eius. Ut quidem quidem sit quas
					                  </div>
					                </div>{/*<!-- End activity item-->*/}

					              </div>

					            </div>
					          </div>{/*<!-- End Recent Activity -->*/}

					          {/*<!-- Budget Report -->*/}
					          <div className="card">
					            <div className="filter">
					              <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
					              <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
					                <li className="dropdown-header text-start">
					                  <h6>Filter</h6>
					                </li>

					                <li><a className="dropdown-item" href="#">Today</a></li>
					                <li><a className="dropdown-item" href="#">This Month</a></li>
					                <li><a className="dropdown-item" href="#">This Year</a></li>
					              </ul>
					            </div>

					            <div className="card-body pb-0">
					              <h5 className="card-title">Budget Report <span>| This Month</span></h5>

					              <div id="budgetChart" style={{minHeight: "400px", userSelect: "none"}} className="echart">
					                <div style={{position: "relative", width: "279px", height: "400px", padding: "0px", margin: "0px", borderWidth: "0px", cursor: "default"}}>
					                  <Apexchart/>
					                </div>
					              </div>
					            </div>
					          </div>{/*<!-- End Budget Report -->*/}

					          {/*<!-- Website Traffic -->*/}
					          <div className="card">
					            <div className="filter">
					              <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
					              <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
					                <li className="dropdown-header text-start">
					                  <h6>Filter</h6>
					                </li>

					                <li><a className="dropdown-item" href="#">Today</a></li>
					                <li><a className="dropdown-item" href="#">This Month</a></li>
					                <li><a className="dropdown-item" href="#">This Year</a></li>
					              </ul>
					            </div>

					            <div className="card-body pb-0">
					              <h5 className="card-title">Website Traffic <span>| Today</span></h5>

					              <div id="trafficChart" style={{minHeight: "400px", userSelect: "none", position: "relative"}} className="echart">
					              <div style={{position: "relative", width: "279px", height: "400px", padding: "0px", margin: "0px", borderWidth: "0px"}}>
					              <canvas 
					                style={{
					                  position: "absolute", 
					                  left: "0px", 
					                  top: "0px", 
					                  width: "279px", 
					                  height: "400px", 
					                  userSelect: "none", 
					                  padding: "0px", 
					                  margin: "0px", 
					                  borderWidth: "0px", 
					                }}>
					              </canvas>
					              </div>
					              <div className=""></div>
					              </div>

					            </div>
					          </div>{/*<!-- End Website Traffic -->*/}

					          {/*<!-- News & Updates Traffic -->*/}
					          <div className="card">
					            <div className="filter">
					              <a className="icon" href="#" data-bs-toggle="dropdown"><i className="bi bi-three-dots"></i></a>
					              <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
					                <li className="dropdown-header text-start">
					                  <h6>Filter</h6>
					                </li>

					                <li><a className="dropdown-item" href="#">Today</a></li>
					                <li><a className="dropdown-item" href="#">This Month</a></li>
					                <li><a className="dropdown-item" href="#">This Year</a></li>
					              </ul>
					            </div>

					            <div className="card-body pb-0">
					              <h5 className="card-title">News &amp; Updates <span>| Today</span></h5>

					              <div className="news">
					                <div className="post-item clearfix">
					                  <img src="assets/img/news-1.jpg" alt=""/>
					                  <h4><a href="#">Nihil blanditiis at in nihil autem</a></h4>
					                  <p>Sit recusandae non aspernatur laboriosam. Quia enim eligendi sed ut harum...</p>
					                </div>

					                <div className="post-item clearfix">
					                  <img src="assets/img/news-2.jpg" alt=""/>
					                  <h4><a href="#">Quidem autem et impedit</a></h4>
					                  <p>Illo nemo neque maiores vitae officiis cum eum turos elan dries werona nande...</p>
					                </div>

					                <div className="post-item clearfix">
					                  <img src="assets/img/news-3.jpg" alt=""/>
					                  <h4><a href="#">Id quia et et ut maxime similique occaecati ut</a></h4>
					                  <p>Fugiat voluptas vero eaque accusantium eos. Consequuntur sed ipsam et totam...</p>
					                </div>

					                <div className="post-item clearfix">
					                  <img src="assets/img/news-4.jpg" alt=""/>
					                  <h4><a href="#">Laborum corporis quo dara net para</a></h4>
					                  <p>Qui enim quia optio. Eligendi aut asperiores enim repellendusvel rerum cuder...</p>
					                </div>

					                <div className="post-item clearfix">
					                  <img src="assets/img/news-5.jpg" alt=""/>
					                  <h4><a href="#">Et dolores corrupti quae illo quod dolor</a></h4>
					                  <p>Odit ut eveniet modi reiciendis. Atque cupiditate libero beatae dignissimos eius...</p>
					                </div>

					              </div>{/*<!-- End sidebar recent posts-->*/}

					            </div>
					          </div>{/*<!-- End News & Updates -->*/}
        					</div>
        					{/*<!-- End Right side columns -->*/}
				    	</div>
    				</section>
					</main>
			    {/*main ends*/}
			</div>
	)
}

export default Dashboard