import { Dropdown } from "../../helpers/Dropdown"
import { useRef, useState } from "react"
import LeadFilterMenu from "./LeadFilterMenu"
import Linechart from "../../utils/Charts/Linechart";
import Apexchart from "../../utils/Charts/Apexchart";
const CampaignList = () => {
    const filterDropdownMenuRef = useRef<HTMLDivElement>(null)
    const [filterMenuDroppedDown, setFilterMenuDroppedDown] = useState<boolean>(false)
  return (
    <>
        <div>
          {/*main starts*/}
          <main id="main" className="main">
            {/*<!-- Start Page Title -->*/}
            <div className="pagetitle">
              <h1>Dashboard</h1>
              <nav>
                <ol className="breadcrumb">
                  <li className="breadcrumb-item"><a href="index.html">Home</a></li>
                  <li className="breadcrumb-item active">Dashboard</li>
                </ol>
              </nav>
            </div>
            {/*<!-- End Page Title -->*/}

            <section className="section dashboard">
              <div className = 'card py-4'>
                <div className = "card-body">
                <div className="row">
                  {/*<!-- Start the full width column -->*/}
                  <div className="col-lg-12">
                    <div className="row">
                    {/*begin::Card toolbar*/}
                    <div className="d-flex justify-content-end align-items-center">
                      <div className="px-2 mw-150px">
                        {/*begin::Select2*/}
                        <select
                          className="form-select select2-hidden-accessible"
                          data-control="select2"
                          data-hide-search="true"
                          data-placeholder="Status"
                          data-kt-ecommerce-product-filter="status"
                          data-select2-id="select2-data-9-cpv9"
                          tabIndex={-1}
                          aria-hidden="true"
                          data-kt-initialized="1"
                        >
                          <option data-select2-id="select2-data-11-qb6b"></option>
                          <option value="select team" selected>Select Team</option>
                          <option value="Team 01">team 01</option>
                          <option value="Team 02">team 02</option>
                          <option value="Team 03">team 03</option>
                          </select>
                        {/*end::Select2*/}
                      </div>
                      <div className="px-2 mw-150px">
                        {/*begin::Select2*/}
                        <select
                          className="form-select select2-hidden-accessible"
                          data-control="select2"
                          data-hide-search="true"
                          data-placeholder="Status"
                          data-kt-ecommerce-product-filter="status"
                          data-select2-id="select2-data-9-cpv9"
                          tabIndex={-1}
                          aria-hidden="true"
                          data-kt-initialized="1"
                        >
                          <option data-select2-id="select2-data-11-qb6b"></option>
                          <option value="campaign" selected>Campaign</option>
                          <option value="campaign 01">Campaign 01</option>
                          <option value="campaign 02">Campaign 02</option>
                          <option value="campaign 03">Campaign 03</option>
                          </select>
                        {/*end::Select2*/}
                      </div>
                      <div className="px-2 mw-150px">
                        <input className='form-control form-control' type="date" placeholder='Custom Date'/>
                      </div>
                      {/*begin::settings*/}
                      <div className="px-1 mw-15px">
                      <a href="#" className="btn btn-link"> <i className="fs-5 bi bi-gear-wide-connected"></i> </a>
                      </div>
                      {/*end::settings*/}
                    </div>
                    {/*end::Card toolbar*/}
                    </div>
                  </div>
                {/*<!-- End the full width column -->*/}
                </div>

                <div className="row">
                {/*<!-- Start the full width column -->*/}
                  <div className="col-lg-12">
                    <div className="row">
                    {/*begin::Card toolbar*/}
                    <div className="d-flex justify-content-end align-items-center">
                      <div className="px-2 mw-200px">
                        <input
                          type="text"
                          data-kt-ecommerce-product-filter="search"
                          className="form-control"
                          placeholder="Search"
                        />
                      </div>
                      <div className="px-2 mw-150px" ref={filterDropdownMenuRef}>
                        {/*begin::Select2*/}
                        <select
                          className="form-select select2-hidden-accessible"
                          data-control="select2"
                          data-hide-search="true"
                          data-placeholder="Status"
                          data-kt-ecommerce-product-filter="status"
                          data-select2-id="select2-data-9-cpv9"
                          tabIndex={-1}
                          aria-hidden="true"
                          data-kt-initialized="1"
                          onMouseEnter={()=>setFilterMenuDroppedDown(!filterMenuDroppedDown)}
                        >
                          <option value="lead">+ Lead</option>
                          </select>
                        {/*end::Select2*/}
                      </div>
                      {/*begin::settings*/}
                      <div className="px-1 mw-15px">
                        {/*begin::Add product*/}
                        <a href="#" className="btn btn-link"> <i className="fs-2 bi bi-arrow-clockwise"></i></a> 
                        <a href="#" className="btn btn-link"> <i className="fs-2 bi bi-arrow-left-right"></i></a>
                        <a href="#" className="btn btn-link"> <i className="fs-2 bi bi-funnel"></i></a>
                        <a href="#" className="btn btn-link"> <i className="fs-2 bi bi-pencil-fill"></i></a>
                        <a href="#" className="btn btn-link"> <i className="fs-2 bi bi-trash"></i></a>
                        {/*end::Add product*/}
                      </div>
                      {/*end::settings*/}
                    </div>
                    {/*end::Card toolbar*/}
                    </div>
                  </div>
                {/*<!-- End the full width column -->*/}
                </div>

                {/*Start::The dropdown code snippet*/}
                <div className='card'>
                  <Dropdown
                    menuRef={filterDropdownMenuRef}
                    droppedDown={filterMenuDroppedDown}
                    setDroppedDown={setFilterMenuDroppedDown}
                    width={250}
                    hideOnBlur={true}
                    >
                    <LeadFilterMenu/>
                  </Dropdown>
                </div>
                {/*Ends::The dropdown code snippet*/}

                <div className="row">
                {/*<!-- Start the full width column -->*/}
                  <div className="col-lg-12">
                    <div className="row">
                    {/*begin::Card toolbar*/}
                    <div className="d-flex justify-content-end align-items-center">
                      <div className="px-2 w-100 mw-600px">
                      {/*begin::Card body*/}
                      <div className="card pt-0">
                        {/*begin::Card body*/}
                        <div className="card-header">
                          <h6>The campaign list</h6>
                        </div>
                        <div className="card-body">
                        {/*begin::Table*/}
                        <div className="dataTables_wrapper dt-bootstrap4 no-footer">
                          <div className="table-responsive">
                            <table
                              className="table table-bordered align-middle"
                            >
                              <thead>
                                <tr>
                                  <th
                                    rowSpan={1}
                                    colSpan={1}
                                    style={{width: "29.9px"}}
                                    aria-label=""
                                  >
                                    <div className="form-check me-3 d-flex justify-content-center">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        value="1"
                                      />
                                    </div>
                                  </th>
                                  <th
                                    className="min-w-200px sorting fw-bold text-black text-start ps-8"
                                    tabIndex={0}
                                    aria-controls="kt_ecommerce_products_table"
                                    rowSpan={1}
                                    colSpan={1}
                                    style={{width: "275.667px"}}
                                    aria-label="Product: activate to sort column ascending"
                                  >
                                    <span>Lead Id</span> 
                                  </th>
                                  <th
                                    className="text-center min-w-100px sorting fw-bold text-black text-center"
                                    tabIndex={0}
                                    aria-controls="kt_ecommerce_products_table"
                                    rowSpan={1}
                                    colSpan={1}
                                    style={{width: "139.6px"}}
                                    aria-label="SKU: activate to sort column ascending"
                                  >
                                    First Name
                                  </th>
                                  <th
                                    className="text-center min-w-70px sorting fw-bold text-black text-center"
                                    tabIndex={0}
                                    aria-controls="kt_ecommerce_products_table"
                                    rowSpan={1}
                                    colSpan={1}
                                    style={{width: "126.167px"}}
                                    aria-label="Qty: activate to sort column ascending"
                                  >
                                    Last Name
                                  </th>
                                  <th
                                    className="text-center min-w-100px sorting fw-bold text-black text-center"
                                    tabIndex={0}
                                    aria-controls="kt_ecommerce_products_table"
                                    rowSpan={1}
                                    colSpan={1}
                                    style={{width: "139.6px"}}
                                    aria-label="Price: activate to sort column ascending"
                                  >
                                    Company Name
                                  </th>
                                  <th
                                    className="text-center min-w-100px sorting fw-bold text-black text-center"
                                    tabIndex={0}
                                    aria-controls="kt_ecommerce_products_table"
                                    rowSpan={1}
                                    colSpan={1}
                                    style={{width: "139.6px"}}
                                    aria-label="Rating: activate to sort column ascending"
                                  >
                                    Email
                                  </th>
                                  <th
                                    className="text-center min-w-100px sorting fw-bold text-black text-center"
                                    tabIndex={0}
                                    aria-controls="kt_ecommerce_products_table"
                                    rowSpan={1}
                                    colSpan={1}
                                    style={{width: "139.6px"}}
                                    aria-label="Status: activate to sort column ascending"
                                  >
                                    Mobile No.
                                  </th>
                                </tr>
                              </thead>
                              <tbody className="fw-semibold text-gray-600">
                                <tr className="odd border-2 rounded-5">
                                  <td className='d-flex justify-content-center'>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input className="form-check-input" type="radio" value="1" />
                                    </div>
                                  </td>
                                  <td>
                                    <div className="d-flex align-items-center justify-content-start">
                                      <div className="ms-5">
                                        {/*begin::Title*/}
                                        <a
                                          href="#"
                                          className="text-hover-primary fs-5 fw-bold"
                                          data-kt-ecommerce-product-filter="product_name"
                                          >lead00227</a
                                        >
                                        {/*end::Title*/}
                                      </div>
                                    </div>
                                  </td>
                                  <td className="text-center pe-0">
                                    <span className="fw-bold">Prajakta</span>
                                  </td>
                                  <td className="text-center pe-0" data-order="41">
                                    <span className="fw-bold ms-3">Joshi</span>
                                  </td>
                                  <td className="text-center pe-0">Bluewhirl</td>
                                  <td className="text-center pe-0" data-order="rating-5">
                                    prajakta@bluewhirl.io
                                    <div className="rating justify-content-end">
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                    </div>
                                  </td>
                                  <td className="text-center pe-0" data-order="Published">
                                    {/*begin::Badges*/}
                                    <div>8975192545</div>
                                    {/*end::Badges*/}
                                  </td>
                                </tr>
                                <tr className="even">
                                  <td className='d-flex justify-content-center'>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input className="form-check-input" type="radio" value="1" />
                                    </div>
                                  </td>
                                  <td>
                                    <div className="d-flex align-items-center justify-content-start">
                                      <div className="ms-5">
                                        {/*begin::Title*/}
                                        <a
                                          href="#"
                                          className="text-hover-primary fs-5 fw-bold"
                                          data-kt-ecommerce-product-filter="product_name"
                                          >lead00227</a
                                        >
                                        {/*end::Title*/}
                                      </div>
                                    </div>
                                  </td>
                                  <td className="text-center pe-0">
                                    <span className="fw-bold">Prajakta</span>
                                  </td>
                                  <td className="text-center pe-0" data-order="41">
                                    <span className="fw-bold ms-3">Joshi</span>
                                  </td>
                                  <td className="text-center pe-0">Bluewhirl</td>
                                  <td className="text-center pe-0" data-order="rating-5">
                                    prajakta@bluewhirl.io
                                    <div className="rating justify-content-end">
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                    </div>
                                  </td>
                                  <td className="text-center pe-0" data-order="Published">
                                    {/*begin::Badges*/}
                                    <div>8975192545</div>
                                    {/*end::Badges*/}
                                  </td>
                                </tr>
                                <tr className="odd">
                                  <td className='d-flex justify-content-center'>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input className="form-check-input" type="radio" value="1" />
                                    </div>
                                  </td>
                                  <td>
                                    <div className="d-flex align-items-center justify-content-start">
                                      <div className="ms-5">
                                        {/*begin::Title*/}
                                        <a
                                          href="#"
                                          className="text-hover-primary fs-5 fw-bold"
                                          data-kt-ecommerce-product-filter="product_name"
                                          >lead00227</a
                                        >
                                        {/*end::Title*/}
                                      </div>
                                    </div>
                                  </td>
                                  <td className="text-center pe-0">
                                    <span className="fw-bold">Prajakta</span>
                                  </td>
                                  <td className="text-center pe-0" data-order="41">
                                    <span className="fw-bold ms-3">Joshi</span>
                                  </td>
                                  <td className="text-center pe-0">Bluewhirl</td>
                                  <td className="text-center pe-0" data-order="rating-5">
                                    prajakta@bluewhirl.io
                                    <div className="rating justify-content-end">
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                    </div>
                                  </td>
                                  <td className="text-center pe-0" data-order="Published">
                                    {/*begin::Badges*/}
                                    <div>8975192545</div>
                                    {/*end::Badges*/}
                                  </td>
                                </tr>
                                <tr className="even">
                                  <td className='d-flex justify-content-center'>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input className="form-check-input" type="radio" value="1" />
                                    </div>
                                  </td>
                                  <td>
                                    <div className="d-flex align-items-center justify-content-start">
                                      <div className="ms-5">
                                        {/*begin::Title*/}
                                        <a
                                          href="#"
                                          className="text-hover-primary fs-5 fw-bold"
                                          data-kt-ecommerce-product-filter="product_name"
                                          >lead00227</a
                                        >
                                        {/*end::Title*/}
                                      </div>
                                    </div>
                                  </td>
                                  <td className="text-center pe-0">
                                    <span className="fw-bold">Prajakta</span>
                                  </td>
                                  <td className="text-center pe-0" data-order="41">
                                    <span className="fw-bold ms-3">Joshi</span>
                                  </td>
                                  <td className="text-center pe-0">Bluewhirl</td>
                                  <td className="text-center pe-0" data-order="rating-5">
                                    prajakta@bluewhirl.io
                                    <div className="rating justify-content-end">
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                      <div className="rating-label checked">
                                        <i className="ki-duotone ki-star fs-6"></i>
                                      </div>
                                    </div>
                                  </td>
                                  <td className="text-center pe-0" data-order="Published">
                                    {/*begin::Badges*/}
                                    <div>8975192545</div>
                                    {/*end::Badges*/}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        {/*end::Table*/}
                      </div>  
                      </div>
                      {/*end::Card body*/}
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
                {/*<!-- End the full width column -->*/}

                </div>
              </div>
            </section>
          </main>
          {/*main ends*/}
      </div>
    </>
  )
}

export default CampaignList
