 import MultiStepProgressBar from "../../../utils/MultiStepProgressBar/MultiStepProgressBar"

const LeadInfo = () => {
    return (
        <div>
        {/*main starts*/}
        <main id="main" className="main">
            <div className="d-flex flex-column">
            {/*start:: The 1st stack */}
            <div className="d-flex justify-content-between container-fluid">
                <div className="d-flex justify-contents-left align-items-center">
                    <img src="#" className="rounded-circle bg-danger mx-2"/>
                    <input type="time" className="mx-2 form-control"/>
                    <input type="text" className="form-control" placeholder="Mobile no."/>
                    <i className="mx-2 bi bi-keyboard fs-2x"></i>
                    <i className="mx-2 bi bi-telephone-fill fs-2x"></i>
                </div>
                <div className="d-flex justify-contents-right align-items-center">
                    <img src="" alt="" />
                    <input type="select" className="mx-2 form-select" placeholder="select team"/>
                    <input type="text" disabled className="form-control" placeholder="Pause"/>
                    {/*begin::settings*/}
                    <a href="#" className="btn btn-link mx-2"> 
                    <i className="fs-2 bi bi-gear-wide-connected"></i> </a>
                    {/*end::settings*/}
                    <i></i>
                    <i></i>
                </div>
            </div>
            {/*end:: The 1st stack */}

            {/*start:: The 2nd stack */}
            <div className="d-flex justify-content-between container-fluid mt-4">
                <div className="d-flex justify-contents-left align-items-center">
                    <label htmlFor="">Campaign</label>
                </div>
                <div className="d-flex justify-contents-right align-items-center">
                    <button type="button" className="btn btn-primary btn-sm">+ Contact</button>
                    <button type="button" className="btn btn-outline-primary btn-sm">+ Task</button>
                </div>
            </div>
            {/*end:: The 2nd stack */}

            {/*start:: The 3rd stack */}
            <div className="d-flex justify-content-between container-fluid mt-4">
                <div className="row w-100">
                    {/* start:: The left side stack */}
                    <div className="col-3">
                        <div className="card">
                            <div className="card-body">
                                {/* <h5 className="card-title">Card title</h5>
                                <h6 className="card-subtitle mb-2 text-body-secondary">Card subtitle</h6> */}
                                <div className="d-flex flex-column">
                                    <div className="d-flex justify-content-between align-items-center">
                                        <div className="d-flex justify-content.left"><p className="card-text">Lead Information</p></div>
                                        <div className="d-flex justify-content-right"><i className="bi bi-pencil"></i>
                                        <i className="bi bi-gear"></i></div>
                                    </div>
                                    <div className="d-flex justify-content-between align-items-center">
                                        <div><p className="card-text">Account</p></div>
                                        <div className="form-check form-switch"><input className="form-check-input" type="checkbox" role="switch"/></div>
                                        <div>Contact</div>
                                    </div>
                                    <div>Name: Sam Miller</div>
                                    <div>Email: sam15@gmail.com</div>
                                    <div className="dflex"><i className="bi bi-telephone-fill"></i> : 8790123467</div>
                                    <div className="dflex"><i className="bi bi-whatsapp"></i> : 8790123467</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* end:: The left side stack */}
                    {/* start:: The middle stack */}
                    <div className="col-6">
                        <div className="d-flex flex-column">
                            <div className="d-flex">
                                <span><h6>Lead Detail</h6></span>
                            </div>
                            <div>
                                <div className="card">
                                <div className="card-body d-flex justify-content-between">
                                    <div className="d-flex align-items-center">
                                        <a href="#" className="card-link"><img src="/media/logos/CircleOneLogo.png" alt="mrJohn" width='40px' height='40px' /></a>
                                        <div className="d-flex flex-column mx-6">
                                            <p><h6 className="card-subtitle mb-2 text-body-secondary">Lead Id</h6></p>
                                            <p className="card-text">Support00227</p>
                                        </div>
                                        <div className="d-flex flex-column">
                                            <p><h6 className="card-subtitle mb-2 text-body-secondary">Created Date</h6></p>
                                            <p className="card-text">4/27/21, 6:41 pm</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <img src="/media/logos/CircleOneLogo.png" alt="mrJohn" width='40px' height='40px' />
                                        <h6 className="card-subtitle mb-2 text-body-secondary">Prajakta</h6>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div>
                                <div className="card mt-2">
                                        <MultiStepProgressBar/>
                                </div>
                            </div>
                            <div className="card mt-4">
                            <div className="d-flex">
                                <button className="btn btn-outline-primary text-dark rounded-0 justify-content-center border">Lead Information</button>
                                <button className="btn btn-outline-primary text-dark rounded-0 justify-content-center border">Channel History</button>
                                <button className="btn btn-outline-primary text-dark rounded-0 justify-content-center border">Messages</button>
                                <button className="btn btn-outline-primary text-dark rounded-0 justify-content-center border">Trail</button>
                                <button className="btn btn-outline-primary text-dark rounded-0 justify-content-center border">Tasks</button>
                            </div>
                            </div>

                        </div>
                    </div>
                    {/* end:: The middle stack */}
                    {/* start:: The right side stack */}
                    <div className="col-3">
                        <div className="d-flex flex-column">
                            <div className="card p-2">
                                    {/* <h5 className="card-title">Card title</h5> */}
                                    <p className="text-center card-subtitle mb-2 text-body-secondary">KM</p>
                                    <div className="card px-2">
                                        <p>First Conversation</p>
                                    </div>
                                    <div className="card px-2">
                                        <p>Testing</p>
                                    </div>
                                    <div className="card px-2">
                                        <p>Photo</p>
                                    </div>
                                    <div className="card px-2">
                                        <p>Lead Status</p>
                                    </div>
                                    <div className="card px-2">
                                        <p></p>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                    {/* end:: The right side stack */}
                </div>
            </div>
            {/*end:: The 3rd stack */}

            {/*start:: The 4th stack */}
            <div className="d-flex justify-content-between container-fluid mt-4">
                <div className="row w-100">
                    {/* start:: The left side stack */}
                    <div className="col-3">
                    </div>
                    {/* end:: The left side stack */}
                    {/* start:: The middle stack */}
                    <div className="col-6">
                    <div className="card mt-4">
                        <div className="row w-100">
                            <div className="col-12">
                                {/*begin::Body*/}
                                <div className="py-10">
                                    {/*begin::Timeline*/}
                                    <div className="circleOnetimeline-label">
                                        {/*begin::Item*/}
                                        <div className="timeline-item mx-16 d-flex align-items-center">
                                            {/*begin::Label*/}
                                            <div className="circleOnetimeline-label fw-bold text-gray-800 fs-6">{/* 08:42 */}</div>
                                            {/*end::Label*/}

                                            {/*begin::Badge*/}
                                            <div className="timeline-badge">
                                                <div className="bg-danger rounded-5" >
                                                    <i className="fa fa-solid fa-envelope fs-1"></i>
                                                </div>
                                            </div>
                                            {/*end::Badge*/}

                                            {/*begin::Text*/}
                                            <div className="fw-mormal timeline-content bg-secondary p-2 mx-10">
                                                <span>My Order hasn't arrived yet</span>
                                                <span className="text-primary px-2">REF-ID-0020-03309</span>
                                                <span className="text-muted">25/06/2021 11:13Am</span><br />
                                                <span className="text-muted">I ordered the top in small size</span>
                                            </div>
                                            {/*end::Text*/}
                                        </div>
                                        {/*end::Item*/}

                                        {/*begin::Item*/}
                                        <div className="timeline-item mx-15 d-flex align-items-center">
                                            {/*begin::Label*/}
                                            <div className="circleOnetimeline-label fw-bold text-gray-800 fs-6">{/* 08:42 */}</div>
                                            {/*end::Label*/}

                                            {/*begin::Badge*/}
                                            <div className="timeline-badge">
                                                <div className="bg-success rounded-5" >
                                                    <i className="fa fa-solid fa-phone fs-1"></i>
                                                </div>
                                            </div>
                                            {/*end::Badge*/}

                                            {/*begin::Text*/}
                                            <div className="fw-mormal timeline-content bg-secondary p-2 mx-10">
                                                <span>My Order hasn't arrived yet</span>
                                                <span className="text-primary px-2">REF-ID-0020-03309</span>
                                                <span className="text-muted">25/06/2021 11:13Am</span><br />
                                                <span className="text-muted">I ordered the top in small size</span>
                                            </div>
                                            {/*end::Text*/}
                                        </div>
                                        {/*end::Item*/}

                                        {/*begin::Item*/}
                                        <div className="timeline-item mx-15 d-flex align-items-center">
                                            {/*begin::Label*/}
                                            <div className="circleOnetimeline-label fw-bold text-gray-800 fs-6">{/* 08:42 */}</div>
                                            {/*end::Label*/}

                                            {/*begin::Badge*/}
                                            <div className="timeline-badge">
                                                <div className="bg-success rounded-5 fs-1" >
                                                    <i className="bi bi-chat"></i>
                                                </div>
                                            </div>
                                            {/*end::Badge*/}

                                            {/*begin::Text*/}
                                            <div className="fw-mormal timeline-content bg-secondary p-2 mx-10">
                                                <span>My Order hasn't arrived yet</span>
                                                <span className="text-primary px-2">REF-ID-0020-03309</span>
                                                <span className="text-muted">25/06/2021 11:13Am</span><br />
                                                <span className="text-muted">I ordered the top in small size</span>
                                            </div>
                                            {/*end::Text*/}
                                        </div>
                                        
                                        {/*begin::Item*/}
                                        <div className="timeline-item mx-15 d-flex align-items-center">
                                            {/*begin::Label*/}
                                            <div className="circleOnetimeline-label fw-bold text-gray-800 fs-6">{/* 08:42 */}</div>
                                            {/*end::Label*/}

                                            {/*begin::Badge*/}
                                            <div className="timeline-badge">
                                                <div className="bg-danger rounded-5" >
                                                    <i className="fa fa-solid fa-phone fs-1"></i>
                                                </div>
                                            </div>
                                            {/*end::Badge*/}

                                            {/*begin::Text*/}
                                            <div className="fw-mormal timeline-content bg-secondary p-2 mx-10">
                                                <span>My Order hasn't arrived yet</span>
                                                <span className="text-primary px-2">REF-ID-0020-03309</span>
                                                <span className="text-muted">25/06/2021 11:13Am</span><br />
                                                <span className="text-muted">I ordered the top in small size</span>
                                            </div>
                                            {/*end::Text*/}
                                        </div>
                                        {/*end::Item*/}
                                    </div>
                                    {/*end::Timeline*/}
                                </div>
                                {/*end: Card Body*/}
                            </div>
                        </div>

                        </div>
                    </div>
                    {/* end:: The middle stack */}
                    {/* start:: The right stack */} 
                    <div className="col-3">
                        <div className="card p-2">
                           <div className="flex-column">
                                {/*begin::Contacts*/}
                                <div className="card card-flush">
                                    {/*begin::Card header*/}
                                    <div className="card-header pt-7" id="kt_chat_contacts_header">
                                        {/*begin::Form*/}
                                        <form className="w-100 position-relative">  
                                            {/*begin::Icon*/}
                                            <i className="ki-duotone ki-magnifier fs-3 text-gray-500 position-absolute top-50 ms-5 translate-middle-y"><span className="path1"></span><span className="path2"></span></i>            {/*end::Icon*/}

                                            {/*begin::Input*/}
                                            <input type="text" className="form-control form-control-solid px-13" name="search" value="" placeholder="Search by username or email..."/>
                                            {/*end::Input*/}

                                            <div className="d-flex flex-stack py-4">
                                                    {/*begin::Details*/}
                                                    <div className="d-flex align-items-center">
                                                        {/*begin::Label*/}
                                                            <label><h6>Our Team</h6></label>
                                                        {/*end::Label*/}
                                                    </div>
                                                    {/*end::Details*/}

                                                    {/*begin::actionbuttons*/}
                                                    <div className="d-flex flex-column align-items-end ms-2">
                                                        <div className="d-flex">
                                                            <span className="text-muted fs-7 mb-1 px-5"><i className="bi bi-person-add fs-2x"></i></span>
                                                            <span className="text-muted fs-7 mb-1"><i className="bi bi-caret-down-square fs-2x"></i></span>
                                                        </div>    
                                                    </div>
                                                    {/*end::actionbuttons*/}
                                                </div>
                                        </form>
                                        {/*end::Form*/}
                                    </div>
                                    {/*end::Card header*/}

                                    {/*begin::Card body*/}
                                    <div className="card-body pt-5" id="kt_chat_contacts_body">
                                        {/*begin::List*/}
                                        <div className="scroll-y me-n5 pe-5 h-200px h-lg-auto" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_app_header, #kt_toolbar, #kt_app_toolbar, #kt_footer, #kt_app_footer, #kt_chat_contacts_header" data-kt-scroll-wrappers="#kt_content, #kt_app_content, #kt_chat_contacts_body" data-kt-scroll-offset="5px" style={{maxHeight: '150px'}}>
                                                {/*begin::User*/}
                                                <div className="d-flex flex-stack py-4">
                                                    {/*begin::Details*/}
                                                    <div className="d-flex align-items-center">
                                                        {/*begin::Avatar*/}
                                                        <div className="symbol  symbol-45px symbol-circle ">
                                                            <img alt="Pic" src="/metronic8/demo1/assets/media/avatars/300-5.jpg"/>
                                                        <div className="symbol-badge bg-success start-100 top-100 border-4 h-8px w-8px ms-n2 mt-n2"></div>
                                                        </div>
                                                        {/*end::Avatar*/}
                                                        {/*begin::Details*/}
                                                        <div className="ms-5">
                                                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
                                                            <div className="fw-semibold text-muted">Was machst du?</div>
                                                        </div>
                                                        {/*end::Details*/}
                                                    </div>
                                                    {/*end::Details*/}

                                                    {/*begin::Last seen*/}
                                                    <div className="d-flex flex-column align-items-end ms-2">
                                                        <div className="d-flex">
                                                            <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
                                                            <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
                                                        </div>    
                                                    </div>
                                                    {/*end::Last seen*/}
                                                </div>
                                                {/*end::User*/}
                                                {/*begin::Separator*/}
                                                <div className="separator separator-dashed d-none"></div>
                                                {/*end::Separator*/}

                                                {/*begin::User*/}
                                                <div className="d-flex flex-stack py-4">
                                                    {/*begin::Details*/}
                                                    <div className="d-flex align-items-center">
                                                        {/*begin::Avatar*/}
                                                        <div className="symbol  symbol-45px symbol-circle ">
                                                        <span className="symbol-label  bg-light-danger text-danger fs-6 fw-bolder ">M</span>
                                                        </div>
                                                        {/*end::Avatar*/}
                                                        {/*begin::Details*/}
                                                        <div className="ms-5">
                                                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
                                                            <div className="fw-semibold text-muted">Was machst du?</div>
                                                        </div>
                                                        {/*end::Details*/}
                                                    </div>
                                                    {/*end::Details*/}

                                                    {/*begin::Last seen*/}
                                                    <div className="d-flex flex-column align-items-end ms-2">
                                                        <div className="d-flex">
                                                            <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
                                                            <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
                                                        </div>    
                                                    </div>
                                                    {/*end::Last seen*/}
                                                </div>
                                                {/*end::User*/}
                                                {/*begin::Separator*/}
                                                <div className="separator separator-dashed d-none"></div>
                                                {/*end::Separator*/}

                                                {/*begin::User*/}
                                                <div className="d-flex flex-stack py-4">
                                                    {/*begin::Details*/}
                                                    <div className="d-flex align-items-center">
                                                        {/*begin::Avatar*/}
                                                        <div className="symbol  symbol-45px symbol-circle ">
                                                        <span className="symbol-label  bg-light-danger text-danger fs-6 fw-bolder ">M</span>
                                                        </div>
                                                        {/*end::Avatar*/}
                                                        {/*begin::Details*/}
                                                        <div className="ms-5">
                                                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
                                                            <div className="fw-semibold text-muted">Was machst du?</div>
                                                        </div>
                                                        {/*end::Details*/}
                                                    </div>
                                                    {/*end::Details*/}

                                                    {/*begin::Last seen*/}
                                                    <div className="d-flex flex-column align-items-end ms-2">
                                                        <div className="d-flex">
                                                            <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
                                                            <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
                                                        </div>    
                                                    </div>
                                                    {/*end::Last seen*/}
                                                </div>
                                                {/*end::User*/}
                                                {/*begin::Separator*/}
                                                <div className="separator separator-dashed d-none"></div>
                                                {/*end::Separator*/}


                                                {/*begin::User*/}
                                                <div className="d-flex flex-stack py-4">
                                                    {/*begin::Details*/}
                                                    <div className="d-flex align-items-center">
                                                        {/*begin::Avatar*/}
                                                        <div className="symbol  symbol-45px symbol-circle ">
                                                        <span className="symbol-label  bg-light-danger text-danger fs-6 fw-bolder ">M</span>
                                                        </div>
                                                        {/*end::Avatar*/}
                                                        {/*begin::Details*/}
                                                        <div className="ms-5">
                                                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
                                                            <div className="fw-semibold text-muted">Was machst du?</div>
                                                        </div>
                                                        {/*end::Details*/}
                                                    </div>
                                                    {/*end::Details*/}

                                                    {/*begin::Last seen*/}
                                                    <div className="d-flex flex-column align-items-end ms-2">
                                                        <div className="d-flex">
                                                            <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
                                                            <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
                                                        </div>    
                                                    </div>
                                                    {/*end::Last seen*/}
                                                </div>
                                                {/*end::User*/}
                                                {/*begin::Separator*/}
                                                <div className="separator separator-dashed d-none"></div>
                                                {/*end::Separator*/}


                                                {/*begin::User*/}
                                                <div className="d-flex flex-stack py-4">
                                                    {/*begin::Details*/}
                                                    <div className="d-flex align-items-center">
                                                        {/*begin::Avatar*/}
                                                        <div className="symbol  symbol-45px symbol-circle ">
                                                        <span className="symbol-label  bg-light-danger text-danger fs-6 fw-bolder ">M</span>
                                                        </div>
                                                        {/*end::Avatar*/}
                                                        {/*begin::Details*/}
                                                        <div className="ms-5">
                                                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
                                                            <div className="fw-semibold text-muted">Was machst du?</div>
                                                        </div>
                                                        {/*end::Details*/}
                                                    </div>
                                                    {/*end::Details*/}

                                                    {/*begin::Last seen*/}
                                                    <div className="d-flex flex-column align-items-end ms-2">
                                                        <div className="d-flex">
                                                            <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
                                                            <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
                                                        </div>    
                                                    </div>
                                                    {/*end::Last seen*/}
                                                </div>
                                                {/*end::User*/}
                                                {/*begin::Separator*/}
                                                <div className="separator separator-dashed d-none"></div>
                                                {/*end::Separator*/}

                                        </div>
                                        {/*end::List*/}
                                    </div>
                                    {/*end::Card body*/}
                                </div>
                                {/*end::Contacts*/}
                            </div>
                        </div>
                    </div>
                    {/* end:: The right stack */}
                </div>
            </div>
            {/*ends:: The 4th stack */}    
            </div>
        </main>
        {/*main ends*/}
    </div>
    )
}

export default LeadInfo