/* eslint-disable jsx-a11y/anchor-is-valid */
import { Navigate, Outlet, Route, Routes } from 'react-router-dom'
import { Login } from './Login'
import { toAbsoluteUrl } from '../../helpers'
import { Registration } from './Registration'
import { ForgotPassword } from './ForgotPassword'
import { useEffect } from 'react'
import welcomeimage from '../../assets/images/signature3D.png'

const AuthLayout = () => {
  useEffect(() => {
    document.body.classList.add('bg-white')
    return () => {
      document.body.classList.remove('bg-white')
    }
  }, [])

  return (
          <Outlet />
  )
}

const AuthPage = () => (
  <Routes>
    <Route element={<AuthLayout />}>
      <Route path='login' element={<Login />} />
      <Route path='registration' element={<Registration />} />
      <Route path='forgot-password' element={<ForgotPassword />} />
      <Route index element={<Navigate to='/auth/login' />} />
    </Route>
  </Routes>
)

export { AuthPage }
