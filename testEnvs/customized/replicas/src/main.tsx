import React from 'react'
import ReactDOM from 'react-dom/client'
/*import App from './App.tsx'*/
import SOhome from './SOhome.tsx'
/* import './index.css' */

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <SOhome/>
  </React.StrictMode>,
)

/*This is a custom personal workspace that is implemented with just NiceAdmin atop React+Vite+TS+Bootstrap environment*/