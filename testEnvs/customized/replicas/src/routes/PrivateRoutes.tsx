import Dashboard from "../pages/Dashboard"
import { Route, Routes } from 'react-router-dom';
import { MasterLayout } from '../layout/MasterLayout';
import CampaignRoutes from "../pages/campaign/CampaignRoutes";
import { useState } from "react";
import TestMain from "../pages/TestB4Inclusion/TestMain";

const PrivateRoutes = () => {
	const [showSideBar, setShowSideBar] = useState(true);
	const toggleSidebar = () => { setShowSideBar(!showSideBar); }

	return (
		<Routes>
			<Route element = {<MasterLayout showSideBar = {showSideBar} toggleSidebar = {toggleSidebar}/>}>
				<Route path = 'dashboard' element = {<Dashboard showSideBar = {showSideBar}/>} />
				<Route path = 'campaign/*' element = {<CampaignRoutes />} />
				<Route path = 'testb4inclusion/*' element = {<TestMain />} />
			</Route>
		</Routes>
	)
}

export { PrivateRoutes }	