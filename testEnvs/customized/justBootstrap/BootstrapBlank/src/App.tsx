import { useState } from 'react'
import reactLogo from './assets/react.svg'
import bootstrapLogo from './assets/bootstrap-fill.svg'
import typescriptLogo from './assets/filetype-tsx.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
        <a href="https://getbootstrap.com/" target="_blank">
          <img src={bootstrapLogo} className="logo" alt="Bootstrap logo" />
        </a>
        <a href="https://getbootstrap.com/" target="_blank">
          <img src={typescriptLogo} className="logo" alt="TypeScript logo" />
        </a>
      </div>
      <h1>Vite + React + TypeScript + Bootstrap</h1>
      <div className="card container py-4 px-3 mx-auto">
        <button className='btn btn-primary' onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <h4>
          Edit <code>src/App.tsx</code> and save to test HMR
        </h4>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>

      {/* <div className="container py-4 px-3 mx-auto">
        <h1>Hello, Bootstrap and Vite!</h1>
        <button className="btn btn-primary">Primary button</button>
      </div> */}
    </>
  )
}

export default App
