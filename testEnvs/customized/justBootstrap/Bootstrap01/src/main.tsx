import React from 'react'
import ReactDOM from 'react-dom/client'
import Dashboard from './testpages/Dashboard'
/*import App from './App.tsx'*/
/* import './index.css' */

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    {/*<App />*/}
    <Dashboard/>
  </React.StrictMode>,
)
