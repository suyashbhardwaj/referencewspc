import React from 'react'
/*import ReactDOM from 'react-dom/client'*/
import ReactDOM from 'react-dom/client'
/*import App from './App.tsx'*/
/* import './index.css' */
import './assets/scss/style.scss'
/*import NestedDD from './testui/customized/NestedDD'*/
import TestSomeUI from './testui/readymade/TestSomeUI'
/*import ScheduleTicket from './testui/projectloading/scheduleTicketForm/'*/
/*import NestedDDconf from './testui/customized/nestedddconf/NestedDDconf'*/
/*import FormBuild from './testui/projectloading/formbuildNrender/FormBuild'*/
/*import RenderForm from './testui/projectloading/renderedformwa/RenderForm'*/
/*import SOhome from './SOhome'*/
/*import Chatlist from './testui/readymade/Chatlist'*/
/*import Timeline from './testui/readymade/Timeline'*/

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <TestSomeUI/>
    {/*<FormBuild/>*/}
  </React.StrictMode>,
)

/*This is a custom personal workspace that is implemented with just NiceAdmin Layout + Metronic Theme Styles atop React+Vite+TS+Bootstrap environment*/
/*double rendering is a result of using React.StrictMode, which is just to ensure that the application being build is production ready*/