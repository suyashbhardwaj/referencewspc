import { useEffect, useState } from 'react';
import { CloseButton } from "react-bootstrap";
import ReactModal from 'react-modal';
import NewContactModal from "./NewContactModal";
import axios from 'axios';
import { useFormik } from 'formik'
import { convertDateFormat } from './common/utilities/justMethods';
import { NewScheduleModalProps } from './common/interfaces';
import { customModalStyles } from './common/commonConfigurations';
import { newTicketScheduleParserSchema } from './common/parserSchemas';

const initialValues = {
  title:"",
  description:"",
  contact:0,
  ticket_channel:"",
  team:"",
  disposition:0,
  disposition_description:"",
  status:null,
  status_type:"",
  date_from:"",
  date_to:"",
  schedule_timer:"",
  daily_time:""
}

const NewScheduleModal: React.FC<NewScheduleModalProps> = ({closeModal}) => {
  const [suggestions, setSuggestions] = useState([]);
  const [selectedContact, setSelectedContact] = useState<number>(0);
  const [isAddNewContactModalOpen, setAddNewContactModalOpen] = useState(false);
  const [selectedDisposition, setSelectedDisposition] = useState<number>(0);
  const [theStatuses, setTheStatuses] = useState([]);
  const [theDispositions, setTheDispositions] = useState([]);
  const [currentLoggedInUser, setCurrentLoggedInUser] = useState();
  const [loading, setLoading] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [formSubmissionStatus, setFormSubmissionStatus] = useState("");
  
  async function fetchStatus() { axios.get("http://botgo.localhost:8000/api/v1/common/disposition/1/lookup_by_disposition/", { headers: { Authorization: `Token ${localStorage.getItem('auth-token')}`,},})
      .then((res: any) => {
        res.status ===200 && setTheStatuses(res.data.all_statuses)
      }).catch((err:any)=>console.log(err))}
  
  const searchContactByMailID = (skey:string) => {
    console.log('tried searching ',skey);
    axios.get(`http://botgo.localhost:8000/api/v1/customers/contacts/search-by-email/`, 
      {headers: { 
        Authorization: `Token ${localStorage.getItem('auth-token')}`, 
      },params: {
        email: skey
      }})
      .then((res: any) => {if(res.status===200) setSuggestions(res.data)})
      .catch((err:any)=>console.log(err))
  }
  
  const openAddContactModal = () => { setAddNewContactModalOpen(true); };
  const closeAddContactModal = () => { setAddNewContactModalOpen(false); };
    
  const handleDispositionDropdownChange = (evt:any) => { setSelectedDisposition(Number.parseInt(evt.target.value)); fetchStatus() }

  const submitForm = async (valObj: any) => {
        const promise = new Promise((resolve, reject) => {
          console.log('resolved with ', resolve)
          console.log('rejected with ', reject)
        })
        console.log('new ticket schedule modal form submitted with payload ', valObj)
        try {
            const response = await axios
                .post(`http://botgo.localhost:8000/api/v1/tickets/auto-ticket-scheduler/`, valObj, {headers: { Authorization: `Bearer ${localStorage.getItem('auth-token')}`}})
                .then((res) => {
                    console.log('response is ', res)
                    if(res.status === 201) { alert("schedule has been created"); closeModal();}
                    else {alert("failed to create a new schedule")}
                })
        } catch (error) { console.error('Error making the POST request:', error); setFormSubmissionStatus('Error submitting the form')
        } finally { setSubmitting(false) /* Set submitting to false to enable the form again */ }
        return promise
    }

  const formik = useFormik({
        initialValues,
        validationSchema: newTicketScheduleParserSchema,
        onSubmit: (values, { setStatus, setSubmitting }) => {
          console.log(`submitted the form with contact as ${selectedContact} team as ${currentLoggedInUser.id} disposition as ${selectedDisposition} & other values as ${values}`);
          const date_fromFormatted = convertDateFormat(values.date_from)
          const date_toFormatted = convertDateFormat(values.date_to)
          if(!(selectedContact===0 || selectedDisposition === 0 || currentLoggedInUser === "")) {
            setLoading(true)  
            submitForm({
              "contact":selectedContact,
              "daily_time":values.daily_time,
              "date_from":date_fromFormatted,
              "date_to":date_toFormatted,
              "description":values.description,
              "disposition":selectedDisposition,
              "disposition_description":values.disposition_description,
              "schedule_timer":values.schedule_timer,
              "status":null,
              "status_type":values.status_type,
              "team":String(currentLoggedInUser.id),
              "ticket_channel":values.ticket_channel,
              "title":values.title,
              })
            .then((res: any) => {
                /* console.log('response.status is ', res.status) */
                console.log('response is ', res)
                closeModal()
              if (res.data.signIn.status === 201) {
                console.log('received success response against contact creation form submission!')
                /* setLoading(false) */
                closeModal()
              } else {
                /* setLoading(false) */
                console.log(res)
                setSubmitting(false)
              }
            })
            .catch((err: any) => {
              /* setLoading(false) */
              setSubmitting(false)
              console.error(err)
            })
          }
      }});

  useEffect(() => {
    async function fetchDispositions() {
      axios.get("http://botgo.localhost:8000/api/v1/users/me/", { headers: { Authorization: `Token ${localStorage.getItem('auth-token')}`,},})
        .then((res: any) => {
          if(res.status ===200) {
            axios.get("http://botgo.localhost:8000/api/v1/common/disposition/?team_id=1&", 
            { headers: { Authorization: `Token ${localStorage.getItem('auth-token')}`}, params: { team_id: res.data.id }})
            .then((res2:any)=>{
              console.log("res2 is ",res2);
              setTheDispositions(res2.data);
            }).catch((err:any)=>alert("200 not received on res2"))}
        }).catch((err:any)=>console.log(err))
    }
    async function fetchLoggedInUserDetails() {
      axios.get("http://botgo.localhost:8000/api/v1/users/me/", { headers: { Authorization: `Token ${localStorage.getItem('auth-token')}`,},})
        .then((res: any) => {
          if(res.status ===200) { setCurrentLoggedInUser(res.data)}
        }).catch((err:any)=>console.log(err))
    }
    fetchDispositions();
    fetchLoggedInUserDetails();
  }, []);

  return(<>
    <div className='modal-dialog modal-dialog-scrollable'>
      <div className="text-end">
        <CloseButton onClick={closeModal} />
      </div>

      <form className='form w-100 fv-plugins-bootstrap5 fv-plugins-framework' noValidate onSubmit={formik.handleSubmit}>
        <div className="text-center mb-4">
          <label className="form-label fs-4 fw-bolder text-dark card-title">
            Schedule a Ticket
          </label>
        </div>
        {currentLoggedInUser === "" && 
          <div className='fv-plugins-message-container text-danger'>
            <span role='alert'>Current Logged In User Details not available</span>
          </div>
        }

        <div>
          <input type="email" className="form-control is-invalid" id="floatingInputInvalid" placeholder="name@example.com" value="test@example.com"/>
          <label htmlFor="floatingInputInvalid">Invalid input</label>
        </div>
        
        <div className="row g-5 g-xl-8 mb-6">
          {/*title feed*/}
          <div className="col-xl-6">
            <input
              id='title'
              name='title'
              placeholder='Title'
              onChange={formik.handleChange}
              value={formik.values.title} type="text" className="form-control required" />
            
            {formik.touched.title && formik.errors.title && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.title}</span>
            </div>
            )}
          </div>

          {/*ticket description feed*/}
          <div className="col-xl-6">
            <label>Ticket Description</label>
            <input
              id='description'
              name='description'
              onChange={formik.handleChange}
              value={formik.values.description} type="text" className="form-control" />
            
            {formik.touched.description && formik.errors.description && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.description}</span>
            </div>
            )}
          </div>
        </div>

        <div className="text-center mb-4">
          <label className="form-label fs-4 fw-bolder text-dark card-title">
            Customer Details
          </label>
        </div>
        <div className="row g-5 g-xl-8 mb-4">
          <div className="col-xl-6">
            <label>Email id</label>
            <input
              type="text" 
              className='form-control' 
              onChange={(evt)=>searchContactByMailID(evt.target.value)} placeholder="type in here"
              />

            <select 
              className={`form-select ${suggestions.length ? '':'d-none'}`} 
              value={selectedContact} 
              onChange={(evt) => setSelectedContact(Number.parseInt(evt.target.value))}
              >
              <option value="">Select an option...</option>
              {suggestions.map((suggestion, index) => (
                <option key={index} value={suggestion.id}>{suggestion.email}</option>
              ))}
            </select>
            
          </div>
          <div className="col-xl-6">
            <label className="required">Customer Name</label>
            <input
              type="text" 
              value = {selectedContact} 
              className="form-control" 
              />
          </div>
          {selectedContact === 0 && 
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>Contact has not been provided</span>
            </div>
          }
        </div>

        <div className="text-end mt-6 mb-4">
          <button className="btn btn-sm btn-secondary" onClick={openAddContactModal}>
            <i className="bi bi-plus fs-2 me-2"></i>
            Contact
          </button>
        </div>

        <ReactModal isOpen={isAddNewContactModalOpen} onRequestClose={closeAddContactModal} style={customModalStyles} contentLabel="New Contact">
            <NewContactModal closeModal={closeAddContactModal} />
        </ReactModal>

        <div className="row g-5 g-xl-8 mb-4">
          <div className="col-xl-6">
            <label>Select Channel</label>
            <select 
              className="form-select select2-hidden-accessible"
              id='ticket_channel'
              name='ticket_channel'
              onChange={formik.handleChange}
              value={formik.values.ticket_channel} 
              >
              <option value="select team">
                Select
              </option>
              <option value="Phone">Phone</option>
              <option value="Email">Email</option>
              <option value="Chat">Chat</option>
              <option value="Social Media">Social Media</option>
              <option value="WhatsApp">WhatsApp</option>
            </select>
            {formik.touched.ticket_channel && formik.errors.ticket_channel && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.ticket_channel}</span>
            </div>
            )}
          </div>
          <div className="col-xl-6">
            <label>Select disposition</label>
            <select
                className="form-select select2-hidden-accessible"
                onChange={handleDispositionDropdownChange}
                value={selectedDisposition}
              >
              <option value="">Select Disposition</option>
              {theDispositions.map((disposition)=>( <option value={disposition.id}>{disposition.topic}</option> ))}
            </select>
            {selectedDisposition === 0 && 
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert'>Disposition has not been provided</span>
              </div>
            }
          </div>

          <div className="col-xl-6">
            <label>Remarks</label>
            <input
              id='disposition_description'
              name='disposition_description'
              onChange={formik.handleChange}
              value={formik.values.disposition_description} 
              type="text" 
              className="form-control" 
              />

            {formik.touched.disposition_description && formik.errors.disposition_description && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.disposition_description}</span>
            </div>
            )}
          </div>
          <div className="col-xl-6">
            <label>Select status</label>
            <select 
              className="form-select select2-hidden-accessible"
              id='status_type'
              name='status_type'
              onChange={formik.handleChange}
              value={formik.values.status_type} 
              >
              <option value="">Select Status</option>
              {theStatuses.map((status)=>{ if(typeof status.status !== 'object') return <option value={status.status}>{status.status}</option> })}
            </select>

            {formik.touched.status_type && formik.errors.status_type && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.status_type}</span>
            </div>
            )}
          </div>
        </div>

        <div className="text-center mb-4">
          <label className="form-label fs-4 fw-bolder text-dark card-title">
            Schedule
          </label>
        </div>

        <div className="row g-5 g-xl-8 mb-6">
          <div className="col-xl-6">
            <label>Start</label>
            <input
              id='date_from'
              name='date_from'
              onChange={formik.handleChange}
              value={formik.values.date_from}
              className="form-control form-control-lg"
              type="date"
              placeholder="Custom Date"
            />

            {formik.touched.date_from && formik.errors.date_from && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.date_from}</span>
            </div>
            )}
          </div>

          <div className="col-xl-6">
            <label>End</label>
            <input
              id='date_to'
              name='date_to'
              onChange={formik.handleChange}
              value={formik.values.date_to}
              className="form-control form-control-lg"
              type="date"
              placeholder="Custom Date"
            />

            {formik.touched.date_to && formik.errors.date_to && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.date_to}</span>
            </div>
            )}
          </div>

          <div className="col-xl-6">
            <label>Select frequency</label>
            <select 
              className="form-select select2-hidden-accessible"
              id='schedule_timer'
              name='schedule_timer'
              onChange={formik.handleChange}
              value={formik.values.schedule_timer} 
              >
              <option value="select team" selected hidden>
                Select
              </option>
              <option value="daily">Daily</option>
              <option value="weekly">Weekly</option>
              <option value="monthly">Monthly</option>
            </select>

            {formik.touched.schedule_timer && formik.errors.schedule_timer && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.schedule_timer}</span>
            </div>
            )}
          </div>

          <div className="col-xl-6 ">
            <label>Time</label>
            <input
              id='daily_time'
              name='daily_time'
              onChange={formik.handleChange}
              value={formik.values.daily_time}
              className="form-control form-control-lg"
              type="time"
              placeholder="Custom Time"
            />

            {formik.touched.daily_time && formik.errors.daily_time && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.daily_time}</span>
            </div>
            )}
          </div>
        </div>

        <div className="text-end mt-6 mb-4">
          <button className="btn btn-sm btn-secondary" type= "submit">
            Schedule Ticket
          </button>
        </div>

      </form>
    </div>  
  </>)
}

export default NewScheduleModal