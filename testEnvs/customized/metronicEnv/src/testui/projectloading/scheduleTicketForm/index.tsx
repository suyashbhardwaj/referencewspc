import { useState, useEffect } from "react"
import ReactModal from "react-modal";
import NewScheduleModal from "./NewScheduleModal";
import { customModalStyles } from './common/commonConfigurations';

const ScheduleTicket = () => {
  const [isNewTicketModalOpen, setIsNewTicketModalOpen] = useState(true);
  const openModal = () => { setIsNewTicketModalOpen(true); };
  const closeModal = () => { setIsNewTicketModalOpen(false); };

  useEffect(()=>{ localStorage.setItem('auth-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE3MTY0OTM5NDksInRlYW0iOiIxIiwiYWNjb3VudCI6ZmFsc2V9.YBAqIYi6Ic8DADdhiVQFhiOywElL3U7b1xXuHutlx60')});
  return (
    <>
      <div className="container mt-10">
        <button className="btn btn-primary" onClick={openModal}>OpenTheModal</button>
      </div>
      <ReactModal
        isOpen={isNewTicketModalOpen}
        onRequestClose={closeModal}
        style={customModalStyles}
        contentLabel="New Ticket Schedule"
        >
        <NewScheduleModal closeModal={closeModal}/>
      </ReactModal>
    </>
  )
}

export default ScheduleTicket