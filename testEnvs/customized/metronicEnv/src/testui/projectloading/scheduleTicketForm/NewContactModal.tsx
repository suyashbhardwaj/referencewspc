import { useEffect, useState } from "react";
import { MultiSelect } from 'react-multi-select-component'
import { CloseButton } from "react-bootstrap";
import axios from 'axios';
import { useFormik } from 'formik'
import * as Yup from 'yup'

/*common: interfaces, initialValues, parserSchema, modalprovisions, apiEndpoints, token, utilityFunctions, readyFormInputs*/

interface IProps {
    closeModal: () => void;
  }

const initialValues = {
    account: "",
    accessible_teams: [],
    name: "",
    email: "",
    phone_number: "",
    facebook_id: "",
    whatsapp_number: "",
    twitter_id: "",
    dynamic_fields: {},
    tags: []
  }

const parserSchema = Yup.object().shape({
    account: Yup.string().nullable(),
    accessible_teams: Yup.array().of(Yup.string()).nullable(),
    name: Yup.string().required('Name is required'),
    email: Yup.string().email('Invalid email').required('Email is required'),
    phone_number: Yup.string().nullable(),
    facebook_id: Yup.string().nullable(),
    whatsapp_number: Yup.string().nullable(),
    twitter_id: Yup.string().nullable(),
    dynamic_fields: Yup.object().nullable(),
    tags: Yup.array().of(Yup.string()).nullable(),
  });

const NewContactModal: React.FC<IProps> = ({ closeModal }) => {
    const [theTeams, setTheTeams] = useState([]);
    const [selectedTeams, setSelectedTeams] = useState([]);
    const [loading, setLoading] = useState(false);
    const [submitting, setSubmitting] = useState(false);
    const [formSubmissionStatus, setFormSubmissionStatus] = useState("");
    const handleSubmit = (evt:any) => {
        evt.preventDefault();
        const dummypayload = {
            "account": "",
            "accessible_teams": [
                "1",
                "2"
            ],
            "name": "Abhinev Qasba",
            "email": "abhinev@qasba.in",
            "phone_number": "9284823946",
            "facebook_id": "",
            "whatsapp_number": "9284823946",
            "twitter_id": "",
            "dynamic_fields": {},
            "tags": []
        }
    }

    const submitForm = async (valObj: any) => {
        const promise = new Promise((resolve, reject) => {
          console.log('resolved with ', resolve)
          console.log('rejected with ', reject)
        })
        console.log('resume parser form submitted with payload ', valObj)
        try {
            const response = await axios
                .post(`http://botgo.localhost:8000/api/v1/customers/contacts/`, valObj, {headers: { Authorization: `Bearer ${localStorage.getItem('auth-token')}`}} /* , {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'x-rapidapi-host': 'file-upload8.p.rapidapi.com',
                    'x-rapidapi-key': 'your-rapidapi-key-here'
                }
                }*/)
                .then((res) => {
                    console.log('response is ', res)
                    if(res.status === 201) { alert("contact has been created"); closeModal();}
                    else {alert("failed to create a new contact")}
                })
        } catch (error) { console.error('Error making the POST request:', error); setFormSubmissionStatus('Error submitting the form')
        } finally { setSubmitting(false) /* Set submitting to false to enable the form again */ }
        return promise
    }

    const formik = useFormik({
        initialValues,
        validationSchema: parserSchema,
        onSubmit: (values, { setStatus, setSubmitting }) => {
          setLoading(true)
          submitForm({
            "account": values.account,
            "accessible_teams": selectedTeams.map(x=>x.value),
            "name": values.name,
            "email": values.email,
            "phone_number": values.phone_number,
            "facebook_id": values.facebook_id,
            "whatsapp_number": values.whatsapp_number,
            "twitter_id": values.twitter_id,
            "dynamic_fields": values.dynamic_fields,
            "tags": values.tags
            })
            .then((res: any) => {
                /* console.log('response.status is ', res.status) */
                console.log('response is ', res)
                closeModal()
              if (res.data.signIn.status === 201) {
                console.log('received success response against contact creation form submission!')
                /* setLoading(false) */
                closeModal()
              } else {
                /* setLoading(false) */
                console.log(res)
                setSubmitting(false)
              }
            })
            .catch((err: any) => {
              /* setLoading(false) */
              setSubmitting(false)
              console.error(err)
            })
        }
      })

    useEffect(() => {
        async function fetchTeams() {
          axios
            .get("http://botgo.localhost:8000/api/v1/users/teams/", {
            /* .get(`${process.env.BOTGOAPIBASEURL}/users/teams/`, { */
              headers: {
                /* Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE3MTYzMDQ5NzIsInRlYW0iOiIxIiwiYWNjb3VudCI6ZmFsc2V9.id5FPd9cX3E_7WxiFbIvt_gQix0JSOPKFenvww8aSWE`, */
                /* Authorization: `Bearer ${process.env.accessToken}`, */
                Authorization: `Bearer ${localStorage.getItem('auth-token')}`,
              },
            })
            .then((res: any) => {
              console.log(res);
              setTheTeams(res.data);
            })
            .catch((err)=>console.log(err))
        }
        fetchTeams();
      }, []);

    return (
    <div>
        <div className="text-end">
            <CloseButton onClick={closeModal} />
        </div>
        <form className='form w-100 fv-plugins-bootstrap5 fv-plugins-framework' noValidate onSubmit={formik.handleSubmit}>
        {formik.status ? (<div className='mb-lg-15 alert alert-danger'>{' '} <div className='alert-text font-weight-bold'>{formik.status}</div>{' '}</div>): (<div></div>)}
            <div className="text-center mb-4">
                <label className="form-label fs-4 fw-bolder text-dark card-title">
                New Contact
                </label>
            </div>
            <div className="row">
                <div className="col">
                    <label className="form-label">Select Account</label>
                    <select 
                        className="form-select select2-hidden-accessible"
                        id='account'
                        name='account'
                        onChange={formik.handleChange}
                        value={formik.values.account}
                        >
                        <option value=""></option>
                        <option value="">Select No Account</option>
                    </select>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label className="form-label">Team</label>
                    <MultiSelect
                        options={theTeams.map((team: any) => ({
                            label: team.name,
                            value: team.id
                            }))}
                        value={selectedTeams}
                        onChange={setSelectedTeams}
                        labelledBy='Select Teams'
                        />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label htmlFor="" className="form-label">Name</label>
                    <input 
                        type="text" 
                        className="form-control"
                        id='name'
                        name='name'
                        onChange={formik.handleChange}
                        value={formik.values.name}
                        />
                    {formik.touched.name && formik.errors.name && (
                    <div className='fv-plugins-message-container text-danger'>
                      <span role='alert'>{formik.errors.name}</span>
                    </div>
                    )}
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label htmlFor="" className="form-label">Email Address</label>
                    <input 
                        type="text" 
                        className="form-control"
                        id='email'
                        name='email'
                        onChange={formik.handleChange}
                        value={formik.values.email}
                        />
                    {formik.touched.email && formik.errors.email && (
                    <div className='fv-plugins-message-container text-danger'>
                      <span role='alert'>{formik.errors.email}</span>
                    </div>
                    )}
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label htmlFor="" className="form-label">Mobile No</label>
                    <input 
                        type="text" 
                        className="form-control"
                        id='phone_number'
                        name='phone_number'
                        onChange={formik.handleChange}
                        value={formik.values.phone_number}
                        />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label htmlFor="" className="form-label">WhatsApp No</label>
                    <input 
                        type="text" 
                        className="form-control"
                        id='whatsapp_number'
                        name='whatsapp_number'
                        onChange={formik.handleChange}
                        value={formik.values.whatsapp_number}
                        />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label htmlFor="" className="form-label">Twitter Id</label>
                    <input 
                        type="text" 
                        className="form-control"
                        id='twitter_id'
                        name='twitter_id'
                        onChange={formik.handleChange}
                        value={formik.values.twitter_id}
                        />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label htmlFor="" className="form-label">Facebook Id</label>
                    <input 
                        type="text" 
                        className="form-control"
                        id='facebook_id'
                        name='facebook_id'
                        onChange={formik.handleChange}
                        value={formik.values.facebook_id}
                        />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <button className="btn btn-primary mt-4" type='submit'>Save</button>
                </div>
            </div>
        </form>
    </div>
  )
}

export default NewContactModal