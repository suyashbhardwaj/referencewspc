import * as Yup from 'yup'

export const newTicketScheduleParserSchema = Yup.object().shape({
  title:Yup.string().required('Please provide a title'),
  description:Yup.string().required('Please provide a description'),
  /*contact:Yup.number().required("Please select a contact").positive().integer(),*/
  ticket_channel:Yup.string().required('Please select a ticket channel'),
  /*team:Yup.string().required('A team has to be there'),*/
  /*disposition:Yup.number().required("Please select a disposition").positive().integer(),*/
  disposition_description:Yup.string().required('Please provide a disposition description'),
  status:Yup.string().nullable(),
  status_type:Yup.string().required('Please select a status'),
  date_from:Yup.string().required('Please provide a start date'),
  date_to:Yup.string().required('Please provide an end date'),
  schedule_timer:Yup.string().required('Please select a schedule frequency'),
  daily_time:Yup.string().required('Please select a time'),
  });