export const convertDateFormat = (dateString: string) => {
    // Create a Date object from the input string
    const date = new Date(dateString);
    
    // Get the year, month, and day from the date object
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Month is zero-based, so add 1
    const day = String(date.getDate()).padStart(2, '0');
    
    // Get the time in the desired format (18:30:00)
    const time = '18:30:00';
    
    // Construct the output string
    const outputString = `${year}-${month}-${day}T${time}.000Z`;
    
    return outputString;
  }