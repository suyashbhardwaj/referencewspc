export const customModalStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      /*width: "650px",*/

      /*width: '80%',*/
      /*height: '80%',
      margin: 'auto',
      overflow: 'hidden', // Prevents content from overflowing modal
      overflowY: 'auto', // Enables vertical scrolling*/
    },
  };