import React from "react"
import { useState, useEffect, useRef } from "react"
import SignatureCanvas from 'react-signature-canvas'
import axios from "axios";
import { useParams } from 'react-router-dom'
// import { SAVE_FORMSUBMITDATA } from "../graphql/mutations";
// import { useMutation } from "@apollo/client";
// import { useQuery } from '@apollo/client'
// import { GET_FORMRENDERJSON } from '../graphql/query';
// import { useNavigate } from "react-router-dom";
import allFormData from './allFormData.json'

interface CaptchaProps {
  onRefresh: (newCaptcha: string) => void
  inputTextName: any
  inputTextNameColor: any
}
interface RatingsProps {
  label: string
  color: string
  alignment: any
  index: any
}
interface SignatureDiv {
  element: any
  index: any
}
interface DateRange {
  index: number
}

const RenderForm = () => {
  // const navigate = useNavigate();
  const params = useParams();
  const formId = params.uuid;
  const [formElements, setFormElements] = useState<any>(allFormData[9].formData);
  const [formSubmissionData, setFormSubmissionData] = useState([]);
  const [rating, setRating] = useState(0)
  const [starClass, setStarClass] = useState('active')
  // const [saveFormSubmitData] = useMutation(SAVE_FORMSUBMITDATA)
  const RENDEREDFORM_FILE_UPLOAD_URL = import.meta.env.VITE_FILE_UPLOAD;
  const fileUrl = useRef<string | null>(null)
  const captchaPassFlagRef = useRef(false);
  const [captchaError, setCaptchaError] = useState("");
  const [formStyles, setFormStyles] = useState({cardStyle:"", cardBackground:""});
  const [loadingOl, setLoading] = useState(false);
  const dateRange = useRef({ startdate: "", enddate: "" });
  const signatureRef = useRef();
  const [canvasEnable, setCanvasEnable] = useState(true);
  // const {
  //   loading,
  //   error,
  //   data,
  // } = useQuery(GET_FORMRENDERJSON, {
  //   variables: {
  //     input: {
  //       formUuid: formId
  //     }
  //   },
  //   notifyOnNetworkStatusChange: true
  // })

  const loading = false;
  const error = false;

  // if (error) console.log(error.message);
  /* if (loading) return <h1>query response is being loaded</h1> */

  // useEffect(()=>{
  //   console.log("data now available as ", data);
  //   if (!!data) {
  //     setFormStyles({...formStyles, cardStyle:data.getFormBuilderDetailsByFormUuid.form.cardstyle, cardBackground: data.getFormBuilderDetailsByFormUuid.form.cardBackground})
  //     setFormElements(data.getFormBuilderDetailsByFormUuid.form.formData);
  //   }
  // },[data])

    const handleSubmit = (evt:any) => {
    evt.preventDefault(); 
    if(!captchaPassFlagRef.current) {setCaptchaError("wont proceed due to incorrect captcha input"); return;}
    setLoading(true)
    console.log("submitted the form with obj as "); 
    console.dir(formSubmissionData);
    // saveFormSubmitData({
    //   variables: {
    //     input: {
    //       formUuid: formId,
    //       formValue: formSubmissionData
    //     }
    //   }
    // })
    //   .then((res: any) => {
    //     console.log("response received against the form submission is "); console.dir(res);
    //     setLoading(true);
    //     /* if (res.data.saveFormDetailsByFormUuid.status === 201) { */
    //       navigate(`/botgoforms/formsubmitted/${formId}`)
    //     /* } else {
    //       navigate(`/error/${formId}`)
    //     } */
    //   })
    //   .catch((err) => {
    //     console.log("error raised against the form submission is "); console.dir(err);
    //     navigate(`/error/${formId}`)
    //   })
    }

    const Captcha: React.FC<CaptchaProps> = ({ onRefresh, inputTextName, inputTextNameColor }) => {
        const [userInput, setUserInput] = useState('');
        const [captchaText, setCaptchaText] = useState('');
        const canvasRef = useRef(null); 

        const generateRandomChar = (min: number, max: number) => String.fromCharCode(Math.floor(Math.random() * (max - min + 1) + min)); 
        
        const drawCaptchaOnCanvas = (ctx: any, captcha: string) => { 
          ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); 
          const textColors = ['rgb(0,0,0)', 'rgb(130,130,130)']; 
          const letterSpace = 150 / captcha.length; 
          for (let i = 0; i < captcha.length; i++) { 
            const xInitialSpace = 25; 
            ctx.font = '20px Roboto Mono'; 
            ctx.fillStyle = textColors[Math.floor( 
              Math.random() * 2)]; 
            ctx.fillText( 
              captcha[i], 
              xInitialSpace + i * letterSpace, 
              
              // Randomize Y position slightly 
              Math.floor(Math.random() * 16 + 25), 
              100 
            ); 
          } 
        }; 
        
        const initializeCaptcha = (ctx: any) => { 
          setUserInput(''); 
          const newCaptcha = generateCaptchaText(); 
          setCaptchaText(newCaptcha); 
          drawCaptchaOnCanvas(ctx, newCaptcha); 
        };

        const generateCaptchaText = () => { 
          let captcha = ''; 
          for (let i = 0; i < 3; i++) { 
            captcha += generateRandomChar(65, 90); 
            captcha += generateRandomChar(97, 122); 
            captcha += generateRandomChar(48, 57); 
          } 
          return captcha.split('').sort( 
            () => Math.random() - 0.5).join(''); 
        };
        
        const handleUserInputChange = (e:any) => { 
          captchaPassFlagRef.current = captchaText === e.target.value
          console.log(captchaPassFlagRef.current);
          setUserInput(e.target.value); 
        };

        const handleRefresh = () => {
          initializeCaptcha(canvasRef.current.getContext('2d'))
          /* captchacode = generateRandomCaptcha() */
          if (onRefresh) {
            onRefresh("YoYo")
          }
        }

        useEffect(() => {
          // Generate the initial captcha only once
          const canvas = canvasRef.current; 
          const ctx = canvas.getContext('2d'); 
          initializeCaptcha(ctx); 
        }, []) // Empty dependency array ensures this effect runs only once

        return (
          <>
            <>
              <div className='row captcha-content align-items-center mx-md-2'>
                <div className='col-md-9'>
                  <div className='captcha-box border rounded p-2 mr-3 mr-md-4 me-n5'>
                    {/* <span className='captcha-value'>{captcha}</span> */}
                    <canvas ref={canvasRef} 
                      width="300"
                      height="50"
                      background-color='grey'> 
          					</canvas> 
                  </div>
                </div>
                <div className='col-md-3'>
                  <button type='button' className='btn btn-refresh' onClick={handleRefresh}>
                    <i className='bi bi-arrow-clockwise' style={{ color: 'black', fontSize:'x-large' }}></i>
                  </button>
                </div>
              </div>
              <div className='row p-3'>
                <div className='col-md-12'>
                  <label
                    htmlFor='captchaInput'
                    className='form-label'
                    style={{ color: inputTextNameColor }}
                  >
                    {inputTextName}
                  </label>
                </div>
              </div>
              <div className='row'>
                <div className='col-md-12'>
                  <div>
                    <input type='text' id='captchaInput' className='form-control' value={userInput} onChange={handleUserInputChange}/>
                  </div>
                </div>
              </div>
            </>
            {/* </div> */}
          </>
        )
    }
    const handleCaptchaRefresh = (index: number, newCaptcha: string) => {
        // Update the Captcha value in the state
        const updatedFormElements = [...formElements]
        // Check if formInput exists and has options property
        if (
          updatedFormElements[index] &&
          updatedFormElements[index].formInput &&
          Array.isArray(updatedFormElements[index].formInput.options)
        ) {
          updatedFormElements[index].formInput.options[0].value = newCaptcha
          setFormElements(updatedFormElements)
        } else {
          // Handle the case where the expected structure is not present
          console.error('Unexpected structure in formElements')
        }
    }
    const handleSwitchToggle = (index: any) => {
      setFormElements((prevFormElements: any) => {
        const updatedFormElements = [...prevFormElements];
        updatedFormElements[index] = {...updatedFormElements[index], checked: !updatedFormElements[index].checked, value: !updatedFormElements[index].checked}
        return updatedFormElements;
      });
    }
    const handleCheckboxToggle = (index: any, innerIndex: any, selected: any) => {
      console.log(`index is ${index}, innerIndex is ${innerIndex}, selected is ${selected}`);
      setFormElements((prevFormElements: any) => {
        const updatedFormElements = [...prevFormElements];
        const updatedOptions = [...updatedFormElements[index].options];

        if (selected === undefined){ 
                updatedOptions[innerIndex] = { ...updatedOptions[innerIndex], selected: true }; }
        else {  updatedOptions[innerIndex] = { ...updatedOptions[innerIndex], selected: !selected };}

        let str = "";
        updatedOptions.forEach((value) => {
            if (!!value.selected) str += value.text + "; ";
        });

        updatedFormElements[index] = { ...updatedFormElements[index], options: updatedOptions, value: str,};

        return updatedFormElements;
        });
    }
    const DateRange: React.FC<DateRange> = ({index}) => {
      function handleChange(e:any) {
        const name = e.target.name;
        const value = e.target.value;
        dateRange.current = {...dateRange.current, [name]: value}
        console.log("start & end date as ", dateRange);
        if(dateRange.current.startdate || dateRange.current.enddate) textHandler(`${dateRange.current.startdate} to ${dateRange.current.enddate}`, index);
      }

      console.log(dateRange.current.startdate);
      console.log(dateRange.current.enddate);
      return (
        <div className='d-flex'>
            <label htmlFor="startdate">Start Date:</label>
              <input
                className='form-control form-control-lg form-control-solid'
                type="date"
                name="startdate"
                min={new Date().toISOString().split("T")[0]}
                onChange={handleChange}
                required
                value={dateRange.current.startdate && dateRange.current.startdate}
              />
              <label htmlFor="enddate">End Date:</label>
              <input
                className='form-control form-control-lg form-control-solid'
                type="date"
                name="enddate"
                /* disabled={dateRange.current.startdate === "" ? true: false} */
                min={dateRange.current.startdate ? new Date(dateRange.current.startdate).toISOString().split("T")[0]: ""}
                onChange={handleChange}
                required
                value={dateRange.current.enddate && dateRange.current.enddate}
              />
       </div>
      );
    }
    const handleRadioToggle = (groupIndex: any, optionIndex: any) => {
      setFormElements((prevFormElements: any) => {
        const updatedFormElements = [...prevFormElements];
        updatedFormElements[groupIndex] = {...updatedFormElements[groupIndex], value: updatedFormElements[groupIndex].options[optionIndex].text}
        return updatedFormElements;
      });
    }
    async function getFileURL(file:any, index:any) {
      try {
        let response = await axios.post(RENDEREDFORM_FILE_UPLOAD_URL as string, {files:[{fileName: file.name,mimeType: file.type}]});
        // Need to optimise this logic later to handle multiple file uploads

        const uploadURL = response.data[0].uploadURL;
        console.log({ response });
        
        // Send file to uploadURL after the url is created in s3
        await axios.put(uploadURL, file);
        textHandler(response.data[0].url, index);
      } catch (err) {
        console.log(err);
      }
    }
    const handleFileUpload = (index: any, event: any) => {
      console.log('action on element with index ', index)
      const file = event.target.files[0]
      setFormElements((prevFormElements: any) => {
        const updatedFormElements = [...prevFormElements];
        updatedFormElements[index] = {...updatedFormElements[index], uploadedfile: file}
        return updatedFormElements;
      });

      getFileURL(file, index);
      console.log('Uploaded file:', file)
    }
    const Ratings: React.FC<RatingsProps> = ({ label, color, alignment, index }) => {
        const rate = (stars: number) => {
          setRating(stars)
          textHandler(stars,index);
          resetStars()
        }
    
        const resetStars = () => {
          setStarClass('')
        }
    
        return (
          <>
            <div className='review-container'>
            <label className='form-label  fs-6  required' style={{ color: color }} >
              {label}                                                        
            </label>
              <div className='star-widget'>
                <div className='d-flex'>
                  <div style={{ display: 'inline-block', fontSize: '48px', textAlign: alignment }}>
                    {[1, 2, 3, 4, 5].map((index) => (
                      <div
                        key={index}
                        className={`star ${starClass}`}
                        onClick={() => rate(index)}
                        /* onMouseOver={() => highlightStars(index)}
                      onMouseOut={resetStars} */
                        style={{
                          display: 'inline-block',
                          margin: '0 4px',
                          cursor: 'pointer',
                          color: rating >= index ? '#ffdd00' : '#ccc'
                        }}
                      >
                        ★
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </>
        )
    }
    const SignatureDiv: React.FC<SignatureDiv> = ({ element, index }) => {
      const sigRef = React.useRef() as React.MutableRefObject<any>;
      const handleSignatureEnd = () => {
        signatureRef.current = sigRef.current.toDataURL();
        const canvas = sigRef.current.getCanvas();
        canvas.toBlob(async (blob: any) => {
          blob.name = 'signatureinput.png'
          window.URL.revokeObjectURL(fileUrl.current!)
          console.log("the converted to blog is ", blob);
          getFileURL(blob, index);
          console.log('Uploaded file:', blob)
          },'image/png',1 ) 
        
        setCanvasEnable(false);
      }

      const clearSignature = () => {
        setCanvasEnable(true);
        sigRef.current.clear();
      }
      
      return (
        <>
          <div style={{ 
            display: 'flex', 
            fontSize: '48px', 
            backgroundColor: element.padColor,
            justifyContent:"space-around",
            alignItems:"end"
            }}>
            <div>
            {!canvasEnable ? (
              <div>
                <img src={signatureRef.current} alt="sign image" width={element.width} height={element.height} className="sigCanvas"/>
              </div>
              ):(
              <SignatureCanvas
                penColor={'black'}
                backgroundColor={element.padColor}
                canvasProps={{
                  width: element.width,
                  height: element.height,
                  className: 'sigCanvas'
                }}
                ref={sigRef}
                onEnd={handleSignatureEnd}
                />
            )}
            </div>
            <div>
              <button type = 'button' className="btn btn-lg btn-link w-auto " onClick={clearSignature}>Clear</button>
            </div>
          </div>
        </>
      )
    }
    const textHandler = (value: any, index: number) => {
      /* console.log(`updating value ${value} at index ${index}`) */
      setFormElements((prevFormElements: any) => {
          const updatedFormElements = [...prevFormElements];
          // Update the 'value' field of the specified element at the given 'index'
          updatedFormElements[index] = { ...updatedFormElements[index], value: value };

          return updatedFormElements;
      });
    };

    useEffect(() => {
      if (formElements) {
        // Extracting form submission data for elements with a label
        const submissionData = formElements
        .filter((element: any) => element.label && element.label!=='Captcha') // Filter only elements with a label but not captcha
        .map((element: any) => ({
          label: element.label,
          value: element.type === 'switch' ? element.value || false : element.value || "",
        }));
  
        setFormSubmissionData(submissionData);
        }
    }, [formElements]);

    const conditionalBackgroundColor = formStyles.cardStyle === '' ? 'white' : formStyles.cardStyle

    return (
        <div className='d-sm-flex justify-content-center' style={{background:`url(${formStyles.cardBackground || ''})`,backgroundRepeat:"no-repeat" , backgroundSize:"contain", width:'100vw'}}>
                     <div className='rounded shadow-lg container-sm mx-0 mt-5 mb-4 rounded-4' style={{ overflow: 'hidden'}}>
                        {formElements.map((element: any, /* index: any */) =>
                            element.type === 'banner' && (
                            <div
                                className='banner'
                                style={{
                                    backgroundImage: `url(${element.bannerSrc || ''})`,
                                    backgroundSize: '100% 100%',
                                    /* backgroundRepeat: 'no-repeat', */
                                    height: '33vh',
                                    width: '100%',
                                    top: 0
                                }}
                            ></div>
                            )
                        )}
                            <div className='content-wrapper p-10 p-lg-15' style={{ backgroundColor: conditionalBackgroundColor}}>
                                <form className='form w-100 fv-plugins-bootstrap5 fv-plugins-framework p-5' noValidate id='kt_login_signup_form' onSubmit={handleSubmit}>
                                    {formElements.map((element: any, index: any) =>
                                        element.type !== 'button' && (
                                            <div className={`fv-row ${element.type === 'heading' || element.type === 'subheading' ? 'mb-5' : 'mb-3'} ${element.type === 'button' ? 'text-center' : ''}`} style={{ background : 'initial' }}>
                                                {element.type === 'captcha' && (
                                                <>
                                                    <div className='d-sm-flex align-items-center justify-content-around'>
                                                      <label className='form-label fw-bolder text-dark fs-6 required '>
                                                          {element.label}
                                                      </label>
                                                      <Captcha
                                                          onRefresh={(newCaptcha: any) =>
                                                          handleCaptchaRefresh(index, newCaptcha)
                                                          }
                                                          inputTextName={element.inputTextName}
                                                          inputTextNameColor={element.inputTextNameColor}
                                                      />
                                                    </div>
                                                    <div><h6 className="mt-4 text-center text-danger">{captchaError}</h6></div>
                                                </>
                                                )}
                                                {element.type === 'heading' && (
                                                <element.size
                                                    className={`mb-2`}
                                                    style={{
                                                    color: element.color,
                                                    textAlign: element.alignment,
                                                    }}
                                                >
                                                    {element.formHeading}
                                                </element.size>
                                                )}
                                                {element.type === 'subheading' && (
                                                <p
                                                    className={`fs-5`}
                                                    style={{
                                                    color: element.color,
                                                    textAlign: element.alignment
                                                    }}
                                                >
                                                    {loading? 'your form is being loaded..': (error? error.message: element.formSubHeading)}
                                                </p>
                                                )}
                                                {element.type === 'text' && (
                                                <>
                                                    <label
                                                    className='form-label  fs-6 required'
                                                    style={{ color: element.color }}
                                                    >
                                                    {element.label}
                                                    </label>
                                                    <input
                                                    placeholder={element.placeholder}
                                                    type='text'
                                                    autoComplete='off'
                                                    className='form-control form-control-lg form-control-solid'
                                                    id = {index}
                                                    name = {index}
                                                    value={element.value}
                                                    onChange={(e) => textHandler(e.target.value, index)}
                                                    />
                                                </>
                                                )}
                                                {element.type === 'select' && (
                                                <>
                                                    <label
                                                    className='form-label  fs-6 required'
                                                    style={{ color: element.color }}
                                                    >
                                                    {element.label}
                                                    </label>
                                                    <div className='position-relative mb-3'>
                                                    <select
                                                        className='form-select form-select-solid form-select'
                                                        id = {index}
                                                        name = {index}
                                                        value={element.value}
                                                        onChange={(e)=>textHandler(e.target.value,index)}
                                                    >
                                                        {element.options.map(
                                                        (option: any, optionIndex: any) => (
                                                            <option
                                                            key={optionIndex}
                                                            value={option.text}
                                                            selected={option.selected}
                                                            >
                                                            {option.text}
                                                            </option>
                                                        )
                                                        )}
                                                    </select>
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'formLogo' && (
                                                <>
                                                    <div
                                                    className={`d-flex justify-content-${
                                                        element.alignment === 'right'
                                                        ? 'end'
                                                        : element.alignment
                                                    }`}
                                                    >
                                                    <img src={element.imgSrc} alt='logo' />
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'switch' && (
                                                <>
                                                    <label
                                                    className='form-label  fs-6  required'
                                                    style={{ color: element.color }}
                                                    >
                                                    {element.label}
                                                    </label>
                                                    <div
                                                    key={element.id || index}
                                                    className={`fv-row mb-7 ${
                                                        element.type === 'button' ? 'text-center' : ''
                                                    }`}
                                                    >
                                                    <div className='form-check form-switch'>
                                                        <input
                                                        className='form-check-input'
                                                        type='checkbox'
                                                        /* id = {index} */
                                                        name = {index}
                                                        value = {element.checked}
                                                        onChange={() => handleSwitchToggle(index)}
                                                        checked={element.checked}
                                                        />
                                                    </div>
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'checkbox' && (
                                                <>
                                                    <div key={element.id || index} className='fv-row mb-7'>
                                                    <label
                                                        className={`form-label fs-6 required`}
                                                        style={{ color: element.color }}
                                                    >
                                                        {element.label}
                                                    </label>
                                                    {element.options.map((option: any, optionIndex: any) => (
                                                        <div
                                                        key={`${element.type}-${element.id}-${optionIndex}`}
                                                        className='form-check'
                                                        >
                                                        <input
                                                            type='checkbox'
                                                            className='form-check-input'
                                                            /* id = {index} */
                                                            name = {index}
                                                            value={option.text}
                                                            onChange={ ()=> handleCheckboxToggle(index,optionIndex,option.selected) }
                                                            checked={option.select}
                                                        />
                                                        <label
                                                            className='form-check-label m-1'
                                                            htmlFor={`${element.name}-${option.value}`}
                                                            style={{ color: element.color }}
                                                        >
                                                            {option.text}
                                                        </label>
                                                        </div>
                                                    ))}
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'radio' && (
                                                <>
                                                    <div key={element.id || index} className='fv-row mb-7'>
                                                    <label
                                                        className={`form-label fs-6 required`}
                                                        style={{ color: element.color }}
                                                    >
                                                        {element.label}
                                                    </label>
                                                    {element.options.map((option: any, optionIndex: any) => (
                                                        <div
                                                        key={`${element.type}-${element.id}-${optionIndex}`}
                                                        className='form-check'
                                                        >
                                                        <input
                                                            type='radio'
                                                            className='form-check-input'
                                                            /* id = {index} */
                                                            name = {index}
                                                            value={option.value}
                                                            checked={option.selected}
                                                            onChange={() => 
                                                              handleRadioToggle(index, optionIndex)
                                                            }
                                                        />
                                                        <label
                                                            className='form-check-label m-1'
                                                            htmlFor={`${element.name}-${option.value}`}
                                                        >
                                                            {option.text}
                                                        </label>
                                                        </div>
                                                    ))}
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'file' && (
                                                <>
                                                    <div key={element.id || index} className='fv-row mb-7'>
                                                    <label
                                                        className={`form-label fs-6 required`}
                                                        style={{ color: element.color }}
                                                    >
                                                        {element.label}
                                                    </label>
                                                    <div className='mb-3'>
                                                        <input
                                                        type='file'
                                                        accept={element.accept}
                                                        className='form-control'
                                                        multiple={element.isMultiple}
                                                        id = {index}
                                                        name = {index}
                                                        onChange={(e) => handleFileUpload(index, e)}
                                                        /* value={} */
                                                        />
                                                    </div>
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'divider' && (
                                                <hr
                                                    className='my-4'
                                                    style={{
                                                    borderColor: element.color || '#000',
                                                    borderWidth: element.thickness + 'px',
                                                    borderStyle: 'solid'
                                                    }}
                                                />
                                                )}
                                                {element.type === 'textarea' && (
                                                <>
                                                    <label
                                                    className={`form-label fs-6 required`}
                                                    style={{ color: element.color }}
                                                    >
                                                    {element.label}
                                                    </label>
                                                    <textarea
                                                      placeholder={element.placeholder}
                                                      rows={element.rows || 4} // Specify the number of rows
                                                      className='form-control form-control-lg form-control-solid'
                                                      id = {index}
                                                      name = {index}
                                                      value={element.value}
                                                      onChange={(e)=>textHandler(e.target.value,index)}
                                                    />
                                                </>
                                                )}
                                                {element.type === 'datetime' && (
                                                <>
                                                    <label
                                                    className='form-label fs-6 required'
                                                    style={{ color: element.color }}
                                                    >
                                                    {element.label}
                                                    </label>
                                                    <div
                                                    key={element.id || index}
                                                    className={`fv-row mb-7 ${
                                                        element.type === 'button' ? 'text-center' : ''
                                                    }`}
                                                    >
                                                    <div>
                                                        <input
                                                        className='form-control form-control-lg form-control-solid'
                                                        type='datetime-local'
                                                        id = {index}
                                                        name = {index}
                                                        value={element.value}
                                                        onChange={(e)=>textHandler(e.target.value,index)}
                                                        />
                                                    </div>
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'date' && (
                                                <>
                                                    <label
                                                    className='form-label fs-6 required'
                                                    style={{ color: element.color }}
                                                    >
                                                    {element.label}
                                                    </label>
                                                    <div
                                                    key={element.id || index}
                                                    className={`fv-row mb-7 ${
                                                        element.type === 'button' ? 'text-center' : ''
                                                    }`}
                                                    >
                                                    <div>
                                                        <input
                                                        className='form-control form-control-lg form-control-solid'
                                                        type='date'
                                                        id = {index}
                                                        name = {index}
                                                        value={element.value}
                                                        onChange={(e)=>textHandler(e.target.value,index)}
                                                        />
                                                    </div>
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'time' && (
                                                <>
                                                    <label
                                                    className='form-label fs-6 required'
                                                    style={{ color: element.color }}
                                                    >
                                                    {element.label}
                                                    </label>
                                                    <div
                                                    key={element.id || index}
                                                    className={`fv-row mb-7 ${
                                                        element.type === 'button' ? 'text-center' : ''
                                                    }`}
                                                    >
                                                    <div>
                                                        <input
                                                        className='form-control form-control-lg form-control-solid'
                                                        type='time'
                                                        id = {index}
                                                        name = {index}
                                                        value={element.value}
                                                        onChange={(e)=>textHandler(e.target.value,index)}
                                                        />
                                                    </div>
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'daterange' && (
                                                <>
                                                    <label
                                                    className='form-label fs-6 required'
                                                    style={{ color: element.color }}
                                                    >
                                                    {element.label}
                                                    </label>
                                                    <div
                                                    key={element.id || index}
                                                    className={`fv-row mb-7 ${
                                                        element.type === 'button' ? 'text-center' : ''
                                                    }`}
                                                    >
                                                    <DateRange index = {index}/>
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'ratings' && (
                                                <>
                                                    <div>
                                                    <Ratings
                                                        label={element.label}
                                                        color={element.color}
                                                        alignment={element.alignment}
                                                        index={index}
                                                    />
                                                    </div>
                                                </>
                                                )}
                                                {element.type === 'signature' && (
                                                <>
                                                    <div className='d-grid'>
                                                      <label
                                                        className='form-label  fs-6  required'
                                                        style={{ color: element.color }}
                                                      >
                                                        {element.label}
                                                      </label>
                                                      <SignatureDiv 
                                                        element = {element}
                                                        index={index}
                                                      />
                                                    </div>
                                                </>
                                                )}

                                            </div>
                                            )
                                        )
                                    }
                                    {formElements.map((element: any, index: any) =>
                                        element.type === 'button' && (
                                        // Render the button separately
                                        <div key={element.id || index} className='text-center mt-4'>
                                            <button style={{ color: element.btnTxtColor, backgroundColor: element.btnBgColor }}
                                                type={element.buttonType === "button"?"submit":element.buttonType}
                                                id={element.id}
                                                disabled= {loadingOl}
                                                className={`btn btn-lg ${element.buttonType === 'submit' && 'btn-primary' } w-100 `}
                                                onClick={()=> {if(element.buttonType === "reset" && element.text === "click to retry") window.location.reload();}}
                                                >
                                                  {!loadingOl && <span className='indicator-label'>{element.text}</span>}
                                                  {loadingOl && (
                                                    <span className='indicator-progress' style={{ display: 'block' }}>
                                                      Please wait...
                                                      <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                                                    </span>
                                                  )}
                                            </button>
                                        </div>
                                        )
                                    )}
                                </form>  
                            </div>
                        </div>
                    
        </div>
    )
}

export default RenderForm