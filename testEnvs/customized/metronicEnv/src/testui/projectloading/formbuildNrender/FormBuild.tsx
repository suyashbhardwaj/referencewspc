import { useState } from 'react';
import FormFields from './FormFields';
import FormDesign from './FormDesign';
import FormPage from './FormPage';
import FormFieldCustomization from './FormFieldCustomization';
import ReactModal from 'react-modal';
import SaveFormBuilder from './SaveFormBuilder';

const FormBuild = () => {
	const [activeTabIndex, setActiveTabIndex] = useState(0)
  	const tabTitles = ['Form Fields', 'Form Themes']
  	const [formElements, setFormElements] = useState<any>([])
	const [selectedField, setSelectedFields] = useState<any>(0)
	const [ThemeTextColor, setThemeTextColor] = useState('#000000')
	const [formJson, setFormJson] = useState<any>()
	const [togglerFieldIndex, setTogglerFieldIndex] = useState(-1)
  	const [subFormFieldIndexes, setSubFormFieldIndexes] = useState([])
  	const [showSaveButton, setShowSaveButton] = useState(false)
  	const [isSaveModalOpen, setIsSaveModalOpen] = useState<boolean>(false)

	const uuid="";
	const handleBack = () => {
	    /*navigate('/automation/process-automation/form-builder')*/
	    console.log("handleback clicked");
	  }
	const customModalStyles = {
	    content: {
	      top: '50%',
	      left: '50%',
	      right: 'auto',
	      bottom: 'auto',
	      marginRight: '-50%',
	      transform: 'translate(-30%, -60%)',
	      width: '350px'
	    }
	}
	const afterOpenSaveModal = () => {}
	const closeSaveModal = () => { setIsSaveModalOpen(false) }
	const openSaveModal = () => { setIsSaveModalOpen(true) }
	const formBuilderData: any[] = /*data?.getAllFormBuilderByBgAccountId?.data[0].formData ||*/ []

	return (
		<>
			<section className='row p-4 scroll-y'>
		        
		        <div className='col-lg-2 col-auto d-flex flex-column card rounded border border-secondary scroll-y h-lg-800px' style={{ position: 'sticky', top: '0' }}>
		          <ul className='nav nav-stretch nav-line-tabs border-transparent d-flex justify-content-center flex-nowrap mb-5'>
		          	{tabTitles.map((title, index) => (
	              	<li className='nav-item' key={Math.random()}>
		                <span
		                  className={`nav-link cursor-pointer ${
		                    activeTabIndex === index ? 'active fw-bolder' : ''
		                  }`}
		                  onClick={() => {
		                    setActiveTabIndex(index)
		                  }}
		                  role='tab'
		                >
		                  {title}
		                </span>
		            </li>))
		      		}
		          </ul>
		          {activeTabIndex === 0 && (
		            <FormFields
		              formElements={formElements}
		              setFormElements={setFormElements}
		              selectedField={selectedField}
		              ThemeTextColor={ThemeTextColor}
		            />
		          )}
		          <div className='scroll-y'>
		            {activeTabIndex === 1 && (
		              <FormDesign
		                formElements={formElements}
		                setFormElements={setFormElements}
		                setFormJson={setFormJson}
		                formJson={formJson}
		                setThemeTextColor={setThemeTextColor}
		              />
		            )}
		          </div>
		        </div>

		        <div className='col-auto my-lg-0 my-4 border border-warning mx-3 rounded p-0 scroll-y h-lg-800px position-relative 'style={{ backgroundImage: `url(${formJson?.cardBackground || ''})`}}>
		          <div className='d-flex flex-column'>
		            <span role='button' className='mt-4 ms-4 text-hover-primary' onClick={handleBack}>
		              <i className='bi bi-arrow-left text-black' style={{ fontSize: '30px' }}></i>
		            </span>
		            <div
		              className='position-absolute end-0 p-3 m-3 d-flex align-items-end'
		              style={{ zIndex: 0 }}
		            >
		              <span className='me-8' role='button'>
		                <i className='bi bi-download text-black' style={{ fontSize: '2rem' }}></i>
		              </span>
		              <a href={`/automation/process-automation/form-builder/share-form/${uuid}`}>
		                <span role='button'><i className='bi bi-share-fill text-black' style={{ fontSize: '30px' }}></i></span>
		              </a>
		            </div>
		          </div>
		          {/* Form Component */}
		          <FormPage
		            formElements={formElements}
		            setFormElements={setFormElements}
		            setSelectedFields={setSelectedFields}
		            selectedField={selectedField}
		            formJson={formJson}
		            togglerFieldIndex={togglerFieldIndex}
		            setTogglerFieldIndex={setTogglerFieldIndex}
		            subFormFieldIndexes={subFormFieldIndexes}
		            setSubFormFieldIndexes={setSubFormFieldIndexes}
		          />
		        </div>

		        <div className='col-lg-3 col-lg-auto d-flex flex-column card rounded border border-secondary scroll-y h-lg-800px'>
		          {showSaveButton && (
		            <>
		              <button className='btn btn-primary w-100 mt-5' onClick={openSaveModal}>
		                Save
		              </button>
		              <hr />
		            </>
		          )}
		          <FormFieldCustomization
		            formElements={formElements}
		            setFormElements={setFormElements}
		            selectedField={selectedField}
		            setSelectedFields={setSelectedFields}
		            formBuilderData={formBuilderData}
		            showSaveButton={showSaveButton}
		            togglerFieldIndex={togglerFieldIndex}
		            setTogglerFieldIndex={setTogglerFieldIndex}
		            subFormFieldIndexes={subFormFieldIndexes}
		            setSubFormFieldIndexes={setSubFormFieldIndexes}
		          />
		        </div>
		    </section>
		    <ReactModal
		        isOpen={isSaveModalOpen}
		        onAfterOpen={afterOpenSaveModal}
		        onRequestClose={closeSaveModal}
		        style={customModalStyles}
		        contentLabel='Save'
		      >
		        <SaveFormBuilder
		          closeSaveModal={closeSaveModal}
		          formElements={formElements}
		          formJson={formJson}
		          setFormJson={setFormJson}
		        />
		    </ReactModal>
		</>
	)
}

export default FormBuild