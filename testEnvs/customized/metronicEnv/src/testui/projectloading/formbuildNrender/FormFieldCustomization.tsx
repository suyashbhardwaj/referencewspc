import FieldConditionalCustomization from "./FieldConditionalCustomization"
import SubFormSetup from "./SubFormSetup"

interface IProps {
  formElements: any
  setFormElements: any
  selectedField: any
  setSelectedFields: any
  formBuilderData: any
  showSaveButton: any
  togglerFieldIndex: any
  setTogglerFieldIndex: any
  subFormFieldIndexes: any
  setSubFormFieldIndexes: any
}

const FormFieldCustomization: React.FC<IProps> = ({formElements, selectedField, setFormElements, setSelectedFields, setTogglerFieldIndex, togglerFieldIndex, setSubFormFieldIndexes}) => {
	let selectedElement = formElements[selectedField]
	const handleDuplicate = () => {
	    let lastLeftFieldId = Math.max(...formElements.map((field: any) => field.fieldId))
	    const duplicatedElement = { ...selectedElement, fieldId: lastLeftFieldId + 1 }

	    if (selectedElement.type !== 'banner') {
	      const updatedFormElements = [...formElements]
	      const selectedIndex = formElements.findIndex((el: any) => el.id === selectedElement.id)

	      if (selectedIndex !== -1) {
	        const newIndex = selectedField + 1
	        updatedFormElements.splice(newIndex, 0, duplicatedElement)
	        setFormElements(updatedFormElements)
	        console.log('Duplicate element created successfully!')
	      }
	    } else {
	      console.log("You can't create a duplicate banner.")
	    }
	}

	const handleDelete = () => {
    if (selectedField !== null && formElements.length > 0) {
      const updatedFormElements = formElements.filter((element: any, index: any) => index !== selectedField)

      // Adjust selectedField when deleting the first element
      const updatedSelectedField = Math.min(selectedField, updatedFormElements.length - 1)

      setFormElements(updatedFormElements)
      setSelectedFields(updatedSelectedField)
      console.log('Form field deleted successfully!')
    }
  	}

  	const getMaxFromRule = (rule: any) => {
	    // Check if rule is defined before using match
	    if (rule) {
	      const maxMatch = rule.match(/max-(\d+)/)
	      return maxMatch ? maxMatch[1] : ''
	    }
	    return ''
	}
  	
  	const getMinFromRule = (rule: any) => {
	    console.log(rule)
	    if (rule) {
	      console.log(rule)
	      const minMatch = rule.match(/min-(\d+)/)
	      console.log(minMatch)
	      return minMatch ? minMatch[1] : ''
	    }
	    return ''
	}

	const getMaxSizeFromRule = (rule: any) => {
	    if (rule) {
	      console.log(rule)
	      const maxFileMatch = rule.match(/maxFileSize-(\d+)/)
	      console.log(maxFileMatch)
	      return maxFileMatch ? maxFileMatch[1] : ''
	    }
	    return ''
	}

  	// Function to handle Max File Upload change
  	const handleMaxUploadChange = (max: any) => {
	    setFormElements((prev: any) => {
	      const updated = [...prev]

	      const validationRule = updated[selectedField].validation?.[0]
	      console.log(getMaxSizeFromRule(validationRule.rule))
	      console.log(validationRule.rule)
	      // Construct rule string based on max value, checking if max is empty
	      validationRule.rule =
	        max !== ''
	          ? `required|max-${max}|maxFileSize-${getMaxSizeFromRule(validationRule.rule)}`
	          : `required|max-''|maxFileSize-${getMaxSizeFromRule(validationRule.rule)}`
	      return updated
	    })
	}

	// Function to handle Max File Size change
	const handleMaxSizeChange = (size: any) => {
	    setFormElements((prev: any) => {
	      const updated = [...prev]
	      const selectedFieldObject = updated[selectedField]

	      if (!Object.isExtensible(selectedFieldObject)) {
	        // If the object is not extensible, create a new object that extends the existing one
	        updated[selectedField] = {
	          ...selectedFieldObject
	        }
	      }

	      const validationRule = updated[selectedField].validation?.[0]

	      if (validationRule) {
	        // Construct rule string based on size value, checking if size is empty
	        const newValidationRule = {
	          ...validationRule,
	          rule:
	            size !== ''
	              ? `required|max-${getMaxFromRule(validationRule.rule)}|maxFileSize-${size}`
	              : `required|max-${getMaxFromRule(validationRule.rule)}|maxFileSize-''`
	        }

	        // Update the validation array with the new validation rule
	        updated[selectedField].validation = [newValidationRule]
	      }

	      return updated
	    })
	}

	return (
		<>
			<div className='mt-5'>
        		{selectedElement ? (<>
        			<h4 className='text-center'> {selectedElement.type.toUpperCase()} CONTENT</h4>
		            <hr />
		            
		            {/* THE DUPLICATE & DELETE SECTION */}
		            <div>
		              <button className='btn btn-primary w-100 m-1' onClick={handleDuplicate}>
		                Duplicate
		              </button>
		              <button className='btn btn-danger w-100 m-1' onClick={handleDelete}>
		                Delete
		              </button>
		            </div>
		            
		            {/* THE OPTIONS SECTION */}
		            <div className='mt-5'>
		            	<h5>OPTIONS:</h5>

		            	{/* setter & getter for label */}
			            {selectedElement.type === 'text' && (
			                <div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Label:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.label}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const updatedElement = { ...updated[selectedField] }
			                          updatedElement.label = e.target.value
			                          updated[selectedField] = updatedElement
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Placeholder:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.placeholder}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const updatedElement = { ...updated[selectedField] }
			                          updatedElement.placeholder = e.target.value
			                          updated[selectedField] = updatedElement
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  {/* Exclude color for text type */}
			                  <div className='mb-3'>
			                    <label className='form-label'>Color:</label>
			                    <input
			                      type='color'
			                      className='form-control form-control-color'
			                      value={selectedElement.color}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element
			                          updatedElement.color = e.target.value
			                          updated[selectedField] = updatedElement
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mt-5'>
			                    <div className='d-flex justify-content-between'>
			                      <h5>VALIDATION</h5>
			                      <div className='mb-5'>
			                        <div className='form-check form-switch'>
			                          <input
			                            className='form-check-input'
			                            type='checkbox'
			                            id='toggleSwitch'
			                            checked={
			                              selectedElement.validation &&
			                              selectedElement.validation[0]?.rule.includes('required')
			                            }
			                            onChange={() => {
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                console.log(updated)
			                                const selectedFieldObject = updated[selectedField]

			                                // Check if the object is not extensible, create a new object that extends the existing one
			                                if (!Object.isExtensible(selectedFieldObject)) {
			                                  updated[selectedField] = {
			                                    ...selectedFieldObject,
			                                    validation: [
			                                      {
			                                        rule: 'undefined', // Set your default rule here
			                                        errorMessage: null
			                                      }
			                                    ]
			                                  }
			                                }

			                                const validationRule = formElements[selectedField].validation?.[0]
			                                console.log(validationRule)

			                                if (!validationRule) {
			                                  // Validation field is not present, add default validation
			                                  updated[selectedField].validation = [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                } else {
			                                  console.log(updated[selectedField])
			                                  // Validation field is present, toggle the rule
			                                  const isRuleUndefined = validationRule.rule.includes('undefined')
			                                  console.log(isRuleUndefined)

			                                  // Create a new object with updated values
			                                  const updatedValidationRule = {
			                                    rule: isRuleUndefined ? 'required' : 'undefined',
			                                    errorMessage: isRuleUndefined ? 'null' : null
			                                  }

			                                  // Update the validation array with the new object
			                                  updated[selectedField].validation = [updatedValidationRule]
			                                }

			                                // Log the updated state for debugging
			                                console.log('Updated state:', updated)

			                                return updated
			                              })
			                            }}
			                          />
			                          <label className='form-check-label' htmlFor='toggleSwitch'>
			                            {selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                              ? 'Required'
			                              : ' Undefined'}
			                          </label>
			                        </div>
			                      </div>
			                    </div>
			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Validation Message:</label>
			                      <input
			                        type='text'
			                        className='form-control'
			                        value={selectedElement.validation?.[0]?.errorMessage || ''}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]
			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }

			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              // Create a new object instead of modifying the existing one
			                              const newValidationRule = {
			                                ...validationRule,
			                                errorMessage: e.target.value
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }

			                            return updated
			                          })
			                        }
			                      />
			                    </div>

			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Max:</label>
			                      <input
			                        type='number'
			                        className='form-control'
			                        value={getMaxFromRule(selectedElement.validation?.[0]?.rule)}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]
			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }

			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              const max = e.target.value
			                              const maxValue = getMaxFromRule(validationRule.rule)
			                              const minValue = getMinFromRule(validationRule.rule)

			                              // Create a new object instead of modifying the existing one
			                              const newValidationRule = {
			                                ...validationRule,
			                                rule:
			                                  max !== ''
			                                    ? `required|max-${max}|min-${minValue}`
			                                    : `required|max-''|min-${minValue}`
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }

			                            return updated
			                          })
			                        }
			                      />
			                    </div>

			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Min:</label>
			                      <input
			                        type='number'
			                        className='form-control'
			                        value={getMinFromRule(selectedElement.validation?.[0]?.rule)}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]

			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }
			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              const min = e.target.value
			                              const maxValue = getMaxFromRule(validationRule.rule)
			                              const minValue = getMinFromRule(validationRule.rule)

			                              const newValidationRule = {
			                                ...validationRule,
			                                rule:
			                                  min !== ''
			                                    ? `required|max-${maxValue}|min-${min}`
			                                    : `required|max-${maxValue}|min-''`
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }
			                            return updated
			                          })
			                        }
			                      />
			                    </div>
			                  </div>
			                </div>
			            )}

						{/* setters & getters for label/srcUrl, color, size, alignment */}
			            {[
			                'heading',
			                'subheading',
			                'formLogo',
			                'select',
			                'switch',
			                'radio',
			                'checkbox',
			                'file',
			                'datetime',
			                'date',
			                'time',
			                'daterange',
			                'banner',
			                'captcha',
			                'ratings',
			                'subform'
			              ].includes(selectedElement.type) && (
			                <div>
			                  {/* label/srcUrl: v(pick values) from formHeading, formSubHeading, imgSrc, label & setting up of their respective ^(set) */}
			                  <div className='mb-3'>
			                    <label className='form-label'>
			                      {' '}
			                      {selectedElement.type === 'banner' || selectedElement.type === 'formLogo'
			                        ? 'SrcUrl:'
			                        : 'Label:'}
			                    </label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={
			                        selectedElement.type === 'heading'
			                          ? selectedElement.formHeading
			                          : selectedElement.type === 'subheading'
			                          ? selectedElement.formSubHeading
			                          : selectedElement.type === 'formLogo'
			                          ? selectedElement.imgSrc
			                          : selectedElement.type === 'select'
			                          ? selectedElement.label
			                          : selectedElement.type === 'switch'
			                          ? selectedElement.label
			                          : selectedElement.type === 'radio'
			                          ? selectedElement.label
			                          : selectedElement.type === 'checkbox'
			                          ? selectedElement.label
			                          : selectedElement.type === 'file'
			                          ? selectedElement.label
			                          : selectedElement.type === 'datetime'
			                          ? selectedElement.label
			                          : selectedElement.type === 'date'
			                          ? selectedElement.label
			                          : selectedElement.type === 'time'
			                          ? selectedElement.label
			                          : selectedElement.type === 'daterange'
			                          ? selectedElement.label
			                          : selectedElement.type === 'banner'
			                          ? selectedElement.bannerSrc
			                          : selectedElement.type === 'captcha'
			                          ? selectedElement.label
			                          : selectedElement.type === 'ratings'
			                          ? selectedElement.label
			                          : selectedElement.type === 'subform'
			                          ? selectedElement.label
			                          : selectedElement.type
			                      }
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                          if (selectedElement.type === 'heading') {
			                            updatedElement.formHeading = e.target.value
			                          } else if (selectedElement.type === 'subheading') {
			                            updatedElement.formSubHeading = e.target.value
			                          } else if (
			                            selectedElement.type === 'formLogo' ||
			                            selectedElement.type === 'select' ||
			                            selectedElement.type === 'switch' ||
			                            selectedElement.type === 'radio' ||
			                            selectedElement.type === 'checkbox' ||
			                            selectedElement.type === 'file' ||
			                            selectedElement.type === 'datetime' ||
			                            selectedElement.type === 'date' ||
			                            selectedElement.type === 'time' ||
			                            selectedElement.type === 'daterange' ||
			                            selectedElement.type === 'banner' ||
			                            selectedElement.type === 'captcha' ||
			                            selectedElement.type === 'subform' ||
			                            selectedElement.type === 'ratings'
			                          ) {
			                            updatedElement.label = e.target.value

			                            // Add the following conditions to update bannerSrc for the 'banner' type
			                            if (selectedElement.type === 'banner') {
			                              updatedElement.bannerSrc = e.target.value
			                            } else if (selectedElement.type === 'formLogo') {
			                              updatedElement.imgSrc = e.target.value
			                            }
			                          }

			                          updated[selectedField] = updatedElement
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  {/* color: v(pick) & ^(set) values */}
			                  {selectedElement.type !== 'formLogo' &&
			                    selectedElement.type !== 'banner' && ( // Exclude color for formLogo
			                      <div className='mb-3'>
			                        <label className='form-label'>Color:</label>
			                        <input
			                          type='color'
			                          className='form-control form-control-color'
			                          value={selectedElement.color}
			                          onChange={(e) =>
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                              updatedElement.color = e.target.value

			                              updated[selectedField] = updatedElement
			                              return updated
			                            })
			                          }
			                        />
			                      </div>
			                    )}

			                  {/* size, alignment: v(pick) & ^(set) values */}
			                  {(selectedElement.type === 'heading' ||
			                    selectedElement.type === 'subheading' ||
			                    selectedElement.type === 'formLogo' ||
			                    selectedElement.type === 'ratings') && (
			                    <>
			                      {selectedElement.type === 'heading' && (
			                        <div className='mb-3'>
			                          <label className='form-label'>Size:</label>
			                          <select
			                            className='form-select'
			                            value={selectedElement.size}
			                            onChange={(e) =>
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                                updatedElement.size = e.target.value

			                                updated[selectedField] = updatedElement
			                                return updated
			                              })
			                            }
			                          >
			                            <option value='h1'>H1</option>
			                            <option value='h2'>H2</option>
			                            <option value='h3'>H3</option>
			                            <option value='h4'>H4</option>
			                            <option value='h5'>H5</option>
			                            <option value='h6'>H6</option>
			                          </select>
			                        </div>
			                      )}

			                      <div className='mb-3'>
			                        <label className='form-label'>Alignment:</label>
			                        <select
			                          className='form-select'
			                          value={selectedElement.alignment}
			                          onChange={(e) =>
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                              updatedElement.alignment = e.target.value

			                              updated[selectedField] = updatedElement
			                              return updated
			                            })
			                          }
			                        >
			                          <option value='left'>Left</option>
			                          <option value='center'>Center</option>
			                          <option value='right'>Right</option>
			                        </select>
			                      </div>
			                    </>
			                  )}
			                </div>
			              )}

			              {/* setter & getter for condition on the respective field */}
			              {[
			                'text',
			                'heading',
			                'subheading',
			                'formLogo',
			                'select',
			                'switch',
			                'radio',
			                'checkbox',
			                'file',
			                'datetime',
			                'date',
			                'time',
			                'daterange',
			                'banner',
			                'captcha',
			                'ratings',
			                'divider',
			                'textarea',
			                'signature',
			                'button'
			              ].includes(selectedElement.type) && (
			                <div className='bg-success p-2 text-dark bg-opacity-50'>
			                  <div className='d-flex justify-content-between'>
			                    <h5>Render Conditionally</h5>
			                    <div className='mb-5'>
			                      <div className='form-check form-switch'>
			                        <input
			                          className='form-check-input'
			                          type='checkbox'
			                          id='toggleSwitch'
			                          checked={
			                            selectedElement.includeOnCondition &&
			                            selectedElement.includeOnCondition?.conditionActive
			                          }
			                          onChange={() => {
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              const selectedFieldObject = updated[selectedField]

			                              // Check if the object is not extensible, create a new object that extends the existing one
			                              if (!Object.isExtensible(selectedFieldObject)) {
			                                updated[selectedField] = {
			                                  ...selectedFieldObject
			                                }
			                              }

			                              /* set up the sub-json part now because conditional field customization is being set */
			                              if (updated[selectedField].includeOnCondition) {
			                                const isConditionalInclusionSet =
			                                  updated[selectedField].includeOnCondition.conditionActive
			                                updated[selectedField].includeOnCondition = {
			                                  ...updated[selectedField].includeOnCondition,
			                                  conditionActive: isConditionalInclusionSet ? false : true
			                                }
			                              }
			                              return updated
			                            })
			                            !formElements[selectedField].includeOnCondition.conditionActive &&
			                              setTogglerFieldIndex(-1)
			                          }}
			                        />
			                        <label className='form-check-label' htmlFor='toggleSwitch'>
			                          {selectedElement.includeOnCondition &&
			                          selectedElement.includeOnCondition?.conditionActive
			                            ? 'Enabled'
			                            : ' Disabled'}
			                        </label>
			                      </div>
			                    </div>
			                  </div>

			                  <div
			                    className={`mb-3 ${
			                      selectedElement.includeOnCondition.conditionActive ? '' : 'd-none'
			                    }`}
			                  >
			                    <FieldConditionalCustomization
			                      formElements={formElements}
			                      setFormElements={setFormElements}
			                      selectedElement={selectedElement}
			                      selectedField={selectedField}
			                      togglerFieldIndex={togglerFieldIndex}
			                      setTogglerFieldIndex={setTogglerFieldIndex}
			                    />
			                  </div>
			                </div>
			              )}

			              {/* setter & getter for fields included in the respective subform */}
			              {selectedElement.type === 'subform' && (
			                <>
			                  <div className='bg-secondary p-2 text-dark bg-opacity-50'>
			                    <div className='mb-3'>
			                      <SubFormSetup
			                        formElements={formElements}
			                        setFormElements={setFormElements}
			                        selectedElement={selectedElement}
			                        selectedField={selectedField}
			                        setTogglerFieldIndex={setTogglerFieldIndex}
			                        setSubFormFieldIndexes={setSubFormFieldIndexes}
			                      />
			                    </div>
			                  </div>
			                </>
			              )}

			              {selectedElement.type === 'select' && (
			                <>
			                  <div>
			                    {selectedElement.options.map((option: any, optionIndex: any) => (
			                      <div key={optionIndex} className='mb-3'>
			                        <label className='form-label'>Option {optionIndex + 1}:</label>
			                        <div className='input-group'>
			                          <input
			                            type='text'
			                            className='form-control'
			                            value={option.text}
			                            onChange={(e) =>
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                                updatedElement.options[optionIndex] = {
			                                  ...updatedElement.options[optionIndex],
			                                  text: e.target.value,
			                                  value: e.target.value
			                                }

			                                updated[selectedField] = updatedElement
			                                return updated
			                              })
			                            }
			                          />
			                          <button
			                            className='btn btn-danger'
			                            type='button'
			                            onClick={() =>
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                                updatedElement.options.splice(optionIndex, 1)

			                                updated[selectedField] = updatedElement
			                                return updated
			                              })
			                            }
			                          >
			                            Delete
			                          </button>
			                        </div>
			                      </div>
			                    ))}
			                    <button
			                      className='btn btn-success'
			                      type='button'
			                      onClick={() =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                          updatedElement.options.push({ text: '', value: '', selected: false })

			                          updated[selectedField] = updatedElement
			                          return updated
			                        })
			                      }
			                    >
			                      Add Option
			                    </button>
			                  </div>
			                  <div className='mt-5'>
			                    <div className='d-flex justify-content-between'>
			                      <h5>VALIDATION</h5>
			                      <div className='mb-5'>
			                        <div className='form-check form-switch'>
			                          <input
			                            className='form-check-input'
			                            type='checkbox'
			                            id='toggleSwitch'
			                            checked={
			                              selectedElement.validation &&
			                              selectedElement.validation[0]?.rule.includes('required')
			                            }
			                            onChange={() => {
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                console.log(updated)
			                                const selectedFieldObject = updated[selectedField]

			                                // Check if the object is not extensible, create a new object that extends the existing one
			                                if (!Object.isExtensible(selectedFieldObject)) {
			                                  updated[selectedField] = {
			                                    ...selectedFieldObject,
			                                    validation: [
			                                      {
			                                        rule: 'undefined', // Set your default rule here
			                                        errorMessage: null
			                                      }
			                                    ]
			                                  }
			                                }

			                                const validationRule = formElements[selectedField].validation?.[0]
			                                console.log(validationRule)

			                                if (!validationRule) {
			                                  // Validation field is not present, add default validation
			                                  updated[selectedField].validation = [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                } else {
			                                  console.log(updated[selectedField])
			                                  // Validation field is present, toggle the rule
			                                  const isRuleUndefined = validationRule.rule.includes('undefined')
			                                  console.log(isRuleUndefined)

			                                  // Create a new object with updated values
			                                  const updatedValidationRule = {
			                                    rule: isRuleUndefined ? 'required' : 'undefined',
			                                    errorMessage: isRuleUndefined ? 'null' : null
			                                  }

			                                  // Update the validation array with the new object
			                                  updated[selectedField].validation = [updatedValidationRule]
			                                }

			                                // Log the updated state for debugging
			                                console.log('Updated state:', updated)

			                                return updated
			                              })
			                            }}
			                          />
			                          <label className='form-check-label' htmlFor='toggleSwitch'>
			                            {selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                              ? 'Required'
			                              : ' Undefined'}
			                          </label>
			                        </div>
			                      </div>
			                    </div>
			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Validation Message:</label>
			                      <input
			                        type='text'
			                        className='form-control'
			                        value={selectedElement.validation?.[0]?.errorMessage || ''}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]
			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }

			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              // Create a new object instead of modifying the existing one
			                              const newValidationRule = {
			                                ...validationRule,
			                                errorMessage: e.target.value
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }

			                            return updated
			                          })
			                        }
			                      />
			                    </div>
			                  </div>
			                </>
			              )}
			              {selectedElement.type === 'checkbox' && (
			                <>
			                  <div>
			                    {selectedElement.options.map((option: any, optionIndex: any) => (
			                      <div key={optionIndex} className='mb-3'>
			                        <label className='form-label'>Option {optionIndex + 1}:</label>
			                        <div className='input-group'>
			                          <input
			                            type='text'
			                            className='form-control'
			                            value={option.text}
			                            onChange={(e) =>
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                                updatedElement.options[optionIndex].text = e.target.value

			                                updated[selectedField] = updatedElement
			                                return updated
			                              })
			                            }
			                          />
			                          <button
			                            className='btn btn-danger'
			                            type='button'
			                            onClick={() =>
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                                updatedElement.options.splice(optionIndex, 1)

			                                updated[selectedField] = updatedElement
			                                return updated
			                              })
			                            }
			                          >
			                            Delete
			                          </button>
			                        </div>
			                      </div>
			                    ))}
			                    <button
			                      className='btn btn-success'
			                      type='button'
			                      onClick={() =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                          updatedElement.options.push({ text: '', value: '', selected: false })

			                          updated[selectedField] = updatedElement
			                          return updated
			                        })
			                      }
			                    >
			                      Add Option
			                    </button>
			                    <div className='mt-5'>
			                      <div className='d-flex justify-content-between'>
			                        <h5>VALIDATION</h5>
			                        <div className='mb-5'>
			                          <div className='form-check form-switch'>
			                            <input
			                              className='form-check-input'
			                              type='checkbox'
			                              id='toggleSwitch'
			                              checked={
			                                selectedElement.validation &&
			                                selectedElement.validation[0]?.rule.includes('required')
			                              }
			                              onChange={() => {
			                                setFormElements((prev: any) => {
			                                  const updated = [...prev]
			                                  console.log(updated)
			                                  const selectedFieldObject = updated[selectedField]

			                                  // Check if the object is not extensible, create a new object that extends the existing one
			                                  if (!Object.isExtensible(selectedFieldObject)) {
			                                    updated[selectedField] = {
			                                      ...selectedFieldObject,
			                                      validation: [
			                                        {
			                                          rule: 'undefined', // Set your default rule here
			                                          errorMessage: null
			                                        }
			                                      ]
			                                    }
			                                  }

			                                  const validationRule = formElements[selectedField].validation?.[0]
			                                  console.log(validationRule)

			                                  if (!validationRule) {
			                                    // Validation field is not present, add default validation
			                                    updated[selectedField].validation = [
			                                      {
			                                        rule: 'undefined', // Set your default rule here
			                                        errorMessage: null
			                                      }
			                                    ]
			                                  } else {
			                                    console.log(updated[selectedField])
			                                    // Validation field is present, toggle the rule
			                                    const isRuleUndefined =
			                                      validationRule.rule.includes('undefined')
			                                    console.log(isRuleUndefined)

			                                    // Create a new object with updated values
			                                    const updatedValidationRule = {
			                                      rule: isRuleUndefined ? 'required' : 'undefined',
			                                      errorMessage: isRuleUndefined ? 'null' : null
			                                    }

			                                    // Update the validation array with the new object
			                                    updated[selectedField].validation = [updatedValidationRule]
			                                  }

			                                  // Log the updated state for debugging
			                                  console.log('Updated state:', updated)

			                                  return updated
			                                })
			                              }}
			                            />
			                            <label className='form-check-label' htmlFor='toggleSwitch'>
			                              {selectedElement.validation &&
			                              selectedElement.validation[0]?.rule.includes('required')
			                                ? 'Required'
			                                : ' Undefined'}
			                            </label>
			                          </div>
			                        </div>
			                      </div>
			                      <div
			                        className={`mb-3 ${
			                          selectedElement.validation?.[0]?.rule?.includes('required')
			                            ? ''
			                            : 'd-none'
			                        }`}
			                      >
			                        <label className='form-label'>Validation Message:</label>
			                        <input
			                          type='text'
			                          className='form-control'
			                          value={selectedElement.validation?.[0]?.errorMessage || ''}
			                          onChange={(e) =>
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              const selectedFieldObject = updated[selectedField]

			                              if (!Object.isExtensible(selectedFieldObject)) {
			                                // If the object is not extensible, create a new object that extends the existing one
			                                updated[selectedField] = {
			                                  ...selectedFieldObject
			                                }
			                              }

			                              const validationRule = updated[selectedField].validation?.[0]

			                              if (validationRule) {
			                                // Create a new object instead of modifying the existing one
			                                const newValidationRule = {
			                                  ...validationRule,
			                                  errorMessage: e.target.value
			                                }

			                                // Update the validation array with the new validation rule
			                                updated[selectedField].validation = [newValidationRule]
			                              }

			                              return updated
			                            })
			                          }
			                        />
			                      </div>
			                    </div>
			                  </div>
			                </>
			              )}
			              {selectedElement.type === 'divider' && (
			                <div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Color:</label>
			                    <input
			                      type='color'
			                      className='form-control form-control-color'
			                      value={selectedElement.color}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            color: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Thickness (in pixels):</label>
			                    <input
			                      type='number'
			                      className='form-control'
			                      value={selectedElement.thickness}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            thickness: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'textarea' && (
			                <div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Label:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.label}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            label: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Placeholder:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.placeholder}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            placeholder: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Color:</label>
			                    <input
			                      type='color'
			                      className='form-control form-control-color'
			                      value={selectedElement.color}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            color: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Rows:</label>
			                    <input
			                      type='number'
			                      className='form-control'
			                      value={selectedElement.rows}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            rows: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mt-5'>
			                    <div className='d-flex justify-content-between'>
			                      <h5>VALIDATION</h5>
			                      <div className='mb-5'>
			                        <div className='form-check form-switch'>
			                          <input
			                            className='form-check-input'
			                            type='checkbox'
			                            id='toggleSwitch'
			                            checked={
			                              selectedElement.validation &&
			                              selectedElement.validation[0]?.rule.includes('required')
			                            }
			                            onChange={() => {
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                console.log(updated)
			                                const selectedFieldObject = updated[selectedField]

			                                // Check if the object is not extensible, create a new object that extends the existing one
			                                if (!Object.isExtensible(selectedFieldObject)) {
			                                  updated[selectedField] = {
			                                    ...selectedFieldObject,
			                                    validation: [
			                                      {
			                                        rule: 'undefined', // Set your default rule here
			                                        errorMessage: null
			                                      }
			                                    ]
			                                  }
			                                }

			                                const validationRule = formElements[selectedField].validation?.[0]
			                                console.log(validationRule)

			                                if (!validationRule) {
			                                  // Validation field is not present, add default validation
			                                  updated[selectedField].validation = [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                } else {
			                                  console.log(updated[selectedField])
			                                  // Validation field is present, toggle the rule
			                                  const isRuleUndefined = validationRule.rule.includes('undefined')
			                                  console.log(isRuleUndefined)

			                                  // Create a new object with updated values
			                                  const updatedValidationRule = {
			                                    rule: isRuleUndefined ? 'required' : 'undefined',
			                                    errorMessage: isRuleUndefined ? 'null' : null
			                                  }

			                                  // Update the validation array with the new object
			                                  updated[selectedField].validation = [updatedValidationRule]
			                                }

			                                // Log the updated state for debugging
			                                console.log('Updated state:', updated)

			                                return updated
			                              })
			                            }}
			                          />
			                          <label className='form-check-label' htmlFor='toggleSwitch'>
			                            {selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                              ? 'Required'
			                              : ' Undefined'}
			                          </label>
			                        </div>
			                      </div>
			                    </div>
			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Validation Message:</label>
			                      <input
			                        type='text'
			                        className='form-control'
			                        value={selectedElement.validation?.[0]?.errorMessage || ''}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]
			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }

			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              // Create a new object instead of modifying the existing one
			                              const newValidationRule = {
			                                ...validationRule,
			                                errorMessage: e.target.value
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }

			                            return updated
			                          })
			                        }
			                      />
			                    </div>
			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Max:</label>
			                      <input
			                        type='number'
			                        className='form-control'
			                        value={getMaxFromRule(selectedElement.validation?.[0]?.rule)}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]
			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }

			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              const max = e.target.value
			                              const maxValue = getMaxFromRule(validationRule.rule)
			                              const minValue = getMinFromRule(validationRule.rule)

			                              // Create a new object instead of modifying the existing one
			                              const newValidationRule = {
			                                ...validationRule,
			                                rule:
			                                  max !== ''
			                                    ? `required|max-${max}|min-${minValue}`
			                                    : `required|max-''|min-${minValue}`
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }

			                            return updated
			                          })
			                        }
			                      />
			                    </div>

			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Min:</label>
			                      <input
			                        type='number'
			                        className='form-control'
			                        value={getMinFromRule(selectedElement.validation?.[0]?.rule)}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]

			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }
			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              const min = e.target.value
			                              const maxValue = getMaxFromRule(validationRule.rule)
			                              const minValue = getMinFromRule(validationRule.rule)

			                              const newValidationRule = {
			                                ...validationRule,
			                                rule:
			                                  min !== ''
			                                    ? `required|max-${maxValue}|min-${min}`
			                                    : `required|max-${maxValue}|min-''`
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }
			                            return updated
			                          })
			                        }
			                      />
			                    </div>
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'radio' && (
			                <div>
			                  {selectedElement.options.map((option: any, optionIndex: any) => (
			                    <div key={optionIndex} className='mb-3'>
			                      <label className='form-label'>Option {optionIndex + 1}:</label>
			                      <div className='input-group'>
			                        <input
			                          type='text'
			                          className='form-control'
			                          value={option.text}
			                          onChange={(e) =>
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                              updatedElement.options[optionIndex].text = e.target.value

			                              updated[selectedField] = updatedElement
			                              return updated
			                            })
			                          }
			                        />
			                        <button
			                          className='btn btn-danger'
			                          type='button'
			                          onClick={() =>
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                              updatedElement.options.splice(optionIndex, 1)

			                              updated[selectedField] = updatedElement
			                              return updated
			                            })
			                          }
			                        >
			                          Delete
			                        </button>
			                      </div>
			                    </div>
			                  ))}
			                  <button
			                    className='btn btn-success'
			                    type='button'
			                    onClick={() =>
			                      setFormElements((prev: any) => {
			                        const updated = [...prev]
			                        const updatedElement = { ...updated[selectedField] } // Create a deep copy of the selected element

			                        updatedElement.options.push({ text: '', value: '', selected: false })

			                        updated[selectedField] = updatedElement
			                        return updated
			                      })
			                    }
			                  >
			                    Add Option
			                  </button>
			                  <div className='mt-5'>
			                    <div className='d-flex justify-content-between'>
			                      <h5>VALIDATION</h5>
			                      <div className='mb-5'>
			                        <div className='form-check form-switch'>
			                          <input
			                            className='form-check-input'
			                            type='checkbox'
			                            id='toggleSwitch'
			                            checked={
			                              selectedElement.validation &&
			                              selectedElement.validation[0]?.rule.includes('required')
			                            }
			                            onChange={() => {
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                console.log(updated)
			                                const selectedFieldObject = updated[selectedField]

			                                // Check if the object is not extensible, create a new object that extends the existing one
			                                if (!Object.isExtensible(selectedFieldObject)) {
			                                  updated[selectedField] = {
			                                    ...selectedFieldObject,
			                                    validation: [
			                                      {
			                                        rule: 'undefined', // Set your default rule here
			                                        errorMessage: null
			                                      }
			                                    ]
			                                  }
			                                }

			                                const validationRule = formElements[selectedField].validation?.[0]
			                                console.log(validationRule)

			                                if (!validationRule) {
			                                  // Validation field is not present, add default validation
			                                  updated[selectedField].validation = [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                } else {
			                                  console.log(updated[selectedField])
			                                  // Validation field is present, toggle the rule
			                                  const isRuleUndefined = validationRule.rule.includes('undefined')
			                                  console.log(isRuleUndefined)

			                                  // Create a new object with updated values
			                                  const updatedValidationRule = {
			                                    rule: isRuleUndefined ? 'required' : 'undefined',
			                                    errorMessage: isRuleUndefined ? 'null' : null
			                                  }

			                                  // Update the validation array with the new object
			                                  updated[selectedField].validation = [updatedValidationRule]
			                                }

			                                // Log the updated state for debugging
			                                console.log('Updated state:', updated)

			                                return updated
			                              })
			                            }}
			                          />
			                          <label className='form-check-label' htmlFor='toggleSwitch'>
			                            {selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                              ? 'Required'
			                              : ' Undefined'}
			                          </label>
			                        </div>
			                      </div>
			                    </div>
			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Validation Message:</label>
			                      <input
			                        type='text'
			                        className='form-control'
			                        value={selectedElement.validation?.[0]?.errorMessage || ''}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]
			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }

			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              // Create a new object instead of modifying the existing one
			                              const newValidationRule = {
			                                ...validationRule,
			                                errorMessage: e.target.value
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }

			                            return updated
			                          })
			                        }
			                      />
			                    </div>
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'captcha' && (
			                <div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Enter Text:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.inputTextName}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            inputTextName: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Text Color:</label>
			                    <input
			                      type='color'
			                      className='form-control form-control-color'
			                      value={selectedElement.inputTextNameColor}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            inputTextNameColor: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mt-5'>
			                    <div className='d-flex justify-content-between'>
			                      <h5>VALIDATION</h5>
			                      <div className='mb-5'>
			                        <div className='form-check form-switch'>
			                          <input
			                            className='form-check-input'
			                            type='checkbox'
			                            id='toggleSwitch'
			                            checked={
			                              selectedElement.validation &&
			                              selectedElement.validation[0]?.rule.includes('required')
			                            }
			                            onChange={() => {
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                console.log(updated)
			                                const selectedFieldObject = updated[selectedField]

			                                // Check if the object is not extensible, create a new object that extends the existing one
			                                if (!Object.isExtensible(selectedFieldObject)) {
			                                  updated[selectedField] = {
			                                    ...selectedFieldObject,
			                                    validation: [
			                                      {
			                                        rule: 'undefined', // Set your default rule here
			                                        errorMessage: null
			                                      }
			                                    ]
			                                  }
			                                }

			                                const validationRule = formElements[selectedField].validation?.[0]
			                                console.log(validationRule)

			                                if (!validationRule) {
			                                  // Validation field is not present, add default validation
			                                  updated[selectedField].validation = [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                } else {
			                                  console.log(updated[selectedField])
			                                  // Validation field is present, toggle the rule
			                                  const isRuleUndefined = validationRule.rule.includes('undefined')
			                                  console.log(isRuleUndefined)

			                                  // Create a new object with updated values
			                                  const updatedValidationRule = {
			                                    rule: isRuleUndefined ? 'required' : 'undefined',
			                                    errorMessage: isRuleUndefined ? 'null' : null
			                                  }

			                                  // Update the validation array with the new object
			                                  updated[selectedField].validation = [updatedValidationRule]
			                                }

			                                // Log the updated state for debugging
			                                console.log('Updated state:', updated)

			                                return updated
			                              })
			                            }}
			                          />
			                          <label className='form-check-label' htmlFor='toggleSwitch'>
			                            {selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                              ? 'Required'
			                              : ' Undefined'}
			                          </label>
			                        </div>
			                      </div>
			                    </div>
			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Validation Message:</label>
			                      <input
			                        type='text'
			                        className='form-control'
			                        value={selectedElement.validation?.[0]?.errorMessage || ''}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]
			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }

			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              // Create a new object instead of modifying the existing one
			                              const newValidationRule = {
			                                ...validationRule,
			                                errorMessage: e.target.value
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }

			                            return updated
			                          })
			                        }
			                      />
			                    </div>
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'signature' && (
			                <div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Label:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.label}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            label: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>InkColor:</label>
			                    <input
			                      type='color'
			                      className='form-control form-control-color'
			                      value={selectedElement.color}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            color: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                    <label className='form-label'>PadColor:</label>
			                    <input
			                      type='color'
			                      className='form-control form-control-color'
			                      value={selectedElement.padColor}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            padColor: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3 d-grid'>
			                    <label className='form-label'>Width:</label>
			                    <input
			                      type='range'
			                      min='300'
			                      max='1000'
			                      value={selectedElement.width}
			                      className='slider'
			                      id='myRange'
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            width: Number(e.target.value)
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                    <label className='form-label'>height:</label>
			                    <input
			                      type='range'
			                      min='100'
			                      max='500'
			                      value={selectedElement.height}
			                      className='slider'
			                      id='myRange'
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            height: Number(e.target.value)
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>

			                  <div className='mt-5'>
			                    <div className='d-flex justify-content-between'>
			                      <h5>VALIDATION</h5>
			                      <div className='mb-5'>
			                        <div className='form-check form-switch'>
			                          <input
			                            className='form-check-input'
			                            type='checkbox'
			                            id='toggleSwitch'
			                            checked={
			                              selectedElement.validation &&
			                              selectedElement.validation[0]?.rule.includes('required')
			                            }
			                            onChange={() => {
			                              setFormElements((prev: any) => {
			                                const updated = [...prev]
			                                console.log(updated)
			                                const selectedFieldObject = updated[selectedField]

			                                // Check if the object is not extensible, create a new object that extends the existing one
			                                if (!Object.isExtensible(selectedFieldObject)) {
			                                  updated[selectedField] = {
			                                    ...selectedFieldObject,
			                                    validation: [
			                                      {
			                                        rule: 'undefined', // Set your default rule here
			                                        errorMessage: null
			                                      }
			                                    ]
			                                  }
			                                }

			                                const validationRule = formElements[selectedField].validation?.[0]
			                                console.log(validationRule)

			                                if (!validationRule) {
			                                  // Validation field is not present, add default validation
			                                  updated[selectedField].validation = [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                } else {
			                                  console.log(updated[selectedField])
			                                  // Validation field is present, toggle the rule
			                                  const isRuleUndefined = validationRule.rule.includes('undefined')
			                                  console.log(isRuleUndefined)

			                                  // Create a new object with updated values
			                                  const updatedValidationRule = {
			                                    rule: isRuleUndefined ? 'required' : 'undefined',
			                                    errorMessage: isRuleUndefined ? 'null' : null
			                                  }

			                                  // Update the validation array with the new object
			                                  updated[selectedField].validation = [updatedValidationRule]
			                                }

			                                // Log the updated state for debugging
			                                console.log('Updated state:', updated)

			                                return updated
			                              })
			                            }}
			                          />
			                          <label className='form-check-label' htmlFor='toggleSwitch'>
			                            {selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                              ? 'Required'
			                              : ' Undefined'}
			                          </label>
			                        </div>
			                      </div>
			                    </div>
			                    <div
			                      className={`mb-3 ${
			                        selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                      }`}
			                    >
			                      <label className='form-label'>Validation Message:</label>
			                      <input
			                        type='text'
			                        className='form-control'
			                        value={selectedElement.validation?.[0]?.errorMessage || ''}
			                        onChange={(e) =>
			                          setFormElements((prev: any) => {
			                            const updated = [...prev]
			                            const selectedFieldObject = updated[selectedField]

			                            if (!Object.isExtensible(selectedFieldObject)) {
			                              // If the object is not extensible, create a new object that extends the existing one
			                              updated[selectedField] = {
			                                ...selectedFieldObject
			                              }
			                            }

			                            const validationRule = updated[selectedField].validation?.[0]

			                            if (validationRule) {
			                              // Create a new object instead of modifying the existing one
			                              const newValidationRule = {
			                                ...validationRule,
			                                errorMessage: e.target.value
			                              }

			                              // Update the validation array with the new validation rule
			                              updated[selectedField].validation = [newValidationRule]
			                            }

			                            return updated
			                          })
			                        }
			                      />
			                    </div>
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'file' && (
			                <div className='mt-5'>
			                  <div className='d-flex justify-content-between'>
			                    <h5>VALIDATION</h5>
			                    <div className='mb-5'>
			                      <div className='form-check form-switch'>
			                        <input
			                          className='form-check-input'
			                          type='checkbox'
			                          id='toggleSwitch'
			                          checked={
			                            selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                          }
			                          onChange={() => {
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              console.log(updated)
			                              const selectedFieldObject = updated[selectedField]

			                              // Check if the object is not extensible, create a new object that extends the existing one
			                              if (!Object.isExtensible(selectedFieldObject)) {
			                                updated[selectedField] = {
			                                  ...selectedFieldObject,
			                                  validation: [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                }
			                              }

			                              const validationRule = formElements[selectedField].validation?.[0]
			                              console.log(validationRule)

			                              if (!validationRule) {
			                                // Validation field is not present, add default validation
			                                updated[selectedField].validation = [
			                                  {
			                                    rule: 'undefined', // Set your default rule here
			                                    errorMessage: null
			                                  }
			                                ]
			                              } else {
			                                console.log(updated[selectedField])
			                                // Validation field is present, toggle the rule
			                                const isRuleUndefined = validationRule.rule.includes('undefined')
			                                console.log(isRuleUndefined)

			                                // Create a new object with updated values
			                                const updatedValidationRule = {
			                                  rule: isRuleUndefined ? 'required' : 'undefined',
			                                  errorMessage: isRuleUndefined ? 'null' : null
			                                }

			                                // Update the validation array with the new object
			                                updated[selectedField].validation = [updatedValidationRule]
			                              }

			                              // Log the updated state for debugging
			                              console.log('Updated state:', updated)

			                              return updated
			                            })
			                          }}
			                        />
			                        <label className='form-check-label' htmlFor='toggleSwitch'>
			                          {selectedElement.validation &&
			                          selectedElement.validation[0]?.rule.includes('required')
			                            ? 'Required'
			                            : ' Undefined'}
			                        </label>
			                      </div>
			                    </div>
			                  </div>
			                  <div
			                    className={`mb-3 ${
			                      selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                    }`}
			                  >
			                    <label className='form-label'>Validation Message:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.validation?.[0]?.errorMessage || ''}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const selectedFieldObject = updated[selectedField]

			                          if (!Object.isExtensible(selectedFieldObject)) {
			                            // If the object is not extensible, create a new object that extends the existing one
			                            updated[selectedField] = {
			                              ...selectedFieldObject
			                            }
			                          }

			                          const validationRule = updated[selectedField].validation?.[0]

			                          if (validationRule) {
			                            // Create a new object instead of modifying the existing one
			                            const newValidationRule = {
			                              ...validationRule,
			                              errorMessage: e.target.value
			                            }

			                            // Update the validation array with the new validation rule
			                            updated[selectedField].validation = [newValidationRule]
			                          }

			                          return updated
			                        })
			                      }
			                    />
			                  </div>

			                  {/* New file size validation */}
			                  <div
			                    className={`mb-3 ${
			                      selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                    }`}
			                  >
			                    <label className='form-label'>Max File Size (In Bytes):</label>
			                    <input
			                      type='number'
			                      className='form-control'
			                      value={getMaxSizeFromRule(selectedElement.validation?.[0]?.rule)}
			                      onChange={(e) => handleMaxSizeChange(e.target.value)}
			                    />
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'date' && (
			                <div className='mt-5'>
			                  <div className='d-flex justify-content-between'>
			                    <h5>VALIDATION</h5>
			                    <div className='mb-5'>
			                      <div className='form-check form-switch'>
			                        <input
			                          className='form-check-input'
			                          type='checkbox'
			                          id='toggleSwitch'
			                          checked={
			                            selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                          }
			                          onChange={() => {
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              console.log(updated)
			                              const selectedFieldObject = updated[selectedField]

			                              // Check if the object is not extensible, create a new object that extends the existing one
			                              if (!Object.isExtensible(selectedFieldObject)) {
			                                updated[selectedField] = {
			                                  ...selectedFieldObject,
			                                  validation: [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                }
			                              }

			                              const validationRule = formElements[selectedField].validation?.[0]
			                              console.log(validationRule)

			                              if (!validationRule) {
			                                // Validation field is not present, add default validation
			                                updated[selectedField].validation = [
			                                  {
			                                    rule: 'undefined', // Set your default rule here
			                                    errorMessage: null
			                                  }
			                                ]
			                              } else {
			                                console.log(updated[selectedField])
			                                // Validation field is present, toggle the rule
			                                const isRuleUndefined = validationRule.rule.includes('undefined')
			                                console.log(isRuleUndefined)

			                                // Create a new object with updated values
			                                const updatedValidationRule = {
			                                  rule: isRuleUndefined ? 'required' : 'undefined',
			                                  errorMessage: isRuleUndefined ? 'null' : null
			                                }

			                                // Update the validation array with the new object
			                                updated[selectedField].validation = [updatedValidationRule]
			                              }

			                              // Log the updated state for debugging
			                              console.log('Updated state:', updated)

			                              return updated
			                            })
			                          }}
			                        />
			                        <label className='form-check-label' htmlFor='toggleSwitch'>
			                          {selectedElement.validation &&
			                          selectedElement.validation[0]?.rule.includes('required')
			                            ? 'Required'
			                            : ' Undefined'}
			                        </label>
			                      </div>
			                    </div>
			                  </div>
			                  <div
			                    className={`mb-3 ${
			                      selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                    }`}
			                  >
			                    <label className='form-label'>Validation Message:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.validation?.[0]?.errorMessage || ''}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const selectedFieldObject = updated[selectedField]

			                          if (!Object.isExtensible(selectedFieldObject)) {
			                            // If the object is not extensible, create a new object that extends the existing one
			                            updated[selectedField] = {
			                              ...selectedFieldObject
			                            }
			                          }

			                          const validationRule = updated[selectedField].validation?.[0]

			                          if (validationRule) {
			                            // Create a new object instead of modifying the existing one
			                            const newValidationRule = {
			                              ...validationRule,
			                              errorMessage: e.target.value
			                            }

			                            // Update the validation array with the new validation rule
			                            updated[selectedField].validation = [newValidationRule]
			                          }

			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'time' && (
			                <div className='mt-5'>
			                  <div className='d-flex justify-content-between'>
			                    <h5>VALIDATION</h5>
			                    <div className='mb-5'>
			                      <div className='form-check form-switch'>
			                        <input
			                          className='form-check-input'
			                          type='checkbox'
			                          id='toggleSwitch'
			                          checked={
			                            selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                          }
			                          onChange={() => {
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              console.log(updated)
			                              const selectedFieldObject = updated[selectedField]

			                              // Check if the object is not extensible, create a new object that extends the existing one
			                              if (!Object.isExtensible(selectedFieldObject)) {
			                                updated[selectedField] = {
			                                  ...selectedFieldObject,
			                                  validation: [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                }
			                              }

			                              const validationRule = formElements[selectedField].validation?.[0]
			                              console.log(validationRule)

			                              if (!validationRule) {
			                                // Validation field is not present, add default validation
			                                updated[selectedField].validation = [
			                                  {
			                                    rule: 'undefined', // Set your default rule here
			                                    errorMessage: null
			                                  }
			                                ]
			                              } else {
			                                console.log(updated[selectedField])
			                                // Validation field is present, toggle the rule
			                                const isRuleUndefined = validationRule.rule.includes('undefined')
			                                console.log(isRuleUndefined)

			                                // Create a new object with updated values
			                                const updatedValidationRule = {
			                                  rule: isRuleUndefined ? 'required' : 'undefined',
			                                  errorMessage: isRuleUndefined ? 'null' : null
			                                }

			                                // Update the validation array with the new object
			                                updated[selectedField].validation = [updatedValidationRule]
			                              }

			                              // Log the updated state for debugging
			                              console.log('Updated state:', updated)

			                              return updated
			                            })
			                          }}
			                        />
			                        <label className='form-check-label' htmlFor='toggleSwitch'>
			                          {selectedElement.validation &&
			                          selectedElement.validation[0]?.rule.includes('required')
			                            ? 'Required'
			                            : ' Undefined'}
			                        </label>
			                      </div>
			                    </div>
			                  </div>
			                  <div
			                    className={`mb-3 ${
			                      selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                    }`}
			                  >
			                    <label className='form-label'>Validation Message:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.validation?.[0]?.errorMessage || ''}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const selectedFieldObject = updated[selectedField]

			                          if (!Object.isExtensible(selectedFieldObject)) {
			                            // If the object is not extensible, create a new object that extends the existing one
			                            updated[selectedField] = {
			                              ...selectedFieldObject
			                            }
			                          }

			                          const validationRule = updated[selectedField].validation?.[0]

			                          if (validationRule) {
			                            // Create a new object instead of modifying the existing one
			                            const newValidationRule = {
			                              ...validationRule,
			                              errorMessage: e.target.value
			                            }

			                            // Update the validation array with the new validation rule
			                            updated[selectedField].validation = [newValidationRule]
			                          }

			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'datetime' && (
			                <div className='mt-5'>
			                  <div className='d-flex justify-content-between'>
			                    <h5>VALIDATION</h5>
			                    <div className='mb-5'>
			                      <div className='form-check form-switch'>
			                        <input
			                          className='form-check-input'
			                          type='checkbox'
			                          id='toggleSwitch'
			                          checked={
			                            selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                          }
			                          onChange={() => {
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              console.log(updated)
			                              const selectedFieldObject = updated[selectedField]

			                              // Check if the object is not extensible, create a new object that extends the existing one
			                              if (!Object.isExtensible(selectedFieldObject)) {
			                                updated[selectedField] = {
			                                  ...selectedFieldObject,
			                                  validation: [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                }
			                              }

			                              const validationRule = formElements[selectedField].validation?.[0]
			                              console.log(validationRule)

			                              if (!validationRule) {
			                                // Validation field is not present, add default validation
			                                updated[selectedField].validation = [
			                                  {
			                                    rule: 'undefined', // Set your default rule here
			                                    errorMessage: null
			                                  }
			                                ]
			                              } else {
			                                console.log(updated[selectedField])
			                                // Validation field is present, toggle the rule
			                                const isRuleUndefined = validationRule.rule.includes('undefined')
			                                console.log(isRuleUndefined)

			                                // Create a new object with updated values
			                                const updatedValidationRule = {
			                                  rule: isRuleUndefined ? 'required' : 'undefined',
			                                  errorMessage: isRuleUndefined ? 'null' : null
			                                }

			                                // Update the validation array with the new object
			                                updated[selectedField].validation = [updatedValidationRule]
			                              }

			                              // Log the updated state for debugging
			                              console.log('Updated state:', updated)

			                              return updated
			                            })
			                          }}
			                        />
			                        <label className='form-check-label' htmlFor='toggleSwitch'>
			                          {selectedElement.validation &&
			                          selectedElement.validation[0]?.rule.includes('required')
			                            ? 'Required'
			                            : ' Undefined'}
			                        </label>
			                      </div>
			                    </div>
			                  </div>
			                  <div
			                    className={`mb-3 ${
			                      selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                    }`}
			                  >
			                    <label className='form-label'>Validation Message:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.validation?.[0]?.errorMessage || ''}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const selectedFieldObject = updated[selectedField]

			                          if (!Object.isExtensible(selectedFieldObject)) {
			                            // If the object is not extensible, create a new object that extends the existing one
			                            updated[selectedField] = {
			                              ...selectedFieldObject
			                            }
			                          }

			                          const validationRule = updated[selectedField].validation?.[0]

			                          if (validationRule) {
			                            // Create a new object instead of modifying the existing one
			                            const newValidationRule = {
			                              ...validationRule,
			                              errorMessage: e.target.value
			                            }

			                            // Update the validation array with the new validation rule
			                            updated[selectedField].validation = [newValidationRule]
			                          }

			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'daterange' && (
			                <div className='mt-5'>
			                  <div className='d-flex justify-content-between'>
			                    <h5>VALIDATION</h5>
			                    <div className='mb-5'>
			                      <div className='form-check form-switch'>
			                        <input
			                          className='form-check-input'
			                          type='checkbox'
			                          id='toggleSwitch'
			                          checked={
			                            selectedElement.validation &&
			                            selectedElement.validation[0]?.rule.includes('required')
			                          }
			                          onChange={() => {
			                            setFormElements((prev: any) => {
			                              const updated = [...prev]
			                              console.log(updated)
			                              const selectedFieldObject = updated[selectedField]

			                              // Check if the object is not extensible, create a new object that extends the existing one
			                              if (!Object.isExtensible(selectedFieldObject)) {
			                                updated[selectedField] = {
			                                  ...selectedFieldObject,
			                                  validation: [
			                                    {
			                                      rule: 'undefined', // Set your default rule here
			                                      errorMessage: null
			                                    }
			                                  ]
			                                }
			                              }

			                              const validationRule = formElements[selectedField].validation?.[0]
			                              console.log(validationRule)

			                              if (!validationRule) {
			                                // Validation field is not present, add default validation
			                                updated[selectedField].validation = [
			                                  {
			                                    rule: 'undefined', // Set your default rule here
			                                    errorMessage: null
			                                  }
			                                ]
			                              } else {
			                                console.log(updated[selectedField])
			                                // Validation field is present, toggle the rule
			                                const isRuleUndefined = validationRule.rule.includes('undefined')
			                                console.log(isRuleUndefined)

			                                // Create a new object with updated values
			                                const updatedValidationRule = {
			                                  rule: isRuleUndefined ? 'required' : 'undefined',
			                                  errorMessage: isRuleUndefined ? 'null' : null
			                                }

			                                // Update the validation array with the new object
			                                updated[selectedField].validation = [updatedValidationRule]
			                              }

			                              // Log the updated state for debugging
			                              console.log('Updated state:', updated)

			                              return updated
			                            })
			                          }}
			                        />
			                        <label className='form-check-label' htmlFor='toggleSwitch'>
			                          {selectedElement.validation &&
			                          selectedElement.validation[0]?.rule.includes('required')
			                            ? 'Required'
			                            : ' Undefined'}
			                        </label>
			                      </div>
			                    </div>
			                  </div>
			                  <div
			                    className={`mb-3 ${
			                      selectedElement.validation?.[0]?.rule?.includes('required') ? '' : 'd-none'
			                    }`}
			                  >
			                    <label className='form-label'>Validation Message:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.validation?.[0]?.errorMessage || ''}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          const selectedFieldObject = updated[selectedField]

			                          if (!Object.isExtensible(selectedFieldObject)) {
			                            // If the object is not extensible, create a new object that extends the existing one
			                            updated[selectedField] = {
			                              ...selectedFieldObject
			                            }
			                          }

			                          const validationRule = updated[selectedField].validation?.[0]

			                          if (validationRule) {
			                            // Create a new object instead of modifying the existing one
			                            const newValidationRule = {
			                              ...validationRule,
			                              errorMessage: e.target.value
			                            }

			                            // Update the validation array with the new validation rule
			                            updated[selectedField].validation = [newValidationRule]
			                          }

			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                </div>
			              )}
			              {selectedElement.type === 'button' && (
			                <div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Button Text:</label>
			                    <input
			                      type='text'
			                      className='form-control'
			                      value={selectedElement.text}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            text: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Text Color:</label>
			                    <input
			                      type='color'
			                      className='form-control form-control-color'
			                      value={selectedElement.btnTxtColor}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            btnTxtColor: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                  <div className='mb-3'>
			                    <label className='form-label'>Button Color:</label>
			                    <input
			                      type='color'
			                      className='form-control form-control-color'
			                      value={selectedElement.btnBgColor}
			                      onChange={(e) =>
			                        setFormElements((prev: any) => {
			                          const updated = [...prev]
			                          updated[selectedField] = {
			                            ...updated[selectedField],
			                            btnBgColor: e.target.value
			                          }
			                          return updated
			                        })
			                      }
			                    />
			                  </div>
			                </div>
			            )}
		            </div>
        		</>): (
		          <div className='text-center'>No element selected</div>
        		)}
        	</div>
		</>
	)
}

export default FormFieldCustomization