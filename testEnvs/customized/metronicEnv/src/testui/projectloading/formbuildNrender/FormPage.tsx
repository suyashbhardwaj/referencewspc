import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import SignatureCanvas from 'react-signature-canvas'
import { useEffect, useState } from 'react';

interface IProps {
  formElements: any
  setFormElements: any
  setSelectedFields: any
  selectedField: any
  formJson: any
  togglerFieldIndex: any
  setTogglerFieldIndex: any
  subFormFieldIndexes: any
  setSubFormFieldIndexes: any
}
interface CaptchaProps {
  onRefresh: (newCaptcha: string) => void
  inputTextName: any
  inputTextNameColor: any
}
interface RatingsProps {
  label: string
  color: string
  alignment: any
}

const FormPage: React.FC<IProps> = ({formElements, formJson, setFormElements, selectedField, setSelectedFields, setTogglerFieldIndex, setSubFormFieldIndexes, togglerFieldIndex, subFormFieldIndexes}) => {
	const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => { event.preventDefault() }
	const onDragEnd = (result: any) => {
	    if (!result.destination) return // Dragged outside the list

	    const updatedFormElements = Array.from(formElements)
	    const [movedElement] = updatedFormElements.splice(result.source.index, 1)
	    updatedFormElements.splice(result.destination.index, 0, movedElement)

	    setFormElements(updatedFormElements)
	    setSelectedFields(result.destination.index)
	  }

	const handleFieldClick = (index: number) => {
    formElements[index].includeOnCondition.conditionActive
      ? setTogglerFieldIndex(formElements[index].includeOnCondition.basedOnElementOfIndex)
      : setTogglerFieldIndex(-1)
    formElements[index].type === 'subform'
      ? setSubFormFieldIndexes(formElements[index].subformFields.map((field: any) => field.copiedFromFieldAtFieldId))
      : setSubFormFieldIndexes([])
    setSelectedFields(index)
    console.log(index)
  	}

  	const handleSwitchToggle = (index: any) => {console.log('switch toggled with index ',index)}
  	const handleCheckboxToggle = (index: any, optionalIndex: any) => {console.log(`checkbox toggled with index ${index} & ${optionalIndex}`)}
  	const handleFileUpload = (index: any, event: any) => {
    const file = event.target.files[0]
    // Handle the file as needed (e.g., store it in state or perform further processing)
    // You can access the file using the 'file' variable.
    console.log(`Uploaded file: at index ${index}`, file)
    // Add your additional logic here
  	}

  	const Captcha: React.FC<CaptchaProps> = ({ onRefresh, inputTextName, inputTextNameColor }) => {
    const [captcha, setCaptcha] = useState('')
    useEffect(() => {
      // Generate the initial captcha only once
      setCaptcha(generateRandomCaptcha())
    }, []) // Empty dependency array ensures this effect runs only once
    function generateRandomCaptcha() {
      return Math.random().toString(36).slice(2, 8).toUpperCase()
    }
    const handleRefresh = () => {
      const newCaptcha = generateRandomCaptcha()
      setCaptcha(newCaptcha)
      if (onRefresh) {
        onRefresh(newCaptcha)
      }
    }

    return (
      <>
        <>
          <div className='row captcha-content align-items-center mx-md-4'>
            <div className='col-md-7'>
              <div className='captcha-box border rounded p-2 mr-3 mr-md-4 me-n5'>
                <span className='captcha-value'>{captcha}</span>
              </div>
            </div>
            <div className='col-md-3'>
              <button type='button' className='btn btn-refresh' onClick={handleRefresh}>
                <i className='fas fa-sync-alt fa-lg' style={{ color: 'black' }}></i>
              </button>
            </div>
          </div>
          <div className='row mt-3'>
            <div className='col-md-12'>
              <label
                htmlFor='captchaInput'
                className='form-label'
                style={{ color: inputTextNameColor }}
              >
                {inputTextName}
              </label>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-12'>
              <div>
                <input type='text' id='captchaInput' className='form-control' />
              </div>
            </div>
          </div>
        </>
        {/* </div> */}
      </>
    )
  	}

  	const handleCaptchaRefresh = (index: number, newCaptcha: string) => {
    // Update the Captcha value in the state
    const updatedFormElements = [...formElements]
    // Check if formInput exists and has options property
    if (
      updatedFormElements[index] &&
      updatedFormElements[index].formInput &&
      Array.isArray(updatedFormElements[index].formInput.options)
    ) {
      updatedFormElements[index].formInput.options[0].value = newCaptcha
      setFormElements(updatedFormElements)
    } else {
      // Handle the case where the expected structure is not present
      console.error('Unexpected structure in formElements')
    }
  	}

  	const Ratings: React.FC<RatingsProps> = ({ label, color, alignment }) => {
    const [rating, setRating] = useState(0)
    const [starClass, setStarClass] = useState('active')
    console.log('alignment is ', alignment)
    const rate = (stars: number) => {
      setRating(stars)
      resetStars()
    }

    const resetStars = () => {
      setStarClass('')
    }

    return (
      <>
        <div className='review-container'>
          <label className='form-label  fs-6 required' style={{ color: color }}>
            {label}
          </label>
          <div className='star-widget'>
            <div className='d-flex'>
              <div style={{ display: 'inline-block', fontSize: '48px', textAlign: alignment }}>
                {[1, 2, 3, 4, 5].map((index) => (
                  <div
                    key={index}
                    className={`star ${starClass}`}
                    onClick={() => rate(index)}
                    /* onMouseOver={() => highlightStars(index)}
                  onMouseOut={resetStars} */
                    style={{
                      display: 'inline-block',
                      margin: '0 4px',
                      cursor: 'pointer',
                      color: rating >= index ? '#FFDD00' : '#ccc'
                    }}
                  >
                    ★
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </>
    )
  	}

	const conditionalBackgroundColor = formJson?.cardStyle === '' ? 'white' : formJson?.cardStyle
	return (
		<>
			<DragDropContext onDragEnd={onDragEnd}>
		      	<Droppable droppableId='form-elements' direction='vertical'>
		        	{(provided: any) => (
					<div ref={provided.innerRef} {...provided.droppableProps} className='container mt-8'>
		            	<div className='card-title justify-content-center align-center w-100'>
		              		<div className='d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed'>
		                		<div className='d-flex flex-center flex-column flex-column-fluid p-6 pb-lg-20'>
		                  			<div className='w-lg-800px rounded shadow-sm w-sm-100 mx-auto' style={{ position: 'relative', overflow: 'hidden' }}>
			                  			{formElements.map((element: any, index: any) => element.type === 'banner' && (
					                        // Render the button separately
					                          	<div className='banner'
					                            	style={{
						                            	backgroundImage: `url(${element.bannerSrc || ''})`,
						                            	backgroundSize: 'cover',
						                            	height: '200px',
						                            	width: '100%',
						                            	top: 0
						                            }}
					                            onClick={() => handleFieldClick(index)}>
					                            </div>
					                        )
					                    )}
					                    <div className={`content-wrapper p-10 p-lg-15`} style={{position: 'relative', backgroundColor: conditionalBackgroundColor }}>
					                      	<form className='form w-100 fv-plugins-bootstrap5 fv-plugins-framework' noValidate id='kt_login_signup_form' onSubmit={handleSubmit}>
					                      	{formElements.map(
					                          (element: any, index: any) =>
					                            // Check if the element type is 'button'
					                            element.type !== 'button' && (
					                              // Render other elements as draggable
					                              <Draggable
					                                key={element.id || index}
					                                draggableId={element.id || index.toString()}
					                                index={index}
					                              >
					                                {(providedDraggable: any, snapshot: any) => (
					                                  <div
					                                    ref={providedDraggable.innerRef}
					                                    {...providedDraggable.draggableProps}
					                                    {...providedDraggable.dragHandleProps}
					                                    className={`fv-row mb-7 ${
					                                      element.type === 'button' ? 'text-center' : ''
					                                    } ${
					                                      selectedField === index
					                                        ? 'border border-primary border-dotted'
					                                        : ''
					                                    }`}
					                                    style={{
					                                      border:
					                                        formElements[selectedField].type !== 'subform'
					                                          ? element.fieldId === togglerFieldIndex
					                                            ? '1px solid red'
					                                            : ''
					                                          : subFormFieldIndexes.includes(element.fieldId)
					                                          ? '2px solid green'
					                                          : '',
					                                      background: snapshot.isDragging ? 'white' : 'initial',
					                                      ...providedDraggable.draggableProps.style
					                                    }}
					                                    onClick={() => handleFieldClick(index)} // Handle click here
					                                  >
					                                    {element.type === 'captcha' && (
					                                      <>
					                                        <div className='d-flex align-items-center'>
					                                          <label className='form-label fw-bolder text-dark fs-6 required '>
					                                            {element.label}
					                                          </label>
					                                          <Captcha
					                                            onRefresh={(newCaptcha: any) =>
					                                              handleCaptchaRefresh(index, newCaptcha)
					                                            }
					                                            inputTextName={element.inputTextName}
					                                            inputTextNameColor={element.inputTextNameColor}
					                                          />
					                                        </div>
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <div className='mt-4'>
					                                              <span
					                                                style={{
					                                                  color:
					                                                    formElements[index]?.validation[0]?.messageColor
					                                                }}
					                                              >
					                                                {formElements[index]?.validation[0]?.errorMessage}
					                                              </span>
					                                            </div>
					                                          )}
					                                      </>
					                                    )}
					                                    {/* Rest of the element rendering logic */}
					                                    {element.type === 'heading' && (
					                                      <element.size
					                                        className={`mb-4`}
					                                        style={{
					                                          color: element.color,
					                                          textAlign: element.alignment
					                                        }}
					                                      >
					                                        {element.formHeading}
					                                      </element.size>
					                                    )}
					                                    {element.type === 'subheading' && (
					                                      <p
					                                        className={`fs-5`}
					                                        style={{
					                                          color: element.color,
					                                          textAlign: element.alignment
					                                        }}
					                                      >
					                                        {element.formSubHeading}
					                                      </p>
					                                    )}
					                                    {element.type === 'text' && (
					                                      <>
					                                        <label
					                                          className='form-label  fs-6 required'
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <input
					                                          placeholder={element.placeholder}
					                                          type='text'
					                                          autoComplete='off'
					                                          className='form-control form-control-lg form-control-solid'
					                                        />
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'select' && (
					                                      <>
					                                        <label
					                                          className='form-label  fs-6 required'
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <div className='position-relative mb-3'>
					                                          <select
					                                            className='form-select form-select-solid form-select'
					                                            name={element.name}
					                                          >
					                                            {element.options.map(
					                                              (option: any, optionIndex: any) => (
					                                                <option
					                                                  key={optionIndex}
					                                                  value={option.value}
					                                                  selected={option.selected}
					                                                >
					                                                  {option.text}
					                                                </option>
					                                              )
					                                            )}
					                                          </select>
					                                          {formElements[index]?.validation &&
					                                            formElements[index]?.validation[0]?.errorMessage && (
					                                              <span
					                                                style={{
					                                                  color:
					                                                    formElements[index]?.validation[0]?.messageColor
					                                                }}
					                                              >
					                                                {formElements[index]?.validation[0]?.errorMessage}
					                                              </span>
					                                            )}
					                                        </div>
					                                      </>
					                                    )}
					                                    {element.type === 'formLogo' && (
					                                      <>
					                                        <div
					                                          className={`d-flex justify-content-${
					                                            element.alignment === 'right'
					                                              ? 'end'
					                                              : element.alignment
					                                          }`}
					                                        >
					                                          <img src={element.imgSrc} alt='logo' />
					                                        </div>
					                                      </>
					                                    )}
					                                    {element.type === 'switch' && (
					                                      <>
					                                        <label
					                                          className='form-label  fs-6  required'
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <div
					                                          key={element.id || index}
					                                          className={`fv-row mb-7 ${
					                                            element.type === 'button' ? 'text-center' : ''
					                                          }`}
					                                          onClick={() => handleFieldClick(index)}
					                                        >
					                                          <div className='form-check form-switch'>
					                                            <input
					                                              className='form-check-input'
					                                              type='checkbox'
					                                              id={`switch-${element.id || index}`}
					                                              checked={element.checked}
					                                              onChange={() => handleSwitchToggle(index)}
					                                            />
					                                          </div>
					                                        </div>
					                                      </>
					                                    )}
					                                    {element.type === 'checkbox' && (
					                                      <>
					                                        <div key={element.id || index} className='fv-row mb-7'>
					                                          <label
					                                            className={`form-label fs-6 required`}
					                                            style={{ color: element.color }}
					                                          >
					                                            {element.label}
					                                          </label>
					                                          {element.options.map((option: any, optionIndex: any) => (
					                                            <div
					                                              key={`${element.type}-${element.id}-${optionIndex}`}
					                                              className='form-check'
					                                            >
					                                              <input
					                                                type='checkbox'
					                                                className='form-check-input'
					                                                id={`${element.name}-${option.value}`}
					                                                value={option.value}
					                                                onChange={() =>
					                                                  handleCheckboxToggle(index, optionIndex)
					                                                }
					                                              />
					                                              <label
					                                                className='form-check-label m-1'
					                                                htmlFor={`${element.name}-${option.value}`}
					                                                style={{ color: element.color }}
					                                              >
					                                                {option.text}
					                                              </label>
					                                            </div>
					                                          ))}
					                                        </div>
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'radio' && (
					                                      <>
					                                        <div key={element.id || index} className='fv-row mb-7'>
					                                          <label
					                                            className={`form-label fs-6 required`}
					                                            style={{ color: element.color }}
					                                          >
					                                            {element.label}
					                                          </label>
					                                          {element.options.map((option: any, optionIndex: any) => (
					                                            <div
					                                              key={`${element.type}-${element.id}-${optionIndex}`}
					                                              className='form-check'
					                                            >
					                                              <input
					                                                type='radio'
					                                                className='form-check-input'
					                                                id={`${element.name}-${option.value}`}
					                                                value={option.value}
					                                                checked={
					                                                  option.selected
					                                                } /* need not note which option was selected while building the form */
					                                                /* onChange={() =>
					                                                  handleRadioToggle(index, optionIndex)
					                                                } */
					                                              />
					                                              <label
					                                                className='form-check-label m-1'
					                                                htmlFor={`${element.name}-${option.value}`}
					                                                style={{ color: element.color }}
					                                              >
					                                                {option.text}
					                                              </label>
					                                            </div>
					                                          ))}
					                                        </div>
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'file' && (
					                                      <>
					                                        <div key={element.id || index} className='fv-row mb-7'>
					                                          <label
					                                            className={`form-label fs-6 required`}
					                                            style={{ color: element.color }}
					                                          >
					                                            {element.label}
					                                          </label>
					                                          <div className='mb-3'>
					                                            <input
					                                              type='file'
					                                              className='form-control'
					                                              id={`${element.name}`}
					                                              onChange={(e) => handleFileUpload(index, e)}
					                                            />
					                                          </div>
					                                        </div>
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'divider' && (
					                                      <hr
					                                        className='my-4'
					                                        style={{
					                                          borderColor: element.color || '#000',
					                                          borderWidth: element.thickness + 'px',
					                                          borderStyle: 'solid'
					                                        }}
					                                      />
					                                    )}
					                                    {element.type === 'textarea' && (
					                                      <>
					                                        <label
					                                          className={`form-label fs-6 required`}
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <textarea
					                                          placeholder={element.placeholder}
					                                          rows={element.rows || 4} // Specify the number of rows
					                                          className='form-control form-control-lg form-control-solid'
					                                        />
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'datetime' && (
					                                      <>
					                                        <label
					                                          className='form-label fs-6 required'
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <div
					                                          key={element.id || index}
					                                          className={`fv-row mb-7 ${
					                                            element.type === 'button' ? 'text-center' : ''
					                                          }`}
					                                          onClick={() => handleFieldClick(index)}
					                                        >
					                                          <div>
					                                            <input
					                                              className='form-control form-control-lg form-control-solid'
					                                              type='datetime-local'
					                                              id={`datetime-${element.id || index}`}
					                                            />
					                                          </div>
					                                        </div>
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'date' && (
					                                      <>
					                                        <label
					                                          className='form-label fs-6 required'
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <div
					                                          key={element.id || index}
					                                          className={`fv-row mb-7 ${
					                                            element.type === 'button' ? 'text-center' : ''
					                                          }`}
					                                          onClick={() => handleFieldClick(index)}
					                                        >
					                                          <div>
					                                            <input
					                                              className='form-control form-control-lg form-control-solid'
					                                              type='date'
					                                              id={`date-${element.id || index}`}
					                                            />
					                                          </div>
					                                        </div>
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'time' && (
					                                      <>
					                                        <label
					                                          className='form-label fs-6 required'
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <div
					                                          key={element.id || index}
					                                          className={`fv-row mb-7 ${
					                                            element.type === 'button' ? 'text-center' : ''
					                                          }`}
					                                          onClick={() => handleFieldClick(index)}
					                                        >
					                                          <div>
					                                            <input
					                                              className='form-control form-control-lg form-control-solid'
					                                              type='time'
					                                              id={`time-${element.id || index}`}
					                                            />
					                                          </div>
					                                        </div>
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'daterange' && (
					                                      <>
					                                        <label
					                                          className='form-label fs-6 required'
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <div
					                                          key={element.id || index}
					                                          className={`fv-row mb-7 ${
					                                            element.type === 'button' ? 'text-center' : ''
					                                          }`}
					                                          onClick={() => handleFieldClick(index)}
					                                        >
					                                          <div className='d-flex'>
					                                            <label>start date</label>
					                                            <input
					                                              type='date'
					                                              name='startDate'
					                                              className='form-control form-control-lg form-control-solid'
					                                            />
					                                            <label>end date</label>
					                                            <input
					                                              type='date'
					                                              name='endDate'
					                                              className='form-control form-control-lg form-control-solid'
					                                            />
					                                          </div>
					                                        </div>
					                                        {formElements[index]?.validation &&
					                                          formElements[index]?.validation[0]?.errorMessage && (
					                                            <span
					                                              style={{
					                                                color:
					                                                  formElements[index]?.validation[0]?.messageColor
					                                              }}
					                                            >
					                                              {formElements[index]?.validation[0]?.errorMessage}
					                                            </span>
					                                          )}
					                                      </>
					                                    )}
					                                    {element.type === 'signature' && (
					                                      <>
					                                        <div className='d-grid'>
					                                          <label
					                                            className='form-label  fs-6 required'
					                                            style={{ color: element.color }}
					                                            onClick={() => handleFieldClick(index)}
					                                          >
					                                            {element.label}
					                                          </label>
					                                          <SignatureCanvas
					                                            penColor={element.color}
					                                            backgroundColor={element.padColor}
					                                            canvasProps={{
					                                              width: element.width,
					                                              height: element.height,
					                                              className: 'sigCanvas'
					                                            }}
					                                          />
					                                          {formElements[index]?.validation &&
					                                            formElements[index]?.validation[0]?.errorMessage && (
					                                              <span
					                                                style={{
					                                                  color:
					                                                    formElements[index]?.validation[0]?.messageColor
					                                                }}
					                                              >
					                                                {formElements[index]?.validation[0]?.errorMessage}
					                                              </span>
					                                            )}
					                                        </div>
					                                      </>
					                                    )}
					                                    {element.type === 'ratings' && (
					                                      <>
					                                        <div>
					                                          <Ratings
					                                            label={element.label}
					                                            color={element.color}
					                                            alignment={element.alignment}
					                                          />
					                                        </div>
					                                      </>
					                                    )}
					                                    {element.type === 'subform' && (
					                                      <>
					                                        <label
					                                          className='form-label  fs-6  required'
					                                          style={{ color: element.color }}
					                                        >
					                                          {element.label}
					                                        </label>
					                                        <div
					                                          key={element.id || index}
					                                          className={`fv-row mb-7 ${
					                                            element.type === 'button' ? 'text-center' : ''
					                                          }`}
					                                          onClick={() => handleFieldClick(index)}
					                                        >
					                                          <div className='form-check form-switch'>
					                                            <input
					                                              className='form-check-input'
					                                              type='checkbox'
					                                              id={`switch-${element.id || index}`}
					                                              checked={element.checked}
					                                              onChange={() => handleSwitchToggle(index)}
					                                            />
					                                          </div>
					                                        </div>
					                                      </>
					                                    )}
					                                  </div>
					                                )}
					                              </Draggable>
					                            )
					                        )}
											
											{formElements.map(
					                          (element: any, index: any) =>
					                            element.type === 'button' && (
					                              // Render the button separately
					                              <div key={element.id || index} className='text-center my-2'>
					                                <button
					                                  style={{
					                                    color: element.btnTxtColor,
					                                    backgroundColor: element.btnBgColor
					                                  }}
					                                  type={element.buttonType}
					                                  id={element.id}
					                                  className={`btn btn-lg ${
					                                    element.buttonType === 'submit' && 'btn-primary'
					                                  } w-100 `}
					                                  onClick={() => handleFieldClick(index)}
					                                >
					                                  {element.text}
					                                </button>
					                              </div>
					                            )
					                        )}
											</form>
										</div>
									</div>
		                		</div>
		              		</div>
		            	</div>
		          	</div>
		          	)}
		      </Droppable>
		    </DragDropContext>
		</>
	)
}

export default FormPage