interface IProps {
  formElements: any
  setFormElements: any
  selectedField: any
  ThemeTextColor: any
}

const FormFields: React.FC<IProps> = ({ThemeTextColor, formElements, setFormElements, selectedField}) => {
	let fields = [
    {
      heading: 'Frequently used',
      color: 'rgb(34, 197, 94)',
      background: 'rgb(240, 253, 244)',
      borderColor: 'rgb(187, 247, 208)',
      subComponent: [
        {
          icon: 'bi bi-pencil-square',
          fieldname: 'Short answer',
          formInput: {
            type: 'text',
            label: 'short answer',
            color: ThemeTextColor,
            placeholder: 'short answer',
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-card-list',
          fieldname: 'checkbox',
          formInput: {
            type: 'checkbox',
            label: 'checkbox',
            color: ThemeTextColor,
            name: 'checkbox',
            options: [
              { value: 'a', text: 'a' },
              { value: 'b', text: 'b' },
              { value: 'c', text: 'c' }
            ],
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-envelope',
          fieldname: 'Email input',
          formInput: {
            type: 'text',
            label: 'Email:',
            placeholder: 'Enter your email',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        }
      ]
    },
    {
      heading: 'Display text',
      color: 'rgb(100, 116, 139)',
      background: 'rgb(248, 250, 252)',
      borderColor: 'rgb(226, 232, 240)',
      subComponent: [
        {
          icon: 'bi bi-type-h1',
          fieldname: 'Heading',
          formInput: {
            type: 'heading',
            formHeading: 'Heading',
            alignment: 'center',
            color: ThemeTextColor,
            size: 'h5',
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-card-text',
          fieldname: 'Paragraph',
          formInput: {
            type: 'subheading',
            formSubHeading: 'Paragraph',
            color: ThemeTextColor,
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-display',
          fieldname: 'Banner',
          formInput: {
            type: 'banner',
            bannerSrc:
              'https://chatbot-project.s3.ap-south-1.amazonaws.com/botgo/assets/Form_Template_01.jpeg',
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        }
      ]
    },
    {
      heading: 'Choices',
      color: 'rgb(245, 158, 11)',
      background: 'rgb(255, 251, 235)',
      borderColor: 'rgb(253, 230, 138)',
      subComponent: [
        {
          icon: 'bi bi-image',
          fieldname: 'Form Logo',
          formInput: {
            type: 'formLogo',
            imgSrc: 'https://devdashboard.botgo.io/media/logos/logo.png',
            alignment: 'center',
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-caret-down-square',
          fieldname: 'Dropdown',
          formInput: {
            type: 'select',
            label: 'dropdown',
            name: 'dropdown',
            color: ThemeTextColor,
            options: [
              { value: 'a', text: 'a' },
              { value: 'b', text: 'b', selected: true },
              { value: 'c', text: 'c' }
            ],
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-toggle-on',
          fieldname: 'Switch',
          formInput: {
            type: 'switch',
            label: 'Toggle',
            color: ThemeTextColor,
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-check-square',
          fieldname: 'Checkboxes',
          formInput: {
            type: 'checkbox',
            label: 'checkbox',
            color: ThemeTextColor,
            name: 'checkbox',
            options: [
              { value: 'a', text: 'a' },
              { value: 'b', text: 'b' },
              { value: 'c', text: 'c' }
            ],
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-circle',
          fieldname: 'radio',
          formInput: {
            type: 'radio',
            label: 'radio',
            color: ThemeTextColor,
            name: 'radio',
            options: [
              { value: 'a', text: 'a' },
              { value: 'b', text: 'b' },
              { value: 'c', text: 'c' }
            ],
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-ui-checks-grid',
          fieldname: 'Sub Form',
          formInput: {
            type: 'subform',
            label: 'Add a Subform',
            color: ThemeTextColor,
            checked: false,
            subformFields: [
              {
                type: 'switch',
                label: 'Expand',
                color: ThemeTextColor,
                checked: true,
                includeOnCondition: {
                  conditionActive: false,
                  basedOnElementOfIndex: null,
                  forValue: 'undefined',
                  comparisionOperation: '='
                },
                fieldId: 2000
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        }
      ]
    },
    {
      heading: 'Date and Time',
      color: 'rgb(168, 85, 247)',
      background: 'rgb(250, 245, 255)',
      borderColor: 'rgb(233, 213, 255)',
      subComponent: [
        {
          icon: 'bi bi-calendar-date',
          fieldname: 'Date-time',
          formInput: {
            type: 'datetime',
            label: 'Date & Time',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-calendar',
          fieldname: 'Date picker',
          formInput: {
            type: 'date',
            label: 'Date',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },

        {
          icon: 'bi bi-clock',
          fieldname: 'Time picker',
          formInput: {
            type: 'time',
            label: 'Time',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        }
      ]
    },
    {
      heading: 'Text',
      color: 'rgb(34, 197, 94)',
      background: 'rgb(240, 253, 244)',
      borderColor: 'rgb(187, 247, 208)',
      subComponent: [
        {
          icon: 'bi bi-card-heading',
          fieldname: 'Short answer',
          formInput: {
            type: 'text',
            label: 'Short answer',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-card-text',
          fieldname: 'Long answer',
          formInput: {
            type: 'textarea',
            label: 'Long answer:',
            rows: 4,
            placeholder: 'Enter your long answer',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        }
      ]
    },
    {
      heading: 'Contact info',
      color: 'rgb(20, 184, 166)',
      background: 'rgb(240, 253, 250)',
      borderColor: 'rgb(153, 246, 228)',
      subComponent: [
        {
          icon: 'bi bi-envelope',
          fieldname: 'Email input',
          formInput: {
            type: 'text',
            label: 'Email:',
            placeholder: 'Enter your email',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-telephone',
          fieldname: 'Phone number',
          formInput: {
            type: 'text',
            label: 'Phone number:',
            placeholder: 'Enter your mobile number',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-geo-alt',
          fieldname: 'Address',
          formInput: {
            type: 'text',
            label: 'Address:',
            placeholder: 'Enter your address',
            color: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        }
      ]
    },
    {
      heading: 'Miscellaneous',
      color: 'rgb(6, 182, 212)',
      background: 'rgb(236, 254, 255)',
      borderColor: 'rgb(165, 243, 252)',
      subComponent: [
        {
          icon: 'bi bi-file-earmark-arrow-up',
          fieldname: 'File upload',
          formInput: {
            type: 'file',
            label: 'file upload',
            color: ThemeTextColor,
            name: 'fileUpload',
            accept: '/* specify accepted file types here, e.g., ".pdf,.docx" */',
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-shield-lock',
          fieldname: 'Captcha',
          formInput: {
            type: 'captcha',
            label: 'Captcha',
            color: ThemeTextColor,
            name: 'Captcha',
            inputTextName: 'Enter the text from the image:',
            inputTextNameColor: ThemeTextColor,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-arrow-bar-right',
          fieldname: 'Divider',
          formInput: {
            type: 'divider',
            color: ThemeTextColor,
            thickness: 1,
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-menu-button-wide-fill',
          fieldname: 'Button',
          formInput: {
            type: 'button',
            text: 'PressMe',
            btnTxtColor: '#ffffff',
            btnBgColor: '#0095e8',
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-vector-pen',
          fieldname: 'Signature',
          formInput: {
            type: 'signature',
            label: 'authorizing authority',
            color: ThemeTextColor,
            padColor: '#f5f8fa',
            alignment: 'left',
            width: 500,
            height: 200,
            validation: [
              {
                rule: 'undefined'
              }
            ],
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        },
        {
          icon: 'bi bi-star',
          fieldname: 'Ratings',
          formInput: {
            type: 'ratings',
            label: 'rate & review this',
            color: ThemeTextColor,
            alignment: 'left',
            includeOnCondition: {
              conditionActive: false,
              basedOnElementOfIndex: null,
              forValue: 'undefined',
              comparisionOperation: '='
            }
          }
        }
      ]
    }
	];
	const cardStyle = {
	    height: '100px', // Set a fixed height for the cards
	    width: '80px',
	    boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)', // Apply a shadow to the cards
	    borderRadius: '5px' // Add some border radius for a rounded appearance
	}
	const FormFieldHandler = (component: any) => {
	    let thefieldindex = formElements.length ? Math.max(...formElements.map((field: any) => field.fieldId)): 0
	    let indexedFormInput = { ...component.formInput, fieldId: thefieldindex + 1 }
	    
      // Check if the component type is 'banner' or 'captcha'
	    if (component.formInput.type === 'banner' || component.formInput.type === 'captcha') {
	      // Find if there's an existing banner or captcha in formElements
	      const existingIndex = formElements.findIndex((element: any) => element.type === component.formInput.type)

	      // If an existing element is found, override it; otherwise, add the new element
	      if (existingIndex !== -1) {console.warn(`${component.formInput.type} is already present.`)
	      } else {setFormElements([...formElements, indexedFormInput]); console.log(`Successfully added ${component.formInput.type} element`)}

	    } else {
	      // For other types, simply add the component to the formElements
	      const updatedFormElements = selectedField !== null
	          ? [...formElements.slice(0, selectedField + 1), indexedFormInput, ...formElements.slice(selectedField + 1)]
	          : [...formElements, indexedFormInput]

	      setFormElements(updatedFormElements)
	      console.log(`Successfully added ${component.formInput.type} element`)
	    }
	}

	return (
		<div>
			{fields.map((section, sectionIndex) => (
		        <div key={sectionIndex}>
		          	<div className='text-sm text-gray-400 font-medium mt-3 mb-3'>{section.heading}</div>
		          	<div className='row'>
		            	{section.subComponent.map((component, componentIndex) => (
			            <div className='col-lg-4 col-md-6 col-auto mb-4 cursor-pointer' key={componentIndex} onClick={() => FormFieldHandler(component)}>
			                <div className='card' style={cardStyle}>
			                  <div className='card-body d-flex flex-column align-items-center'>
			                    <div>
			                      <i
			                        className={`p-3 rounded bg-gray-50 ${component.icon}`}
			                        style={{
			                          color: section.color,
			                          background: section.background,
			                          borderColor: section.borderColor
			                        }}
			                      ></i>
			                    </div>
			                    <div className='text-gray-700 text-xs font-medium text-center leading-3 h-6 items-center mt-3 cursor-pointer'>
			                      {component.fieldname}
			                    </div>
			                  </div>
			                </div>
			            </div>))
		            	}
		          	</div>
		        </div>
		      ))}
		</div>
	)
}

export default FormFields