interface Props {
  closeSaveModal: () => void
  formElements: any
  formJson: any
  setFormJson: any
}

const SaveFormBuilder:React.FC<Props> = ({closeSaveModal}) => {
	const SaveChanges = async () => {
    try {
      /*const inputRes = await SaveFormJson({
        variables: {
          input: {
            formUuid: uuid,
            cardStyle: formJson.cardStyle,
            cardBackground: formJson.cardBackground,
            featuresToUpdate: {
              formData: formElements
            }
          }
        }
      })*/

      closeSaveModal()
      /*if (inputRes.data.updateFormBuilderByFormStringId.message) {
	        toast.success(inputRes.data.updateFormBuilderByFormStringId.message)
	      } else {
	        toast.error('Failed to save changes.')					
	      }*/
	    } catch (err) {
	      console.warn('Sorry! Unexpected Error.')
	      console.error(err)
	    }
	}

	return (
		<div className='card-body'>
	      <p className='fs-5 fw-bold'>Are you sure you want to save these changes?</p>

	      <div className='d-flex justify-content-end mt-10'>
	        <button type='reset' className='btn btn-sm btn-white me-2' onClick={() => closeSaveModal()}>
	          Cancel
	        </button>

	        <button type='submit' className='btn btn-sm btn-primary' onClick={SaveChanges}>
	          <span className='indicator-label'>Save</span>
	        </button>
	      </div>
	    </div>
	)
}

export default SaveFormBuilder