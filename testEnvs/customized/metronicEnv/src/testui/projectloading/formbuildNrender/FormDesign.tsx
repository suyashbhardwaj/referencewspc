import { useState } from 'react';

interface IProps {
  formElements: any
  setFormElements: any
  setFormJson: any
  formJson: any
  setThemeTextColor: any
}

const FormDesign: React.FC<IProps> = ({setThemeTextColor, setFormElements, setFormJson}) => {
	const [selectedTheme, setSelectedTheme] = useState<number | null>(null)
  const formTheme = [
    {
      themeSrc:
        'https://chatbot-project.s3.ap-south-1.amazonaws.com/botgo/assets/form_template_11.png',
      themeName: 'Default',
      customTheme: {
        backgroundImgSrc: '',
        backgroundColor: '#FFFFFF',
        headingColor: '##000000',
        paragraphColor: '#181C32',
        color: '#3F4254',
        btnTextColor: '#FFFFFF',
        btnBgColor: '#4C72FB',
        messageColor: 'red'
      }
    },
    {
      themeSrc: 'https://botgo.io/chatbotTemplates/images/chatbot-category/Feedback-Bot.png',
      themeName: 'Techy',
      customTheme: {
        backgroundImgSrc:
          'https://chatbot-project.s3.ap-south-1.amazonaws.com/botgo/assets/Form_Template_06.jpeg',
        backgroundColor: 'rgba(255, 255, 255, 0)',
        headingColor: '#2C3345',
        paragraphColor: '#57647E',
        color: '#FFFFFF',
        btnTextColor: '#FFFFFF',
        btnBgColor: '#0099FF',
        messageColor: 'yellow'
      }
    },
    {
      themeSrc: 'https://botgo.io/chatbotTemplates/images/chatbot-category/Feedback-Bot.png',
      themeName: 'Brick Wall',
      customTheme: {
        backgroundImgSrc:
          'https://chatbot-project.s3.ap-south-1.amazonaws.com/botgo/assets/Form_Template_08.jpeg',
        backgroundColor: 'rgba(55, 15, 1, 0.8)',
        headingColor: '#FFFFFF',
        paragraphColor: '#FFFFFF',
        color: '#FFFFFF',
        btnTextColor: '#666666',
        btnBgColor: '#F5F5F5',
        messageColor: 'red'
      }
    },
    {
      themeSrc: 'https://botgo.io/chatbotTemplates/images/chatbot-category/Feedback-Bot.png',
      themeName: 'Purple Galaxy',
      customTheme: {
        backgroundImgSrc:
          'https://chatbot-project.s3.ap-south-1.amazonaws.com/botgo/assets/Form_Template_03.jpeg',
        backgroundColor: '#FFFFFF',
        headingColor: '#0D016A',
        paragraphColor: '#0D016A',
        color: '#0D016A',
        btnTextColor: '#fff',
        btnBgColor: '#9836DE',
        messageColor: 'red'
      }
    },
    {
      themeSrc: 'https://botgo.io/chatbotTemplates/images/chatbot-category/Feedback-Bot.png',
      themeName: 'Blue Ocean',
      customTheme: {
        backgroundImgSrc:
          'https://chatbot-project.s3.ap-south-1.amazonaws.com/botgo/assets/Form_Template_07.jpeg',
        backgroundColor: '#FFFFFF',
        headingColor: '#2A6368',
        paragraphColor: '#2A6368',
        color: '2C3345',
        btnTextColor: '#FFFFFF',
        btnBgColor: '#249BB4',
        messageColor: 'red'
      }
    }
  ]
  const handleThemeClick = (customTheme: any) => {
    setThemeTextColor(customTheme.color)
    setFormElements((prevFormElements: any) =>
      prevFormElements.map((element: any) => {
        const updatedElement = { ...element }
        if (updatedElement.type === 'divider') {
          updatedElement.color = customTheme.color
          updatedElement.thickness = customTheme.dividerThickness || 1
        } else if (['heading', 'subheading'].includes(updatedElement.type)) {
          updatedElement.color = customTheme.color
        } else if (['text', 'textarea'].includes(updatedElement.type)) {
          updatedElement.color = customTheme.color
          // updatedElement.validation[0].messageColor = customTheme.messageColor
          updatedElement.placeholderColor = customTheme.placeholderColor || '#999'
        } else if (['button'].includes(updatedElement.type)) {
          updatedElement.btnTxtColor = customTheme.btnTextColor
          updatedElement.btnBgColor = customTheme.btnBgColor
        } else if (updatedElement.type === 'datetime') {
          updatedElement.color = customTheme.color
          // updatedElement.validation[0].messageColor = customTheme.messageColor
        } else if (updatedElement.type === 'date') {
          updatedElement.color = customTheme.color
          // updatedElement.validation[0].messageColor = customTheme.messageColor
        } else if (updatedElement.type === 'time') {
          updatedElement.color = customTheme.color
          // updatedElement.validation[0].messageColor = customTheme.messageColor
        } else if (updatedElement.type === 'radio') {
          updatedElement.color = customTheme.color
          // updatedElement.validation[0].messageColor = customTheme.messageColor
        } else if (updatedElement.type === 'checkbox') {
          updatedElement.color = customTheme.color
          // updatedElement.validation[0].messageColor = customTheme.messageColor
        } else if (updatedElement.type === 'select') {
          updatedElement.color = customTheme.color
          // updatedElement.validation[0].messageColor = customTheme.messageColor
        } else if (updatedElement.type === 'switch') {
          updatedElement.color = customTheme.color
          // updatedElement.validation[0].messageColor = customTheme.messageColor
        }
        return updatedElement
      })
    )
    setFormJson((prevFormJson: any) => {
      // Copy the existing formJson
      const updatedFormJson = { ...prevFormJson }

      // Update or add the backgroundImg and backgroundColor keys
      updatedFormJson.cardBackground = customTheme.backgroundImgSrc
      updatedFormJson.cardStyle = customTheme.backgroundColor

      return updatedFormJson
    })
    setSelectedTheme(formTheme.findIndex((theme) => theme.customTheme === customTheme))
  }
  
	return (
		<>
		{formTheme.map((theme: any, index: any) => (
        <div
          className={`card m-2 mb-7 text-decoration-none ${
            selectedTheme === index ? 'selected-theme' : ''
          }`}
          role='button'
          style={{
            width: '20rem',
            marginBottom: '1rem',
            overflow: 'hidden',
            boxShadow: '2px 2px 9px rgba(204, 204, 204)',
            transform: 'scale(1)',
            cursor: 'pointer'
          }}
          key={index}
          onClick={() => handleThemeClick(theme.customTheme)}
        >
          <img
            className='card-img-top'
            src={theme.themeSrc}
            alt={`Bot Card Logo - ${theme.themeName}`}
          />
          <button className='btn btn-secondary'>{theme.themeName}</button>
        </div>
      ))}
		</>
	)
}

export default FormDesign