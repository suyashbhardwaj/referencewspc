import { useRef } from 'react'
interface IProps {
  formElements: any
  setFormElements: any
  selectedElement: any
  selectedField: any
  setTogglerFieldIndex: any
  setSubFormFieldIndexes: any
}

const SubFormSetup: React.FC<IProps> = ({
  formElements,
  setFormElements,
  selectedElement,
  selectedField,
  setSubFormFieldIndexes
}) => {
  let subformfieldscounter = useRef(1)
  const FormFieldHandler = (component: any, index: number) => {
    let thefieldindex = formElements.length
      ? Math.max(...formElements.map((field: any) => field.fieldId))
      : 0
    let conditionalInclusionDependency = /* selectedElement.includeOnCondition */ {
      conditionActive: true,
      basedOnElementOfIndex: 2000,
      comparisionOperation: '=',
      forValue: 'true'
    }

    let indexedFormInput = {
      ...component,
      copiedFromFieldAtFieldId: component.fieldId,
      includeOnCondition: conditionalInclusionDependency,
      fieldId: thefieldindex + subformfieldscounter.current++
    }

    setSubFormFieldIndexes((prev: any) => {
      const udpatedsffi = [...prev, component.fieldId]
      return udpatedsffi
    })

    // Check if the component type is 'banner' or 'captcha'
    if (component.type === 'banner' || component.type === 'captcha') {
      // Find if there's an existing banner or captcha in formElements
      const existingIndex = formElements.findIndex(
        (element: any) => element.type === component.type
      )

      // If an existing element is found, override it; otherwise, add the new element
      if (existingIndex !== -1) {
        console.log(`${component.type} is already present.`)
      } else {
        setFormElements([...formElements, indexedFormInput])
        console.log(`Successfully added ${component.type} element`)
      }
    } else {
      // For other types, simply add the component to the formElements
      setFormElements((prev: any) => {
        /* const updatedFE = [...prev.slice(0, selectedField + 1), indexedFormInput, ...prev.slice(selectedField + 1)] */
        const updatedFE = [...prev]
        const selectedFieldObject = updatedFE[selectedField]
        if (!Object.isExtensible(selectedFieldObject)) {
          updatedFE[selectedField] = {
            ...selectedFieldObject
          }
        }
        updatedFE[selectedField].subformFields = [
          ...updatedFE[selectedField].subformFields,
          indexedFormInput
        ]
        return updatedFE
      })
      console.log(`Successfully added ${component.type} element`)
    }
  }

  const handleDelete = (component: any, index: number) => {
    setSubFormFieldIndexes((prev: any) => {
      let udpatedsffi = [...prev]
      udpatedsffi = udpatedsffi.filter((fieldId) => fieldId !== component.fieldId)
      return udpatedsffi
    })

    setFormElements((prev: any) => {
      const updatedFE = [...prev]
      const selectedFieldObject = updatedFE[selectedField]
      if (!Object.isExtensible(selectedFieldObject)) {
        updatedFE[selectedField] = {
          ...selectedFieldObject
        }
      }
      updatedFE[selectedField].subformFields = updatedFE[selectedField].subformFields.filter(
        (sff: any) => sff.copiedFromFieldAtFieldId !== index
      )
      return updatedFE
    })
    console.log(
      `selected field with fieldId ${component.copiedFromFieldAtFieldId} has been removed from the subform`
    )
  }

  return (
    <div>
      <label className='form-label'>Subform Fields:</label>
      <div>
        <table>
          <tbody>
            {formElements.map((element: any, index: any) => {
              return (
                <tr
                  className={
                    element.label !== selectedElement.label && element.type !== 'button'
                      ? ``
                      : `d-none`
                  }
                >
                  <td className='text-center'>
                    <input
                      type='checkbox'
                      className='btn-check mx-1'
                      name='togglerField'
                      autoComplete='off'
                      id={`${index}-subform-field`}
                      /* checked = {index === selectedElement.includeOnCondition.basedOnElementOfIndex && true } */
                      checked={selectedElement.subformFields.find(
                        (se: any) => se.copiedFromFieldAtFieldId === formElements[index].fieldId
                      )}
                      onChange={(e) => {
                        console.log(`checked ${e.currentTarget.checked} on ${e.currentTarget.id}`)
                        e.currentTarget.checked
                          ? FormFieldHandler(formElements[index], index)
                          : /* updated[selectedField].subformFields = [
                                ...updated[selectedField].subformFields,
                                updated[index]
                              ] */ /* updated[selectedField].subformFields = updated[
                                selectedField
                              ].subformFields.filter((fld, ix) => ix === index) */
                            /* handleDelete(formElements[index]) */
                            handleDelete(formElements[index], index)
                      }}
                    />
                    <label className='btn btn-secondary' htmlFor={`${index}-subform-field`}>
                      {element.label ? element.label : element.type}
                    </label>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default SubFormSetup