import { MultiSelect } from 'react-multi-select-component'
import { useEffect, useState } from 'react';

interface IProps {
  formElements: any
  setFormElements: any
  selectedElement: any
  selectedField: any
  togglerFieldIndex: any
  setTogglerFieldIndex: any
}

const FieldConditionalCustomization: React.FC<IProps> = ({formElements, setFormElements, selectedElement, togglerFieldIndex, setTogglerFieldIndex, selectedField}) => {
	const [selectedx, setSelectedx] = useState([{ label: '', value: '' }])
		useEffect(() => {
		    setFormElements((prev: any) => {
		      const updated = [...prev]
		      let str = ''
		      selectedx.forEach((entry: any) => (str += entry.value + ';'))
		      const arrayVals = selectedx.map((entry: any) => entry.value)
		      updated[selectedField].includeOnCondition = {
		        ...updated[selectedField].includeOnCondition,
		        forValue: { selectedx, arrayVals, str }
		      }
		      return updated
		    })
	  }, [selectedx])

	return (
		<div>
	      	<label className='form-label'>Based On Item:</label>
	      	<div>
	        	<table>
	          		<tbody>
	            	{formElements.map((element: any, index: any) => {
	              		return (
	              			<tr className={element.label !== selectedElement.label ? `` : `d-none`}>
			                  {/* the first column: radio buttons of fields currrently present on the form (except the selected one) */}
				                <td className='text-center' colSpan={element.fieldId !== togglerFieldIndex ? 3 : 1}>
				                    <input
				                      type='radio'
				                      className='btn-check mx-1'
				                      name='togglerField'
				                      autoComplete='off'
				                      id={`${index}-dependency-field-label`}
				                      checked={
				                        selectedElement.includeOnCondition.basedOnElementOfIndex &&
				                        element.fieldId === selectedElement.includeOnCondition.basedOnElementOfIndex
				                      }
				                      onChange={(e:any) => {
				                        setTogglerFieldIndex(element.fieldId)
				                        setFormElements((prev: any) => {
				                          const updated = [...prev]
				                          const selectedFieldObject = updated[selectedField]
				                          if (!Object.isExtensible(selectedFieldObject)) {
				                            updated[selectedField] = {
				                              ...selectedFieldObject
				                            }
				                          }
				                          updated[selectedField].includeOnCondition = {
				                            ...updated[selectedField].includeOnCondition,
				                            conditionActive: true,
				                            basedOnElementOfIndex: element.fieldId
				                          }
				                          return updated
				                        })
				                      }}
				                    />
				                    <label className='btn btn-secondary' htmlFor={`${index}-dependency-field-label`}>
			                      		{element.label ? element.label : element.type}
			                    	</label>
			                  	</td>

			                  	{/* the second column for selection of an operator for condition match with value in toggler field */}
				                {element.fieldId === togglerFieldIndex && (
				                    <>
				                      <td colSpan={1}>
				                        <select
				                          name='comparision'
				                          className={`form-select form-select-lg`}
				                          onChange={(e) => {
				                            console.dir(e.target.value)
				                            setFormElements((prev: any) => {
				                              const updated = [...prev]
				                              updated[selectedField].includeOnCondition = {
				                                ...updated[selectedField].includeOnCondition,
				                                comparisionOperation: e.target.value
				                              }
				                              return updated
				                            })
				                          }}
				                        >
				                          {(element.type === 'checkbox'
				                            ? ['=', '!=', 'has selections']
				                            : ['<', '>', '=', '!=']
				                          ).map((op: string) => (
				                            <option
				                              value={op}
				                              selected={
				                                op === selectedElement.includeOnCondition.comparisionOperation
				                              }
				                            >
				                              {op}
				                            </option>
				                          ))}
				                        </select>
				                      </td>
				                    </>
				                )}

				                {/* the third field: text/select input(s) the value(s) to be matched with */}
				                {element.fieldId === togglerFieldIndex &&
				                    (element.type === 'radio' ||
				                    element.type === 'select' ||
				                    element.type === 'switch' ? (
				                      <td colSpan={1}>
				                        <select
				                          name={`${index}-dependency-value`}
				                          className={`form-select form-select-lg`}
				                          id={`${index}-dependency-field-value`}
				                          value={
				                            element.fieldId ===
				                            selectedElement.includeOnCondition.basedOnElementOfIndex
				                              ? selectedElement.includeOnCondition.forValue &&
				                                selectedElement.includeOnCondition.forValue
				                              : ''
				                          }
				                          onChange={(e) => {
				                            console.dir(e.target.value)
				                            setFormElements((prev: any) => {
				                              const updated = [...prev]
				                              updated[selectedField].includeOnCondition = {
				                                ...updated[selectedField].includeOnCondition,
				                                forValue: e.currentTarget.value
				                              }
				                              return updated
				                            })
				                          }}
				                        >
				                          {element.type === 'switch' ? (
				                            <>
				                              <option value='true'>true</option>
				                              <option value='false'>false</option>
				                            </>
				                          ) : (
				                            element.options.map((option: any) => (
				                              <option
				                                value={option.text}
				                                selected={
				                                  option.text ===
				                                  (element.fieldId ===
				                                  selectedElement.includeOnCondition.basedOnElementOfIndex
				                                    ? selectedElement.includeOnCondition.forValue &&
				                                      selectedElement.includeOnCondition.forValue
				                                    : '')
				                                    ? true
				                                    : false
				                                }
				                              >
				                                {option.text}
				                              </option>
				                            ))
				                          )}
				                        </select>
				                      </td>
				                    ) : element.type === 'checkbox' ? (
				                      <>
				                        <td colSpan={1}>
				                          <MultiSelect
				                            options={element.options.map((option: any) => ({
				                              label: option.text,
				                              value: option.text
				                            }))}
				                            value={selectedx}
				                            onChange={setSelectedx}
				                            labelledBy='Select'
				                          />
				                        </td>
				                      </>
				                    ) : (
				                      <>
				                        <td colSpan={1} className='w-50'>
				                          <input
				                            type='text'
				                            className='form-control mx-1'
				                            placeholder='for value'
				                            name={`${index}-dependency-value`}
				                            disabled={element.fieldId !== togglerFieldIndex}
				                            id={`${index}-dependency-field-value`}
				                            value={
				                              element.fieldId ===
				                              selectedElement.includeOnCondition.basedOnElementOfIndex
				                                ? selectedElement.includeOnCondition.forValue &&
				                                  selectedElement.includeOnCondition.forValue
				                                : ''
				                            }
				                            onInput={(e) => {
				                              console.dir(e.currentTarget)
				                              setFormElements((prev: any) => {
				                                const updated = [...prev]

				                                updated[selectedField].includeOnCondition = {
				                                  ...updated[selectedField].includeOnCondition,
				                                  forValue: e.currentTarget.value
				                                }
				                                return updated
				                              })
				                            }}
				                          />
				                        </td>
				                      </>
				                    ))}
				                    </tr>
				                    )
            })}
          </tbody>
        </table>
      </div>
    </div>
	)
}

export default FieldConditionalCustomization