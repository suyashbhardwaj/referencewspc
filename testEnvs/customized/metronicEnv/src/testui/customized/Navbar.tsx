import MenuItems from "./MenuItems";

const menuitems = [
  {
    title: "Home",
  },
  {
    title: "Services",
    submenu: [
      {
        title: "Web Design",
      },
      {
        title: "Web Development",
        submenu: [
          {
            title: "Frontend",
          },
          {
            title: "Backend",
            submenu: [
              {
                title: "NodeJS",
              },
              {
                title: "PHP",
                url: "php",
              },
            ],
          },
        ],
      },
      {
        title: "SEO",
      },
    ],
  },
  {
    title: "About US",
    submenu: [
    		{
    			title: "Who we are"
    		},
    		{
    			title: "Our values"
    		}
    	]
  },
];

const Navbar = () => {
	return (
		<div>
			<nav>
				<ul className="menus">
					{
						menuitems.map((menu, index) => {
							const depthLevel = 0;
							return <MenuItems item={menu} key = {index} depthLevel={depthLevel}/>
						})
					}					
				</ul>
			</nav>
		</div>
	)
}

export default Navbar