import {useState, useEffect, useRef} from 'react'
import Dropdown from './Dropdown'

interface MenuItemsProps {
	item: any
	key: number
	depthLevel: number
}

const MenuItems: React.FC<MenuItemsProps> = ({item, depthLevel}) => {
	const [dropdown, setDropdown] = useState(false);
	let ref = useRef<any>();
	
	useEffect(()=>{
		const handler = (event: any) => { if(dropdown && ref.current && !ref.current.contains(event.target)) {setDropdown(false)}}
		document.addEventListener("mousedown",handler);
		document.addEventListener("touchstart",handler);
		return() => {
			document.addEventListener("mousedown",handler);
			document.addEventListener("touchstart",handler);
			};
		},[dropdown]);

	const onMouseEnter = () => {
		window.innerWidth>960 && setDropdown(true);
	}

	const onMouseLeave = () => {
		window.innerWidth>960 && setDropdown(false);
	}

	return (
		<li className='menu-items' ref={ref} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
			{item.submenu ? (<>
				<button type="button" aria-haspopup="menu" aria-expanded = {dropdown?"true":"false"} onClick={()=>setDropdown((prev)=>!prev)}>
					{item.title}{" "}
					{depthLevel>0 ? <span>&raquo;</span>: <span className='arrow'/>}
				</button>
				<Dropdown depthLevel={depthLevel} submenus={item.submenu} dropdown={dropdown}/>
			</>)
			:(<a href="/#">{item.title}</a>)}
		</li>
	)
}

export default MenuItems