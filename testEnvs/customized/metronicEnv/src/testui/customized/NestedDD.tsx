import './NestedDD.css'
import { useEffect, useRef, useState } from 'react';

interface NestedDDProps {
menuItemsData: any
}
interface MenuItemsProps {
	items: any
	depthLevel: number
}
interface DropdownProps {
	submenus: any
	dropdown: any
	depthLevel: number
}

const Dropdown: React.FC<DropdownProps> = ({ submenus, dropdown, depthLevel }) => {
  depthLevel = depthLevel + 1;
  const dropdownClass = depthLevel > 1 ? "dropdown-submenu" : "";

  return (
    <ul className={`dropdown ${dropdownClass} ${dropdown ? "show" : ""}`}>
      {submenus.map((submenu: any, index: number) => (
        <MenuItems items={submenu} key={index} depthLevel={depthLevel} />
      ))}
    </ul>
  );
};
const MenuItems: React.FC<MenuItemsProps> = ({ items, depthLevel }) => {
  const [dropdown, setDropdown] = useState(false);
  let ref = useRef<any>();

  useEffect(() => {
    const handler = (event: any) => {
      if (dropdown && ref.current && !ref.current.contains(event.target)) {
        setDropdown(false);
      }
    };
    document.addEventListener("mousedown", handler);
    document.addEventListener("touchstart", handler);
    return () => {
      // Cleanup the event listener
      document.removeEventListener("mousedown", handler);
      document.removeEventListener("touchstart", handler);
    };
  }, [dropdown]);

  const onMouseEnter = () => {
    setDropdown(true);
  };

  const onMouseLeave = () => {
    setDropdown(false);
  };

  const toggleDropdown = () => {
    setDropdown((prev) => !prev);
  };

  const closeDropdown = () => {
    dropdown && setDropdown(false);
  };

  return (
    <li
      className="menu-items"
      ref={ref}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onClick={closeDropdown}>
      {items.url && items.submenu ? (
        <>
          <button
            type="button"
            aria-haspopup="menu"
            aria-expanded={dropdown ? "true" : "false"}
            onClick={() => toggleDropdown()}>
            <a href={items.url}>{items.title}</a>
            {depthLevel > 0 ? <span>&raquo;</span> : <span className="arrow" />}
          </button>
          <Dropdown
            depthLevel={depthLevel}
            submenus={items.submenu}
            dropdown={dropdown}
          />
        </>
      ) : !items.url && items.submenu ? (
        <>
          <button
            type="button"
            aria-haspopup="menu"
            aria-expanded={dropdown ? "true" : "false"}>
            {items.title}
            {depthLevel > 0 ? <span>&raquo;</span> : <span className="arrow" />}
          </button>
          <Dropdown
            depthLevel={depthLevel}
            submenus={items.submenu}
            dropdown={dropdown}
          />
        </>
      ) : (
        <a href={items.url}>{items.title}</a>
      )}
    </li>
  );
};

const NestedDD: React.FC<NestedDDProps> = ({menuItemsData}) => {
	const depthLevel = 0;
	return (
		<div>
	    	<header>
		    	<div className="nav-area p-0">
			        {/*<a href="/" className="logo">
			          Logo
			        </a>*/}
		        	{/* for large screens */}
		        	<nav className="desktop-nav">
				    	<ul className="menus m-0 p-0">
				        {menuItemsData.map((menu: any, index: number) => {
				          return <MenuItems items={menu} key={index} depthLevel={depthLevel} />;
				        })}
				    	</ul>
				    </nav>
			        {/* for small screens */}
			        {/*<MobileNav />*/}
		      	</div>
		    </header>
	    </div>
	)
}

export default NestedDD