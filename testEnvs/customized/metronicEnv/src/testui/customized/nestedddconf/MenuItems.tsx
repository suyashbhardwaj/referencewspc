export const menuItemsData = [
  {
    theFieldId:0,
    title: "Documentary",
    url: "/page/documentary",
  },
  {
    theFieldId:1,
    title: "Filmein",
    url: "/page/movies",
    submenu: [
      {
        theFieldId:2,
        title: "Hindi Movies",
        url: "/page/hindi/movies",
        submenu: [
          {
            theFieldId:3,
            title: "Action Movies",
            url: "/page/hindi/movies/action",
            submenu: [
              {
                theFieldId:4,
                title: "2021",
                url: "/page/hindi/movies/action/2021",
              },
              ]
          },
          {
            theFieldId:5,
            title: "Romantic Movies",
            url: "/page/hindi/movies/romantic",
            submenu: [
              {
                theFieldId:6,
                title: "Adult",
                url: "/page/hindi/movies/romantic/adult",
              },
              ]
          },
          ] 
      },
      {
        theFieldId:7,
        title: "Telegu Movies",
        url: "/page/hindi/movies",
        submenu: [
          {
            theFieldId:8,
            title: "Action Movies",
            url: "/page/hindi/movies/action",
          },
          ] 
      },
      ]
  },
  {
    theFieldId:9,
    title: "series",
    url: "/about",
    submenu: [
      {
        theFieldId:10,
        title: "Comedy",
        url: "/page/comedy",
      },
      ]
  }
  ];