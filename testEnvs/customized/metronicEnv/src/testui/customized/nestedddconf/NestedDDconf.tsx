import NestedDD from '../NestedDD';
import { useEffect, useRef, useState } from 'react';
/*import { menuItemsData } from './MenuItems';*/

interface NestedThingProps {
  items: any
  setItems: any
  level: number
}

const NestedDDconfigure: React.FC<NestedThingProps> = ({ items, setItems, level }) => {

  console.log(items, "props received");
  const lastAssignedFieldId = useRef(0);
  
  function nestInTheFieldWithId(fieldId: number, menuItems: any) {
    for (let menuItem of menuItems) {
      if (menuItem.theFieldId === fieldId) {
        menuItem.submenu = [{theFieldId:lastAssignedFieldId.current+1, title:"sample", url:""}]
        lastAssignedFieldId.current++;
        return menuItems;
        }
      if (menuItem.submenu) {
        const updatedSubmenu = nestInTheFieldWithId(fieldId, menuItem.submenu);
        if (updatedSubmenu) {
          menuItem.submenu = updatedSubmenu; // Update submenu if needed
          return menuItems;
	    	}
	      }
	  	}
  	return menuItems;
	}

  function addToTheFieldWithId(fieldId: number, menuItems: any) {
    for(let menuItem of menuItems) {
      if(menuItem.theFieldId === fieldId) {
        menuItems.push({theFieldId: lastAssignedFieldId.current+1, title: 'sample', url:''})
        lastAssignedFieldId.current++;
        return menuItems;
        }
      if(menuItem.submenu) {
        const updatedSubmenu = addToTheFieldWithId(fieldId, menuItem.submenu);
        if (updatedSubmenu) {
          menuItem.submenu = updatedSubmenu; // Update submenu if needed
          return menuItems;
	    	  }
	      }
	  	}
  	return menuItems;
	}

  function updateTheFieldWithId(fieldId: number, menuItems: any, newTitle = null) {
    for (const menuItem of menuItems) {
      if (menuItem.theFieldId === fieldId) {
        if (newTitle !== null) {
          menuItem.title = newTitle;
        }
        return menuItems; // Return the entire array with the updated object
      }
      if (menuItem.submenu) {
        const updatedSubmenu = updateTheFieldWithId(fieldId, menuItem.submenu, newTitle);
        if (updatedSubmenu) {
          menuItem.submenu = updatedSubmenu; // Update submenu if needed
          return menuItems; // Return the entire array with the updated submenu
        }
      }
    }
    return null; // Field with specified ID not found
  }

  const addBlankItem = (nextToTheTheFieldWithId: number) => {
    console.log(`tried adding a new item next to the field with id ${nextToTheTheFieldWithId}`);
    setItems((prev: any)=> {
      /*const updated = [...prev, {theFieldId: lastAssignedFieldId.current+1, title: '', url:''}];
      lastAssignedFieldId.current++;
      return updated;*/
    	return addToTheFieldWithId(nextToTheTheFieldWithId, prev)
    });
  };

  const addSubArray = (underTheFieldWithId: number) => {
    console.log(`tried adding a new menu under the field with id ${underTheFieldWithId}`);
    setItems((prev: any)=> {
    /*const updatedOne = prev.map((x:any)=>x.theFieldId===underTheFieldWithId?{...x, submenu:[{theFieldId:lastAssignedFieldId.current+1, title:"", url:""}]}:x)
    lastAssignedFieldId.current++;
    return updatedOne;*/

    return nestInTheFieldWithId(underTheFieldWithId, prev);
    
    });
  };

  const handleInputChange = (forFieldWithId: number, e:any) => {
    setItems((prev: any)=>{
      const updated = updateTheFieldWithId(forFieldWithId, prev, e.target.value)
      return updated;
    })
  }

  // console.log(items.map(t:any => t))



  return(
    <div>
      {items?.map((item: any) => {
          return (<>
            <div className='d-flex align-items-center' style={{ marginLeft: 150 * level }}>
                <input className = "form-control w-200px" type="text" placeholder={item.title} onChange={(e)=>handleInputChange(item.theFieldId, e)}/>
                <button className='btn btn-link' onClick={()=>addBlankItem(item.theFieldId)}><i className="mx-2 fs-2 bi bi-plus-lg"></i></button>
                <button className='btn btn-link' onClick={()=>addSubArray(item.theFieldId)}><i className="mx-2 fs-2x bi bi-node-plus-fill"></i></button>
            </div>
            {item.submenu && <NestedDDconfigure items={item.submenu} setItems = {setItems} level={level + 1}/>}
          </>);
        })
      }
    </div>
    )
}

const NestedDDconf = () => {
	const [items, setItems] = useState([{ theFieldId:0, title: "sample", url: "" }])


	/*const [items, setItems] = useState(menuItemsData)*/
  useEffect(()=>{
    console.log("rerendered")
  },[items])
	return (
		<div className='mx-10 mt-10'>
      <div className='row'>
    
    		<div className='col-6'>
    			<NestedDDconfigure items={items} setItems = {setItems} level={0}/>    
    		</div>

    		<div className='col-6 d-flex justify-content-center bg-secondary'>
      		<div className='mt-8'>
	         	<NestedDD menuItemsData = {items}/>
      		</div>
    		</div>

      </div>
	  </div>
	)
}

export default NestedDDconf