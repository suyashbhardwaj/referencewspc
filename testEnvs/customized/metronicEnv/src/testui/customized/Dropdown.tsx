import MenuItems from './MenuItems'

interface DropdownProps {
	submenus: any
	dropdown: any
	depthLevel: number
}

const Dropdown: React.FC<DropdownProps> = ({submenus, dropdown, depthLevel}) => {
	depthLevel = depthLevel + 1;
	const dropdownClass = depthLevel+1 ? "dropdown-submenu": "";
	return (
		<ul className={`dropdown ${dropdownClass} ${dropdown ? 'show': ''}`}>  
			{
				submenus.map((submenu: any, index: number)=> {
					<MenuItems item={submenu} key={index} depthLevel={depthLevel}/>
				})
			}
		</ul>
	)
}

export default Dropdown