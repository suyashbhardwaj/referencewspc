import TelephoneInput from './TelephoneInput';
import SearchKeyUtility from './SearchKeyUtility';
import Chatai from './Chatai';
import CreateTicketModal from './CreateTicketModal';
import ListOnlyLeafNodes from './ListOnlyLeafNodes';
import Timeline from './Timeline';
import FormBuild from '../projectloading/formbuildNrender/FormBuild';

const TestSomeUI = () => {
  return (
    <div className = "container-fluid mt-10">
      {/*<TelephoneInput/>
      <hr/>
      <SearchKeyUtility/>*/}

      {/*<Chatai/>*/}
      {/*<CreateTicketModal/>*/}
      {/*<ListOnlyLeafNodes/>*/}
      {/*<Timeline/>*/}
      <FormBuild/>
    </div>
  );
};

export default TestSomeUI;