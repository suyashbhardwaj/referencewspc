import React, { useState } from 'react'
import * as Yup from 'yup'
import { useFormik } from 'formik'
/* import { useMutation } from '@apollo/client'
import { CREATE_TEAM } from '../../graphql/mutation'
import { GET_TEAMS } from '../../graphql/query' */
import clsx from 'clsx'

interface IProps {
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
  closeCreateTeamModal: () => void
  ticketlist: any
  setTicketList: any
}

const createTicketSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, 'Minimum 3 characters')
    .max(50, 'Maximum 50 characters')
    .required('User name is required'),
  email: Yup.string().min(3, 'Minimum 3 characters').max(50, 'Maximum 50 characters'),
  contact: Yup.string().min(3, 'Minimum 3 characters').max(50, 'Maximum 50 characters'),
  subject: Yup.string()
    .min(3, 'Minimum 3 characters')
    .max(50, 'Maximum 50 characters')
    .required('a summarized issue desc is needed'),
  category: Yup.string().min(3, 'Minimum 3 characters').max(50, 'Maximum 50 characters'),
  priority: Yup.string().required('some priority has to be provided'),
  description: Yup.string(),
  enclosed: Yup.string(),
  additionalInfo: Yup.string()
})

const CreateTicketModal: React.FC<IProps> = ({
  setIsLoading,
  closeCreateTeamModal,
  setTicketList
}) => {
  const [loadingOl, setLoading] = useState(false)
  /* const [createTeam, { loading }] = useMutation(CREATE_TEAM, {
    refetchQueries: [{ query: GET_TEAMS }]
  }) */
  const [status, setStatus] = useState('')
  const [submitting, setSubmitting] = useState(false)

  const initialValues = {
    name: '',
    email: '',
    contact: '',
    subject: '',
    category: '',
    priority: '',
    description: '',
    enclosed: '',
    additionalInfo: ''
  }

  const submitForm = async (val: any) => {
    const ticket2bPushed = {
      ticketId: '314d-23cr-vsk3-ld3q',
      raisedByUser: {
        name: val.variables.input.name,
        email: val.variables.input.email,
        contact: val.variables.input.contact
      },
      ticketDet: {
        subject: val.variables.input.subject,
        category: val.variables.input.category,
        priority: val.variables.input.priority,
        description: val.variables.input.description
      },
      enclosed: val.variables.input.enclosed,
      additionalInfo: val.variables.input.additionalInfo,
      pickedByAgent: '',
      resolutionStatus: 'open',
      progress: [
        { action: 'comprehend the issue', currentlyAt: true, iterationNo: 0 },
        { action: 'figure out root cause', currentlyAt: false, iterationNo: 0 },
        { action: 'formulate a solution', currentlyAt: false, iterationNo: 0 },
        { action: 'work on it', currentlyAt: false, iterationNo: 0 },
        { action: 'done', currentlyAt: false, iterationNo: 0 }
      ]
    }

    const promise = new Promise((resolve, reject) => {
      console.log('resolved with ', resolve)
      console.log('rejected with ', reject)
    })
    console.dir(val)
    try {
      const response = await axios
        /* .post(`${process.env.REACT_APP_SET_TICKET_URL}`, ticket2bPushed, { */
        .get(`${process.env.REACT_APP_SET_TICKET_URL}`)
        .then((res: any) => {
          console.log(res)
        })
    } catch (error) {
      // Handle errors
      console.error('Error making the POST request:', error)
      // Set any status or perform actions based on the error
      setStatus('Error submitting the form')
    } finally {
      // Set submitting to false to enable the form again
      setSubmitting(false)
    }

    setTicketList((prevList: any) => {
      const updatedList = [...prevList]
      updatedList.push(ticket2bPushed)
      return updatedList
    })
    setLoading(false)
    closeCreateTeamModal()
    return promise
  }

  const formik = useFormik({
    initialValues,
    enableReinitialize: true,
    validationSchema: createTicketSchema,
    /* onSubmit: (values, { resetForm }) => {
      setIsLoading(true)
      createTeam({
        variables: {
          input: {
            name: values.name,
            description: values.description
          }
        }
      })
        .then((res: any) => {
          setIsLoading(false)
          resetForm()
          closeCreateTeamModal()
          if (res.data.createTeam.status >= 200 && res.data.createTeam.status < 300) {
            toast.success(res.data.createTeam.message)
          } else {
            toast.error(res.data.createTeam.message)
          }
        })
        .catch((err) => {
          setIsLoading(false)
          toast.error('Sorry! Unexpected Error.')
          console.error(err)
        })
    } */
    onSubmit: (values, { setStatus, setSubmitting }) => {
      setLoading(true)
      submitForm({
        variables: {
          input: {
            name: values.name,
            email: values.email,
            contact: values.contact,
            subject: values.subject,
            category: values.category,
            priority: values.priority,
            description: values.description,
            enclosed: values.enclosed,
            additionalInfo: values.additionalInfo
          }
        }
      })
        .then((res: any) => {
          if (res.data.signIn.status === 200) {
            toast.success('received success response against resume parser form submission!')
            /* setLoading(false) */
            document.location.reload()
          } else {
            /* setLoading(false) */
            toast.error(res.data.signIn.message)
            setSubmitting(false)
          }
        })
        .catch((err: any) => {
          /* setLoading(false) */
          setSubmitting(false)
          console.error(err)
        })
    }
  })

  return (
    <div id='kt_modal_new_ticket' tabIndex={-1} aria-hidden='true'>
      <div className='modal-dialog shadow-lg modal-dialog-centered mw-750px'>
        <div className='modal-content rounded'>
          <div className='modal-header pb-0 border-0 justify-content-end'>
            <div className='btn btn-sm btn-icon btn-active-color-primary' data-bs-dismiss='modal'>
              <i className='ki-duotone ki-cross fs-1'>
                <span className='path1'></span>
                <span className='path2'></span>
              </i>
            </div>
          </div>
          <div className='modal-body scroll-y px-10 px-lg-15 pt-0 pb-15'>
            <form className='form g3 row' onSubmit={formik.handleSubmit} noValidate>
              <div className='text-center'>
                <h1 className='mb-3'>Create Ticket</h1>
                <div className='text-gray-500 fw-semibold fs-5'>
                  If you need more info, please check{' '}
                  <a href='' className='fw-bold link-primary'>
                    Support Guidelines
                  </a>
                  .
                </div>
              </div>

              <div className='row g-6 mb-8'>
                <div className='col-md-4 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>User Name</label>

                  <input
                    {...formik.getFieldProps('name')}
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': formik.touched.name && formik.errors.name },
                      {
                        'is-valid': formik.touched.name && !formik.errors.name
                      }
                    )}
                    type='text'
                    name='name'
                    autoComplete='off'
                  />

                  {formik.touched.name && formik.errors.name && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.name}</span>
                    </div>
                  )}
                </div>

                <div className='col-md-4 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>Email</label>

                  <input
                    {...formik.getFieldProps('email')}
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': formik.touched.email && formik.errors.email },
                      {
                        'is-valid': formik.touched.email && !formik.errors.email
                      }
                    )}
                    type='text'
                    name='email'
                    autoComplete='off'
                  />
                  {formik.touched.email && formik.errors.email && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.email}</span>
                    </div>
                  )}
                </div>

                <div className='col-md-4 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>Contact</label>

                  <input
                    {...formik.getFieldProps('contact')}
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': formik.touched.contact && formik.errors.contact },
                      {
                        'is-valid': formik.touched.contact && !formik.errors.contact
                      }
                    )}
                    type='text'
                    name='contact'
                    autoComplete='off'
                  />
                  {formik.touched.contact && formik.errors.contact && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.contact}</span>
                    </div>
                  )}
                </div>
              </div>

              <div className='row g-2 mb-8'>
                <div className='col-md-3 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>Issue (summary)</label>
                  <input
                    {...formik.getFieldProps('subject')}
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': formik.touched.subject && formik.errors.subject },
                      {
                        'is-valid': formik.touched.subject && !formik.errors.subject
                      }
                    )}
                    type='text'
                    name='subject'
                    autoComplete='off'
                  />
                  {formik.touched.subject && formik.errors.subject && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.subject}</span>
                    </div>
                  )}
                </div>

                <div className='col-md-3 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>Category</label>
                  <select
                    {...formik.getFieldProps('category')}
                    className={clsx(
                      `form-select form-select-solid select2-hidden-accessible form-control form-control-lg form-control-solid`,
                      { 'is-invalid': formik.touched.category && formik.errors.category },
                      { 'is-valid': formik.touched.category && !formik.errors.category }
                    )}
                    data-control='select2'
                    data-hide-search='true'
                    data-placeholder='Select a catrgory'
                    name='category'
                    data-select2-id='select2-data-21-zjes'
                    tabIndex={-1}
                    aria-hidden='true'
                    data-kt-initialized='1'
                  >
                    <option value='' data-select2-id='select2-data-23-s65x'>
                      Select a category...
                    </option>
                    <option value='UI related'>UI related</option>
                    <option value='Application Logic'>Application Logic</option>
                    <option value='Backend Issue'>Backend Issue</option>
                    <option value='Feature request'>Feature request</option>
                  </select>
                  {formik.touched.category && formik.errors.category && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.category}</span>
                    </div>
                  )}
                </div>
                <div className='col-md-3 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>Priority</label>
                  <input
                    {...formik.getFieldProps('priority')}
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': formik.touched.priority && formik.errors.priority },
                      {
                        'is-valid': formik.touched.priority && !formik.errors.priority
                      }
                    )}
                    type='number'
                    name='priority'
                    autoComplete='off'
                  />
                  {formik.touched.priority && formik.errors.priority && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.priority}</span>
                    </div>
                  )}
                </div>
                <div className='col-md-3 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>Description</label>
                  <input
                    {...formik.getFieldProps('description')}
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': formik.touched.description && formik.errors.description },
                      {
                        'is-valid': formik.touched.description && !formik.errors.description
                      }
                    )}
                    type='text'
                    name='description'
                    autoComplete='off'
                  />
                  {formik.touched.description && formik.errors.description && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.description}</span>
                    </div>
                  )}
                </div>
              </div>

              <div className='row g-2 mb-13'>
                <div className='col-md-6 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>Enclosure</label>
                  <input
                    {...formik.getFieldProps('enclosed')}
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': formik.touched.enclosed && formik.errors.enclosed },
                      {
                        'is-valid': formik.touched.enclosed && !formik.errors.enclosed
                      }
                    )}
                    type='file'
                    name='enclosed'
                    autoComplete='off'
                  />
                  {formik.touched.enclosed && formik.errors.enclosed && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.enclosed}</span>
                    </div>
                  )}
                </div>
                <div className='col-md-6 fv-row'>
                  <label className='required fs-6 fw-semibold mb-2'>Additional Info</label>
                  <input
                    {...formik.getFieldProps('additionalInfo')}
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      {
                        'is-invalid': formik.touched.additionalInfo && formik.errors.additionalInfo
                      },
                      {
                        'is-valid': formik.touched.additionalInfo && !formik.errors.additionalInfo
                      }
                    )}
                    type='text'
                    name='additionalInfo'
                    autoComplete='off'
                  />
                  {formik.touched.additionalInfo && formik.errors.additionalInfo && (
                    <div className='fv-plugins-message-container'>
                      <span role='alert'>{formik.errors.additionalInfo}</span>
                    </div>
                  )}
                </div>
              </div>

              <div className='text-center'>
                <button
                  type='reset'
                  id='kt_modal_new_ticket_cancel'
                  className='btn btn-light me-3'
                  onClick={closeCreateTeamModal}
                >
                  Cancel
                </button>

                <button
                  type='submit'
                  id='kt_modal_new_ticket_submit'
                  className='btn btn-primary'
                  disabled={formik.isSubmitting || !formik.isValid || !formik.values.name}
                >
                  {!loadingOl && <span className='indicator-label'>Create</span>}
                  {loadingOl && (
                    <span className='indicator-progress' style={{ display: 'block' }}>
                      Please wait...{' '}
                      <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                    </span>
                  )}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CreateTicketModal