import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import { useState } from 'react';

const TelephoneInput = () => {
	const [value, setValue] = useState<any>()
	return (
		<div>
			<PhoneInput
		        placeholder="Enter phone number"
		        className='form-control'
		        value={value}
		        onChange={setValue}/>
		</div>
	)
}

export default TelephoneInput