import { useEffect } from 'react';

/*sample nested data*/
const tickets = [
  {
    "id": 12,
    "topic": "SETTLEMENT",
    "sub_topics": [
      {
        "id": 13,
        "topic": "Settlement SMS Notification",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 14,
        "topic": "Settlement Delayed Credit",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 15,
        "topic": "Settlement Bifurcation/ Info",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 16,
        "topic": "Settlement Statement Request",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 17,
        "topic": "Settlement HELD/ Reason",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 18,
        "topic": "Settlement Processed/ Credit Not Received",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 19,
        "topic": "Settlement Confirmation",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      }
    ],
    "priority": "low",
    "ticket_name": null,
    "team": 5,
    "hide": false
  },
  {
    "id": 20,
    "topic": "PACKAGE & PRICING",
    "sub_topics": [
      {
        "id": 21,
        "topic": "Device Addon/ Downgrade/Upgrade",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 22,
        "topic": "TDR Change Request",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 23,
        "topic": "MMC/ AMC Clarification",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 24,
        "topic": "Excess MMC/AMC Debited",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 25,
        "topic": "TDR/Package/SIM Addon Miscommunication",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      },
      {
        "id": 26,
        "topic": "Intial Payment Mismatch",
        "sub_topics": [],
        "priority": "low",
        "ticket_name": null,
        "team": 5,
        "hide": false
      }
    ],
    "priority": "low",
    "ticket_name": null,
    "team": 5,
    "hide": false
  }
];

const ListOnlyLeafNodes = () => {
  const assignment = 23;
  const userTweaked = [
  {
    "label": "Settlement SMS Notification",
    "id": 13,
    "isSetForMapping": false,
    "steps": [
      "SETTLEMENT",
      "Settlement SMS Notification"
    ]
  },
  {
    "label": "Settlement Delayed Credit",
    "id": 14,
    "isSetForMapping": false,
    "steps": [
      "SETTLEMENT",
      "Settlement Delayed Credit"
    ]
  },
  {
    "label": "Settlement Bifurcation/ Info",
    "id": 15,
    "isSetForMapping": true,
    "steps": [
      "SETTLEMENT",
      "Settlement Bifurcation/ Info"
    ]
  },
  {
    "label": "Settlement Statement Request",
    "id": 16,
    "isSetForMapping": false,
    "steps": [
      "SETTLEMENT",
      "Settlement Statement Request"
    ]
  },
  {
    "label": "Settlement HELD/ Reason",
    "id": 17,
    "isSetForMapping": false,
    "steps": [
      "SETTLEMENT",
      "Settlement HELD/ Reason"
    ]
  },
  {
    "label": "Settlement Processed/ Credit Not Received",
    "id": 18,
    "isSetForMapping": false,
    "steps": [
      "SETTLEMENT",
      "Settlement Processed/ Credit Not Received"
    ]
  },
  {
    "label": "Settlement Confirmation",
    "id": 19,
    "isSetForMapping": false,
    "steps": [
      "SETTLEMENT",
      "Settlement Confirmation"
    ]
  },
  {
    "label": "Device Addon/ Downgrade/Upgrade",
    "id": 21,
    "isSetForMapping": false,
    "steps": [
      "PACKAGE & PRICING",
      "Device Addon/ Downgrade/Upgrade"
    ]
  },
  {
    "label": "TDR Change Request",
    "id": 22,
    "isSetForMapping": true,
    "steps": [
      "PACKAGE & PRICING",
      "TDR Change Request"
    ]
  },
  {
    "label": "MMC/ AMC Clarification",
    "id": 23,
    "isSetForMapping": true,
    "steps": [
      "PACKAGE & PRICING",
      "MMC/ AMC Clarification"
    ]
  },
  {
    "label": "Excess MMC/AMC Debited",
    "id": 24,
    "isSetForMapping": true,
    "steps": [
      "PACKAGE & PRICING",
      "Excess MMC/AMC Debited"
    ]
  },
  {
    "label": "TDR/Package/SIM Addon Miscommunication",
    "id": 25,
    "isSetForMapping": false,
    "steps": [
      "PACKAGE & PRICING",
      "TDR/Package/SIM Addon Miscommunication"
    ]
  },
  {
    "label": "Intial Payment Mismatch",
    "id": 26,
    "isSetForMapping": false,
    "steps": [
      "PACKAGE & PRICING",
      "Intial Payment Mismatch"
    ]
  }
];

// Function to update tickets array based on userTweaked and assignment
const updateFetchedTickets = (tickets, userTweaked, assignment) => {
  // Create a map of id to isSetForMapping from userTweaked for quick lookup
  const idToIsSetForMapping = {};
  userTweaked.forEach(item => {
    idToIsSetForMapping[item.id] = item.isSetForMapping;
  });

  // Recursive function to update tickets array
  const updateTicketsRecursive = (arr) => {
    arr.forEach(ticket => {
      // Check if ticket's id exists in idToIsSetForMapping and isSetForMapping is true
      if (idToIsSetForMapping.hasOwnProperty(ticket.id) && idToIsSetForMapping[ticket.id]) {
        ticket.ticket_name = assignment; // Update ticket_name field
      }
      
      // Recursively update sub_topics if present
      if (ticket.sub_topics && ticket.sub_topics.length > 0) {
        updateTicketsRecursive(ticket.sub_topics);
      }
    });
  };

  // Start updating tickets array recursively
  updateTicketsRecursive(tickets);

  return tickets; // Return updated tickets array
};

const handleTrigger = () => {
  // Call the function to update tickets
  const updatedTickets = updateFetchedTickets(tickets, userTweaked, assignment);
  console.log(updatedTickets); // Output the updated tickets array
}

useEffect(() => {
    // Function to recursively iterate through tickets and nested sub_topics
    const findEmptySubTopics = (tickets) => {
      tickets.forEach(ticket => {
        // Check if the current ticket's sub_topics is an empty array
        if (ticket.sub_topics.length === 0) {
          console.log(ticket.topic); // Output the topic field's value
        } else {
          // Recursively check sub_topics if it's not empty
          findEmptySubTopics(ticket.sub_topics);
        }
      });
    };

    // Call the function initially with the tickets prop
    findEmptySubTopics(tickets);
  }, [tickets]);

  return (
    <div>
      <h1>Tickets with Empty sub_topics:</h1>
      <button className="btn btn-primary" onClick={handleTrigger}>TestMe</button>
    </div>
  );
};

export default ListOnlyLeafNodes