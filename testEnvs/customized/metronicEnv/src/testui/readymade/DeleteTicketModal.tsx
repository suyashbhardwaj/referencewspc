import React from 'react'
import { useMutation } from '@apollo/client'
import { toast } from 'react-toastify'
import { GET_ALL_FORM_BUILDER_BY_BG_ACCOUNT_ID } from '../../graphql/query'
import { DELETE_FORM_BUILDER_BY_FORM_ID } from '../../graphql/mutation'

interface Props {
  ticketInfo: any
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
  closeDeleteModal: () => void

  ticketlist: any
  setTicketList: any
}

const DeleteTicketModal: React.FC<Props> = ({
  ticketInfo,
  setIsLoading,
  closeDeleteModal,
  ticketlist,
  setTicketList
}) => {
  const [DeleteForm] = useMutation(DELETE_FORM_BUILDER_BY_FORM_ID, {
    refetchQueries: [
      {
        query: GET_ALL_FORM_BUILDER_BY_BG_ACCOUNT_ID,
        variables: {
          input: {
            formUuid: null
          }
        }
      }
    ]
  })
  console.log(ticketInfo)
  const deleteThisTicket = (ticketId: any) => {
    setIsLoading(true)
    console.log(`ticket ${ticketId} has been set up to be deleted. Will delete...`)
    setTicketList((prevList: any) => {
      const updatedList = [...ticketlist]
      return updatedList.filter((ticket) => ticket.ticketId !== ticketInfo.ticketId)
    })
    closeDeleteModal()
    /* DeleteForm({
      variables: {
        input: {
          formUuid: formUuids
        }
      }
    })
      .then((res: any) => {
        setIsLoading(false)
        closeDeleteModal()
        if (res) {
          toast.success(res.data.deleteFormBuilderByFormId.message)
        } else {
          toast.error('Unable to delete Form')
        }
      })
      .catch((err) => {
        setIsLoading(false)
        toast.error('Sorry! Unexpected Error.')
        console.log(err)
      }) */
  }

  return (
    <div className='card-body'>
      <p className='fs-5 fw-bold'>Are you sure you want to delete the following Ticket ?</p>
      <div className='card-text'>
        Ticket Name: <strong>{ticketInfo.ticketDet.subject}</strong>
      </div>
      <div className='card-text'>
        Ticket Description: <strong>{ticketInfo.ticketDet.description}</strong>
      </div>

      <div className='d-flex justify-content-end mt-10'>
        <button
          type='reset'
          className='btn btn-sm btn-white btn-active-light-primary me-2'
          onClick={closeDeleteModal}
        >
          Cancel
        </button>

        <button
          type='submit'
          className='btn btn-sm btn-danger'
          onClick={() => {
            deleteThisTicket(ticketInfo.ticketId)
          }}
        >
          <span className='indicator-label'>Delete</span>
        </button>
      </div>
    </div>
  )
}

export default DeleteTicketModal
