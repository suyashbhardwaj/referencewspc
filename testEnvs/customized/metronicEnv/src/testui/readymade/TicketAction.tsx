import React, { useState } from 'react'
import Modal from 'react-modal'
import { Dropdown } from '../../helpers/components/Dropdown'
import DeleteTicketModal from './DeleteTicketModal'
import EditTicketModal from './EditTicketModal'
Modal.setAppElement('#root')

interface Props {
  userActionDropdownMenuRefs: React.MutableRefObject<any[]>
  ticketInfo: any
  index: number
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>

  ticketlist: any
  setTicketList: any
}

const TicketAction: React.FC<Props> = ({
  userActionDropdownMenuRefs,
  ticketInfo,
  index,
  setIsLoading,

  ticketlist,
  setTicketList
}) => {
  const [userActionMenuDroppedDown, setUserActionMenuDroppedDown] = useState<boolean>(false)
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState<boolean>(false)
  const [isEditTicketModalOpen, setIsEditTicketModalOpen] = useState<boolean>(false)

  const customModalStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      width: '40vw',
      transform: 'translate(-30%, -60%)'
    }
  }

  const openDeleteModal = () => {
    setIsDeleteModalOpen(true)
  }

  const afterOpenDeleteModal = () => {
    // subtitle.style.color = '#f00'
  }

  const closeDeleteModal = () => {
    setIsDeleteModalOpen(false)
  }

  const openEditTicketModal = () => {
    setIsEditTicketModalOpen(true)
  }

  const afterOpenEditModal = () => {
    // subtitle.style.color = '#f00'
  }

  const closeEditTicketModal = () => {
    setIsEditTicketModalOpen(false)
  }

  const handle = () => {
    setUserActionMenuDroppedDown(!userActionMenuDroppedDown)
  }

  return (
    <>
      <button
        className={`btn btn-sm btn-icon btn-active-light-primary`}
        onClick={handle}
        ref={(element) => (userActionDropdownMenuRefs.current[index] = element)} // Assign the ref
      >
        <i className='bi bi-three-dots-vertical fs-2'></i>
      </button>
      <Dropdown
        menuRef={userActionDropdownMenuRefs.current[index]} // Use the same ref
        droppedDown={userActionMenuDroppedDown}
        setDroppedDown={setUserActionMenuDroppedDown}
        styles={{ transform: ' translate(-24px, 40px)' }}
      >
        <div className='p-5 py-3'>
          <p className='link-danger hoverable' role='button' onClick={openEditTicketModal}>
            <i className='bi bi-trash link-danger'></i>
            <span className='px-2'>Edit Ticket</span>
          </p>

          <p className='link-danger hoverable' role='button' onClick={openDeleteModal}>
            <i className='bi bi-trash link-danger'></i>
            <span className='px-2'>Delete</span>
          </p>

          <Modal
            isOpen={isDeleteModalOpen}
            onAfterOpen={afterOpenDeleteModal}
            onRequestClose={closeDeleteModal}
            style={customModalStyles}
            contentLabel='Delete Bot'
          >
            <DeleteTicketModal
              ticketInfo={ticketInfo}
              setIsLoading={setIsLoading}
              closeDeleteModal={closeDeleteModal}
              ticketlist={ticketlist}
              setTicketList={setTicketList}
            />
          </Modal>

          <Modal
            isOpen={isEditTicketModalOpen}
            onAfterOpen={afterOpenEditModal}
            onRequestClose={closeEditTicketModal}
            contentLabel='Edit Bot'
          >
            <EditTicketModal
              ticketInfo={ticketInfo}
              setIsLoading={setIsLoading}
              closeEditTicketModal={closeEditTicketModal}
              ticketlist={ticketlist}
              setTicketList={setTicketList}
            />
          </Modal>
        </div>
      </Dropdown>
    </>
  )
}

export default TicketAction
