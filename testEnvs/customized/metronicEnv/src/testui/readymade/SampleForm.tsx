/*Is a sample validated (using Yup & Formik) form */
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

// Define Yup schema for validation
const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  lastName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  age: Yup.number()
    .min(18, 'Must be at least 18')
    .required('Required'),
  termsAccepted: Yup.boolean()
    .oneOf([true], 'You must accept the terms and conditions'),
  gender: Yup.string()
    .required('Required'),
});

const SampleForm = () => {
  const handleSubmit = (values, actions) => {
    // Simulate API call (replace with actual API call using fetch, axios, etc.)
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
      actions.setSubmitting(false);
    }, 500);
  };

  return (
    <div>
      <h1>Sample Form</h1>
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          email: '',
          age: '',
          termsAccepted: false,
          gender: '',
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting }) => (
          <Form>
            <div>
              <label htmlFor="firstName">First Name</label>
              <Field type="text" id="firstName" name="firstName" />
              <ErrorMessage name="firstName" component="div" className="error" />
            </div>

            <div>
              <label htmlFor="lastName">Last Name</label>
              <Field type="text" id="lastName" name="lastName" />
              <ErrorMessage name="lastName" component="div" className="error" />
            </div>

            <div>
              <label htmlFor="email">Email</label>
              <Field type="email" id="email" name="email" />
              <ErrorMessage name="email" component="div" className="error" />
            </div>

            <div>
              <label htmlFor="age">Age</label>
              <Field type="number" id="age" name="age" />
              <ErrorMessage name="age" component="div" className="error" />
            </div>

            <div>
              <label>Gender</label>
              <div>
                <label>
                  <Field type="radio" name="gender" value="male" />
                  Male
                </label>
                <label>
                  <Field type="radio" name="gender" value="female" />
                  Female
                </label>
                <label>
                  <Field type="radio" name="gender" value="other" />
                  Other
                </label>
              </div>
              <ErrorMessage name="gender" component="div" className="error" />
            </div>

            <div>
              <label>
                <Field type="checkbox" name="termsAccepted" />
                I accept the terms and conditions
              </label>
              <ErrorMessage name="termsAccepted" component="div" className="error" />
            </div>

            <button type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default SampleForm;