import React, { useEffect, useState } from 'react'

const PagenationUtility = ({data, setCurrentData}) => {
    const [itemsPerPage, setItemsPerPage] = useState(5) // You can adjust this based on your requirement
    const [currentPage, setCurrentPage] = useState(1)
    const nextPage = () => { setCurrentPage((prev) => prev + 1) }
    const prevPage = () => { setCurrentPage((prev) => prev - 1) }
    const [totalPages, setTotalPages] = useState<number>(1);
    const indexOfLastPage = currentPage * itemsPerPage
    const indexOfFirstPage = indexOfLastPage - itemsPerPage

    // Generate snake handle pagination numbers
    const generatePaginationNumbers = () => {
        const numbers = []
        const maxVisibleNumbers = 4 // Maximum visible pagination numbers

        if (totalPages <= maxVisibleNumbers) {
        // If total pages are less than or equal to max visible numbers, show all
        for (let i = 1; i <= totalPages; i++) {
            numbers.push(i)
        }
        } else {
        const middleIndex = Math.ceil(maxVisibleNumbers / 2) // Middle index of the pagination numbers
        let startPage = currentPage - middleIndex + 1
        let endPage = currentPage + middleIndex - 1

        if (startPage < 1) {
            // If start page is less than 1, adjust start and end page accordingly
            endPage = startPage + maxVisibleNumbers - 1
            startPage = 1
        } else if (endPage > totalPages) {
            // If end page is greater than total pages, adjust start and end page accordingly
            startPage = endPage - maxVisibleNumbers + 1
            endPage = totalPages
        }

        for (let i = startPage; i <= endPage; i++) {
            numbers.push(i)
        }
        }
        console.log('total pages set to be as ', numbers)
        return numbers
    }
    const paginate = (pageNumber: any) => { setCurrentPage(pageNumber) }
    useEffect(()=>{
        if(data.length){
            setTotalPages(Math.ceil(data?.length/itemsPerPage))
            setCurrentData(data?.slice(indexOfFirstPage, indexOfLastPage));
        }
    }, [data, currentPage, itemsPerPage])
    
    return (<>
    <div className='row'>
        <div className='col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'>
        <div className='dataTables_length' id='kt_ecommerce_products_table_length'>
            <label>
            <select
                className='form-select form-select-sm form-select-solid'
                onChange={(evt) => setItemsPerPage(Number(evt.target.value))}
            >
                <option value='5'>5</option>
                <option value='10'>10</option>
                <option value='20'>20</option>
                <option value='50'>50</option>
            </select>
            </label>
        </div>
        </div>
        <div className='col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'>
        <div className='dataTables_paginate paging_simple_numbers'>
            <ul className='pagination'>
            <li
                className={`paginate_button page-item previous ${
                currentPage === 1 ? 'disabled' : ''
                }`}
                id='kt_ecommerce_products_table_previous'
            >
                <button
                type='button'
                aria-controls='kt_ecommerce_products_table'
                data-dt-idx='0'
                tabIndex={0}
                className='btn primary page-link'
                onClick={prevPage}
                >
                <i className='previous'></i>
                </button>
            </li>

            {generatePaginationNumbers().map((pageNumber) => (
                <li
                key={pageNumber}
                className={`paginate_button page-item ${
                    currentPage === pageNumber ? 'active' : ''
                }`}
                >
                <button
                    type='button'
                    aria-controls='kt_table_users'
                    data-dt-idx={pageNumber}
                    className='btn primary page-link'
                    onClick={() => paginate(pageNumber)}
                >
                    {pageNumber}
                </button>
                </li>
            ))}

            <li
                className={`paginate_button page-item ${
                currentPage === totalPages ? 'disabled' : ''
                }`}
                id='kt_ecommerce_products_table_next'
            >
                <button
                type='button'
                aria-controls='kt_ecommerce_products_table'
                className='btn primary page-link'
                data-dt-idx='6'
                tabIndex={0}
                onClick={nextPage}
                >
                <i className='next'></i>
                </button>
            </li>
            </ul>
        </div>
        </div>
    </div>
  </>)
}

export default PagenationUtility