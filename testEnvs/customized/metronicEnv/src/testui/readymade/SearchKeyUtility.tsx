import { useEffect, useState } from 'react';

const teamDataSample = [{"id":1,"name":"Sales Team","agent_module_permissions":["users:enable","campaigns:enable","email:enable","settings:enable","dashboard:enable","reports:enable","customers:enable","ticket:enable"],"manager_module_permissions":["users:enable","campaigns:enable","email:enable","settings:enable","dashboard:enable","reports:enable","customers:enable","ticket:enable"],"role":["agent"],"auto_ticket_generate_enabled":true,"auto_ticket_notification_enabled":true,"manual_ticket_notification_enabled":true,"email_notification_enabled":true,"ticket_resolution_sla_emails":["anubha@bluewhirl.io","archana@bluewhirl.io"],"time_zone":"Asia/Kolkata","open_24_7":false,"periodic_ticket_generation":true,"is_default":true},{"id":2,"name":"Dev Team","agent_module_permissions":["dashboard:enable","customers:enable","email:enable","settings:enable","campaigns:enable","ticket:enable"],"manager_module_permissions":["dashboard:enable","customers:enable","email:enable","reports:enable","settings:enable","campaigns:enable","ticket:enable","users:enable"],"role":["agent"],"auto_ticket_generate_enabled":true,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":["yash@yahoo.com"],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":true,"is_default":false},{"id":4,"name":"Service team","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["email:enable","ticket:enable","dashboard:enable","settings:enable"],"role":["manager"],"auto_ticket_generate_enabled":true,"auto_ticket_notification_enabled":true,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":5,"name":"Collections","agent_module_permissions":["campaigns:enable","email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["dashboard:enable","settings:enable","email:enable","ticket:enable","campaigns:enable"],"role":["agent"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":10,"name":"Test Team","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["dashboard:enable","email:enable","settings:enable","campaigns:enable","ticket:enable"],"role":["manager"],"auto_ticket_generate_enabled":true,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":11,"name":"Payswiff  ( Don't Delete)","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["email:enable","ticket:enable","dashboard:enable","settings:enable"],"role":["manager"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":12,"name":"Dev test","agent_module_permissions":["ticket:enable","dashboard:enable","email:enable"],"manager_module_permissions":["dashboard:enable","settings:enable","email:enable","ticket:enable","campaigns:enable"],"role":["manager"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":13,"name":"Credit Team","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["dashboard:enable","customers:enable","email:enable","reports:enable","settings:enable","campaigns:enable","ticket:enable"],"role":["manager"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":true,"manual_ticket_notification_enabled":true,"email_notification_enabled":true,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":false,"periodic_ticket_generation":true,"is_default":false},{"id":17,"name":"Meta","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["settings:enable","email:enable","ticket:enable"],"role":["manager"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":24,"name":"testrain","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["email:enable","ticket:enable","dashboard:enable","setting:enable"],"role":["agent"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":25,"name":"Insurance Policy","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["email:enable","ticket:enable","dashboard:enable","settings:enable"],"role":["manager"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":26,"name":"Under writing Team","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["dashboard:enable","customers:enable","email:enable","reports:enable","settings:enable","ticket:enable","users:enable"],"role":["manager"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":true,"manual_ticket_notification_enabled":true,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":true,"is_default":false},{"id":27,"name":"yihaaoo","agent_module_permissions":["email:enable","ticket:enable","dashboard:enable"],"manager_module_permissions":["dashboard:enable","reports:enable","settings:enable","customers:enable","email:enable","ticket:enable"],"role":["manager"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false},{"id":28,"name":"botgo","agent_module_permissions":["phone:enable","chat:enable","dashboard:enable"],"manager_module_permissions":["email:enable","ticket:enable","dashboard:enable","setting:enable"],"role":["agent"],"auto_ticket_generate_enabled":false,"auto_ticket_notification_enabled":false,"manual_ticket_notification_enabled":false,"email_notification_enabled":false,"ticket_resolution_sla_emails":[],"time_zone":"Asia/Kolkata","open_24_7":true,"periodic_ticket_generation":false,"is_default":false}];
const SearchKeyUtility = () => {
	const [data, setData] = useState(teamDataSample);
	const fetchData = () => {
	    return fetch("https://jsonplaceholder.typicode.com/users")
	      .then((res) => res.json())
	      .then((d) => setData(d));
	  };

  // useEffect(() => { fetchData(); }, []);

  const [query, setQuery] = useState("");
  const search_parameters = Object.keys(Object.assign({}, ...data));
  function search(data:any) {
    return data.filter((data:any) =>
      search_parameters.some((parameter) => data[parameter].toString().toLowerCase().includes(query))
    );
  }

  return (
    <div className="container">
    <center>
      	<h1>Search component in ReactJS</h1>
      	</center>
      	<div className="input-box">
	        <input
	          type="search"
	          name="search-form"
	          id="search-form"
	          className="search-input form-control mb-4"
	          onChange={(e) => setQuery(e.target.value)}
	          placeholder="Search user"
	        />
      	</div>
	    <center>
	        {search(data).map((dataObj:any) => {
		        return (
		            <div className="box">
			            <div className="card">
	    		            {/*<div className="category">@{dataObj.username} </div>*/}
	            			    <div className="heading"> {dataObj.name}
	                  		{/*<div className="author">{dataObj.email}</div>*/}
	                	</div>
	              </div>
	            </div>
	          	);
	        })}
		</center>
    </div>
  );
}

export default SearchKeyUtility