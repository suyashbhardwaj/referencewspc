import React from 'react'
import { BrowserRouter, Navigate, Routes, Route } from 'react-router-dom'
import { AuthPage, Logout } from '../components/auth'
import { AUTH_TOKEN } from '../constants'
import {PrivateRoutes} from './PrivateRoutes'
const MainAppRoutes: React.FC = () => {
	const isAuthorized = localStorage.getItem(AUTH_TOKEN);
	return (
		<BrowserRouter basename = {'/'}>
			<Routes>
				{isAuthorized ? (
					<>
						<Route path = '/*' element = {<PrivateRoutes/>} />
						<Route index element = {<Navigate to = '/dashboard' />} />
						<Route path = '/logout' element = {<Logout />} />
					</>
					):(
					<>
						<Route path = 'auth/*' element = {<AuthPage />} />
						<Route path = '*' element = {<Navigate to = '/auth' />} />
					</>
					)}
			</Routes>
		</BrowserRouter>
	)
}

export {MainAppRoutes}