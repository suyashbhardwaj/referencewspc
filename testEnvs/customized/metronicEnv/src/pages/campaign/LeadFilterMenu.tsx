import { useState } from "react";

const LeadFilterMenu = () => {
    const [isChangeAssigneeOpen, setIsChangeAssigneeOpen] = useState<boolean>(false)
    const [isChangeStatusOpen, setIsChangeStatusOpen] = useState<boolean>(false)
  return (
    <div>
      <form className="card-body px-10 py-1" onSubmit={() => console.log("form submitted")}>
        <div className="fw-bold align-items-center fs-4">Filters</div> <br />
        
        <div className="d-flex justify-content-between align-items-center w-100" onClick={()=>setIsChangeAssigneeOpen(!isChangeAssigneeOpen)}>
          <label className="form-label" htmlFor="changeAssignee">Change Assignee</label>
          <label className="form-label fs-1" htmlFor="changeAssignee">{isChangeAssigneeOpen?'-':'+'}</label>
        </div>
        <div className={`input-group mb-3 d-flex flex-column justify-content-between align-items-start w-100 ${isChangeAssigneeOpen?'':'d-none'}`}>
            <div className="d-flex mb-3">
                <input className="form-control" type="search" name="searchassignee" />
                <span className="input-group-text" id="basic-addon2"><i className="bi bi-search"></i></span>
            </div>
            <div className="form-check mb-2">
            <input className="form-check-input" type="checkbox" value="" id="raj"/>
            <label className="form-check-label" htmlFor="raj">
                Raj
            </label>
            </div>
            <div className="form-check mb-2">
            <input className="form-check-input" type="checkbox" value="" id="tamara"/>
            <label className="form-check-label" htmlFor="tamara">
                Tamara
            </label>
            </div>
            <div className="form-check mb-2">
            <input className="form-check-input" type="checkbox" value="" id="coy"/>
            <label className="form-check-label" htmlFor="coy">
                Coy
            </label>
            </div>
            <div className="form-check mb-2">
            <label className="form-check-label text-center text-secondary" htmlFor="more">+15 more</label>
            </div>
        </div>
        <div className="d-flex justify-content-between align-items-center w-100" onClick={()=>setIsChangeStatusOpen(!isChangeStatusOpen)}>
          <label className="form-label" htmlFor="changeStatus">Change Status</label>
          <label className="form-label fs-1" htmlFor="changeStatus">+</label>
        </div>
      </form>
    </div>
  );
}

export default LeadFilterMenu
