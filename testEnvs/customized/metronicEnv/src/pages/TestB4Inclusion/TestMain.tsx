import { useState } from "react";
// Import Highcharts
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";
import Highcharts3d from "highcharts/highcharts-3d.js";
import Cylinder from "highcharts/modules/cylinder.js";
import Funnel from "highcharts/modules/funnel3d";

Highcharts3d(Highcharts);
Cylinder(Highcharts);
Funnel(Highcharts);

const initialData = {
	options: {
        chart: {
          type: "funnel3d",
          options3d: {
            enabled: true,
            alpha: 10,
            depth: 50,
            viewDistance: 50
          }
        },
        title: {
          text: "Highcharts Funnel3D Chart"
        },
        plotOptions: {
          series: {
            dataLabels: {
              enabled: true,
              format: "<b>{point.name}</b> ({point.y:,.0f})",
              allowOverlap: true,
              y: 10
            },
            neckWidth: "30%",
            neckHeight: "25%",
            width: "80%",
            height: "80%"
          }
        },
        series: [
          {
            name: "Unique users",
            data: [
              ["Website visits", 15654],
              ["Downloads", 4064],
              ["Requested price list", 1987],
              ["Invoice sent", 976],
              ["Finalized", 846]
            ]
          }
        ]
      }
}

const TestMain = () => {
	const [state, ] = useState<any>(initialData)
	return (
		<>
			<HighchartsReact highcharts={Highcharts} options={state.options} />
		</>
	)
}

export default TestMain