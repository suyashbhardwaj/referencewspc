import Dexie from 'dexie';

const productdb = (dbname, table) => {

	// 01 create a new instance of indexedDB database with the name - ${dbname}
	const db = new Dexie(dbname);
  	// 02 specify the version & table headers as an object with {<tableName>: `<field1>, <field2>, <field3>... <fieldN>`} as arg to stores()
	db.version(1).stores(table);
	// 03 open databse & catch any error 
	db.open().catch(err => {
		console.log(err.stack || err)
	})

	return db;
}

const bulkCreate = (dbtable, data) => {
	let flag = empty(data);
	if (flag) {
		dbtable.bulkAdd([data]); /**/
		console.log("data inserted successfully");
	}
	else {console.log("please provide data")}
	return flag;
}

/*check textbox validation*/
const empty = object => {
	let flag = false;
	for(const value in object){
		if (object[value]!="" && object.hasOwnProperty(value)) {
			flag = true;
		}
		else{	flag = false;
		}
	}
	return flag;
}

//get data from the database
const getData = (dbtable, fn) => {
		let index = 0;
		let obj = "";

		dbtable.count((count)=> {
			if(count){
				dbtable.each(table=>{
					// console.log(table);

					obj = Sortobj(table);
					// console.log(obj);
					fn(obj,index++)
				})
			}else {fn(0);}
		})
	}

//sort object
const Sortobj = sortobj => {
		let obj = {};
		obj = {
			id:sortobj.id,
			name:sortobj.name,
			seller:sortobj.seller,
			price:sortobj.price
		}
		return obj;
	}

//create dynamic element
const createEle = (tagname, appendTo, fn) => {
	const element = document.createElement(tagname);
	if (appendTo) {appendTo.appendChild(element);}
	if(fn) fn(element);
}

export default productdb
export {
	bulkCreate,
	getData,
	createEle
}

