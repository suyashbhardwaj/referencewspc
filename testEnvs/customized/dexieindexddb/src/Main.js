
import React, { useEffect } from 'react'
import productdb, {
	bulkCreate,
	getData,
	createEle
} from './js/Module';

function Main() {
	
	let db = productdb("Productdb", {
		products: `++id, name, seller, price`
	});

	useEffect(()=>{
		/*input tags के references ले लिय। */
		const userid = document.getElementById('userid');
		const proname = document.getElementById('proname');
		const seller = document.getElementById('seller');
		const price = document.getElementById('price');

		/*buttons के references ले लिय।*/
		const btncreate = document.getElementById('btn-create');
		const btnread = document.getElementById('btn-read');
		const btnupdate = document.getElementById('btn-update');
		const btndelete = document.getElementById('btn-delete');


		//notfound div का references ले लिया।
		const notfound = document.getElementById('notfound');

		//insert value using create button
		
		btncreate.onclick = (event) => {
			let flag = bulkCreate(db.products, {
				name: proname.value,
				seller: seller.value, 
				price: price.value
			})
			console.log(flag);
			proname.value = seller.value = price.value = "";
			getData(db.products,(data)=>{
				userid.value = data.id + 1 || 1;
			});
		}


		//create event on btn read button
		btnread.onclick = table;

		btnupdate.onclick = () => {
			const id = parseInt(userid.value || 0);
			if (id) {
				db.products.update(id,{
					name: proname.value,
					seller: seller.value,
					price: price.value
				}).then((updated)=>{
					let get = updated?`data updated`: `could not update data`;
					console.log(get);
				})
			}
		}

		//delete records
		btndelete.onclick = () => {
			db.delete();
			db = productdb("Productdb", {
				products: `++id, name, seller, price`
			});
			db.open();
			table();
		}

		//window onload event
		window.onload = () => {
			textID(userid);
		}

		function textID(textboxid){
			getData(db.products, data=>{
				textboxid.value = data.id +1 || 1;
			})
		}

		function table(){
			const tbody = document.getElementById('tbody');
			while(tbody.hasChildNodes()){
				tbody.removeChild(tbody.firstChild);
			}

			getData(db.products, (data)=>{
				if (data) {
					createEle("tr",tbody, tr=>{
						for(const value in data){
							createEle("td",tr, td=>{
								td.textContent = data.price === data[value]?`$${data[value]}`: data[value]
							})
						}
						createEle('td',tr, td=>{
							createEle('i', td, i=>{
								i.className += "fas fa-edit btnedit"
								i.setAttribute(`data-id`,data.id);
								i.onclick = editbtn;
							})
						})
						createEle('td',tr, td=>{
							createEle('i', td, i=>{
								i.className += "fas fa-trash alt btndelete"
								i.setAttribute(`data-id`,data.id);
								i.onclick = deletebtn;
							})
						})
					})
				}
				else {
					notfound.textContent = "No records found in the database";
				}
			})
		}

		function editbtn(event){
			let id = parseInt(event.target.dataset.id);
			db.products.products.get(id, data=>{
				userid.value = data.id || 0;
				proname.value = data.name || "";
				seller.value = data.seller || "";
				price.value = data.price || "";
			})
		}

		function deletebtn(event){
			let id = parseInt(event.target.dataset.id);
			db.products.delete(id);
			table();
		}
	},[]);

	return (
		<>
			<div className="container text-center">
				<h1 className="bg-light py-4 text-info"><i className="fa-solid fa-plug-circle-check"></i>Electronic Store</h1>
			</div>

			<div className="d-flex justify-content-center">
				<form className="w-50">
					<input type="text" id="userid" className="form-control" readOnly placeholder="ID" autoComplete="off"/>
					<input type="text" id="proname" className="form-control" placeholder="Product Name" autoComplete="off"/>
					<div className="row">
						<div className="col">
							<input type="text" id="seller" className="form-control m-0" placeholder="seller" autoComplete="off"/>
						</div>
						<div className="col">
							<input type="text" id="price" className="form-control m-0" placeholder="Price" autoComplete="off"/>
						</div>
					</div>
				</form>
			</div>
			

			<div className="d-flex justify-content-center">
				<button className="btn btn-success" id="btn-create">Create</button>
				<button className="btn btn-primary" id="btn-read">Read</button>
				<button className="btn btn-warning" id="btn-update">Update</button>
				<button className="btn btn-danger" id="btn-delete">Delete</button>


			<div className="container d-flex">
			<table className="table table-striped">
			  <thead>
			    <tr>
			      <th scope="col">ID</th>
			      <th scope="col">Product Name</th>
			      <th scope="col">Seller</th>
			      <th scope="col">Price</th>
			      <th scope="col">Edit</th>
			      <th scope="col">Delete</th>
			    </tr>
			  </thead>
			  <tbody id="tbody">
			    
			  </tbody>
			</table>
			</div>
			<div className="container text-center" id="notfound"></div>
		</>
	)
}

export default Main