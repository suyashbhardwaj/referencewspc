import { useState } from 'react'
/*import './ProgressSteps.css'*/

const ProgressSteps = () => {
    const [state, setState] = useState<any>({ actives: 1, circles: 5, currentActive: 1 });
    const linethrough = { width: `${((state.currentActive-1)/4)*100}%` }

    const nodestyle=(x: number)=> {
        if (state.currentActive >= x) return 'circle active';
        else return 'circle';
    }

    function handleNextClick() {
		if (state.currentActive > 4) { setState({ currentActive: 5 }) }
			else { setState({ currentActive: state.currentActive+1 }) }
	}

	function handlePrevClick() {
		if (state.currentActive < 1) { setState({ currentActive: 1 }) }
			else { setState({ currentActive: state.currentActive-1 }) }
	}

  return (
      <div className = 'container d-flex flex-column align-items-center'>
				<div className="progress-container my-4">
					<div className="progress" id="progress" style = {linethrough}></div>
                    <div className={nodestyle(1)}>{state.currentActive>=1 ? <i className="bi bi-check2"></i>: 1}</div>
                    <div className={nodestyle(2)}>{state.currentActive>=2 ? <i className="bi bi-check2"></i>: 2}</div>
                    <div className={nodestyle(3)}>{state.currentActive>=3 ? <i className="bi bi-check2"></i>: 3}</div>
                    <div className={nodestyle(4)}>{state.currentActive>=4 ? <i className="bi bi-check2"></i>: 4}</div>
                    <div className={nodestyle(5)}>{state.currentActive>=5 ? <i className="bi bi-check2"></i>: 5}</div>
				</div>
        <div className="d-flex">
          <button className="btn" name="prev" disabled = {state.currentActive <= 1} onClick = {handlePrevClick}>Prev</button>
          <button className="btn" name="next" disabled = {state.currentActive > 4} onClick = {handleNextClick}>Next</button>
        </div>
			</div>
  )
}

export default ProgressSteps
