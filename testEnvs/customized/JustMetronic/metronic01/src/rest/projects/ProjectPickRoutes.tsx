import { Navigate, Route, Routes, Outlet } from 'react-router-dom'
import DexieIndexedDBapp from './browserDBApp/DexieIndexedDBapp'
import RenderForm from './renderedformwa/RenderForm'
import React from 'react'

const ProjectPickRoutes: React.FC = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/browserdbapp/*'
          element={
            <>
              <DexieIndexedDBapp />
            </>
          }
        />
        <Route path='renderedform/:uuid' element={<RenderForm/>} />
        <Route index element={<Navigate to='/projectpicks/todoapp' />} />
      </Route>
    </Routes>
  )
}

export default ProjectPickRoutes