import Dexie from 'dexie';
import { useEffect, useState } from 'react';

const DexieIndexedDBapp = () => {
	const productdb = (dbname: string, table: any) => {
		// 01 create a new instance of indexedDB database with the name - ${dbname}
		const db = new Dexie(dbname);
	  	// 02 specify the version & table headers as an object with {<tableName>: `<field1>, <field2>, <field3>... <fieldN>`} as arg to stores()
		db.version(1).stores(table);
		// 03 open databse & catch any error 
		db.open().catch((err: any) => {
			console.log(err.stack || err)
		})

		return db;
		}

	let db: any = productdb("Productdb", {
		products: `++id, name, seller, price`
	});

	/*check textbox validation*/
	const empty = (object: any) => {
		let flag = false;
		for(const value in object){
			if (object[value]!="" && object.hasOwnProperty(value)) {
				flag = true;
				}
			else{	flag = false;
				}
			}
		return flag;
		}

	const bulkCreate = (dbtable: any, data: any) => {
		let flag = empty(data);
		if (flag) {
			dbtable.bulkAdd([data]);
			console.log("data inserted successfully");
		}
		else {console.log("please provide data")}
		return flag;
		}

	//sort object
	const Sortobj: any = (sortobj:any) => {
		let obj = {};
		obj = {
			id:sortobj.id,
			name:sortobj.name,
			seller:sortobj.seller,
			price:sortobj.price
		}
		return obj;
	}

	//get data from the database
	const getData = (dbtable: any, fn: any) => {
		let index = 0;
		let obj = "";

		dbtable.count((count: any)=> {
			if(count){
				dbtable.each((table: any)=>{
					// console.log(table);
					obj = Sortobj(table);
					// console.log(obj);
					fn(obj,index++)
					})
				}else {fn(0);}
			})
		}

	const [formData, setFormData] = useState({userid: "",proname: "",seller: "", price:0});
	const handleChange = (event: any) => {
		const { name, value } = event.target;
	    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
		};

	/* const handleSubmit = (event: any) => {
	    event.preventDefault();
	    alert(`Name: ${formData.name}, Email: ${formData.email}, Message: ${formData.message}`);
	    alert(`nothing`);
		}; */

	const insertRec = () => {
			let flag = bulkCreate(db.products, {
				name: formData.proname,
				seller: formData.seller, 
				price: formData.price
			})
			console.log(flag);
			setFormData((prevFormData) => ({ ...prevFormData, userid: "",proname: "",seller: "", price:0 }));
			getData(db.products,(data: any)=>{
				setFormData((prevFormData) => ({ ...prevFormData, userid: data.id + 1 || 1 }));
			});
		}

	useEffect(()=>{
		function textID(textboxid: string){
			getData(db.products, (data: any)=>{
				textboxid = data.id +1 || 1;
				})
				console.log(textboxid);
			};
		textID(formData.userid);
	})
	return (
		<>
			<div className='d-flex flex-column'>
				<div className="container text-center">
					<h1 className="bg-light py-4 text-info"><i className="fa-solid fa-plug-circle-check"></i>Electronic Store</h1>
				</div>

				<div className="d-flex justify-content-center">
					<form className="w-50 onSubmit = {handleSubmit}">
						<input type="text" id="userid" name="userid" className="form-control" placeholder="ID" autoComplete="off" value={formData.userid} onChange={handleChange}/>
						<input type="text" id="proname" name="proname" className="form-control" placeholder="Product Name" autoComplete="off" value={formData.proname} onChange={handleChange}/>
						<div className="row">
							<div className="col">
								<input type="text" id="seller" name="seller" className="form-control m-0" placeholder="seller" autoComplete="off" value={formData.seller} onChange={handleChange}/>
							</div>
							<div className="col">
								<input type="text" id="price" name="price" className="form-control m-0" placeholder="Price" autoComplete="off" value={formData.price} onChange={handleChange}/>
							</div>
						</div>
					</form>
				</div>

				<div className="d-flex justify-content-center flex-column">
					<div className='d-flex justify-content-center my-4'>
						<button className="mx-1 btn btn-success" id="btn-create" onClick={insertRec}>Create</button>
						<button className="mx-1 btn btn-primary" id="btn-read" onClick={()=>console.log('clicked a button')}>Read</button>
						<button className="mx-1 btn btn-warning" id="btn-update" onClick={()=>console.log('clicked a button')}>Update</button>
						<button className="mx-1 btn btn-danger" id="btn-delete" onClick={()=>console.log('clicked a button')}>Delete</button>
					</div>
					<div className="container d-flex">
						<table className="table table-striped">
						  <thead>
						    <tr>
						      <th scope="col">ID</th>
						      <th scope="col">Product Name</th>
						      <th scope="col">Seller</th>
						      <th scope="col">Price</th>
						      <th scope="col">Edit</th>
						      <th scope="col">Delete</th>
						    </tr>
						  </thead>
						  <tbody id="tbody">
						    
						  </tbody>
						</table>
					</div>
					<div className="container text-center" id="notfound"></div>
				</div>
			</div>
		</>
	)
}

export default DexieIndexedDBapp