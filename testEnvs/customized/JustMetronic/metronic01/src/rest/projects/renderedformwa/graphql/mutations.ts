import { gql } from '@apollo/client'

/* export const SAVE_FORMSUBMITDATA = gql`
    mutation Mutation($input: saveformDataInput!) {
        saveFormDetailsByFormUuid(input: $input) {
        status
        message
        data
        }
    }
  `; */

export const SAVE_FORMSUBMITDATA = gql`
    mutation SaveFormDetailsByFormUuid($input: saveformDataInput!) {
        saveFormDetailsByFormUuid(input: $input) {
        message
    }
  }
`;