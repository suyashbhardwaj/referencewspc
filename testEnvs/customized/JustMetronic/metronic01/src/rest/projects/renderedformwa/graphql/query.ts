import { gql } from '@apollo/client'

/*export const GET_FORMRENDERJSON = gql`
query GetFormBuilderDetailsByFormUuid($input: getFormDetailsByFormUuid!) {
  getFormBuilderDetailsByFormUuid(input: $input) {
    message
    form {
      bgAccountId
      formUuid
      templateID
      name
      description
      formData
      cardstyle
      cardBackground
    }
  }
}
  `;*/

export const GET_FORMRENDERJSON = gql`
query GetFormBuilderDetailsByFormUuid($formId: String!) {
  getFormBuilderDetailsByFormUuid(name: $formId) {
    name
      phone
      id
      address {
        street
        city
      }
    }
  }
`;


// name of the query is findPersonByName here, variable is nameToSearch
/*export const GET_FORMRENDERJSON = gql`
  query GetFormBuilderDetailsByFormUuid($input: getFormDetailsByFormUuid!) {
    getFormBuilderDetailsByFormUuid(name: $input) {
      name
      phone
      id
      address {
        street
        city
      }
    }
  }
`*/