import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import './MyInputField.css';
import React, { useRef, useState } from 'react';

interface MyInputProps {
  onClick: any
}

const MyInputField: React.FC<MyInputProps> = ({onClick}) => {
  const [isPlay, setIsPlay] = useState(false)
  const inputRef = useRef(null);
  const handleClick = (e: any) => {
    e.preventDefault();
    console.log("pahuncha")
    if (onClick) {
      setIsPlay(!isPlay);
      onClick();
    }
  };

  return (<>
    {/*<form action="#" method="POST">*/}
      <div className="input-container">
        <input 
          className="form-control"
          type="text" 
          placeholder={isPlay?'Pause':'Play'}
          name="pauseplaybtn" 
          required 
          onClick={handleClick} 
          readOnly 
          ref={inputRef}/>
          <FontAwesomeIcon icon={faUser} className="icon" onClick={handleClick}/>
      </div>
    {/*</form>*/}
  </>);
};

export default MyInputField;