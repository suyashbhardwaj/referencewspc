import CampaignList from './CampaignList'
import CampaignDashboard from './CampaignDashboard'
import { Navigate, Route, Routes, Outlet } from 'react-router-dom'
import LeadInfo from './Leads/LeadInfo';

const CampaignRoutes = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/list/*'
          element={
            <>
              <CampaignList />
            </>
          }
        />
        <Route path='dashboard' element={<CampaignDashboard/>} />
        <Route path='lead/:uuid' element={<LeadInfo/>} />
        <Route index element={<Navigate to='/campaign/list' />} />
      </Route>
    </Routes>
  )
}

export default CampaignRoutes
