import React, {useState} from 'react'
import { Dropdown } from '../../../helpers/Dropdown'
import Modal from 'react-modal'

Modal.setAppElement('#root')

const customModalStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-30%, -60%)'
  }
}

interface Props {
    taskResActionMenuRef: React.MutableRefObject<any[]>
    user: any
    index: number
  }

  
const TasksActionMenu: React.FC<Props> = ({
    taskResActionMenuRef,
    user,
    index,
  }) => {
    const [parseResActionMenuDroppedDown, setParseResActionMenuDroppedDown] = useState<boolean>(false)
    /* const [, setIsEditModalOpen] = useState<boolean>(false)
    const [, setIsDeleteModalOpen] = useState<boolean>(false) */
    const [isInformationOneModalOpen, setIsInformationOneModalOpen] = useState<boolean>(false)
    const [isInformationTwoModalOpen, setIsInformationTwoModalOpen] = useState<boolean>(false)
    console.log(user);

    /* const openEditModal = () => {
        setIsEditModalOpen(true)
        setParseResActionMenuDroppedDown(false)
    }

    const afterOpenEditModal = () => {
    }

    const closeEditModal = () => {
        setIsEditModalOpen(false)
    }

    const openDeleteModal = () => {
        setIsDeleteModalOpen(true)
        setParseResActionMenuDroppedDown(false)
    }

    const afterOpenDeleteModal = () => {
    }

    const closeDeleteModal = () => {
        setIsDeleteModalOpen(false)
    } */

    const openInformationOneModal = () => {
        setIsInformationOneModalOpen(true)
        setParseResActionMenuDroppedDown(false)
    }

    const afterOpenInformationOneModal = () => {
    }

    const closeInformationOneModal = () => {
        setIsInformationOneModalOpen(false)
    }

    const openInformationTwoModal = () => {
        setIsInformationTwoModalOpen(true)
        setParseResActionMenuDroppedDown(false)
    }

    const afterOpenInformationTwoModal = () => {
    }

    const closeInformationTwoModal = () => {
        setIsInformationTwoModalOpen(false)
    }

  return (
    <>
      <button
        className={`btn btn-sm btn-icon btn-active-light-primary`}
        onClick={() => {
          setParseResActionMenuDroppedDown(!parseResActionMenuDroppedDown)
        }}
      >
        <i className='bi bi-three-dots-vertical fs-2'></i>
      </button>
      <Dropdown
        menuRef={taskResActionMenuRef.current[index]}
        droppedDown={parseResActionMenuDroppedDown}
        setDroppedDown={setParseResActionMenuDroppedDown}
        width = {150}
        height = {80}
      >
        <>
          <p className='link-primary hoverable' role='button' onClick={openInformationOneModal}>
            {/* <i className='bi bi-info-circle-fill'></i> */}
            <span className='px-2'>Edit</span>
          </p>
          <Modal
            isOpen={isInformationOneModalOpen}
            onAfterOpen={afterOpenInformationOneModal}
            onRequestClose={closeInformationOneModal}
            style={customModalStyles}
            contentLabel='Edit'
          >
            {/* <ResendInviteModal
                  user={user}
                  setIsLoading={setIsLoading}
                  closeRsendInviteModal={closeInformationOneModal}
                /> */}
            <h4>Edit</h4>
          </Modal>
          <p className='link-primary hoverable' role='button' onClick={openInformationTwoModal}>
            {/* <i className='bi bi-info-circle-fill'></i> */}
            <span className='px-2'>Status</span>
          </p>
          <Modal
            isOpen={isInformationTwoModalOpen}
            onAfterOpen={afterOpenInformationTwoModal}
            onRequestClose={closeInformationTwoModal}
            style={customModalStyles}
            contentLabel='Status'
          >
            {/* <CancelInviteModal
                  user={user}
                  setIsLoading={setIsLoading}
                  closeInformationTwoModal={closeInformationTwoModal}
                /> */}
            <h4>Status</h4>
          </Modal>
        </>
      </Dropdown>
    </>

  )
}

export default TasksActionMenu
