import { Navigate, Route, Routes } from 'react-router-dom'
import UserSettings from './users/UserSettings'
import ListMenu from './ListMenu'

const SettingRoutes = () => {
  return(
    <section className='row mx-10'>
        <div className='card px-2  col-2 h-800px  border border-secondary rounded '>
          <ListMenu />
        </div>
        <div className='card col ms-xl-6 d-flex flex-column h-800px scroll-y '>
          <Routes>
            <Route path='users' element={<UserSettings />} />
            <Route index element={<Navigate to='users' />} />
          </Routes>
        </div>
    </section>
  )
}

export default SettingRoutes