import { MainAppRoutes } from './routes/MainAppRoutes'
import { ThemeProvider } from './contexts/ThemeContext'
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
    uri: 'http://localhost:4000',
    cache: new InMemoryCache(),
    });

const SOhome = () => {
	return (
		<ThemeProvider>
			<ApolloProvider client = {client}>
		        <MainAppRoutes />
		    </ApolloProvider>
		</ThemeProvider>
	)
}

export default SOhome