import { Link, useNavigate } from "react-router-dom";
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { useState } from "react";
import { AUTH_TOKEN, BOTGO_USER } from "../../constants";
import './login.css'

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Wrong email format')
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Email is required'),
  password: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Password is required')
})

const initialValues = {
  email: '',
  password: ''
}

export function Login() {
  const [, setLoading] = useState(false)
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues,
    validationSchema: loginSchema,
    onSubmit: (values) => {
      console.log('submition initiated', values)
      setLoading(true)
      localStorage.setItem(AUTH_TOKEN, "sldhgfiw46fw4gfv4893")
      localStorage.setItem(BOTGO_USER, "dummyuser")
      /* signin({
        variables: {
          input: {
            email: values.email,
            password: values.password
          }
        }
      })
        .then((res: any) => {
          setLoading(false)
          if (res.data.signIn.status === 200) {
            setLoading(false)
            localStorage.setItem(AUTH_TOKEN, res.data.signIn.data.token)
            localStorage.setItem(BOTGO_USER, JSON.stringify(res.data.signIn.data.user))
            // navigate('/dashboard');
            document.location.reload()
          } else {
            setLoading(false)
            toast.error(res.data.signIn.message)
            setSubmitting(false)
          }
        })
        .catch((err) => {
          setLoading(false)
          setSubmitting(false)
          console.error(err)
        }) */
    }
  })
  console.log(formik);
  const handleSubmition = (evt: any) => {
    evt.preventDefault();
    console.dir(evt);
    localStorage.setItem(AUTH_TOKEN, "sldhgfiw46fw4gfv4893")
    localStorage.setItem(BOTGO_USER, JSON.stringify({"name":"Virat Kohli"}))
    setTimeout(()=>{
      navigate('/dashboard')
      document.location.reload()
    },2000)
  }
  return (
    <div className="section bg-black">
    <div className="container">
      <div className="row full-height justify-content-center">
        <div className="col-12 text-center align-self-center py-5">
          <div className="section pb-5 pt-5 pt-sm-2 text-center">
            <h6 className="mb-0 pb-3"><span>Log In </span><span>Sign Up</span></h6>
                  <input className="checkbox" type="checkbox" id="reg-log" name="reg-log"/>
                  <label htmlFor="reg-log"></label>
            <div className="card-3d-wrap mx-auto">
              <div className="card-3d-wrapper">
                <div className="card-front">
                  <div className="center-wrap">
                    <div className="section text-center">
                      <form className="form" onSubmit={handleSubmition}>
                        <h4 className="mb-4 pb-3">Log In</h4>
                        
                        <div className="form-group">
                          <input type="email" name="logemail" className="form-style" placeholder="Your Email" id="logemail"/>
                          <i className="input-icon uil uil-at"></i>
                        </div>  
                        <div className="form-group mt-2">
                          <input type="password" name="logpass" className="form-style" placeholder="Your Password" id="logpass"/>
                          <i className="input-icon uil uil-lock-alt"></i>
                        </div>
                      
                        <button
                          type="submit"
                          id="kt_sign_in_submit"
                          className="btn btn-lg btn-primary w-100 mt-4"
                        >
                          Continue
                        </button>

                        <p className="mb-0 mt-4 text-center">
                          {/* start::Link */}
                          <Link
                            to="/auth/forgot-password"
                            className="link link-primary fs-6 fw-bolder"
                            style={{ marginLeft: "5px" }}
                          > Forgot your password?
                          </Link>
                          {/* end::Link */}
                        </p>
                      </form>
                    </div>
                  </div>
                </div>
                <div className="card-back">
                  <div className="center-wrap">
                    <div className="section text-center">
                      <h4 className="mb-4 pb-3">Sign Up</h4>
                      <div className="form-group">
                        <input type="text" name="logname" className="form-style" placeholder="Your Full Name" id="logname"/>
                        <i className="input-icon uil uil-user"></i>
                      </div>  
                      <div className="form-group mt-2">
                        <input type="email" name="logemail" className="form-style" placeholder="Your Email" id="logemail"/>
                        <i className="input-icon uil uil-at"></i>
                      </div>  
                      <div className="form-group mt-2">
                        <input type="password" name="logpass" className="form-style" placeholder="Your Password" id="logpass"/>
                        <i className="input-icon uil uil-lock-alt"></i>
                      </div>
                      <a href="#" className="btn mt-4">submit</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div>
  );
}