import { Link } from 'react-router-dom'

export function Registration() {
  return (
    <form
      className='form w-100 h-650px fv-plugins-bootstrap5 fv-plugins-framework'
      noValidate
      id='kt_login_signup_form'

    >
      {/* begin::Heading */}
      <div className='mb-10 text-center'>
        {/* begin::Title */}
        <h1 className='text-dark mb-3'>Create an Account</h1>
        {/* end::Title */}

        {/* begin::Link */}
        <div className='text-gray-400 fw-bold fs-4'>
          Already have an account?
          <Link to='/auth/login' className='link-primary fw-bolder' style={{ marginLeft: '5px' }}>
            Log In
          </Link>
        </div>
        {/* end::Link */}
      </div>




      {/* begin::Form group Firstname */}
      <div className='row fv-row'>
        <div className='col-xl-6'>
          <label className='class="form-label fw-bolder text-dark fs-6'>First name</label>
          <input
            placeholder='First name'
            type='text'
            autoComplete='off'
            className='form-control form-control-lg form-control-solid'

          />

        </div>
        <div className='col-xl-6'>
          {/* begin::Form group Lastname */}
          <div className='fv-row mb-5'>
            <label className='form-label fw-bolder text-dark fs-6'>Last name</label>
            <input
              placeholder='Last name'
              type='text'
              autoComplete='off'
              className='form-control form-control-lg form-control-solid'

            />

          </div>
          {/* end::Form group */}
        </div>
      </div>
      {/* end::Form group */}

      {/* begin::Form group Email */}
      <div className='fv-row mb-7'>
        <label className='form-label fw-bolder text-dark fs-6'>Email</label>
        <input
          placeholder='Email'
          type='email'
          autoComplete='off'
          className='form-control form-control-lg form-control-solid'

        />

      </div>
      {/* end::Form group */}

      {/* begin::Form group Password */}
      <div className='mb-10 fv-row'>
        <div className='mb-1'>
          <label className='form-label fw-bolder text-dark fs-6'>Password</label>
          <div className='input-group'>
            <input
              placeholder='Password'
              autoComplete='off'
              className='form-control form-control-lg form-control-solid'

            />
            <div className='input-group-append'>
              <span
                className='form-control form-control-lg form-control-solid'
                id='togglePassword'

                style={{
                  cursor: 'pointer',
                  borderRadius: '0px 5px 5px 0px' // Adjust the border radius as needed
                }}
              >

              </span>
            </div>
          </div>
        </div>

        <div className='text-muted'>
          Use 8 or more characters with a mix of letters, numbers & symbols.
        </div>
      </div>
      {/* end::Form group */}

      {/* begin::Form group Confirm password */}
      <div className='fv-row mb-5'>
        <label className='form-label fw-bolder text-dark fs-6'>Confirm Password</label>
        <div className='input-group'>
          <input
            placeholder='Password confirmation'
            autoComplete='off'
            className='form-control form-control-lg form-control-solid'

          />
          <div className='input-group-append'>
            <span
              className='form-control form-control-lg form-control-solid'
              id='togglePassword'

              style={{
                cursor: 'pointer',
                borderRadius: '0px 5px 5px 0px' // Adjust the border radius as needed
              }}
            >

            </span>
          </div>
        </div>

      </div>
      {/* end::Form group */}

      {/* begin::Form group */}
      <div className='fv-row mb-10'>
        <div className='form-check form-check-custom form-check-solid'>
          <input
            className='form-check-input'
            type='checkbox'
            id='kt_login_toc_agree'

          />
          <label
            className='form-check-label fw-bold text-gray-700 fs-6'
            htmlFor='kt_login_toc_agree'
          >
            I Agree the{' '}
            <Link to='/auth/terms' className='ms-1 link-primary'>
              Terms and Conditions
            </Link>
            .
          </label>

        </div>
      </div>
      {/* end::Form group */}

      {/* begin::Form group */}
      <div className='text-center'>
        <button
          type='submit'
          id='kt_sign_up_submit'
          className='btn btn-lg btn-primary w-100 mb-5'
        // disabled={formik.isSubmitting || !formik.isValid || !formik.values.acceptTerms}
        >Submit
          {/* {!loadingOL && <span className='indicator-label'>Submit</span>}
          {loadingOL && (
            <span className='indicator-progress' style={{ display: 'block' }}>
              Please wait...{' '}
              <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
            </span>
          )} */}
        </button>

        <Link to='/auth/login'>
          <button
            type='button'
            id='kt_login_signup_form_cancel_button'
            className='btn btn-lg btn-light-primary w-100 mb-5'
          >
            Cancel
          </button>
        </Link>
      </div>
      {/* end::Form group */}
    </form>
  )
}
