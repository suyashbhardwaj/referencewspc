import React from 'react'


const Footer: React.FC = () => {
  

  return (
    <div className='footer py-4 d-flex flex-lg-column' id='kt_footer'>
      {/* begin::Container */}
      <div
        
      >
        {/* begin::Copyright */}
        <div className='text-dark order-2 order-md-1'>
          <span className='text-muted fw-bold me-2'>
            &copy; Copyright {new Date().getFullYear()} &nbsp;
            <a href='https://globtierinfotech.com' className='text-gray-500 text-hover-primary'>
              Globtier Infotech Pvt Ltd. &nbsp;
            </a>
            All Rights Reserved
          </span>
        </div>
        {/* end::Copyright */}
      </div>
      {/* end::Container */}
    </div>
  )
}

export default Footer
