import React, { useContext, useState } from 'react'
/*import img0 from "../../assets/images/signature3D.png";*/
import { Link } from 'react-router-dom'
import { PropsWithChildren } from 'react'
import '../../assets/dashboardSpecificStyles/style.css'
import { ThemeContext } from '../../contexts/ThemeContext'

interface IProps {
  toggleSidebar: any
}

const HeaderWrapper: React.FC<PropsWithChildren<IProps>> = ({toggleSidebar}) => {
  const [menuOpen, setMenuOpen] = useState<boolean>(false);
  const [isOpenProfileMenu, setIsOpenProfileMenu] = useState<boolean>(false);
  const { toggleTheme } = useContext(ThemeContext);

  return (<>
          {/*header starts*/}
            <header id="header" className="header fixed-top d-flex align-items-center position-fixed">
              <div className="d-flex align-items-center justify-content-between"> 
                <Link to='/dashboard'>
                  <img
                    alt='Logo'
                    src='/media/logos/NoGloryItsmeInpic.png'
                    className='h-60px mb-1'
                    style = {{height:"60px"}}
                  />
                </Link>
                <i className="bi bi-list toggle-sidebar-btn" style={{border:'1px solid red'}} onClick={toggleSidebar}></i>
              </div>
              {/*<!-- End Logo -->*/}

              {/*<!-- Start Search Bar -->*/}
              <div className="search-bar">
                <form className="search-form d-flex align-items-center" method="POST" action="#">
                  <input type="text" name="query" placeholder="Search" title="Enter search keyword"/>
                  <button type="submit" title="Search"><i className="bi bi-search"></i></button>
                </form>
              </div>
              {/*<!-- End Search Bar -->*/}

              <div className={`dropdown ms-2 ${menuOpen?'show':''}`}>
                    <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false" onClick={()=>setMenuOpen((prev)=>!prev)}>
                        <i className="bi bi-menu-button-fill"></i>
                    </button>
                    <ul className="dropdown-menu dropdown-menu-end">
                        <li>
                            <Link to="/campaign/list" className="dropdown-item">
                                Campaign List
                            </Link>
                        </li>
                        <li>
                            <Link to="/campaign/lead/639gi" className="dropdown-item">
                                Lead Information
                            </Link>
                        </li>
                        <li>
                            <Link to="/campaign/dashboard" className="dropdown-item">
                                Campaign Dashboard
                            </Link>
                        </li>
                    </ul>
                </div>

              <nav className="header-nav ms-auto">
                <ul className="d-flex align-items-center">

                  <li className="nav-item d-block d-lg-none">
                    <a className="nav-link nav-icon search-bar-toggle " href="#">
                      <i className="bi bi-search"></i>
                    </a>
                  </li>{/*<!-- End Search Icon-->*/}


                  <li className="nav-item dropdown">

                    <a className="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                      <i className="bi bi-bell"></i>
                      <span className="badge bg-primary badge-number">4</span>
                    </a>{/*<!-- End Notification Icon -->*/}

                    <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">
                      <li className="dropdown-header">
                        You have 4 new notifications
                        <a href="#"><span className="badge rounded-pill bg-primary p-2 ms-2">View all</span></a>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li className="notification-item">
                        <i className="bi bi-exclamation-circle text-warning"></i>
                        <div>
                          <h4>Lorem Ipsum</h4>
                          <p>Quae dolorem earum veritatis oditseno</p>
                          <p>30 min. ago</p>
                        </div>
                      </li>

                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li className="notification-item">
                        <i className="bi bi-x-circle text-danger"></i>
                        <div>
                          <h4>Atque rerum nesciunt</h4>
                          <p>Quae dolorem earum veritatis oditseno</p>
                          <p>1 hr. ago</p>
                        </div>
                      </li>

                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li className="notification-item">
                        <i className="bi bi-check-circle text-success"></i>
                        <div>
                          <h4>Sit rerum fuga</h4>
                          <p>Quae dolorem earum veritatis oditseno</p>
                          <p>2 hrs. ago</p>
                        </div>
                      </li>

                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li className="notification-item">
                        <i className="bi bi-info-circle text-primary"></i>
                        <div>
                          <h4>Dicta reprehenderit</h4>
                          <p>Quae dolorem earum veritatis oditseno</p>
                          <p>4 hrs. ago</p>
                        </div>
                      </li>

                      <li>
                        <hr className="dropdown-divider"/>
                      </li>
                      <li className="dropdown-footer">
                        <a href="#">Show all notifications</a>
                      </li>

                    </ul>
                    {/*<!-- End Notification Dropdown Items -->*/}

                  </li>
                  {/*<!-- End Notification Nav -->*/}

                  <li className="nav-item dropdown">

                    <a className="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                      <i className="bi bi-chat-left-text"></i>
                      <span className="badge bg-success badge-number">3</span>
                    </a>{/*<!-- End Messages Icon -->*/}

                    <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow messages">
                      <li className="dropdown-header">
                        You have 3 new messages
                        <a href="#"><span className="badge rounded-pill bg-primary p-2 ms-2">View all</span></a>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li className="message-item">
                        <a href="#">
                          <img src="assets/img/messages-1.jpg" alt="" className="rounded-circle"/>
                          <div>
                            <h4>Maria Hudson</h4>
                            <p>Velit asperiores et ducimus soluta repudiandae labore officia est ut...</p>
                            <p>4 hrs. ago</p>
                          </div>
                        </a>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li className="message-item">
                        <a href="#">
                          <img src="assets/img/messages-2.jpg" alt="" className="rounded-circle"/>
                          <div>
                            <h4>Anna Nelson</h4>
                            <p>Velit asperiores et ducimus soluta repudiandae labore officia est ut...</p>
                            <p>6 hrs. ago</p>
                          </div>
                        </a>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li className="message-item">
                        <a href="#">
                          <img src="assets/img/messages-3.jpg" alt="" className="rounded-circle"/>
                          <div>
                            <h4>David Muldon</h4>
                            <p>Velit asperiores et ducimus soluta repudiandae labore officia est ut...</p>
                            <p>8 hrs. ago</p>
                          </div>
                        </a>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li className="dropdown-footer">
                        <a href="#">Show all messages</a>
                      </li>

                    </ul>{/*<!-- End Messages Dropdown Items -->*/}

                  </li>
                  {/*<!-- End Messages Nav -->*/}

                  <li className="nav-item dropdown pe-3">

                    <a className="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown" onClick={()=>setIsOpenProfileMenu((prev)=>!prev)}>
                      <img src="assets/img/profile-img.jpg" alt="Profile" className="rounded-circle"/>
                      <span className="d-none d-md-block dropdown-toggle px-2">K. Anderson</span>
                    </a>{/*<!-- End Profile Iamge Icon -->*/}

                    <ul 
                      className={`pt-3 p-0 bg-darker ${isOpenProfileMenu?'':'d-none'}`}
                      style = {{position: "absolute", inset: "0px 0px auto auto", margin: "0px", transform: "translate(-10.0333px, 38.7px)"}}
                      >
                      <li className="dropdown-header">
                        <h6>Kevin Anderson</h6>
                        <span>Web Designer</span>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li>
                        <a className="dropdown-item d-flex align-items-center" href="users-profile.html">
                          <i className="bi bi-person"></i>
                          <span>My Profile</span>
                        </a>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li>
                        <a className="dropdown-item d-flex align-items-center" href="users-profile.html">
                          <i className="bi bi-gear"></i>
                          <span>Account Settings</span>
                        </a>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li>
                        <a className="dropdown-item d-flex align-items-center" href="pages-faq.html">
                          <i className="bi bi-question-circle"></i>
                          <span>Need Help?</span>
                        </a>
                      </li>
                      <li>
                        <hr className="dropdown-divider"/>
                      </li>

                      <li>
                        <Link to = '/logout' className = "dropdown-item d-flex align-items-center">
                          <i className="bi bi-box-arrow-right"></i>
                          <span>Sign Out</span>
                        </Link>
                      </li>

                    </ul>{/*<!-- End Profile Dropdown Items -->*/}
                  </li>
                  {/*<!-- End Profile Nav -->*/}

                  <li nav-item>
                    <div className="form-check form-switch mx-4">
                      <input 
                        className="form-check-input" 
                        id ='togglerswitch' 
                        type="checkbox" 
                        role="switch" 
                        style ={{left:0, opacity: "unset", visibility: "visible"}}
                        onChange = {toggleTheme}
                        />
                      <label htmlFor="togglerswitch">Dark Mode</label>
                    </div>
                  </li>

                </ul>
              </nav>{/*<!-- End Icons Navigation -->*/}
            </header>
          {/*header ends*/}
  </>
  )
}

export default HeaderWrapper
