import { useState } from 'react'
import ReactModal from 'react-modal'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import clsx from 'clsx'

const TestSomeUI = () => {
  const [proceed, setProceed] = useState(false)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [loadingOL, setLoading] = useState(false)
  const [theFiles, setTheFiles] = useState<any>([])
  const [activeTabIndex, setActiveTabIndex] = useState(0)
  let uploadedFiles: any = []

  const tabTitles = [
    'Introduction',
    'PDF Upload',
    'Choose OpenAI Model',
    'Get Price Estimate',
    'Get OpenAI API',
    'Name the Bot'
  ]

  const closeModal = () => {
    setIsModalOpen(false)
  }

  const customModalStyles: ReactModal.Styles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-40%, -50%)'
    }
  }

  const initialValues = {
    fileUploads: []
  }

  const IbotAssistantSchema = Yup.object().shape({
    fileUpload: Yup.string().required('A file upload is required to proceed ahead')
  })

  const formik = useFormik({
    initialValues,
    validationSchema: IbotAssistantSchema,
    onSubmit: (values) => {
      console.log("navigate to some route")
    }
  })

  const handleFileUpload = (event: any) => {
    formik.handleChange(event)
    for (let i = 0; i < event.target.files.length; i++) {
      uploadedFiles.push(event.target.files[i])
    }
    console.log('Uploaded file:', uploadedFiles)
    setTheFiles(uploadedFiles)
  }

  return (
    <>
      <div id='kt_app_content' className='app-content  flex-column-fluid '>
        <div className='card-header d-flex align-items-center justify-content-between py-3'>
          <h2 className='card-title m-0'>Set up your IBot Wizard</h2>
          {/* <button className='btn btn-link'>Take me to IBot Listings</button> */}
        </div>
        <div id='kt_app_content_container' className='app-container  container-xxl '>
          <div className='card'>
            <div className='card-body'>
              <div
                className='stepper stepper-links d-flex flex-column pt-15'
                id='kt_create_account_stepper'
              >
                <div className='stepper-nav justify-content-around flex-wrap mb-5'>
                  {tabTitles.map((title, index) => (
                    <div
                      data-kt-stepper-element='nav'
                      className={`stepper-item nav-link cursor-pointer mx-0 ${
                        activeTabIndex === index ? 'current' : 'pending'
                      }`}
                      onClick={() => {
                        setActiveTabIndex(index)
                      }}
                    >
                      <h3 className='stepper-title'>{title}</h3>
                    </div>
                  ))}
                </div>

                <div>
                  {activeTabIndex === 0 && (
                    <>
                      <div className='card card-xl-stretch mb-xl-8'>
                        <div className='card-header align-items-center border-0'>
                          <h3 className='card-title align-items-start flex-column'>
                            <span className='fw-bold mb-2 text-gray-900'>
                              Introduction - Setting Up Your IBot Assistent
                            </span>
                            <span className='text-muted fw-semibold fs-7'>
                              Easily generate & manage PDFs. Steps involved in creating your IBot
                              Assistant:
                            </span>
                          </h3>

                          <div className='card-toolbar'>
                            <button
                              type='button'
                              className='btn btn-sm btn-icon btn-color-primary btn-active-light-primary'
                              data-kt-menu-trigger='click'
                              data-kt-menu-placement='bottom-end'
                            >
                              <i className='ki-duotone ki-category fs-6'>
                                <span className='path1'></span>
                                <span className='path2'></span>
                                <span className='path3'></span>
                                <span className='path4'></span>
                              </i>
                            </button>
                          </div>
                        </div>

                        <div className='card-body pt-5'>
                          <div className='timeline-label'>
                            <div className='timeline-item'>
                              <div className='timeline-label fw-bold text-gray-800 fs-6'>01</div>

                              <div className='timeline-badge'>
                                <i className='fa fa-genderless text-warning fs-1'></i>
                              </div>

                              <div className='fw-mormal timeline-content text-muted ps-3'>
                                Upload your Data/PDF.
                              </div>
                            </div>

                            <div className='timeline-item'>
                              <div className='timeline-label fw-bold text-gray-800 fs-6'>02</div>

                              <div className='timeline-badge'>
                                <i className='fa fa-genderless text-success fs-1'></i>
                              </div>

                              <div className='timeline-content d-flex'>
                                <span className='fw-bold text-gray-800 ps-3'>
                                  Choose your OpenAI key.
                                </span>
                              </div>
                            </div>

                            <div className='timeline-item'>
                              <div className='timeline-label fw-bold text-gray-800 fs-6'>03</div>

                              <div className='timeline-badge'>
                                <i className='fa fa-genderless text-danger fs-1'></i>
                              </div>

                              <div className='timeline-content fw-bold text-gray-800 ps-3'>
                                Get a price estimate.
                              </div>
                            </div>

                            <div className='timeline-item'>
                              <div className='timeline-label fw-bold text-gray-800 fs-6'>04</div>

                              <div className='timeline-badge'>
                                <i className='fa fa-genderless text-primary fs-1'></i>
                              </div>

                              <div className='timeline-content fw-mormal text-muted ps-3'>
                                Get you OpenAI API.
                              </div>
                            </div>

                            <div className='timeline-item'>
                              <div className='timeline-label fw-bold text-gray-800 fs-6'>05</div>

                              <div className='timeline-badge'>
                                <i className='fa fa-genderless text-danger fs-1'></i>
                              </div>

                              <div className='timeline-content fw-semibold text-gray-800 ps-3'>
                                Query your documents.
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                </div>

                {activeTabIndex === 1 && (
                  <form className='mx-auto mw-600px w-100 pt-15 pb-10' id='kt_create_account_form'>
                    <div className='current' data-kt-stepper-element='content'>
                      <div className='w-100'>
                        <div className='pb-10 pb-lg-15'>
                          <h2 className='fw-bold d-flex align-items-center text-gray-900'>
                            Upload a PDF file
                            <span
                              className='ms-1'
                              data-bs-toggle='tooltip'
                              title='Billing is issued based on your selected account typ'
                            >
                              <i className='ki-duotone ki-information-5 text-gray-500 fs-6'>
                                <span className='path1'></span>
                                <span className='path2'></span>
                                <span className='path3'></span>
                              </i>
                            </span>
                          </h2>
                          <div className='text-muted fw-semibold fs-6'>
                            A PDF file would be needed to blah blah blah...
                            <a href='#' className='link-primary fw-bold'>
                              refer here for more details
                            </a>
                            .
                          </div>
                        </div>
                        <div className='fv-row'>
                          <div className='row'>
                            <div className='col-lg-6'>
                              <input
                                type='file'
                                multiple
                                onChange={handleFileUpload}
                                value={formik.values.fileUploads}
                                {...formik.getFieldProps('fileUpload')}
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  {
                                    'is-invalid':
                                      formik.touched.fileUploads && formik.errors.fileUploads
                                  },
                                  {
                                    'is-valid':
                                      formik.touched.fileUploads && !formik.errors.fileUploads
                                  }
                                )}
                              />

                              {/* <label className="btn btn-outline btn-outline-dashed btn-active-light-primary p-7 d-flex align-items-center mb-10" htmlFor="kt_create_account_form_account_type_personal">
                              <i className="ki-duotone ki-badge fs-3x me-5"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span></i>
                              <span className="d-block fw-semibold text-start">                            
                                  <span className="text-gray-900 fw-bold d-block fs-4 mb-2">
                                      Personal Account
                                  </span>
                                  <span className="text-muted fw-semibold fs-6">If you need more info, please check it out</span>
                              </span>
                            </label>    */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                )}

                <div>
                  {activeTabIndex === 2 && (
                    <div className='mb-0 fv-row'>
                      <label className='d-flex align-items-center form-label mb-5'>
                        Choose Your Context Model
                        <span
                          className='ms-1'
                          data-bs-toggle='tooltip'
                          title='Monthly billing will be based on your account plan'
                        >
                          <i className='ki-duotone ki-information-5 text-gray-500 fs-6'>
                            <span className='path1'></span>
                            <span className='path2'></span>
                            <span className='path3'></span>
                          </i>
                        </span>
                      </label>

                      <div className='mb-0'>
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='d-flex align-items-center me-2'>
                            <span className='symbol symbol-50px me-6'>
                              <span className='symbol-label'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  x='0px'
                                  y='0px'
                                  width='100'
                                  height='100'
                                  viewBox='0 0 24 24'
                                >
                                  <path d='M 11.134766 1.0175781 C 10.87173 1.0049844 10.606766 1.0088281 10.337891 1.0332031 C 8.1135321 1.2338971 6.3362243 2.7940749 5.609375 4.8203125 C 3.8970488 5.1768339 2.4372723 6.3048522 1.671875 7.9570312 C 0.73398779 9.9840533 1.1972842 12.30076 2.5878906 13.943359 C 2.0402591 15.605222 2.2856216 17.434472 3.3320312 18.921875 C 4.6182099 20.747762 6.8565685 21.504693 8.9746094 21.121094 C 10.139659 22.427613 11.84756 23.130452 13.662109 22.966797 C 15.886521 22.766098 17.663809 21.205995 18.390625 19.179688 C 20.102972 18.823145 21.563838 17.695991 22.330078 16.042969 C 23.268167 14.016272 22.805368 11.697142 21.414062 10.054688 C 21.960697 8.3934373 21.713894 6.5648387 20.667969 5.078125 C 19.38179 3.2522378 17.143432 2.4953068 15.025391 2.8789062 C 14.032975 1.7660011 12.646869 1.0899755 11.134766 1.0175781 z M 11.025391 2.5136719 C 11.921917 2.5488523 12.754993 2.8745885 13.431641 3.421875 C 13.318579 3.4779175 13.200103 3.5164101 13.089844 3.5800781 L 9.0761719 5.8964844 C 8.7701719 6.0724844 8.5801719 6.3989531 8.5761719 6.7519531 L 8.5175781 12.238281 L 6.75 11.189453 L 6.75 6.7851562 C 6.75 4.6491563 8.3075938 2.74225 10.433594 2.53125 C 10.632969 2.5115 10.83048 2.5060234 11.025391 2.5136719 z M 16.125 4.2558594 C 17.398584 4.263418 18.639844 4.8251563 19.417969 5.9101562 C 20.070858 6.819587 20.310242 7.9019929 20.146484 8.9472656 C 20.041416 8.8773528 19.948163 8.794144 19.837891 8.7304688 L 15.826172 6.4140625 C 15.520172 6.2380625 15.143937 6.2352031 14.835938 6.4082031 L 10.052734 9.1035156 L 10.076172 7.0488281 L 13.890625 4.8476562 C 14.584375 4.4471562 15.36085 4.2513242 16.125 4.2558594 z M 5.2832031 6.4726562 C 5.2752078 6.5985272 5.25 6.7203978 5.25 6.8476562 L 5.25 11.480469 C 5.25 11.833469 5.4362344 12.159844 5.7402344 12.339844 L 10.464844 15.136719 L 8.6738281 16.142578 L 4.859375 13.939453 C 3.009375 12.871453 2.1365781 10.567094 3.0175781 8.6210938 C 3.4795583 7.6006836 4.2963697 6.8535791 5.2832031 6.4726562 z M 15.326172 7.8574219 L 19.140625 10.060547 C 20.990625 11.128547 21.865375 13.432906 20.984375 15.378906 C 20.522287 16.399554 19.703941 17.146507 18.716797 17.527344 C 18.724764 17.401695 18.75 17.279375 18.75 17.152344 L 18.75 12.521484 C 18.75 12.167484 18.563766 11.840156 18.259766 11.660156 L 13.535156 8.8632812 L 15.326172 7.8574219 z M 12.025391 9.7109375 L 13.994141 10.878906 L 13.966797 13.167969 L 11.974609 14.287109 L 10.005859 13.121094 L 10.03125 10.832031 L 12.025391 9.7109375 z M 15.482422 11.761719 L 17.25 12.810547 L 17.25 17.214844 C 17.25 19.350844 15.692406 21.25775 13.566406 21.46875 C 12.449968 21.579344 11.392114 21.244395 10.568359 20.578125 C 10.681421 20.522082 10.799897 20.48359 10.910156 20.419922 L 14.923828 18.103516 C 15.229828 17.927516 15.419828 17.601047 15.423828 17.248047 L 15.482422 11.761719 z M 13.947266 14.896484 L 13.923828 16.951172 L 10.109375 19.152344 C 8.259375 20.220344 5.8270313 19.825844 4.5820312 18.089844 C 3.9291425 17.180413 3.6897576 16.098007 3.8535156 15.052734 C 3.9587303 15.122795 4.0516754 15.205719 4.1621094 15.269531 L 8.1738281 17.585938 C 8.4798281 17.761938 8.8560625 17.764797 9.1640625 17.591797 L 13.947266 14.896484 z'></path>
                                </svg>
                              </span>
                            </span>

                            <span className='d-flex flex-column'>
                              <span className='fw-bold text-gray-800 text-hover-primary fs-5'>
                                GPT-3.5-turbo
                              </span>
                              <span className='fs-6 fw-semibold text-muted'>
                                Context Window: 4,096 tokens
                              </span>
                            </span>
                          </span>

                          <span className='form-check form-check-custom form-check-solid'>
                            <input
                              className='form-check-input'
                              type='radio'
                              name='account_plan'
                              value='1'
                            />
                          </span>
                        </label>

                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='d-flex align-items-center me-2'>
                            <span className='symbol symbol-50px me-6'>
                              <span className='symbol-label'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  x='0px'
                                  y='0px'
                                  width='100'
                                  height='100'
                                  viewBox='0 0 24 24'
                                >
                                  <path d='M 11.134766 1.0175781 C 10.87173 1.0049844 10.606766 1.0088281 10.337891 1.0332031 C 8.1135321 1.2338971 6.3362243 2.7940749 5.609375 4.8203125 C 3.8970488 5.1768339 2.4372723 6.3048522 1.671875 7.9570312 C 0.73398779 9.9840533 1.1972842 12.30076 2.5878906 13.943359 C 2.0402591 15.605222 2.2856216 17.434472 3.3320312 18.921875 C 4.6182099 20.747762 6.8565685 21.504693 8.9746094 21.121094 C 10.139659 22.427613 11.84756 23.130452 13.662109 22.966797 C 15.886521 22.766098 17.663809 21.205995 18.390625 19.179688 C 20.102972 18.823145 21.563838 17.695991 22.330078 16.042969 C 23.268167 14.016272 22.805368 11.697142 21.414062 10.054688 C 21.960697 8.3934373 21.713894 6.5648387 20.667969 5.078125 C 19.38179 3.2522378 17.143432 2.4953068 15.025391 2.8789062 C 14.032975 1.7660011 12.646869 1.0899755 11.134766 1.0175781 z M 11.025391 2.5136719 C 11.921917 2.5488523 12.754993 2.8745885 13.431641 3.421875 C 13.318579 3.4779175 13.200103 3.5164101 13.089844 3.5800781 L 9.0761719 5.8964844 C 8.7701719 6.0724844 8.5801719 6.3989531 8.5761719 6.7519531 L 8.5175781 12.238281 L 6.75 11.189453 L 6.75 6.7851562 C 6.75 4.6491563 8.3075938 2.74225 10.433594 2.53125 C 10.632969 2.5115 10.83048 2.5060234 11.025391 2.5136719 z M 16.125 4.2558594 C 17.398584 4.263418 18.639844 4.8251563 19.417969 5.9101562 C 20.070858 6.819587 20.310242 7.9019929 20.146484 8.9472656 C 20.041416 8.8773528 19.948163 8.794144 19.837891 8.7304688 L 15.826172 6.4140625 C 15.520172 6.2380625 15.143937 6.2352031 14.835938 6.4082031 L 10.052734 9.1035156 L 10.076172 7.0488281 L 13.890625 4.8476562 C 14.584375 4.4471562 15.36085 4.2513242 16.125 4.2558594 z M 5.2832031 6.4726562 C 5.2752078 6.5985272 5.25 6.7203978 5.25 6.8476562 L 5.25 11.480469 C 5.25 11.833469 5.4362344 12.159844 5.7402344 12.339844 L 10.464844 15.136719 L 8.6738281 16.142578 L 4.859375 13.939453 C 3.009375 12.871453 2.1365781 10.567094 3.0175781 8.6210938 C 3.4795583 7.6006836 4.2963697 6.8535791 5.2832031 6.4726562 z M 15.326172 7.8574219 L 19.140625 10.060547 C 20.990625 11.128547 21.865375 13.432906 20.984375 15.378906 C 20.522287 16.399554 19.703941 17.146507 18.716797 17.527344 C 18.724764 17.401695 18.75 17.279375 18.75 17.152344 L 18.75 12.521484 C 18.75 12.167484 18.563766 11.840156 18.259766 11.660156 L 13.535156 8.8632812 L 15.326172 7.8574219 z M 12.025391 9.7109375 L 13.994141 10.878906 L 13.966797 13.167969 L 11.974609 14.287109 L 10.005859 13.121094 L 10.03125 10.832031 L 12.025391 9.7109375 z M 15.482422 11.761719 L 17.25 12.810547 L 17.25 17.214844 C 17.25 19.350844 15.692406 21.25775 13.566406 21.46875 C 12.449968 21.579344 11.392114 21.244395 10.568359 20.578125 C 10.681421 20.522082 10.799897 20.48359 10.910156 20.419922 L 14.923828 18.103516 C 15.229828 17.927516 15.419828 17.601047 15.423828 17.248047 L 15.482422 11.761719 z M 13.947266 14.896484 L 13.923828 16.951172 L 10.109375 19.152344 C 8.259375 20.220344 5.8270313 19.825844 4.5820312 18.089844 C 3.9291425 17.180413 3.6897576 16.098007 3.8535156 15.052734 C 3.9587303 15.122795 4.0516754 15.205719 4.1621094 15.269531 L 8.1738281 17.585938 C 8.4798281 17.761938 8.8560625 17.764797 9.1640625 17.591797 L 13.947266 14.896484 z'></path>
                                </svg>
                              </span>
                            </span>

                            <span className='d-flex flex-column'>
                              <span className='fw-bold text-gray-800 text-hover-primary fs-5'>
                                GPT-3.5-turbo-16k
                              </span>
                              <span className='fs-6 fw-semibold text-muted'>
                                Context Window: 16,385 tokens
                              </span>
                            </span>
                          </span>

                          <span className='form-check form-check-custom form-check-solid'>
                            <input
                              className='form-check-input'
                              type='radio'
                              checked
                              name='account_plan'
                              value='2'
                            />
                          </span>
                        </label>

                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='d-flex align-items-center me-2'>
                            <span className='symbol symbol-50px me-6'>
                              <span className='symbol-label'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  x='0px'
                                  y='0px'
                                  width='100'
                                  height='100'
                                  viewBox='0 0 24 24'
                                >
                                  <path d='M 11.134766 1.0175781 C 10.87173 1.0049844 10.606766 1.0088281 10.337891 1.0332031 C 8.1135321 1.2338971 6.3362243 2.7940749 5.609375 4.8203125 C 3.8970488 5.1768339 2.4372723 6.3048522 1.671875 7.9570312 C 0.73398779 9.9840533 1.1972842 12.30076 2.5878906 13.943359 C 2.0402591 15.605222 2.2856216 17.434472 3.3320312 18.921875 C 4.6182099 20.747762 6.8565685 21.504693 8.9746094 21.121094 C 10.139659 22.427613 11.84756 23.130452 13.662109 22.966797 C 15.886521 22.766098 17.663809 21.205995 18.390625 19.179688 C 20.102972 18.823145 21.563838 17.695991 22.330078 16.042969 C 23.268167 14.016272 22.805368 11.697142 21.414062 10.054688 C 21.960697 8.3934373 21.713894 6.5648387 20.667969 5.078125 C 19.38179 3.2522378 17.143432 2.4953068 15.025391 2.8789062 C 14.032975 1.7660011 12.646869 1.0899755 11.134766 1.0175781 z M 11.025391 2.5136719 C 11.921917 2.5488523 12.754993 2.8745885 13.431641 3.421875 C 13.318579 3.4779175 13.200103 3.5164101 13.089844 3.5800781 L 9.0761719 5.8964844 C 8.7701719 6.0724844 8.5801719 6.3989531 8.5761719 6.7519531 L 8.5175781 12.238281 L 6.75 11.189453 L 6.75 6.7851562 C 6.75 4.6491563 8.3075938 2.74225 10.433594 2.53125 C 10.632969 2.5115 10.83048 2.5060234 11.025391 2.5136719 z M 16.125 4.2558594 C 17.398584 4.263418 18.639844 4.8251563 19.417969 5.9101562 C 20.070858 6.819587 20.310242 7.9019929 20.146484 8.9472656 C 20.041416 8.8773528 19.948163 8.794144 19.837891 8.7304688 L 15.826172 6.4140625 C 15.520172 6.2380625 15.143937 6.2352031 14.835938 6.4082031 L 10.052734 9.1035156 L 10.076172 7.0488281 L 13.890625 4.8476562 C 14.584375 4.4471562 15.36085 4.2513242 16.125 4.2558594 z M 5.2832031 6.4726562 C 5.2752078 6.5985272 5.25 6.7203978 5.25 6.8476562 L 5.25 11.480469 C 5.25 11.833469 5.4362344 12.159844 5.7402344 12.339844 L 10.464844 15.136719 L 8.6738281 16.142578 L 4.859375 13.939453 C 3.009375 12.871453 2.1365781 10.567094 3.0175781 8.6210938 C 3.4795583 7.6006836 4.2963697 6.8535791 5.2832031 6.4726562 z M 15.326172 7.8574219 L 19.140625 10.060547 C 20.990625 11.128547 21.865375 13.432906 20.984375 15.378906 C 20.522287 16.399554 19.703941 17.146507 18.716797 17.527344 C 18.724764 17.401695 18.75 17.279375 18.75 17.152344 L 18.75 12.521484 C 18.75 12.167484 18.563766 11.840156 18.259766 11.660156 L 13.535156 8.8632812 L 15.326172 7.8574219 z M 12.025391 9.7109375 L 13.994141 10.878906 L 13.966797 13.167969 L 11.974609 14.287109 L 10.005859 13.121094 L 10.03125 10.832031 L 12.025391 9.7109375 z M 15.482422 11.761719 L 17.25 12.810547 L 17.25 17.214844 C 17.25 19.350844 15.692406 21.25775 13.566406 21.46875 C 12.449968 21.579344 11.392114 21.244395 10.568359 20.578125 C 10.681421 20.522082 10.799897 20.48359 10.910156 20.419922 L 14.923828 18.103516 C 15.229828 17.927516 15.419828 17.601047 15.423828 17.248047 L 15.482422 11.761719 z M 13.947266 14.896484 L 13.923828 16.951172 L 10.109375 19.152344 C 8.259375 20.220344 5.8270313 19.825844 4.5820312 18.089844 C 3.9291425 17.180413 3.6897576 16.098007 3.8535156 15.052734 C 3.9587303 15.122795 4.0516754 15.205719 4.1621094 15.269531 L 8.1738281 17.585938 C 8.4798281 17.761938 8.8560625 17.764797 9.1640625 17.591797 L 13.947266 14.896484 z'></path>
                                </svg>
                              </span>
                            </span>

                            <span className='d-flex flex-column'>
                              <span className='fw-bold text-gray-800 text-hover-primary fs-5'>
                                GPT-4
                              </span>
                              <span className='fs-6 fw-semibold text-muted'>
                                Context Window: 8,192 tokens
                              </span>
                            </span>
                          </span>

                          <span className='form-check form-check-custom form-check-solid'>
                            <input
                              className='form-check-input'
                              type='radio'
                              checked
                              name='account_plan'
                              value='3'
                            />
                          </span>
                        </label>

                        <label className='d-flex flex-stack mb-0 cursor-pointer'>
                          <span className='d-flex align-items-center me-2'>
                            <span className='symbol symbol-50px me-6'>
                              <span className='symbol-label'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  x='0px'
                                  y='0px'
                                  width='100'
                                  height='100'
                                  viewBox='0 0 24 24'
                                >
                                  <path d='M 11.134766 1.0175781 C 10.87173 1.0049844 10.606766 1.0088281 10.337891 1.0332031 C 8.1135321 1.2338971 6.3362243 2.7940749 5.609375 4.8203125 C 3.8970488 5.1768339 2.4372723 6.3048522 1.671875 7.9570312 C 0.73398779 9.9840533 1.1972842 12.30076 2.5878906 13.943359 C 2.0402591 15.605222 2.2856216 17.434472 3.3320312 18.921875 C 4.6182099 20.747762 6.8565685 21.504693 8.9746094 21.121094 C 10.139659 22.427613 11.84756 23.130452 13.662109 22.966797 C 15.886521 22.766098 17.663809 21.205995 18.390625 19.179688 C 20.102972 18.823145 21.563838 17.695991 22.330078 16.042969 C 23.268167 14.016272 22.805368 11.697142 21.414062 10.054688 C 21.960697 8.3934373 21.713894 6.5648387 20.667969 5.078125 C 19.38179 3.2522378 17.143432 2.4953068 15.025391 2.8789062 C 14.032975 1.7660011 12.646869 1.0899755 11.134766 1.0175781 z M 11.025391 2.5136719 C 11.921917 2.5488523 12.754993 2.8745885 13.431641 3.421875 C 13.318579 3.4779175 13.200103 3.5164101 13.089844 3.5800781 L 9.0761719 5.8964844 C 8.7701719 6.0724844 8.5801719 6.3989531 8.5761719 6.7519531 L 8.5175781 12.238281 L 6.75 11.189453 L 6.75 6.7851562 C 6.75 4.6491563 8.3075938 2.74225 10.433594 2.53125 C 10.632969 2.5115 10.83048 2.5060234 11.025391 2.5136719 z M 16.125 4.2558594 C 17.398584 4.263418 18.639844 4.8251563 19.417969 5.9101562 C 20.070858 6.819587 20.310242 7.9019929 20.146484 8.9472656 C 20.041416 8.8773528 19.948163 8.794144 19.837891 8.7304688 L 15.826172 6.4140625 C 15.520172 6.2380625 15.143937 6.2352031 14.835938 6.4082031 L 10.052734 9.1035156 L 10.076172 7.0488281 L 13.890625 4.8476562 C 14.584375 4.4471562 15.36085 4.2513242 16.125 4.2558594 z M 5.2832031 6.4726562 C 5.2752078 6.5985272 5.25 6.7203978 5.25 6.8476562 L 5.25 11.480469 C 5.25 11.833469 5.4362344 12.159844 5.7402344 12.339844 L 10.464844 15.136719 L 8.6738281 16.142578 L 4.859375 13.939453 C 3.009375 12.871453 2.1365781 10.567094 3.0175781 8.6210938 C 3.4795583 7.6006836 4.2963697 6.8535791 5.2832031 6.4726562 z M 15.326172 7.8574219 L 19.140625 10.060547 C 20.990625 11.128547 21.865375 13.432906 20.984375 15.378906 C 20.522287 16.399554 19.703941 17.146507 18.716797 17.527344 C 18.724764 17.401695 18.75 17.279375 18.75 17.152344 L 18.75 12.521484 C 18.75 12.167484 18.563766 11.840156 18.259766 11.660156 L 13.535156 8.8632812 L 15.326172 7.8574219 z M 12.025391 9.7109375 L 13.994141 10.878906 L 13.966797 13.167969 L 11.974609 14.287109 L 10.005859 13.121094 L 10.03125 10.832031 L 12.025391 9.7109375 z M 15.482422 11.761719 L 17.25 12.810547 L 17.25 17.214844 C 17.25 19.350844 15.692406 21.25775 13.566406 21.46875 C 12.449968 21.579344 11.392114 21.244395 10.568359 20.578125 C 10.681421 20.522082 10.799897 20.48359 10.910156 20.419922 L 14.923828 18.103516 C 15.229828 17.927516 15.419828 17.601047 15.423828 17.248047 L 15.482422 11.761719 z M 13.947266 14.896484 L 13.923828 16.951172 L 10.109375 19.152344 C 8.259375 20.220344 5.8270313 19.825844 4.5820312 18.089844 C 3.9291425 17.180413 3.6897576 16.098007 3.8535156 15.052734 C 3.9587303 15.122795 4.0516754 15.205719 4.1621094 15.269531 L 8.1738281 17.585938 C 8.4798281 17.761938 8.8560625 17.764797 9.1640625 17.591797 L 13.947266 14.896484 z'></path>
                                </svg>
                              </span>
                            </span>

                            <span className='d-flex flex-column'>
                              <span className='fw-bold text-gray-800 text-hover-primary fs-5'>
                                GPT-4-32k
                              </span>
                              <span className='fs-6 fw-semibold text-muted'>
                                Context Window: 32,768 tokens
                              </span>
                            </span>
                          </span>

                          <span className='form-check form-check-custom form-check-solid'>
                            <input
                              className='form-check-input'
                              type='radio'
                              name='account_plan'
                              value='4'
                            />
                          </span>
                        </label>
                      </div>
                    </div>
                  )}
                </div>

                <div className='d-flex justify-content-center'>
                  {activeTabIndex === 3 && (
                    <>
                      <div className='w-50 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75 py-10 px-10'>
                        <div className='mb-7 text-center'>
                          <h1 className='text-gray-900 mb-5 fw-bolder'>Advanced</h1>

                          <div className='text-gray-600 fw-semibold mb-5'>
                            Optimal for 100+ team siz
                            <br />e and grown company
                          </div>

                          <div className='text-center'>
                            <span className='mb-2 text-primary'>$</span>

                            <span
                              className='fs-3x fw-bold text-primary'
                              data-kt-plan-price-month='339'
                              data-kt-plan-price-annual='3399'
                            >
                              339{' '}
                            </span>

                            <span className='fs-7 fw-semibold opacity-50'>
                              /<span data-kt-element='period'>Mon</span>
                            </span>
                          </div>
                        </div>

                        <div className='w-100 mb-10 text-center'>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3'>
                              Up to 10 Active Users{' '}
                            </span>
                            <i className='ki-duotone ki-check-circle fs-1 text-success'>
                              <span className='path1'></span>
                              <span className='path2'></span>
                            </i>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3'>
                              Up to 30 Project Integrations{' '}
                            </span>
                            <i className='ki-duotone ki-check-circle fs-1 text-success'>
                              <span className='path1'></span>
                              <span className='path2'></span>
                            </i>
                          </div>
                          <div className='d-flex align-items-center '>
                            <span className='fw-semibold fs-6 text-gray-600 flex-grow-1'>
                              Unlimited Cloud Space{' '}
                            </span>
                            <i className='ki-duotone ki-cross-circle fs-1'>
                              <span className='path1'></span>
                              <span className='path2'></span>
                            </i>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                </div>

                <div>
                  {activeTabIndex === 4 && (
                    <>
                      <div className='border-bottom'>
                        <input
                          className='form-control form-control-transparent border-0 px-8 min-h-45px'
                          name='compose_subject'
                          placeholder='Please provide your OpenAI key here'
                        />
                      </div>
                    </>
                  )}
                </div>

                <div>
                  {activeTabIndex === 5 && (
                    <>
                      <div className='border-bottom'>
                        <input
                          className='form-control form-control-transparent border-0 px-8 min-h-45px'
                          name='compose_subject'
                          placeholder='Provide a name for your Ibot assistent'
                        />
                      </div>
                    </>
                  )}
                </div>
              </div>
              <div className='d-flex pt-10 justify-content-end '>
                <div className='me-2'>
                  <button
                    type='button'
                    className='btn btn-lg btn-light-primary me-3 position-sticky top-10 right-10'
                    data-kt-stepper-action='previous'
                    onClick={() => {
                      activeTabIndex > 0 && setActiveTabIndex((prev) => prev - 1)
                    }}
                  >
                    <i className='ki-duotone ki-arrow-left fs-3 me-1'>
                      <span className='path1'></span>
                      <span className='path2'></span>
                    </i>
                    Back
                  </button>
                </div>
                <div>
                  {/* <button type="button" className="btn btn-lg btn-primary" data-kt-stepper-action="submit">
                    <span className="indicator-label">
                      Submit
                      <i className="ki-duotone ki-arrow-right fs-3 ms-2 me-0"><span className="path1"></span><span className="path2"></span></i>                                        </span>
                    <span className="indicator-progress">
                      Please wait... <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                    </span>
                  </button> */}

                  <button
                    type='button'
                    className='btn btn-lg btn-primary position-sticky top-10 right-10'
                    data-kt-stepper-action='next'
                    onClick={() => {
                      activeTabIndex < 5 && setActiveTabIndex((prev) => prev + 1)
                    }}
                  >
                    Next
                    <i className='ki-duotone ki-arrow-right fs-3 ms-1 me-0'>
                      <span className='path1'></span>
                      <span className='path2'></span>
                    </i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default TestSomeUI