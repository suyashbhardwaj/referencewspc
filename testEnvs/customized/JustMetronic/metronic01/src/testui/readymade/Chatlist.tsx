const Chatlist = () => {
	return (
		<div>
			<div className="flex-column flex-lg-row-auto w-100 w-lg-300px w-xl-400px mb-10 mb-lg-0">
        		{/*begin::Contacts*/}
				<div className="card card-flush">
				    {/*begin::Card header*/}
				    <div className="card-header pt-7" id="kt_chat_contacts_header">
				        {/*begin::Form*/}
				        <form className="w-100 position-relative">	
				            {/*begin::Icon*/}
				            <i className="ki-duotone ki-magnifier fs-3 text-gray-500 position-absolute top-50 ms-5 translate-middle-y"><span className="path1"></span><span className="path2"></span></i>            {/*end::Icon*/}

				            {/*begin::Input*/}
				            <input type="text" className="form-control form-control-solid px-13" name="search" value="" placeholder="Search by username or email..."/>
				            {/*end::Input*/}
				            {/*begin::User*/}
				                <div className="d-flex flex-stack py-4">
				                    {/*begin::Details*/}
				                    <div className="d-flex align-items-center">
				                        {/*begin::Label*/}
				                        	<label><h6>Our Team</h6></label>
				                        {/*end::Label*/}
				                    </div>
				                    {/*end::Details*/}

				                    {/*begin::actionbuttons*/}
				                    <div className="d-flex flex-column align-items-end ms-2">
				                        <div className="d-flex">
					                        <span className="text-muted fs-7 mb-1 px-5"><i className="bi bi-person-add fs-2x"></i></span>
					                        <span className="text-muted fs-7 mb-1"><i className="bi bi-caret-down-square fs-2x"></i></span>
				                    	</div>    
			                        </div>
				                    {/*end::actionbuttons*/}
				                </div>
				        </form>
				        {/*end::Form*/}
				    </div>
				    {/*end::Card header*/}

				    {/*begin::Card body*/}
				    <div className="card-body pt-5" id="kt_chat_contacts_body">
				        {/*begin::List*/}
				        <div className="scroll-y me-n5 pe-5 h-200px h-lg-auto" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_app_header, #kt_toolbar, #kt_app_toolbar, #kt_footer, #kt_app_footer, #kt_chat_contacts_header" data-kt-scroll-wrappers="#kt_content, #kt_app_content, #kt_chat_contacts_body" data-kt-scroll-offset="5px" style={{maxHeight: '150px'}}>
				                {/*begin::User*/}
				                <div className="d-flex flex-stack py-4">
				                    {/*begin::Details*/}
				                    <div className="d-flex align-items-center">
				                        {/*begin::Avatar*/}
				                        <div className="symbol  symbol-45px symbol-circle ">
				                        	<img alt="Pic" src="/metronic8/demo1/assets/media/avatars/300-5.jpg"/>
				                        <div className="symbol-badge bg-success start-100 top-100 border-4 h-8px w-8px ms-n2 mt-n2"></div>
				                        </div>
				                        {/*end::Avatar*/}
				                        {/*begin::Details*/}
				                        <div className="ms-5">
				                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
				                            <div className="fw-semibold text-muted">Was machst du?</div>
				                        </div>
				                        {/*end::Details*/}
				                    </div>
				                    {/*end::Details*/}

				                    {/*begin::Last seen*/}
				                    <div className="d-flex flex-column align-items-end ms-2">
				                        <div className="d-flex">
					                        <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
					                        <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
				                    	</div>    
			                        </div>
				                    {/*end::Last seen*/}
				                </div>
				                {/*end::User*/}
				                {/*begin::Separator*/}
				                <div className="separator separator-dashed d-none"></div>
				                {/*end::Separator*/}

				                {/*begin::User*/}
				                <div className="d-flex flex-stack py-4">
				                    {/*begin::Details*/}
				                    <div className="d-flex align-items-center">
				                        {/*begin::Avatar*/}
				                        <div className="symbol  symbol-45px symbol-circle ">
				                        <span className="symbol-label  bg-light-danger text-danger fs-6 fw-bolder ">M</span>
				                        </div>
				                        {/*end::Avatar*/}
				                        {/*begin::Details*/}
				                        <div className="ms-5">
				                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
				                            <div className="fw-semibold text-muted">Was machst du?</div>
				                        </div>
				                        {/*end::Details*/}
				                    </div>
				                    {/*end::Details*/}

				                    {/*begin::Last seen*/}
				                    <div className="d-flex flex-column align-items-end ms-2">
				                        <div className="d-flex">
					                        <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
					                        <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
				                    	</div>    
			                        </div>
				                    {/*end::Last seen*/}
				                </div>
				                {/*end::User*/}
				                {/*begin::Separator*/}
				                <div className="separator separator-dashed d-none"></div>
				                {/*end::Separator*/}

				                {/*begin::User*/}
				                <div className="d-flex flex-stack py-4">
				                    {/*begin::Details*/}
				                    <div className="d-flex align-items-center">
				                        {/*begin::Avatar*/}
				                        <div className="symbol  symbol-45px symbol-circle ">
				                        <span className="symbol-label  bg-light-danger text-danger fs-6 fw-bolder ">M</span>
				                        </div>
				                        {/*end::Avatar*/}
				                        {/*begin::Details*/}
				                        <div className="ms-5">
				                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
				                            <div className="fw-semibold text-muted">Was machst du?</div>
				                        </div>
				                        {/*end::Details*/}
				                    </div>
				                    {/*end::Details*/}

				                    {/*begin::Last seen*/}
				                    <div className="d-flex flex-column align-items-end ms-2">
				                        <div className="d-flex">
					                        <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
					                        <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
				                    	</div>    
			                        </div>
				                    {/*end::Last seen*/}
				                </div>
				                {/*end::User*/}
				                {/*begin::Separator*/}
				                <div className="separator separator-dashed d-none"></div>
				                {/*end::Separator*/}


				                {/*begin::User*/}
				                <div className="d-flex flex-stack py-4">
				                    {/*begin::Details*/}
				                    <div className="d-flex align-items-center">
				                        {/*begin::Avatar*/}
				                        <div className="symbol  symbol-45px symbol-circle ">
				                        <span className="symbol-label  bg-light-danger text-danger fs-6 fw-bolder ">M</span>
				                        </div>
				                        {/*end::Avatar*/}
				                        {/*begin::Details*/}
				                        <div className="ms-5">
				                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
				                            <div className="fw-semibold text-muted">Was machst du?</div>
				                        </div>
				                        {/*end::Details*/}
				                    </div>
				                    {/*end::Details*/}

				                    {/*begin::Last seen*/}
				                    <div className="d-flex flex-column align-items-end ms-2">
				                        <div className="d-flex">
					                        <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
					                        <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
				                    	</div>    
			                        </div>
				                    {/*end::Last seen*/}
				                </div>
				                {/*end::User*/}
				                {/*begin::Separator*/}
				                <div className="separator separator-dashed d-none"></div>
				                {/*end::Separator*/}


				                {/*begin::User*/}
				                <div className="d-flex flex-stack py-4">
				                    {/*begin::Details*/}
				                    <div className="d-flex align-items-center">
				                        {/*begin::Avatar*/}
				                        <div className="symbol  symbol-45px symbol-circle ">
				                        <span className="symbol-label  bg-light-danger text-danger fs-6 fw-bolder ">M</span>
				                        </div>
				                        {/*end::Avatar*/}
				                        {/*begin::Details*/}
				                        <div className="ms-5">
				                            <a href="#" className="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Beuger</a>
				                            <div className="fw-semibold text-muted">Was machst du?</div>
				                        </div>
				                        {/*end::Details*/}
				                    </div>
				                    {/*end::Details*/}

				                    {/*begin::Last seen*/}
				                    <div className="d-flex flex-column align-items-end ms-2">
				                        <div className="d-flex">
					                        <span className="text-muted fs-7 mb-1 px-1"><i className="bi bi-mic-fill fs-2x"></i></span>
					                        <span className="text-muted fs-7 mb-1"><i className="bi bi-x-circle-fill fs-2x"></i></span>
				                    	</div>    
			                        </div>
				                    {/*end::Last seen*/}
				                </div>
				                {/*end::User*/}
				                {/*begin::Separator*/}
				                <div className="separator separator-dashed d-none"></div>
				                {/*end::Separator*/}

				        </div>
				        {/*end::List*/}
				    </div>
				    {/*end::Card body*/}
				</div>
				{/*end::Contacts*/}
			</div>
		</div>
	)
}

export default Chatlist