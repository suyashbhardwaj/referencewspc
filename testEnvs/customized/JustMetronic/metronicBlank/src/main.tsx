import React from 'react'
/*import ReactDOM from 'react-dom/client'*/
import ReactDOM from 'react-dom/client'
/*import App from './App.tsx'*/
/* import './index.css' */
import './assets/scss/style.scss'
/*import TestSomeUI from './testui/readymade/TestSomeUI'*/
import SOhome from './SOhome'
/*import Chatlist from './testui/readymade/Chatlist'*/
/*import Timeline from './testui/readymade/Timeline'*/

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
  {/*<div className='container mt-10'>
    <Chatlist/>
  </div>*/}
    {/*<App />*/}
    {/*<Timeline/>*/}
    <SOhome/>
    {/*<TestSomeUI/>*/}
  </React.StrictMode>,
)
