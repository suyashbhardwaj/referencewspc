import Chart from "react-apexcharts";
import { useState } from 'react';

const initialData = {
      options: {
        chart: {
          id: "basic-bar"
        },
        xaxis: {
          categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
        }
      },
      series: [
        {
          name: "series-1",
          data: [30, 40, 45, 50, 49, 60, 70, 91]
        }
      ]
    };

const Apexchart = () => {
  const [state, setState] = useState(initialData)
  const radarData = {options: {
  series: [
    {
      name: "Radar Series 1",
      data: [45, 52, 38, 24, 33, 10]
    },
    {
      name: "Radar Series 2",
      data: [26, 21, 20, 6, 8, 15]
    }
  ],
  fill: {
    opacity: 0.5,
    colors: []
  },
  labels: ['April', 'May', 'June', 'July', 'August', 'September']
}};

  return (
    <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={state.options}
              series={state.series}
              type="bar"
              width="500"
            />
          </div>
        </div>
      </div>
  )
}

export default Apexchart