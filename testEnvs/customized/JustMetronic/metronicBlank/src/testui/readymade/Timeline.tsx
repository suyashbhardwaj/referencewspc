const Timeline = () => {
	return (
		<div className = "container">
			<div className="col-xl-4">
				{/*begin::List Widget 5*/}
				<div className="card card-xl-stretch mb-xl-8">
				    {/*begin::Header*/}
				    <div className="card-header align-items-center border-0 mt-4">
				        <h3 className="card-title align-items-start flex-column">
				            <span className="fw-bold mb-2 text-gray-900">Activities</span>            
				            <span className="text-muted fw-semibold fs-7">890,344 Sales</span>
				        </h3>

				        <div className="card-toolbar">
				            {/*begin::Menu*/}
				            <button type="button" className="btn btn-sm btn-icon btn-color-primary btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
				                <i className="ki-duotone ki-category fs-6"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span></i>            </button>
				{/*begin::Menu 1*/}
				<div className="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_65e57162815df">
				    {/*begin::Header*/}
				    <div className="px-7 py-5">
				        <div className="fs-5 text-gray-900 fw-bold">Filter Options</div>
				    </div>
				    {/*end::Header*/}

				    {/*begin::Menu separator*/}
				    <div className="separator border-gray-200"></div>
				    {/*end::Menu separator*/}
				    

				    {/*begin::Form*/}
				    <div className="px-7 py-5">
				        {/*begin::Input group*/}
				        <div className="mb-10">
				            {/*begin::Label*/}
				            <label className="form-label fw-semibold">Status:</label>
				            {/*end::Label*/}

				            {/*begin::Input*/}
				            <div>
				                <select className="form-select form-select-solid select2-hidden-accessible" multiple data-kt-select2="true" data-close-on-select="false" data-placeholder="Select option" data-dropdown-parent="#kt_menu_65e57162815df" data-allow-clear="true" data-select2-id="select2-data-11-brli" tabindex="-1" aria-hidden="true" data-kt-initialized="1">
				                    <option></option>
				                    <option value="1">Approved</option>
				                    <option value="2">Pending</option>
				                    <option value="2">In Process</option>
				                    <option value="2">Rejected</option>
				                </select><span className="w-100 select2 select2-container select2-container--bootstrap5" dir="ltr" data-select2-id="select2-data-12-bswm" ><span className="selection"><span className="select2-selection select2-selection--multiple form-select form-select-solid" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-disabled="false"><ul className="select2-selection__rendered" id="select2-2drr-container"></ul><span className="select2-search select2-search--inline"><textarea className="select2-search__field" type="search" tabindex="0" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" autocomplete="off" aria-label="Search" aria-describedby="select2-2drr-container" placeholder="Select option" style={{width: "100"}}></textarea></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
				            </div>
				            {/*end::Input*/}
				        </div>
				        {/*end::Input group*/}

				        {/*begin::Input group*/}
				        <div className="mb-10">
				            {/*begin::Label*/}
				            <label className="form-label fw-semibold">Member Type:</label>
				            {/*end::Label*/}

				            {/*begin::Options*/}
				            <div className="d-flex">
				                {/*begin::Options*/}    
				                <label className="form-check form-check-sm form-check-custom form-check-solid me-5">
				                    <input className="form-check-input" type="checkbox" value="1"/>
				                    <span className="form-check-label">
				                        Author
				                    </span>
				                </label>
				                {/*end::Options*/}    

				                {/*begin::Options*/}    
				                <label className="form-check form-check-sm form-check-custom form-check-solid">
				                    <input className="form-check-input" type="checkbox" value="2" checked/>
				                    <span className="form-check-label">
				                        Customer
				                    </span>
				                </label>
				                {/*end::Options*/}    
				            </div>        
				            {/*end::Options*/}    
				        </div>
				        {/*end::Input group*/}

				        {/*begin::Input group*/}
				        <div className="mb-10">
				            {/*begin::Label*/}
				            <label className="form-label fw-semibold">Notifications:</label>
				            {/*end::Label*/}

				            {/*begin::Switch*/}
				            <div className="form-check form-switch form-switch-sm form-check-custom form-check-solid">
				                <input className="form-check-input" type="checkbox" value="" name="notifications" checked/>
				                <label className="form-check-label">
				                    Enabled
				                </label>
				            </div>
				            {/*end::Switch*/}
				        </div>
				        {/*end::Input group*/}

				        {/*begin::Actions*/}
				        <div className="d-flex justify-content-end">
				            <button type="reset" className="btn btn-sm btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true">Reset</button>

				            <button type="submit" className="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
				        </div>
				        {/*end::Actions*/}
				    </div>
				    {/*end::Form*/}
				</div>
				{/*end::Menu 1*/}
				{/*end::Menu*/}
				        </div>
				    </div>
				    {/*end::Header*/}

				    {/*begin::Body*/}
				    <div className="card-body pt-5">
				        {/*begin::Timeline*/}
				        <div className="timeline-label">
				            {/*begin::Item*/}
				            <div className="timeline-item">
				                {/*begin::Label*/}
				                <div className="timeline-label fw-bold text-gray-800 fs-6">08:42</div>
				                {/*end::Label*/}

				                {/*begin::Badge*/}
				                <div className="timeline-badge">
				                    <i className="fa fa-genderless text-warning fs-1"></i>
				                </div>
				                {/*end::Badge*/}

				                {/*begin::Text*/}
				                <div className="fw-mormal timeline-content text-muted ps-3">
				                    Outlines keep you honest. And keep structure
				                </div>
				                {/*end::Text*/}
				            </div>
				            {/*end::Item*/}

				            {/*begin::Item*/}
				            <div className="timeline-item">
				                {/*begin::Label*/}
				                <div className="timeline-label fw-bold text-gray-800 fs-6">10:00</div>
				                {/*end::Label*/}

				                {/*begin::Badge*/}
				                <div className="timeline-badge">
				                    <i className="fa fa-genderless text-success fs-1"></i>
				                </div>
				                {/*end::Badge*/}

				                {/*begin::Content*/}
				                <div className="timeline-content d-flex">
				                    <span className="fw-bold text-gray-800 ps-3">AEOL meeting</span>
				                </div>
				                {/*end::Content*/}
				            </div>
				            {/*end::Item*/}

				            {/*begin::Item*/}
				            <div className="timeline-item">
				                {/*begin::Label*/}
				                <div className="timeline-label fw-bold text-gray-800 fs-6">14:37</div>
				                {/*end::Label*/}

				                {/*begin::Badge*/}
				                <div className="timeline-badge">
				                    <i className="fa fa-genderless text-danger fs-1"></i>
				                </div>
				                {/*end::Badge*/}

				                {/*begin::Desc*/}
				                <div className="timeline-content fw-bold text-gray-800 ps-3">
				                    Make deposit
				                    <a href="#" className="text-primary">USD 700</a>.
				                    to ESL
				                </div>
				                {/*end::Desc*/}
				            </div>
				            {/*end::Item*/}

				            {/*begin::Item*/}
				            <div className="timeline-item">
				                {/*begin::Label*/}
				                <div className="timeline-label fw-bold text-gray-800 fs-6">16:50</div>
				                {/*end::Label*/}

				                {/*begin::Badge*/}
				                <div className="timeline-badge">
				                    <i className="fa fa-genderless text-primary fs-1"></i>
				                </div>
				                {/*end::Badge*/}

				                {/*begin::Text*/}
				                <div className="timeline-content fw-mormal text-muted ps-3">
				                    Indulging in poorly driving and keep structure keep great
				                </div>
				                {/*end::Text*/}
				            </div>
				            {/*end::Item*/}

				            {/*begin::Item*/}
				            <div className="timeline-item">
				                {/*begin::Label*/}
				                <div className="timeline-label fw-bold text-gray-800 fs-6">21:03</div>
				                {/*end::Label*/}

				                {/*begin::Badge*/}
				                <div className="timeline-badge">
				                    <i className="fa fa-genderless text-danger fs-1"></i>
				                </div>
				                {/*end::Badge*/}

				                {/*begin::Desc*/}
				                <div className="timeline-content fw-semibold text-gray-800 ps-3">
				                    New order placed <a href="#" className="text-primary">#XF-2356</a>.
				                </div>
				                {/*end::Desc*/}
				            </div>
				            {/*end::Item*/}

				            {/*begin::Item*/}
				            <div className="timeline-item">
				                {/*begin::Label*/}
				                <div className="timeline-label fw-bold text-gray-800 fs-6">16:50</div>
				                {/*end::Label*/}

				                {/*begin::Badge*/}
				                <div className="timeline-badge">
				                    <i className="fa fa-genderless text-primary fs-1"></i>
				                </div>
				                {/*end::Badge*/}

				                {/*begin::Text*/}
				                <div className="timeline-content fw-mormal text-muted ps-3">
				                    Indulging in poorly driving and keep structure keep great
				                </div>
				                {/*end::Text*/}
				            </div>
				            {/*end::Item*/}

				            {/*begin::Item*/}
				            <div className="timeline-item">
				                {/*begin::Label*/}
				                <div className="timeline-label fw-bold text-gray-800 fs-6">21:03</div>
				                {/*end::Label*/}

				                {/*begin::Badge*/}
				                <div className="timeline-badge">
				                    <i className="fa fa-genderless text-danger fs-1"></i>
				                </div>
				                {/*end::Badge*/}

				                {/*begin::Desc*/}
				                <div className="timeline-content fw-semibold text-gray-800 ps-3">
				                    New order placed <a href="#" className="text-primary">#XF-2356</a>.
				                </div>
				                {/*end::Desc*/}
				            </div>
				            {/*end::Item*/}

				                            {/*begin::Item*/}
				                <div className="timeline-item">
				                    {/*begin::Label*/}
				                    <div className="timeline-label fw-bold text-gray-800 fs-6">10:30</div>
				                    {/*end::Label*/}

				                    {/*begin::Badge*/}
				                    <div className="timeline-badge">
				                        <i className="fa fa-genderless text-success fs-1"></i>
				                    </div>
				                    {/*end::Badge*/}

				                    {/*begin::Text*/}
				                    <div className="timeline-content fw-mormal text-muted ps-3">
				                        Finance KPI Mobile app launch preparion meeting
				                    </div>
				                    {/*end::Text*/}
				                </div>
				                {/*end::Item*/}
				                    </div>
				        {/*end::Timeline*/}
				    </div>
				    {/*end: Card Body*/}
				</div>
				{/*end: List Widget 5*/}
    		</div>
		</div>
	)
}

export default Timeline