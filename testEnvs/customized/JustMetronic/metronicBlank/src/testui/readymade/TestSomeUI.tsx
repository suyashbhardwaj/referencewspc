const TestSomeUI = () => {
	return (
		<>
			{/*<div className="float-start">Float start on all viewport sizes</div><br/>
			<div className="float-end">Float end on all viewport sizes</div><br/>
			<div className="float-none">Don't float on all viewport sizes</div>
			<hr/>
			<div className="float-sm-end">Float end on viewports sized SM (small) or wider</div><br/>
			<div className="float-md-end">Float end on viewports sized MD (medium) or wider</div><br/>
			<div className="float-lg-end">Float end on viewports sized LG (large) or wider</div><br/>
			<div className="float-xl-end">Float end on viewports sized XL (extra large) or wider</div><br/>
			<div className="float-xxl-end">Float end on viewports sized XXL (extra extra large) or wider</div><br/>*/}

			<div className="table-responsive">
			  <table className="table">
			  	<thead className="table-light">
					<tr>
				      <th scope="col">#</th>
				      <th scope="col">First</th>
				      <th scope="col">Last</th>
				      <th scope="col">Handle</th>
				    </tr>
				</thead>
				<tbody className="table-group-divider">
					<tr>
				      <th scope="row">1</th>
				      <td>Mark</td>
				      <td>Otto</td>
				      <td>@mdo</td>
				    </tr>
				    <tr>
				      <th scope="row">2</th>
				      <td>Jacob</td>
				      <td>Thornton</td>
				      <td>@fat</td>
				    </tr>
				    <tr>
				      <th scope="row">3</th>
				      <td colSpan={2}>Larry the Bird</td>
				      <td>@twitter</td>
				    </tr>
				</tbody> 
			  </table>
			</div>
		</>
	)
}

export default TestSomeUI