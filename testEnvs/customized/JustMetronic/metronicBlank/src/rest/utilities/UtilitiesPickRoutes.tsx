import { Navigate, Route, Routes, Outlet } from 'react-router-dom'
import CaptchaDiv from './captcha/CaptchaDiv'
const UtilitiesPickRoutes = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/captcha/*'
          element={
            <>
              <CaptchaDiv />
            </>
          }
        />
        {/*<Route path='dashboard' element={<CampaignDashboard/>} />
        <Route path='lead/:uuid' element={<LeadInfo/>} />*/}
        <Route index element={<Navigate to='/utilitypicks/captcha' />} />
      </Route>
    </Routes>
  )
}

export default UtilitiesPickRoutes