import { useState } from 'react';
import {v4 as uuid} from 'uuid'

const useMyTodoHook = (initialValue) => {
	const [todos, setTodos] = useState(initialValue);
	
	return {
		todos: todos,
		toggleCheck: (todoid) => {
			const updatedTodos = todos.map(todo => todo.id === todoid ? {...todo, completed: !todo.completed} : todo);
			setTodos(updatedTodos); },
		addTodo: (thetodo) => { setTodos([...todos, {id: uuid(), task:thetodo, completed: false }]) },
	 	removeTodo: (todoid) => {
			const updatedTodos = todos.filter(todo => todo.id!==todoid);
			setTodos(updatedTodos); },
	 	editTodo: (todoid, revisedTask) => { 
	 		const updatedTodos = todos.map(todo => todo.id === todoid ? {...todo, task: revisedTask} : todo); setTodos(updatedTodos) }
		}
	}

export default useMyTodoHook