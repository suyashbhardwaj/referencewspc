import React from 'react'
import useInputState from './hooks/useInputState'

const TodoForm = ({addTodo}) => {
	const [input, setInput, resetInput] = useInputState();
	return (
		<div className='card mt-6'>
			<form onSubmit = {(evt)=> {
				evt.preventDefault();
				addTodo(input)
				resetInput();
			}}>
				<input type = "text"
					onChange = {setInput} 
					value = {input} 
					name = 'addtodofield'
					className='form-control'
					placeholder='Enter a ToDo item here'
				/>
			</form>
		</div>
	)	
}

export default TodoForm