import { Navigate, Route, Routes, Outlet } from 'react-router-dom'
import TodoApp from './thetodoApp/TodoApp'
import DexieIndexedDBapp from './browserDBApp/DexieIndexedDBapp'
const ProjectPickRoutes = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/todoapp/*'
          element={
            <>
              <TodoApp />
            </>
          }
        />
        <Route path='browserdbapp' element={<DexieIndexedDBapp/>} />
        {/*<Route path='lead/:uuid' element={<LeadInfo/>} />*/}
        <Route index element={<Navigate to='/projectpicks/todoapp' />} />
      </Route>
    </Routes>
  )
}

export default ProjectPickRoutes