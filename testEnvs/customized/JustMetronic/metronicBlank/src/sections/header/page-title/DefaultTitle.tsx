import React from 'react'

const DefaultTitle: React.FC = () => {
  return (
    <div
      id='kt_page_title'
      data-kt-swapper='true'
      data-kt-swapper-mode='prepend'
      data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
      className={'page-title d-flex'}
    >
      <h4>DefaultTitle</h4>
    </div>
  )
}

export { DefaultTitle }