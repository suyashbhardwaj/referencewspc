import React from 'react'
import { BrowserRouter, Navigate, Routes, Route } from 'react-router-dom'
import { AuthPage, Logout } from '../components/auth'
import { AUTH_TOKEN } from '../constants'
import {PrivateRoutes} from './PrivateRoutes'
import ErrorPage from '../pages/ErrorPage'
import { UserProvider } from '../context/UserContext'
import ApplicationEnvelope from '../components/applicationEnvelope'
const MainAppRoutes: React.FC = () => {
	const isAuthorized = localStorage.getItem(AUTH_TOKEN);
	return (
		<>
		{isAuthorized ? (
			<UserProvider>
				<BrowserRouter basename = {'/'}>
					<Routes>
						<Route element={<ApplicationEnvelope />}>
							<Route path = '/*' element = {<PrivateRoutes/>} />
							<Route index element = {<Navigate to = '/dashboard' />} />
							<Route path = '/logout' element = {<Logout />} />
							<Route path='error/*' element={<ErrorPage />} />
						</Route>
					</Routes>
				</BrowserRouter>
			</UserProvider>
		):(
			<BrowserRouter basename = {'/'}>
				<Routes>
						<Route path = 'auth/*' element = {<AuthPage />} />
						<Route path = '*' element = {<Navigate to = '/auth' />} />
						<Route path='error/*' element={<ErrorPage />} />
				</Routes>
			</BrowserRouter>
		)}
	</>
	)
}

export {MainAppRoutes}