import Dashboard from "../pages/Dashboard"
import { Navigate, Route, Routes } from 'react-router-dom';
import { MasterLayout } from '../layout/MasterLayout';
import CampaignRoutes from "../pages/campaign/CampaignRoutes";
import { useContext, useState } from "react";
import TestMain from "../pages/TestB4Inclusion/TestMain";
import StylepickRoutes from '../rest/stylepicks/StylepickRoutes';
import ProjectPickRoutes from "../rest/projects/ProjectPickRoutes";
import UtilitiesPickRoutes from '../rest/utilities/UtilitiesPickRoutes';
import RTMroutes from '../pages/rtm'
import SettingRoutes from '../pages/settings'
import UserContext from "../context/UserContext";
import VerifyEmail from "../components/auth/VerifyEmail";
import NewClientPage from "../pages/multistep";

const PrivateRoutes = () => {
	const [showSideBar, setShowSideBar] = useState(false);
	const toggleSidebar = () => { setShowSideBar(!showSideBar); }
	const { currentUser, currentUserRole } = useContext(UserContext)

	const isAdmin = currentUserRole.admin
  	const isSuperAdmin = currentUserRole.superAdmin
  	const isAgentOrOperator = currentUserRole.agent || currentUserRole.operator
  	const isFranchiseAdmin = currentUserRole.franchiseAdmin
  	
	return (
		<Routes>
			<Route path='onboarding/*' element={<NewClientPage />} />
	        <Route path='verify-email' element={<VerifyEmail user={currentUser} />} />
			<Route element = {<MasterLayout showSideBar = {showSideBar} toggleSidebar = {toggleSidebar}/>}>
				<Route path='auth/*' element={<>
					{!!currentUser && !currentUser.emailVerified && <Navigate to='/verify-email01' />}
					{!!currentUser && !!currentUser.emailVerified && !isAgentOrOperator && 
						!currentUser.onboardingDone ? (
						<Navigate to='/onboarding/personal-details' />
						) : isAdmin ? (
						<Navigate to='/dashboard' />
						) : isAgentOrOperator ? (
						<Navigate to='/dashboard' />
						) : isSuperAdmin ? (
						<Navigate to='/dashboard' />
						) : isFranchiseAdmin ? (
						<Navigate to='/dashboard' />
						) : (
						<Navigate to='/nothing' />
						)
					}
					</>}
				/>
				{!!currentUser && currentUser.emailVerified && !!currentUser.onboardingDone && (<>
              		{isAdmin && (<>
              			<Route path = 'dashboard' element = {<Dashboard/>} />
						<Route path = 'campaign/*' element = {<CampaignRoutes />} />
						<Route path = 'rtm/*' element={<RTMroutes />} />
						<Route path = 'settings/*' element={<SettingRoutes />} />
						<Route path = 'stylepicks/*' element = {<StylepickRoutes />} />
						<Route path = 'projectpicks/*' element = {<ProjectPickRoutes />} />
						<Route path = 'utilitypicks/*' element = {<UtilitiesPickRoutes />} />
						<Route path = 'testb4inclusion/*' element = {<TestMain />} />
						</>)
              		}
              		{isSuperAdmin && (<>
              			<Route path = 'dashboard' element = {<Dashboard/>} />
						<Route path = 'campaign/*' element = {<CampaignRoutes />} />
						<Route path = 'rtm/*' element={<RTMroutes />} />
		        		<Route path = 'settings/*' element={<SettingRoutes />} />
						<Route path = 'stylepicks/*' element = {<StylepickRoutes />} />
						<Route path = 'projectpicks/*' element = {<ProjectPickRoutes />} />
						<Route path = 'utilitypicks/*' element = {<UtilitiesPickRoutes />} />
						<Route path = 'testb4inclusion/*' element = {<TestMain />} />
						</>)
              		}
              		{/*<Route path='*' element={<Navigate to='/error/404' />} />*/}
              		</>)
				}
				{/*{!!currentUser && !currentUser.emailVerified && (<Route path='*' element={<Navigate to='/verify-email02' />} />)}*/}
				{/*{!!currentUser && !!currentUser.emailVerified && !currentUser.onboardingDone && (<Route path='*' element={<Navigate to='/onboarding/personal-details' />} />)}*/}
			</Route>
		</Routes>
	)
}

export { PrivateRoutes }	