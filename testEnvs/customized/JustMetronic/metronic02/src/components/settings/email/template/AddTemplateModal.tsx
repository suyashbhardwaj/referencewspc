import React, { useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useEditEmailTemplateMutation, useNewEmailTemplateMutation } from "../../../../services/settingsAPIs/EmailConfiguration";

interface IPROPS {
    closeModal: () => void;
    // setStoreDummyData: (data: { subject: string; content: string }) => void;
    selectedTeam:number;
    refetchTemplates: any;
    templateForEdit: any
}

const AddTemplateModal: React.FC<IPROPS> = ({ closeModal, selectedTeam, refetchTemplates, templateForEdit }) => {
    const initialContent = templateForEdit.templateEditEnabled ? templateForEdit.templateData.content: "";
    const [editorContent, setEditorContent] = useState<string>(initialContent);
    const [triggerNewEmailTemplateMutation] = useNewEmailTemplateMutation();
    const [triggerEditEmailTemplateMutation] = useEditEmailTemplateMutation();

    const formik = useFormik({
        initialValues: {
            subject: templateForEdit.templateEditEnabled?templateForEdit.templateData.subject:"",
            content: templateForEdit.templateEditEnabled?templateForEdit.templateData.content:"",
        },
        validationSchema: Yup.object({
            subject: Yup.string().required("Subject is required"),
        }),
        onSubmit: async (values) => {
            const dummyData = {
              subject: values.subject,
              content: editorContent.replace(/<\/?p>/g, ''),
          };

            if(templateForEdit.templateEditEnabled)
                triggerEditEmailTemplateMutation(
                    {
                        templateId:templateForEdit.templateData.id, 
                        body:{
                            attachments: templateForEdit.templateData.attachments,
                            content: dummyData.content,
                            subject: dummyData.subject,
                            id: templateForEdit.templateData.id
                            }
                    }
                )
            else
                triggerNewEmailTemplateMutation({
                    attachments: [],
                    content: dummyData.content,
                    subject: dummyData.subject,
                    team: String(selectedTeam),},)

            // setStoreDummyData(dummyData);
            refetchTemplates();
            closeModal();
        },
    });

    const handleEditorChange = (content: string) => { setEditorContent(content); };
    const handleEditorBlur = () => { formik.setFieldTouched("content", true); };

    return (
        <div
            style={{
                width: "100%",
                padding: "20px",
                background: "white",
                borderRadius: "8px",
            }}
        >
            <p
                // className="btn btn-light"

                onClick={closeModal}
                style={{
                    float: "right",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                    fontSize: "18px",
                }}
            >
                &times;
            </p>
            <h2>{templateForEdit.templateEditEnabled?(<span>Edit a </span>):(<span>Add new </span>)} Template</h2>
            <form onSubmit={formik.handleSubmit}>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "10px",
                        marginBottom: "20px",
                    }}
                >
                    <label>Subject</label>
                    <input
                        name="subject"
                        style={{
                            padding: "8px",
                            overflow: "hidden",
                            width: "100%",
                        }}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.subject}
                    />
                    {formik.touched.subject && formik.errors.subject ? (
                        <div style={{ color: "red" }}>
                            {formik.errors.subject}
                        </div>
                    ) : null}
                </div>
                <ReactQuill
                    value={editorContent}
                    onChange={handleEditorChange}
                    onBlur={handleEditorBlur}
                    modules={modules}
                    formats={formats}
                    placeholder="Compose your email..."
                />
                {formik.touched.content && !editorContent ? (
                    <div style={{ color: "red" }}>Content is required</div>
                ) : null}
                <div style={{ marginTop: "20px", textAlign: "right" }}>
                    <button
                        className="btn btn-light"
                        type="button"
                        onClick={closeModal}
                        style={{ marginRight: "10px" }}
                    >
                        Cancel
                    </button>
                    <button type="submit" className="btn btn-primary">
                        Save
                    </button>
                </div>
            </form>
        </div>
    );
};

// Modules object for setting up the Quill editor toolbar
const modules = {
    toolbar: [
        [{ header: "1" }, { header: "2" }, { font: [] }],
        [{ size: [] }],
        ["bold", "italic", "underline", "strike", "blockquote"],
        [
            { list: "ordered" },
            { list: "bullet" },
            { indent: "-1" },
            { indent: "+1" },
        ],
        ["link", "image", "video"],
        ["clean"],
    ],
};

// Formats objects for setting up Quill editor formats
const formats = [
    "header",
    "font",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "video",
];

export default AddTemplateModal;