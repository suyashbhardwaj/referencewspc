import { useRef, useState } from 'react';
import NestedDD from './multiLevelMenu/NestedDD';

interface IProps {
    // setOpenedPane: any
    setMultiLevelConfiguration: any
}

interface NestedThingProps {
    items: any
    setItems: any
    level: number
    reload: any
    lastAssignedFieldId: any
  }

const NestedDDconfigure: React.FC<NestedThingProps> = ({ items, setItems, level, reload, lastAssignedFieldId }) => {
    console.log(items, "props received");
    function nestInTheFieldWithId(fieldId: number, menuItems: any) {
      for (let menuItem of menuItems) {
        console.log(`comparing ${menuItem.theFieldId} and ${fieldId} and found ${menuItem.theFieldId === fieldId}`)
        if (menuItem.theFieldId === fieldId) {
          console.log("adding submenu")
          menuItem.submenu = [{theFieldId:lastAssignedFieldId.current+1, title:"sample", url:""}]
          lastAssignedFieldId.current++;
          return menuItems;
          }
        if (menuItem.submenu) {
          const updatedSubmenu = nestInTheFieldWithId(fieldId, menuItem.submenu);
          if (updatedSubmenu) {
            menuItem.submenu = updatedSubmenu; // Update submenu if needed
            return menuItems;
          }
        }
      }
    }

    function addToTheFieldWithId(fieldId: number, menuItems: any) {
      for(let menuItem of menuItems) {
        if(menuItem.theFieldId === fieldId) {
          menuItems.push({theFieldId: lastAssignedFieldId.current+1, title: 'sample', url:''})
          lastAssignedFieldId.current++;
          return menuItems;
          }
        if(menuItem.submenu) {
          const updatedSubmenu = addToTheFieldWithId(fieldId, menuItem.submenu);
          if (updatedSubmenu) {
            menuItem.submenu = updatedSubmenu; // Update submenu if needed
            return menuItems;
            }
          }
        }
      }
  
    function updateTheFieldWithId(fieldId: number, menuItems: any, newTitle = null) {
      for (const menuItem of menuItems) {
        if (menuItem.theFieldId === fieldId) {
          if (newTitle !== null) {
            menuItem.title = newTitle;
          }
          return menuItems; // Return the entire array with the updated object
        }
        if (menuItem.submenu) {
          const updatedSubmenu = updateTheFieldWithId(fieldId, menuItem.submenu, newTitle);
          if (updatedSubmenu) {
            menuItem.submenu = updatedSubmenu; // Update submenu if needed
            return menuItems; // Return the entire array with the updated submenu
          }
        }
      }
      return null; // Field with specified ID not found
    }
  
    const addBlankItem = (nextToTheTheFieldWithId: number) => {
      console.log(`tried adding a new item next to the field with id ${nextToTheTheFieldWithId}`);
      setItems((prev: any)=> { return addToTheFieldWithId(nextToTheTheFieldWithId, prev) });
      reload();
    };
  
    const addSubArray = (underTheFieldWithId: number) => {
      console.log(`tried adding a new menu under the field with id ${underTheFieldWithId}`);
      setItems((prev: any)=> { return nestInTheFieldWithId(underTheFieldWithId, prev); });
      reload();
    };
  
    const handleInputChange = (forFieldWithId: number, e:any) => {
      setItems((prev: any)=>{ return updateTheFieldWithId(forFieldWithId, prev, e.target.value) })
      reload();
    }
    return(
      <div>
        {items?.map((item: any, index:number) => {
            return (<>
              <div className='d-flex align-items-center' key={index} style={{ marginLeft: 150 * level }}>
                  <input className = "form-control w-200px" type="text" placeholder={item.title} onChange={(e)=>handleInputChange(item.theFieldId, e)}/>
                  <button className='btn btn-link' onClick={()=>addBlankItem(item.theFieldId)}><i className="mx-2 fs-2 bi bi-plus-lg"></i></button>
                  <button className='btn btn-link' onClick={()=>addSubArray(item.theFieldId)}><i className="mx-2 fs-2x bi bi-node-plus-fill"></i></button>
              </div>
              {item.submenu && <NestedDDconfigure items={item.submenu} setItems = {setItems} level={level + 1} reload={reload} lastAssignedFieldId={lastAssignedFieldId}/>}
            </>);
          })
        }
      </div>
      )
  }

/* const NestedDDConf: React.FC<IProps> = ({setOpenedPane}) => { */
/* const NestedDDConf: React.FC<IProps> = () => { */
const NestedDDConf: React.FC<IProps> = ({setMultiLevelConfiguration}) => {
    const initialCluster = [
      { 
        theFieldId:0, 
        title: "north", 
        url: "", 
        submenu: [
          { 
            theFieldId:2, 
            title: "NCR", 
            url: "",
            submenu: [
              { 
                theFieldId:5, 
                title: "Noida", 
                url: "",
                submenu: [
                  { 
                    theFieldId:8, 
                    title: "Noida Extension", 
                    url: "" 
                  },
                  { 
                    theFieldId:8, 
                    title: "Greater Noida", 
                    url: "" 
                  },
                  { 
                    theFieldId:8, 
                    title: "Noida Native", 
                    url: "" 
                  }
                ]
              },
              { 
                theFieldId:6, 
                title: "Gurugram", 
                url: "" 
              },
              { 
                theFieldId:7, 
                title: "Delhi", 
                url: "" 
              }
            ]
          }
        ]
      },
      { theFieldId:1, title: "west", url: "" , submenu: [{ theFieldId:3, title: "maharashtra", url: "" }]}
    ]
    const [items, setItems] = useState([{ theFieldId:0, title: "sample", url: "" }])
    const [reload, setReload] = useState(false);
    const [savedDD, setSavedDD] = useState<any>({});
    const handleReload = () => { setReload(!reload) }
    const lastAssignedFieldId = useRef(0);
  return (
    <div>
        {/* <div className='row'>
            <div className="col text-end">
                <span></span>
                <button className="btn" onClick={()=>setOpenedPane('Fields')}>Back to Field Configurations<i className="text-primary fs-2x bi bi-arrow-bar-right me-2"></i></button>
            </div>
        </div> */}
        <div className='row'>
          <div className='col-6 d-flex justify-content-center bg-secondary'>
            <div className='my-8 overflow-auto'>
                <NestedDDconfigure items={items} setItems = {setItems} level={0} reload={handleReload} lastAssignedFieldId={lastAssignedFieldId}/>
            </div>
          </div>
          <div className='col-6 d-flex justify-content-center bg-primary'>
            <div className='my-8'>
              <NestedDD menuItemsData = {items}/>
            </div>
          </div>
        </div>
        <div className="row mt-2">
          <div className="col px-0"><button className="btn btn-success w-100" onClick={()=>setMultiLevelConfiguration(items)}>Save dropdown for the field</button></div>
        </div>
    </div>
  )
}

export default NestedDDConf