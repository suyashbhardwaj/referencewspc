import { Formik, Form, Field, ErrorMessage } from 'formik';
import { useEffect, useState } from 'react';
import * as Yup from 'yup';
/*import { useUpdateDynamicFieldDefinitionsMutation } from '../../../../Services/settingsAPIs/CommonAPI';
import { toast } from 'react-toastify';*/
import { toSentenceCase } from '../../../../utils/functions/toSentenceCase';
import { teamresponse } from '../../../../services/apiresponses';
import { MultiSelect } from 'react-multi-select-component';
import { useTeamsQuery } from '../../../../services/settingsAPIs/CommonAPI';
import { useAddOptionToDynamicFieldCheckboxMutation } from '../../../../services/settingsAPIs/TicketSettingsAPI';

// Define Yup schema for validation
const validationSchema = Yup.object().shape({
    label:Yup.string().required('Please provide a field name'),
    field_type:Yup.string().required('Please provide a field type'),
});

interface IProps {
    closeModal: any
    workflowSelected: number
    allTeamIds: number[]
    refetch: any
}

const AddNewFieldModal: React.FC<IProps> = ({closeModal, workflowSelected, allTeamIds, refetch})=> {
    const fieldTypes = ["text", "email", "number", "tel-phone", "text-area", "check-box", "date-time", "date", "time", "drop-down", "multi-level"];
    const [constraintType, setConstraintType] = useState({constraint:'', status:false});
    const [theTeams, setTheTeams] = useState<any[]>([]);
    const [selectedTeams, setSelectedTeams] = useState<any[]>([]);
    const {data:teamsData, isLoading: isLoadingTeams} = useTeamsQuery({skip: !(constraintType.constraint==='mandatory' && constraintType.status),});
    const [lengthQueryFieldEnabled, setLengthQueryFieldEnabled] = useState(false);
    const [checkboxFieldOptionEnabled, setCheckboxFieldOptionEnabled] = useState(false);
    const [labelForCheckboxOption,setLabelForCheckboxOption] = useState("");
    const [fieldTypeSelected, setFieldTypeSelected] = useState("");
    const [triggerAddOptionToCheckboxField] = useAddOptionToDynamicFieldCheckboxMutation();
    const handleToCheckCustomOptions = (evt:any) => {
      setFieldTypeSelected(evt.target.value);
      if(['text','number'].includes(evt.target.value)) setLengthQueryFieldEnabled(true) 
      else setLengthQueryFieldEnabled(false);
    }

    const handleSubmit = (values:any, actions:any) => {
        // Simulate API call (replace with actual API call using fetch, axios, etc.)
        values.is_required=constraintType.status ? (constraintType.constraint==='mandatory'||constraintType.constraint==='both'): false;
        values.is_unique=constraintType.status ? (constraintType.constraint==='unique'||constraintType.constraint==='both'): false;
        values.ticket_name=workflowSelected;
        values.team=allTeamIds;
        values.label = toSentenceCase(values.label);
        setTimeout(() => {
            if(checkboxFieldOptionEnabled && labelForCheckboxOption==='') alert('an option for checkbox is mandatory in the given scenario');
            else {
                alert(JSON.stringify(values, null, 2));
                actions.setSubmitting(false);
                closeModal();
                refetch();
            }
        }, 500);
        /*if(checkboxFieldOptionEnabled && labelForCheckboxOption==='') alert('an option for checkbox is mandatory in the given scenario');
        else
        triggerUpdateDynamicFieldsMutation(values).then((res:any)=>{
            if(res.data) {
                toast.success("Added a field to the workflow");
                closeModal();
                refetch();
            }else toast.error('Some issue')}).catch((err:any)=>console.log(err))*/
    };
  
  useEffect(() => { teamsData ? setTheTeams(teamsData) : setTheTeams(teamresponse)}, [teamsData]);
  useEffect(()=>{
    if(fieldTypeSelected==='check-box' && constraintType.constraint==='mandatory' && constraintType.status) setCheckboxFieldOptionEnabled(true)
    else setCheckboxFieldOptionEnabled(false)},[constraintType, fieldTypeSelected])

  return (
    <div>
      <Formik
        initialValues={{
        field_type: "",
        label: "",
        is_required: false,
        is_unique: false,
        model_name: "ticket_name",
        ticket_name: -1,
        field_length: 0,
        team: []
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting, errors, touched }) => (
          <Form className='form w-100 fv-plugins-bootstrap5 fv-plugins-framework'>
            <div className='d-flex flex-column align-items-center'>
                <div className="d-flex">
                    <Field className="form-control mx-4 min-w-350px" type="text" name="label" id ="label" />
                    {touched.label && errors.label && <div className='text-danger'>{errors.label}</div>}
                    <button 
                      style={{transition: "min-width 0.3s ease"}}
                      className={`${Object.keys(errors).length ? 'btn-secondary min-w-150px': (Object.keys(touched).length ? 'btn-success min-w-250px':'btn-secondary min-w-150px')} btn d-flex align-items-center`} type= "submit" disabled={isSubmitting}>
                      <i className='text-start fs-2 bi bi-plus-lg me-2'></i>
                      <span>Add</span>
                      <i className={`${Object.keys(errors).length ? 'mx-2 bi fs-2': (Object.keys(touched).length ? 'mx-2 bi fs-2 bi-check-circle-fill':'mx-2 bi fs-2')} `}></i>
                    </button>
                </div>
            </div>

            <label htmlFor="" className="form-label">Choose Type</label>
            <div className="row row-cols-6 mt-1">
                {fieldTypes.map((field, index)=>(<>
                <div className="col">
                    <Field className="mt-2 form-check-input" type="radio" name="field_type" value={field} id={`thefield${index}`} onInput={handleToCheckCustomOptions}/>
                    <label className="mx-1 mt-2 form-label" htmlFor={`thefield${index}`}>{toSentenceCase(field)}</label>
                </div>
                </>))}
                <ErrorMessage name="field_type" component="div" className="error" />
            </div>
            
            <div className="d-flex justify-content-center mt-6">
                <input className="ms-4 form-check-input" type="radio" name="constraints" id="mandatory" value="mandatory" onChange={(evt)=>setConstraintType({constraint:'mandatory', status:evt.target.checked})}/>
                <label className="mx-1 form-label" htmlFor="mandatory">Mark as mandatory</label>
                <input className="ms-4 form-check-input" type="radio" name="constraints" id="unique" disabled value="unique" onChange={(evt)=>setConstraintType({constraint:'unique', status:evt.target.checked})}/>
                <label className="mx-1 form-label" htmlFor="unique" style={{color: "grey"}}>Mark as unique</label>
                <input className="ms-4 form-check-input" type="radio" name="constraints" id="masked" disabled value="masked" onChange={(evt)=>setConstraintType({constraint:'masked', status:evt.target.checked})}/>
                <label className="mx-1 form-label" htmlFor="masked" style={{color: "grey"}}>Mark as masked</label>
                <input className="ms-4 form-check-input" type="radio" name="constraints" id="editable" disabled value="editable" onChange={(evt)=>setConstraintType({constraint:'editable', status:evt.target.checked})}/>
                <label className="mx-1 form-label" htmlFor="editable" style={{color: "grey"}}>Mark as editable</label>
                <input className="ms-4 form-check-input" type="radio" name="constraints" id="all" value="all" onChange={(evt)=>setConstraintType({constraint:'all', status:evt.target.checked})}/>
                <label className="mx-1 form-label" htmlFor="all">Mark all</label>
            </div>
            <div className={`row`}>
              <div className={`col ${lengthQueryFieldEnabled?'':'d-none'}`}>
                <label htmlFor="field_length">Field length</label>
                <Field className="form-control mx-4 w-150px" type="number" name="field_length" id ="field_length" placeholder='specify the length of field'/>
              </div>
              <div className={`col ${checkboxFieldOptionEnabled ? '':'d-none'}`}>
                <label htmlFor="checkboxMandatoryOption" className='form-label'>Mandatory Checkbox Option</label>
                <input type="text" className='form-control' name="checkboxMandatoryOption" id="checkboxMandatoryOption" onChange={(evt)=> setLabelForCheckboxOption(toSentenceCase(evt.target.value))}/>
              </div>
              <div className={`col w-200px ${constraintType.constraint==='mandatory' && constraintType.status ? '':'d-none'}`}>
                <label className="form-label">Team</label>
                <MultiSelect
                  options={theTeams.map((team: any) => ({
                      label: team.name,
                      value: team.id
                      }))}
                  value={selectedTeams}
                  onChange={setSelectedTeams}
                  labelledBy='Select Teams'
                  />
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default AddNewFieldModal;
