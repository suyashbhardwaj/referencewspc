/* do we need to make the user do a selction of team when marking the new field as mandatory in the add new field to ticket form? */
/* 
not isEditable in user field
toast message caps start
team select menu remove: ticket fields
attention divert to submit button
caps for all radio labels in add modal
automatic conversion to capital case of form feeds
sequencing of fields draggable
drop a mail to Omar to schedule a time to connect to discuss API needed to save & refer nested JSON save
 */

import { useEffect, useState } from 'react'
import ReactModal from 'react-modal';
import { useDynamicFieldDefinitionsQuery, useTeamsQuery, useWorkflowsQQuery } from '../../../../services/settingsAPIs/CommonAPI';
import { defaultModalStyles, standardModalStyles } from '../../../common/CustomModalStyles';
import AddNewFieldModal from './AddNewFieldModal';
import { useAddChoiceToDynamicFieldDDMutation, useAddOptionToDynamicFieldCheckboxMutation, useDeleteOptionOfDynamicFieldCheckboxMutation, useDeleteTicketFieldChoiceMutation, useDeleteTicketFieldMutation } from '../../../../services/settingsAPIs/TicketSettingsAPI';
import { toast } from 'react-toastify';
import CommonDeleteModal, { DeleteMessageBody } from '../../common/CommonDeleteModal';
import NestedDDConf from '../../common/NestedDDConf';
import PagenationUtility from '../../common/PagenationUtility';
import { toSentenceCase } from '../../../../utils/functions/toSentenceCase';
import LoadingSpinner from '../../../LoadingSpinner';
import EditTicketFieldModal from './EditTicketFieldModal';
import { dynamicFieldDataTicketNameResponse, teamresponse, workflowresponse } from '../../../../services/apiresponses';

const initialTicketFields = [
  {label:"First Name", type:"Text", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
  {label:"Last Name", type:"Text area", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
  {label:"Employee ID", type:"Number", subtype:"", isMandatory:true, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
  {label:"Mobile number", type:"Number", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
  ]

const initialTicketFieldsAPIlike = [
  /* {
    id: 14,
    field_type: "text",
    label: "description",
    is_required: false,
    model_name: "ticket_name",
    ticket_name: 1,
    choices: [],
    identifier: "field_14",
    campaign_name: null,
    multiple_choices: [],
    multiple_level: null,
    is_unique: false,
    is_editable: true,
    team: [],
    field_length: null
  },
  {
    id: 15,
    field_type: "text-area",
    label: "mandate field",
    is_required: true,
    model_name: "ticket_name",
    ticket_name: 1,
    choices: [],
    identifier: "field_15",
    campaign_name: null,
    multiple_choices: [],
    multiple_level: null,
    is_unique: false,
    is_editable: true,
    team: [],
    field_length: null
  } */
]  

const TicketFields = () => {
  const {data:teamsData, isLoading: isLoadingTeams} = useTeamsQuery({});
  const [ticketFields, setTicketFields] = useState(initialTicketFieldsAPIlike);
  const [theTeams, setTheTeams] = useState<any[]>([]);
  const [allTeamIds, setAllTeamIds] = useState<any[]>([]);
  const [selectedTeam, setSelectedTeam] = useState<Number>();
  const { data: theWorkflowsFetched } = useWorkflowsQQuery({});
  const [theworkflows, setTheWorkflows] = useState<any>([]);
  const [workflowSelected, setWorkflowSelected] = useState<number>()
  const fieldTypes = ["text", "email", "number", "tel-phone", "text-area", "check-box", "date-time", "date", "time", "drop-down", "multi-level"];
  const [expandForMoreSpecifications, setExpandForMoreSpecifications] = useState({expansionOn:-1, expanded: false, expandForFieldType:''});   
  const [isAddTicketFieldsModalOpen, setIsAddTicketFieldsModalOpen] = useState(false)
  const { data: dynamicFieldsData, refetch, isLoading } = useDynamicFieldDefinitionsQuery('ticket_name', {});
  const [newDropdownListItem, setNewDropdownListItem] = useState('');
  const [newCheckboxOption, setNewCheckboxOption] = useState('');
  const [editDropdownListItem, setEditDropdownListItem] = useState({enabled:false, forChoiceId:-1, currentVal:""});
  const [editCheckboxListOption, setEditCheckboxListOption] = useState({enabled:false, forChoiceId:-1, currentVal:""});
  const [fieldForDeletion, setFieldForDeletion] = useState<number>();
  const [multiLevelConfiguration, setMultiLevelConfiguration] = useState<any>({});
  const [currentData, setCurrentData] = useState<any>();
  const [isEditFieldModalOpen, setIsEditFieldModalOpen] = useState<boolean>(false);
  const [storingFieldData, setStoringFieldData] = useState(null);
  const [triggerAddChoiceToDDfield] = useAddChoiceToDynamicFieldDDMutation();
  const [triggerAddOptionToCheckboxField] = useAddOptionToDynamicFieldCheckboxMutation();
  const [triggerDeleteChoiceFromTicketField] = useDeleteTicketFieldChoiceMutation();
  const [triggerDeleteOptionFromCheckboxTicketField] = useDeleteOptionOfDynamicFieldCheckboxMutation();
  const [triggerDeletionOfFieldMutation] = useDeleteTicketFieldMutation();

  const openAddTicketFieldModal = () => { setIsAddTicketFieldsModalOpen(true) }
  const closeAddTicketFieldModal = () => { setIsAddTicketFieldsModalOpen(false) }
  
  const closeEditFieldModal = () => { setIsEditFieldModalOpen(false) }

  const [isDeleteTicketFieldModalOpen, setIsDeleteTicketFieldModalOpen] = useState<boolean>(false);
  const openDeleteTicketFieldModal = (fieldId: number) => { setFieldForDeletion(fieldId); setIsDeleteTicketFieldModalOpen(true)}
  const closeDeleteTicketFieldModal = () => {setIsDeleteTicketFieldModalOpen(false); refetch();}

  const handleFirstDropdownChange = (event: any) => { setSelectedTeam(Number.parseInt(event.target.value));};
  const handleDropdownNewListItemEdit = (event:any) => {setNewDropdownListItem(event.target.value)}
  const handleCheckboxNewOptionEdit = (event:any) => {setNewCheckboxOption(event.target.value)}

  const editFieldsHandler = (fields: any) => {
    setIsEditFieldModalOpen(true);
    setStoringFieldData(fields);
  };

  useEffect(() => { if(teamsData) {setAllTeamIds(teamsData.map((team:any)=>team.id)); setTheTeams(teamsData);} else {setAllTeamIds(teamresponse.map(team=>team.id)); setTheTeams(teamresponse);}}, [teamsData]);
  useEffect(() => { theWorkflowsFetched ? setTheWorkflows(theWorkflowsFetched.results): setTheWorkflows(workflowresponse.results)}, [theWorkflowsFetched]);
  /*useEffect(() => { workflowSelected && dynamicFieldsData && setTicketFields(dynamicFieldsData.filter((df:any)=>df.ticket_name===workflowSelected)) }, [workflowSelected, dynamicFieldsData]);*/
  useEffect(() => { workflowSelected && !dynamicFieldsData && setTicketFields(dynamicFieldDataTicketNameResponse.filter((df:any)=>df.ticket_name===workflowSelected)) }, [workflowSelected, dynamicFieldsData]);
  return (
    <div className='card-body px-0 px-lg-10'>
      <div className="row">
        <div className="col">
          <span className="input-group-text" id="basic-addon1">Ticket name fields</span>
        </div>
      </div>

      <div className="row my-4">
        <div className="col-12">
          <div className="card-toolbar d-lg-flex gap-5">
            <div className="d-flex flex-column">
              <label>Select Workflow</label>
              <div className="min-w-250px">
                <select className="form-select" onChange={(e)=>setWorkflowSelected(Number.parseInt(e.target.value))}>
                  <option value={0} selected>Select a Workflow</option>
                  {theworkflows && theworkflows.map((theworkflow:any)=>(<option value={theworkflow.id}>{theworkflow.name}</option>))}
                </select>
              </div>
            </div>
            <span className="input-group-text bg-white border-0 ms-auto align-self-start" id="basic-addon1">
              <div className="ms-auto d-flex">
                <button 
                  className="input-group-text form-control btn-sm form-control align-items-center text-hover-primary fw-bold min-w-150px"
                  disabled={!workflowSelected}
                  onClick={openAddTicketFieldModal}>
                  <i className="bi bi-plus-lg me-2"></i>
                  <span>Option</span>
                </button>
              </div>
            </span>
          </div>
        </div>
      <ReactModal
        isOpen={isAddTicketFieldsModalOpen}
        onRequestClose={closeAddTicketFieldModal}
        style={standardModalStyles}
        contentLabel='Add a new Account Field'
        >
        <AddNewFieldModal closeModal={closeAddTicketFieldModal} workflowSelected={workflowSelected} allTeamIds={allTeamIds} refetch={refetch}/>
      </ReactModal>
      <ReactModal
        isOpen={isEditFieldModalOpen}
        onRequestClose={closeEditFieldModal}
        style={defaultModalStyles}
      >
        <EditTicketFieldModal
            closeModal={closeEditFieldModal}
            setFieldsData={setTicketFields}
            fieldsData={ticketFields}
            storingFieldData={storingFieldData}
            refetch={refetch}
        />
      </ReactModal>
      <ReactModal isOpen={isDeleteTicketFieldModalOpen} onRequestClose={closeDeleteTicketFieldModal} style={defaultModalStyles} contentLabel='Delete a selected status'>
        <CommonDeleteModal closeModal={closeDeleteTicketFieldModal} deleteAPItrigger = {triggerDeletionOfFieldMutation} deletionIdentifier={fieldForDeletion} messageBody={ <DeleteMessageBody bodyParams={{descriptionPromptEnabled:false, deletionOf:"ticket field"}} /> }/>
      </ReactModal>
      </div>

      <div className="row mx-0 mb-2 mt-4 overflow-auto bg-gray-100 rounded-3 py-4">
        <div className="col">
          
          {/* The Table Header */}
          <div className="card my-1" style={{ backgroundColor: '#E4E4E47F' }}>
            <div className="row align-items-center py-4">
              <div className="col-lg-4 col text-start ps-10"><label>Label</label></div>
              <div className="col-lg-2 col text-end">Field Type</div>
              <div className="col text-center"><label>Mandatory</label></div>
              <div className="col text-center"><label>Unique</label></div>
              <div className="col text-center"><label>Masking</label></div>
              <div className="col text-center"><label>Editable</label></div>
              <div className="col text-center"></div>
            </div>
          </div>
          {isLoading ? (
            <LoadingSpinner/>
          ) : (
          <div className="row mx-0" style={{overflowY: "scroll", overflowX: "hidden"}}>
          <div className="col px-1">
          {/* The Table Data */}
          {/* {ticketFields.map((field,index)=>(<> */}
          {currentData && currentData.map((field: any, index: any) => (<>
            <div className="card my-1" key={index}>
              <div className="row align-items-center">
                <div className="col-lg-4 col text-start ps-10">

                  {/* start: simply show the label of the field type or a text box with button before field type label if the field type is drop-down, that too conditionally */}
                  {field.field_type === 'drop-down'
                  ? (<div className="d-flex align-items-center flex-nowrap">
                        <label><div>{toSentenceCase(field.label)}</div></label>
                        <div className={`d-flex mx-2 ${expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded?'':'d-none'}`}>
                          <input type="text" placeholder="Add a new item to the list." className="form-control m-1" value={newDropdownListItem} onChange={handleDropdownNewListItemEdit}/>
                          <button className="btn btn-sm btn-primary m-1" onClick={()=>{triggerAddChoiceToDDfield({choice:newDropdownListItem, field:field.id}); setNewDropdownListItem(''); refetch()}}>Add</button>
                        </div>
                      </div>)
                  : (field.field_type === 'check-box' ? (<>
                      <div className="d-flex align-items-center flex-nowrap">
                        <label><div>{toSentenceCase(field.label)}</div></label>
                        <div className={`d-flex mx-2 ${expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded?'':'d-none'}`}>
                          <input type="text" placeholder="Add a new checkbox option." className="form-control m-1" value={newCheckboxOption} onChange={handleCheckboxNewOptionEdit}/>
                          <button className="btn btn-sm btn-primary m-1" onClick={()=>{triggerAddOptionToCheckboxField({choice:newCheckboxOption, field:field.id}); setNewCheckboxOption(''); refetch()}}>Add</button>
                        </div>
                      </div>
                    </>):(<label>{toSentenceCase(field.label)}</label>))}

                </div>
                
                <div className="col-lg-2 col justify-content-end align-items-center dropdown d-flex">

                  {/* start: show respective buttons to trigger open the customization sections of drop-down or multi-level respectively just before select menu of field type */}
                  {field.field_type === 'drop-down' ? (
                    <button 
                      className="btn btn-link" 
                      onClick={()=>setExpandForMoreSpecifications({expansionOn: index, expanded:!expandForMoreSpecifications.expanded, expandForFieldType:'drop-down'})}
                      >
                      <i className="fs-2x text-dark bi bi-plus-lg me-2"></i>
                    </button>
                    ):field.field_type === 'multi-level' ? (<>
                      <button 
                        className="btn btn-link d-flex me-4" 
                        onClick={()=>setExpandForMoreSpecifications({expansionOn: index, expanded:!expandForMoreSpecifications.expanded, expandForFieldType:'multi-level'})}
                        >
                          <i className="text-primary fs-2 bi bi-arrow-bar-left"></i>
                          Configure
                      </button>
                    </>):field.field_type === 'check-box' ? (<>
                      <button 
                        className="btn btn-link d-flex me-2" 
                        onClick={()=>setExpandForMoreSpecifications({expansionOn: index, expanded:!expandForMoreSpecifications.expanded, expandForFieldType:'check-box'})}
                        >
                        <i className="fs-2x text-dark bi bi-plus-lg me-2"></i>
                      </button>
                    </>):<div></div>}
                  {/* ends */}

                  {/* start: select menu for field type */}
                  <label>{toSentenceCase(field.field_type)}</label>
                  {/* ends */}

                </div>

                {/* start: mandatory, unique, masking, editable togglers */}
                <div className="col text-end d-flex align-items-center justify-content-end">
                  <div className="col text-center">
                    <i  className={`me-4 fs-2 bi ${field.is_required?'bi-check2':'bi bi-x'}`} 
                      style={{cursor:"pointer"}}
                      onClick={() => {
                      setTicketFields(prev=>{
                        const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isMandatory:!thefield.is_required}:thefield)
                        return updated;
                        })
                      }}></i>
                  </div>
                </div>
                <div className="col text-center">
                  <i  className={`me-4 fs-2 bi ${field.is_unique?'bi-check2':'bi bi-x'}`} 
                    style={{cursor:"pointer"}}
                    onClick={() => {
                    setTicketFields(prev=>{
                      const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isUnique:!thefield.is_unique}:thefield)
                      return updated;
                      })
                    }}></i>
                </div>
                <div className="col text-center">
                  <i className={`me-4 fs-2 bi ${field.isMasked?'bi-check2':'bi bi-x'}`} 
                    style={{cursor:"pointer"}}
                    onClick={() => {
                      setTicketFields(prev=>{
                        const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isMasked:!thefield.isMasked}:thefield)
                        return updated;
                        })
                      }}>
                  </i>
                </div>
                <div className="col text-center">
                  <i  className={`me-4 fs-2 bi ${field.isEditable?'bi-check2':'bi bi-x'}`} 
                    style={{cursor:"pointer"}}
                      onClick={() => {
                        setTicketFields(prev=>{
                          const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isEditable:!thefield.isEditable}:thefield)
                          return updated;
                          })
                        }}>
                  </i>
                </div>
                {/* ends */}

                {/* start: edit & delete field icons */}
                <div className="col text-center d-flex">
                  <button className="btn btn-link text-hover-info" onClick={()=>editFieldsHandler(field)}><i className="text-dark ms-auto bi bi-pencil-fill fs-2"></i></button>
                  <button className="btn btn-link text-hover-danger" onClick={()=>openDeleteTicketFieldModal(field.id)}><i className="text-dark bi bi-trash-fill fs-2 mx-4"></i></button>
                </div>
                {/* ends */}

              </div>
            </div>

            {/* start: The dynamic dropdown/multi-level configuration section that expands to set up options to be listed in dropdown/multi-level */} 
            {expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded && expandForMoreSpecifications.expandForFieldType==='drop-down'? (<>
              <div className="card my-1 bg-light-primary" key={`${index}-moreSpec`}>
                {field.choices.map((item, msindex)=>(<>
                  <div className="card-header my-0 min-h-40px d-flex align-items-center justify-content-lg-start" key={msindex}>
                    <div className="d-flex">
                      <button className="btn btn-link" onClick={()=>setEditDropdownListItem({enabled:true, forChoiceId: item.id, currentVal:item.choice})}><i className="ms-auto text-dark text-hover-info bi bi-pencil-fill fs-2"></i></button>
                      <button className="btn btn-link" onClick={()=>{triggerDeleteChoiceFromTicketField(item.id); toast.success('Deletion processed'); refetch();}}><i className="text-dark text-hover-danger px-2 fs-4 fa fa-solid fa-trash"></i></button>
                    </div>
                    <div className="ps-4">
                      {editDropdownListItem.enabled && editDropdownListItem.forChoiceId===item.id ?
                        ( <input 
                            className='form-control-sm' type="text" 
                            placeholder={item.choice} 
                            value={editDropdownListItem.currentVal} 
                            onChange={(evt)=>setEditDropdownListItem({...editDropdownListItem, currentVal:evt.target.value})}
                            onBlur={()=>{
                              alert(`PUT api/v1/dynamicFieldUpdate/choices with payload as {choiceId:${item.id}, value:${editDropdownListItem?.currentVal}}`);
                              refetch();
                              setEditDropdownListItem({...editDropdownListItem, enabled:false})
                            }}/>
                      ):( <label> {item.choice} </label>)
                      }
                    </div>
                  </div>
                </>))}  
              </div>                    
            </>):(expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded && expandForMoreSpecifications.expandForFieldType==='check-box'? (<>
              <div className="card my-1 bg-light-primary" key={`${index}-moreSpec`}>
                {field.multiple_choices.map((item, msindex)=>(<>
                  <div className="card-header my-0 min-h-40px d-flex align-items-center justify-content-lg-start" key={msindex}>
                    <div className="d-flex">
                      <button className="btn btn-link" onClick={()=>setEditCheckboxListOption({enabled:true, forChoiceId: item.id, currentVal:item.choice})}><i className="ms-auto text-dark text-hover-info bi bi-pencil-fill fs-2"></i></button>
                      <button className="btn btn-link" onClick={()=>{triggerDeleteOptionFromCheckboxTicketField(item.id); toast.success('Deletion processed'); refetch();}}><i className="text-dark text-hover-danger px-2 fs-4 fa fa-solid fa-trash"></i></button>
                    </div>
                    <div className="ps-4">
                      {editCheckboxListOption.enabled && editCheckboxListOption.forChoiceId===item.id ?
                        ( <input 
                            className='form-control-sm' type="text" 
                            placeholder={item.choice} 
                            value={editCheckboxListOption.currentVal} 
                            onChange={(evt)=>setEditCheckboxListOption({...editCheckboxListOption, currentVal:evt.target.value})}
                            onBlur={()=>{
                              alert(`PUT api/v1/dynamicFieldUpdate/choices with payload as {choiceId:${item.id}, value:${editCheckboxListOption?.currentVal}}`);
                              refetch();
                              setEditCheckboxListOption({...editCheckboxListOption, enabled:false})
                            }}/>
                      ):( <label> {item.choice} </label>)
                      }
                    </div>
                  </div>
                </>))}  
              </div>                    
            </>):expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded && expandForMoreSpecifications.expandForFieldType==='multi-level' ? (<>
              <div className="card my-1 bg-light-primary" key={`${index}-moreSpec`}>
                <NestedDDConf setMultiLevelConfiguration={setMultiLevelConfiguration}/>
              </div>
            </>):(<></>))
            }
            {/* ends */}

            </>))
          }
          {/* ends: TableData */}
          </div>
          </div>)}
          {ticketFields?.length ? (
          <div className="row mt-4">
              <div className="col">
                  <PagenationUtility data={ticketFields} setCurrentData={setCurrentData}/>
              </div>
          </div>
          ):(<></>)}
        </div>      
      </div> 
    </div>
  )
}

export default TicketFields