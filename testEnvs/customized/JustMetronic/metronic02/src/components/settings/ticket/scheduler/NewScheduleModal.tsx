import React, { useEffect, useState } from "react";
import { CloseButton } from "react-bootstrap";
import ReactModal from "react-modal";
import NewContactModal from "./NewContactModal";
import * as Yup from 'yup'
import { useFormik } from "formik";
import { useDispositionMutation, useEmaildispositionQuery, useMeeQuery } from "../../../../services/settingsAPIs/CommonAPI";
import { useNewScheduleMutation } from "../../../../services/settingsAPIs/TicketSettingsAPI";
import { useMeMutation } from "../../../../services/settingsAPIs/CommonAPI";
interface IProps {
  closeNewScheduleModal: () => void;
  scheduleForEdit: any
}

const NewScheduleModal: React.FC<IProps> = ({ closeNewScheduleModal, scheduleForEdit }) => {
  const customModalStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      width: "650px",
    },
  };
  const initialValues = {
    title:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.title: "",
    description:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.description: "",
    contact:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.contact[0].id: 0,
    ticket_channel:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.ticket_channel: "",
    team:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.team[0].id: "",
    disposition:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.disposition[0].id: 0,
    disposition_description:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.title: "",
    status:null,
    status_type:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.status_type: "",
    date_from:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.date_from: "",
    date_to:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.date_to: "",
    schedule_timer:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.schedule_timer: "",
    daily_time:scheduleForEdit.scheduleEditEnabled?scheduleForEdit.scheduleData.daily_time: ""
  }
  const [triggerMeMutation] = useMeMutation();
  const [triggerDispositionMutation] = useDispositionMutation();
  const [selectedTeam, setSelectedTeam] = useState();
  const { data: teamData } = useMeeQuery({});
  const { data: emailDisposData } = useEmaildispositionQuery(selectedTeam, { skip: !selectedTeam,});
  const [triggerNewScheduleMutation] = useNewScheduleMutation();
  const [isAddNewContactModalOpen, setAddNewContactModalOpen] = useState(false);
  const [suggestions, setSuggestions] = useState([]);
  const [theDispositions, setTheDispositions] = useState([]);
  const [theStatuses, setTheStatuses] = useState([]);
  const [selectedDisposition, setSelectedDisposition] = useState<number>(0);
  const [loading, setLoading] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [formSubmissionStatus, setFormSubmissionStatus] = useState("");
  const [selectedContact, setSelectedContact] = useState<number>(0);
  const [currentLoggedInUser, setCurrentLoggedInUser] = useState<any>();
  const [formEditingEnabled, setFormEditingEnabled] = useState(false);

  function updateKeys(jsonObj) {
    let updatedObj = {};
    for (let key in jsonObj) {
        let updatedKey = key.replace(/['"`]/g, ''); // Remove single, double, or backticks
        updatedObj[updatedKey] = jsonObj[key];
    }
    return updatedObj;
  }

  /* const formToBeLoadedWithValues = scheduleForEdit.scheduleEditEnabled? updateKeys(stripFields(scheduleForEdit.scheduleData, ["id", "day_of_week", "day_of_month"])): initialValues */
  const searchContactByMailID = (skey:string) => {
    console.log('tried searching ',skey);
    axios.get(`http://botgo.localhost:8000/api/v1/customers/contacts/search-by-email/`, 
      {headers: { 
        Authorization: `Bearer ${localStorage.getItem('auth-token')}`, 
      },params: {
        email: skey
      }})
      .then((res: any) => {if(res.status===200) setSuggestions(res.data)})
      .catch((err)=>console.log(err))
  }
  
  async function fetchStatus() {
    axios.get("http://botgo.localhost:8000/api/v1/common/disposition/1/lookup_by_disposition/", { headers: { Authorization: `Bearer ${localStorage.getItem('auth-token')}`,},})
      .then((res: any) => {
        res.status ===200 && setTheStatuses(res.data.all_statuses)
      }).catch((err)=>console.log(err))
  }

  const openAddContactModal = () => { console.log(isAddNewContactModalOpen); setAddNewContactModalOpen(true); };
  const closeAddContactModal = () => { setAddNewContactModalOpen(false); };
  const handleDispositionDropdownChange = (evt) => { setSelectedDisposition(evt.target.value); fetchStatus() }

  const newTicketScheduleParserSchema = Yup.object().shape({
    title:Yup.string().required('Please provide a title'),
    description:Yup.string().required('Please provide a description'),
    ticket_channel:Yup.string().required('Please select a ticket channel'),
    disposition_description:Yup.string().required('Please provide a disposition description'),
    status:Yup.string().nullable(),
    status_type:Yup.string().required('Please select a status'),
    date_from:Yup.string().required('Please provide a start date'),
    date_to:Yup.string().required('Please provide an end date'),
    schedule_timer:Yup.string().required('Please select a schedule frequency'),
    daily_time:Yup.string().required('Please select a time'),
    });

  const submitForm = async (valObj: any) => {
    const promise = new Promise((resolve, reject) => {
      console.log('resolved with ', resolve)
      console.log('rejected with ', reject)
    })
    console.log('new ticket schedule modal form submitted with payload ', valObj)
    try {
      triggerNewScheduleMutation(valObj)
        .then((res:any)=>{ 
          if(res.data) {alert("schedule has been created"); closeNewScheduleModal()} 
          else {alert("failed to create a schedule")}
        })
    } catch (error) { console.error('Error making the POST request:', error); setFormSubmissionStatus('Error submitting the form')
    } finally { setSubmitting(false) /* Set submitting to false to enable the form again */ }
    return promise
  }

  const convertDateFormat = (dateString: string) => {
    // Create a Date object from the input string
    const date = new Date(dateString);
    
    // Get the year, month, and day from the date object
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Month is zero-based, so add 1
    const day = String(date.getDate()).padStart(2, '0');
    
    // Get the time in the desired format (18:30:00)
    const time = '18:30:00';
    
    // Construct the output string
    const outputString = `${year}-${month}-${day}T${time}.000Z`;
    
    return outputString;
  }

  const formik = useFormik({
    initialValues,
    validationSchema: newTicketScheduleParserSchema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
      console.log(`submitted the form with contact as ${selectedContact} team as ${currentLoggedInUser.id} disposition as ${selectedDisposition} & other values as ${values}`);
      const date_fromFormatted = convertDateFormat(values.date_from)
      const date_toFormatted = convertDateFormat(values.date_to)
      if(!(selectedContact===0 || selectedDisposition === 0 || currentLoggedInUser === "")) {
        setLoading(true)  
        submitForm({
          "contact":selectedContact,
          "daily_time":values.daily_time,
          "date_from":date_fromFormatted,
          "date_to":date_toFormatted,
          "description":values.description,
          "disposition":selectedDisposition,
          "disposition_description":values.disposition_description,
          "schedule_timer":values.schedule_timer,
          "status":null,
          "status_type":values.status_type,
          "team":String(currentLoggedInUser.id),
          "ticket_channel":values.ticket_channel,
          "title":values.title,
          })
        .then((res: any) => {
            /* console.log('response.status is ', res.status) */
            console.log('response is ', res)
            closeNewScheduleModal()
          if (res.data.signIn.status === 201) {
            console.log('received success response against contact creation form submission!')
            /* setLoading(false) */
            closeNewScheduleModal()
          } else {
            /* setLoading(false) */
            console.log(res)
            setSubmitting(false)
          }
        })
        .catch((err: any) => {
          /* setLoading(false) */
          setSubmitting(false)
          console.error(err)
        })
      }
  }});

  useEffect(()=>{ teamData && setSelectedTeam(teamData.id) },[teamData]);
  useEffect(()=>{ emailDisposData && setTheDispositions(emailDisposData.data) },[emailDisposData]);
  /*useEffect(() => { 
    triggerMeMutation({})
      .then((res:any)=>{setCurrentLoggedInUser(res.data); triggerDispositionMutation(res.data.id)
          .then((res2:any)=>setTheDispositions(res2.data))
          .catch((disperr:any)=>console.log("error in fetching dispositions ", disperr))
      })
      .catch((meerr:any)=>console.log("error in fetching current login information ", meerr))
  },[]);*/

  return (
    <div>
      <div className="text-end">
        <CloseButton onClick={closeNewScheduleModal} />
      </div>

      <form className='form w-100 fv-plugins-bootstrap5 fv-plugins-framework' noValidate onSubmit={formik.handleSubmit}>
        <div className="text-start mb-4">
          <label className="form-label fs-2 fw-bolder text-dark card-title">
            {scheduleForEdit.scheduleEditEnabled?'Edit the':'Schedule a'} Ticket
          </label>
        </div>
        {currentLoggedInUser === "" && 
          <div className='fv-plugins-message-container text-danger'>
            <span role='alert'>Current Logged In User Details not available</span>
          </div>
        }
        
        <div className="row g-5 g-xl-8 mb-6">
          {/*title feed*/}
          <div className="col-xl-6">
            <div className="d-flex flex-column">
              <label className="required">Title</label>
              <input
                id='title'
                name='title'
                onChange={formik.handleChange}
                disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
                value={formik.values.title} type="text" className="form-control" />
              
              {formik.touched.title && formik.errors.title && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert'>{formik.errors.title}</span>
              </div>
              )}

              <label>Ticket Description</label>
              <textarea 
                className="form-control" 
                placeholder="Leave a ticket schedule description here" 
                id="description" 
                name='description'
                style={{height: '100px'}}
                onChange={formik.handleChange}
                value={formik.values.description}
                disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}></textarea>
              
              {formik.touched.description && formik.errors.description && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert'>{formik.errors.description}</span>
              </div>
              )}
            </div>
          </div>
          <div className="col-xl-6">
            <div className="text-center mb-2">
              <label className="form-label fs-6 fw-bolder text-dark card-title">
                Customer Details
              </label>
            </div>
            <div className="d-flex flex-column">
              <label>Email id</label>
              <input
                type="text" 
                className='form-control' 
                onChange={(evt)=>searchContactByMailID(evt.target.value)} placeholder="type in here"
                disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
                />

              <select 
                className={`form-select ${suggestions.length ? '':'d-none'}`} 
                value={selectedContact} 
                onChange={(evt) => setSelectedContact(Number.parseInt(evt.target.value))}
                >
                <option value="">Select an option...</option>
                {suggestions.map((suggestion, index) => (
                  <option key={index} value={suggestion.id}>{suggestion.email}</option>
                ))}
              </select>

              <label className="required">Customer Name</label>
              <input
                type="text" 
                value = {selectedContact} 
                className="form-control" 
                disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
                />
               {selectedContact === 0 && 
                  <div className='fv-plugins-message-container text-danger'>
                    <span role='alert'>Contact has not been provided</span>
                  </div>
                } 
            </div>
          </div>
        </div>

        
        <div className="row g-5 g-xl-8 mb-4">
          <div className="col-xl-6">
            
            
          </div>
          <div className="col-xl-6">
            
          </div>
          
        </div>

        <div className="text-end mt-6 mb-4">
          <button className="btn btn-sm btn-secondary" onClick={openAddContactModal}>
            <i className="bi bi-plus fs-2 me-2"></i>
            Contact
          </button>
        </div>

        <ReactModal isOpen={isAddNewContactModalOpen} onRequestClose={closeAddContactModal} style={customModalStyles} contentLabel="New Contact">
            <NewContactModal closeModal={closeAddContactModal} />
        </ReactModal>

        <div className="row g-5 g-xl-8 mb-4">
          <div className="col-xl-6">
            <label>Select Channel</label>
            <select 
              className="form-select select2-hidden-accessible"
              id='ticket_channel'
              name='ticket_channel'
              onChange={formik.handleChange}
              value={formik.values.ticket_channel} 
              disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
              >
              <option value="select team">
                Select
              </option>
              <option value="Phone">Phone</option>
              <option value="Email">Email</option>
              <option value="Chat">Chat</option>
              <option value="Social Media">Social Media</option>
              <option value="WhatsApp">WhatsApp</option>
            </select>
            {formik.touched.ticket_channel && formik.errors.ticket_channel && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.ticket_channel}</span>
            </div>
            )}
          </div>
          <div className="col-xl-6">
            <label>Select disposition</label>
            <select
                className="form-select select2-hidden-accessible"
                onChange={handleDispositionDropdownChange}
                value={selectedDisposition}
                disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
              >
              <option value="">Select Disposition</option>
              {theDispositions?.map((disposition)=>( <option value={disposition.id}>{disposition.topic}</option> ))}
            </select>
            {selectedDisposition === 0 && 
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert'>Disposition has not been provided</span>
              </div>
            }
          </div>

          <div className="col-xl-6">
            <label>Remarks</label>
            <input
              id='disposition_description'
              name='disposition_description'
              onChange={formik.handleChange}
              value={formik.values.disposition_description} 
              type="text" 
              className="form-control" 
              disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
              />

            {formik.touched.disposition_description && formik.errors.disposition_description && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.disposition_description}</span>
            </div>
            )}
          </div>
          <div className="col-xl-6">
            <label>Select status</label>
            <select 
              className="form-select select2-hidden-accessible"
              id='status_type'
              name='status_type'
              onChange={formik.handleChange}
              value={formik.values.status_type} 
              disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
              >
              <option value="">Select Status</option>
              {theStatuses.map((status)=>{ if(typeof status.status !== 'object') return <option value={status.status}>{status.status}</option> })}
            </select>

            {formik.touched.status_type && formik.errors.status_type && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.status_type}</span>
            </div>
            )}
          </div>
        </div>

        <div className="text-center mb-4">
          <label className="form-label fs-4 fw-bolder text-dark card-title">
            Schedule
          </label>
        </div>

        <div className="row g-5 g-xl-8 mb-6">
          <div className="col-xl-6">
            <label>Start</label>
            <input
              id='date_from'
              name='date_from'
              onChange={formik.handleChange}
              value={formik.values.date_from}
              className="form-control form-control-lg"
              type="date"
              placeholder="Custom Date"
              disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
            />

            {formik.touched.date_from && formik.errors.date_from && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.date_from}</span>
            </div>
            )}
          </div>

          <div className="col-xl-6">
            <label>End</label>
            <input
              id='date_to'
              name='date_to'
              onChange={formik.handleChange}
              value={formik.values.date_to}
              className="form-control form-control-lg"
              type="date"
              placeholder="Custom Date"
              disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
            />

            {formik.touched.date_to && formik.errors.date_to && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.date_to}</span>
            </div>
            )}
          </div>

          <div className="col-xl-6">
            <label>Select frequency</label>
            <select 
              className="form-select select2-hidden-accessible"
              id='schedule_timer'
              name='schedule_timer'
              onChange={formik.handleChange}
              value={formik.values.schedule_timer} 
              disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
              >
              <option value="select team" defaultValue={"select team"} hidden>
                Select
              </option>
              <option value="daily">Daily</option>
              <option value="weekly">Weekly</option>
              <option value="monthly">Monthly</option>
            </select>

            {formik.touched.schedule_timer && formik.errors.schedule_timer && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.schedule_timer}</span>
            </div>
            )}
          </div>

          <div className="col-xl-6 ">
            <label>Time</label>
            <input
              id='daily_time'
              name='daily_time'
              onChange={formik.handleChange}
              value={formik.values.daily_time}
              className="form-control form-control-lg"
              type="time"
              placeholder="Custom Time"
              disabled={scheduleForEdit.scheduleEditEnabled && !formEditingEnabled}
            />

            {formik.touched.daily_time && formik.errors.daily_time && (
            <div className='fv-plugins-message-container text-danger'>
              <span role='alert'>{formik.errors.daily_time}</span>
            </div>
            )}
          </div>
        </div>

        <div className="text-end mt-6 mb-4">
          {scheduleForEdit.scheduleEditEnabled && (<>
            <button className={`btn btn-sm btn-secondary ${formEditingEnabled?'d-none':''}`} type= "button" onClick={()=>setFormEditingEnabled(true)}>
              Edit
            </button>
          </>)}
          <button className={`btn btn-sm btn-secondary ${scheduleForEdit.scheduleEditEnabled && (formEditingEnabled?'':'d-none')}`} type= "submit">
            {scheduleForEdit.scheduleEditEnabled?'Save':'Schedule Ticket'}
          </button>
        </div>

      </form>
    </div>  
  );
};

export default NewScheduleModal;