import React, { useState } from "react";
interface IProps {
  selectedTeam: number
}

const ManualTicketRules:React.FC<IProps> = ({selectedTeam}) => {
  const [selectedSendTo, setSelectedSendTo] = useState("");
  const [message, setMessage] = useState("");

  const handleSendToChange = (event) => {
    setSelectedSendTo(event.target.value);
  };

  const insertPlaceholder = (placeholder) => {
    setMessage((prevMessage) => prevMessage + placeholder);
  };

  return (
    <>
      <div className="row mt-3">
        <div className="col">
          <span className="input-group-text bg-secondary mt-5">
            Manual ticket rules
          </span>
        </div>
      </div>

      <div className="row mt-5">
        <div className="w-100 mw-200px">
          <label className="d-flex align-items-start">When Status</label>
          {/*begin::Select2*/}
          <select className="form-select select2-hidden-accessible">
            <option value="select team" hidden selected>
              Select Status
            </option>
            <option value="Active">Active</option>
            <option value="Closed">Closed</option>
            <option value="Active">In Progress</option>
          </select>
          {/*end::Select2*/}
        </div>

        <div className="w-100 mw-200px">
          <label className="d-flex align-items-start">Send to</label>
          {/*begin::Select2*/}
          <select
            className="form-select select2-hidden-accessible"
            onChange={handleSendToChange}
          >
            <option value="select team" hidden selected>
              Select
            </option>
            <option value="Creator">Creator</option>
            <option value="Customer">Customer</option>
            <option value="Assignee">Assignee</option>
          </select>
          {/*end::Select2*/}
        </div>
      </div>

      <div className="row gap-5">
        <div className="w-100 mw-200px mt-5 ">
          <div className="d-flex align-items-center">
            <i className="bi bi-person-add me-2 fs-2 text-dark"></i>
            <button
              className="form-control form-control fw-bold"
              onClick={() => insertPlaceholder("{{assigned_by}}")}
            >
              Assigned By
            </button>
          </div>
        </div>

        <div className="w-100 mw-200px mt-5">
          <div className="d-flex align-items-center">
            <i className="bi bi-person-check-fill me-2 fs-2 text-dark"></i>
            <button
              className="form-control form-control fw-bold"
              onClick={() => insertPlaceholder("{{send_by}}")}
            >
              Send By
            </button>
          </div>
        </div>

        <div className="w-100 mw-200px mt-5">
          <div className="d-flex align-items-center">
            <i className="bi bi-ticket-detailed text-dark fs-2 me-2" />
            <button
              className="form-control form-control fw-bold"
              onClick={() => insertPlaceholder("{{ticket_id}}")}
            >
              Ticket ID
            </button>
          </div>
        </div>

        <div className="w-100 mw-225px mt-5">
          <div className="d-flex align-items-center">
            <i className="bi bi-ticket-detailed-fill text-dark fs-2 me-2" />
            <button
              className="form-control form-control fw-bold"
              onClick={() => insertPlaceholder("{{ticket_description}}")}
            >
              Ticket Description
            </button>
          </div>
        </div>
      </div>

      {/* Text area and label */}
      <div className="row mt-5">
        <div className="w-100">
          <div className="mb-0">
            <div className="input-group">
              <span className="input-group-text">Send to:</span>
              <input
                type="email"
                className="form-control"
                aria-label="Send to"
                aria-describedby="basic-addon1"
                value={selectedSendTo}
                readOnly
              />
            </div>
          </div>

          <textarea
            className="form-control"
            rows={4}
            placeholder="Enter Message..."
            value={message}
            onChange={(e) => setMessage(e.target.value)}
          ></textarea>
        </div>

        <div className="text-end mt-5">
          <button className="btn btn-primary">Save</button>
        </div>
      </div>
    </>
  );
};

export default ManualTicketRules;