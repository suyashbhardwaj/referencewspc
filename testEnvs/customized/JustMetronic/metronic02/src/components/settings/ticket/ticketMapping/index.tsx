import React, { useEffect, useState } from "react";
/*import { useEmaildispositionQuery, useTeamsQuery, useWorkflowsQQuery } from "../../../../Services/settingsAPIs/CommonAPI";
import { useTicketMapToDispositionMutation } from "../../../../Services/settingsAPIs/TicketSettingsAPI";*/
import { toast } from "react-toastify";
import { dispositionsForCollectionsTeamResponse, teamresponse, workflowresponse } from "../../../../services/apiresponses";

const TicketMapping = () => {
  /*const {data:teamsData, isLoading: isLoadingTeams} = useTeamsQuery({});*/const teamsData: any[] = teamresponse;
  const [theTeams, setTheTeams] = useState<any[]>([]);
  const [selectedTeam, setSelectedTeam] = useState<Number>();
  /*const { data: theDispositions } = useEmaildispositionQuery(selectedTeam, {skip: !selectedTeam,});*/ const theDispositions = dispositionsForCollectionsTeamResponse;
  /*const { data: theWorkflowsFetched } = useWorkflowsQQuery(selectedTeam, {skip: !selectedTeam,});*/ const theWorkflowsFetched = workflowresponse;
  const [theworkflows, setTheWorkflows] = useState<any>([]);
  const [workflowSelected, setWorkflowSelected] = useState<number>()
  /*const [triggerTicketMapToDisposition] = useTicketMapToDispositionMutation();*/
  async function triggerTicketMapToDisposition(obj:any){console.log('invoked triggerTicketMapToDisposition() with, ',JSON.stringify(obj))}

  const handleFirstDropdownChange = (event: any) => { setSelectedTeam(Number.parseInt(event.target.value));};

  function extractLabelsAndSteps(data: any, steps = []) {
    let result: any[] = [];
    data.forEach((item:any) => {
        if (item.sub_topics.length === 0) {
            // Leaf node found, add to result
            result.push({
                label: item.topic,
                id: item.id,
                isSetForMapping: false,
                steps: [...steps, item.topic]
            });
        } else {
            // Recursive call for each sub-topic
            let newSteps:any = [...steps, item.topic];
            result.push(...extractLabelsAndSteps(item.sub_topics, newSteps));
        }
    });
    return result;
  }
    
  const updateFetchedDispositionsImmutablyV2 = (dispositions: any, userTweaked: any, assignment: any) => {
    console.log(`Received dispositions as:`);
    console.dir(dispositions);
    console.log(`Received userTweaks as:`);
    console.dir(userTweaked);
    console.log(`Assignment is: ${assignment}`);
  
    // Create a map of id to isSetForMapping from userTweaked for quick lookup
    const idToIsSetForMapping:any = {};
    userTweaked.forEach((item:any) => { idToIsSetForMapping[item.id] = item.isSetForMapping; });
  
    // Function to recursively update dispositions array
    const updateDispositionsRecursive = (dispositions:any) => {
      return dispositions.map((disposition:any) => {
        // Check if disposition's id exists in idToIsSetForMapping and isSetForMapping is true
        if (idToIsSetForMapping.hasOwnProperty(disposition.id) && idToIsSetForMapping[disposition.id]) {
          // Create a copy of the disposition object and update disposition_name
          const updatedDisposition = {
            ...disposition,
            ticket_name: assignment
          };
          
          // Recursively update sub_topics if they exist
          if (updatedDisposition.sub_topics && updatedDisposition.sub_topics.length > 0) {
            updatedDisposition.sub_topics = updateDispositionsRecursive(updatedDisposition.sub_topics);
          }
  
          return updatedDisposition;
        } else {
          // If no update needed, return the original disposition object
          const originalDisposition = { ...disposition };
  
          // Recursively update sub_topics if they exist
          if (originalDisposition.sub_topics && originalDisposition.sub_topics.length > 0) {
            originalDisposition.sub_topics = updateDispositionsRecursive(originalDisposition.sub_topics);
          }
  
          return originalDisposition;
        }
      });
    };
  
    // Start updating dispositions array recursively
    const recursivelyUpdatedDispositions = updateDispositionsRecursive(dispositions);
  
    return recursivelyUpdatedDispositions;
  };

  const handleSave = () => {
    const payloadForUpdate = theDispositions;
    const immutablyUpdated = updateFetchedDispositionsImmutablyV2(payloadForUpdate, cardsData, workflowSelected);
    if(!workflowSelected) {alert("please select a workflow to map the disposition to"); return;}
    triggerTicketMapToDisposition({ body:immutablyUpdated,teamId:selectedTeam }).then((res:any)=>res.data && toast.success(`selected dispositions mapped successfully`))
  }

  const [cardsData, setCardData] = useState<any>(null);

  useEffect(() => { setTheTeams(teamsData); }, [teamsData]);
  useEffect(() => { theWorkflowsFetched && setTheWorkflows(theWorkflowsFetched.results); }, [theWorkflowsFetched]);
  useEffect(() => {theDispositions && setCardData(extractLabelsAndSteps(theDispositions))},[theDispositions]);
  return (
    <div className="card-toolbar flex-row-fluid justify-content-end gap-5 p-5">
      <div className="row">
        <div className="w-100 mw-200px">
          <select
            className="form-select select2-hidden-accessible"
            onChange={handleFirstDropdownChange}
          >
            <option value={0}>Select Team</option>
            {theTeams && theTeams.map((team, index)=>( <option key={index} value={team.id}>{team.name}</option> ))}
          </select>
        </div>

        {selectedTeam && (
          <div className="w-100 mw-200px">
            {/*begin::Select2*/}
            <select className="form-select" onChange={(e)=>setWorkflowSelected(Number.parseInt(e.target.value))}>
              <option value={0} selected>Select a Workflow</option>
                {theworkflows && theworkflows.map((theworkflow:any)=>(<option value={theworkflow.id}>{theworkflow.name}</option>))}
            </select>
            {/*end::Select2*/}
          </div>
        )}
        {selectedTeam && (
          <div className="row">
            <div className="col">
              <span className="input-group-text bg-secondary mt-5">
                Ticket Mapping
              </span>
            </div>
          </div>
        )}
        <div className="mt-5">
          {cardsData?.map((card: any, index: number) =>
            selectedTeam ? (
              <div key={index} className="col-12 mt-2">
                <div
                  className={`card p-4 ${
                    card.label === "label" ? "bg-secondary" : ""
                  }`}
                  style={{ minHeight: "50px" }}
                >
                  <div className="row align-items-center">
                    <div className="col-12 col-md-6 col-lg-4 text-start">
                      <div className="col text-start d-flex align-items-center gap-5">
                        <input type="checkbox" 
                          className="form-check-input" 
                          id={`disposition-${index}`} 
                          checked={card.isSetForMapping}
                          onChange={()=>{
                            setCardData((prev:any)=>{
                              const updated = [...prev];
                              updated[index] = {
                                ...updated[index],
                                isSetForMapping: !updated[index].isSetForMapping,
                              };
                              return updated;
                            })
                          }}/>
                        <label htmlFor={`disposition-${index}`} className="ms-2">
                          {card.label}
                        </label>
                      </div>
                    </div>
                    {card.description && (
                      <div className="col-12 col-md-6 col-lg-8 text-start">
                        {card.description}
                      </div>
                    )}
                    {card.steps && (
                      <div className="col-12 col-md-6 col-lg-8 d-flex flex-row align-items-center gap-5">
                        {card.steps.map((step: any, stepIndex: number) => (
                          <React.Fragment key={stepIndex}>
                            <div className="text-start">{step}</div>
                            {stepIndex < card.steps.length - 1 && (
                              <i className="bi bi-arrow-right text-primary"></i>
                            )}
                          </React.Fragment>
                        ))}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            ) : null
          )}

          {/* Save button */}
          {selectedTeam && (
            <div className="text-end mt-5">
              <button className="btn btn-primary" onClick={handleSave}>Save</button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default TicketMapping;