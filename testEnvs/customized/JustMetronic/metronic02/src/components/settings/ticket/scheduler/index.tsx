import { useState, useEffect } from "react";
import ReactModal from "react-modal";
import NewScheduleModal from "./NewScheduleModal";
import { useTicketScheduleMutation, useTicketSchedulesMutation } from "../../../../services/settingsAPIs/TicketSettingsAPI";
import { useTeamsQuery } from "../../../../services/settingsAPIs/CommonAPI";
import { defaultDeleteDialogModalStyles, defaultScrollableModalStyles } from "../../../common/CustomModalStyles";
import DeleteScheduleModal from "./DeleteScheduleModal";
import { teamresponse } from "../../../../services/apiresponses";

const Scheduler = () => {
  const {data:teamsData, isLoading: isLoadingTeams} = useTeamsQuery({});
  const [isNewScheduleModalOpen, setIsNewScheduleModalOpen] = useState(false); 
  const [isDeleteScheduleModalOpen, setIsDeleteScheduleModalOpen] = useState(false); 
  const [selectedTeam, setSelectedTeam] = useState<number>(0);
  const [theTeams, setTheTeams] = useState<any[]>([]);
  const [theSchedules, setTheSchedules] = useState([]);
  const [scheduleForEdit, setScheduleForEdit] = useState<any>({scheduleEditEnabled: false, scheduleData:null});
  const [scheduleForDeletion, setScheduleForDeletion] = useState<number>();
  const [autoTicketSchedulerToggler, setAutoTicketSchedulerToggler] = useState(false);
  const [triggerGetTicketSchedules] = useTicketSchedulesMutation();
  const [triggerGetATicketSchedule] = useTicketScheduleMutation();

  const openNewScheduleModal = () => { setIsNewScheduleModalOpen(true);};
  const closeNewScheduleModal = () => { setIsNewScheduleModalOpen(false); setScheduleForEdit({...scheduleForEdit, scheduleEditEnabled: false}); fetchListOfSchedules();};
  const openDeleteScheduleModal = () => { setIsDeleteScheduleModalOpen(true);};
  const closeDeleteScheduleModal = () => { setIsDeleteScheduleModalOpen(false); fetchListOfSchedules();};

  const handleAutoTicketSchedulerTogglerChange = () => { setAutoTicketSchedulerToggler(!autoTicketSchedulerToggler); };

  const handleFirstDropdownChange = (event) => { setSelectedTeam(event.target.value); };

  const tableHeaders = [
    {
      label: "Title",
      key: "title",
    },
    {
      label: "Contact",
      key: "Contact",
    },
    {
      label: "Channel",
      key: "Channel",
    },
    {
      label: "Frequency",
      key: "Frequency",
    },
    {
      label: "Scheduler time",
      key: "Scheduler time",
    },
  ];

  const getIconClass = (ticket_channel) => {
    switch(ticket_channel) {
      case 'Social-Media': return "bi bi-chat-left-text fs-1 text-dark";
      case 'Email': return "bi bi-chat-left-text fs-1 text-dark";
      case 'Phone': return "bi bi-telephone fs-1 text-dark";
      case 'Social-Media': return "bi bi-chat-left-text fs-1 text-dark";
      case 'Social-Media': return "bi bi-chat-left-text fs-1 text-dark";
      default: return "bi bi-chat-left-text fs-1 text-dark"
    }
  }

  function stripFields(jsonObj, fieldsToRemove) {
    for (let field of fieldsToRemove) {
        delete jsonObj[field];
    }
    return jsonObj;
  }

  async function fetchOneSchedule(scheduleId: number) {
    triggerGetATicketSchedule(scheduleId)
      .then((res:any)=>{
        if(res.data) {
          const strippedEditData = stripFields({...res.data}, ["id", "day_of_week", "day_of_month"]);
          console.log(strippedEditData);
          setScheduleForEdit({scheduleEditEnabled: true, scheduleData: strippedEditData})
          openNewScheduleModal();
        }
      })
    .catch((err)=>console.log(err));
  }
  
  async function deleteSchedule(scheduleId:number) {
    setScheduleForDeletion(scheduleId);
    openDeleteScheduleModal();
  }

  async function fetchListOfSchedules() { triggerGetTicketSchedules(selectedTeam).then((res:any)=>setTheSchedules(res.data)).catch(err=>console.log(err))}

  useEffect(() => { teamsData ? setTheTeams(teamsData) : setTheTeams(teamresponse); }, [teamsData]);
  useEffect(() => { fetchListOfSchedules(); }, [selectedTeam]);

  return (
    <div className="row gap-5 container-fluid">
      
        <div className="row align-items-center">
          <div className="col-md-2">
            <select
              className="form-select select2-hidden-accessible"
              onChange={handleFirstDropdownChange}
              value={selectedTeam}
            >
            <option value="">Select Team</option>
            {theTeams && theTeams.map((team, index)=>( <option value={index+1}>{team.name}</option> ))}
            </select>
          </div>

          {selectedTeam !==0 && (
            <>
              <div className="col-md-7 d-flex align-items-center gap-5 mt-4 mt-lg-0">
                <label className="me-3">Auto ticket scheduler</label>
                <div className="form-check form-switch">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    checked={autoTicketSchedulerToggler}
                    onChange={handleAutoTicketSchedulerTogglerChange}
                  />
                </div>
              </div>
              <div className={`col-md-3 d-flex align-items-center justify-content-end ${autoTicketSchedulerToggler?'':'d-none'} mt-4 mt-lg-0`}>
                <div>
                  <button
                    className="form-control form-control fw-bold"
                    onClick={openNewScheduleModal}
                  >
                    <i className="bi bi-plus fs-2 me-2"></i>
                    New Schedule
                  </button>
                </div>
              </div>

              <div className="container">
                <div className="row">
                  <div className="col-12 col-md-6 mx-auto">
                  <ReactModal
                    isOpen={isNewScheduleModalOpen}
                    onRequestClose={closeNewScheduleModal}
                    style={defaultScrollableModalStyles}
                    contentLabel="New Ticket"
                    >
                    <NewScheduleModal closeNewScheduleModal={closeNewScheduleModal} scheduleForEdit = {scheduleForEdit}/>
                  </ReactModal>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      

      {selectedTeam !==0 && (
        <>
          <div className="row">
            <div className="col">
              <div className="min-w-600px">
                {/* The Table Header */}
                <div
                  className="card mb-n5 my-1 p-4 mx-4"
                  style={{ backgroundColor: "#E4E4E47F" }}
                  >
                  <div className="row align-items-center">
                    {tableHeaders.map((header, index) => (
                      <div className="col-2 text-start" key={index}>
                        <label htmlFor="">{header.label}</label>
                      </div>
                    ))}
                    <div className="col-1 text-center">
                      <label htmlFor="">Action</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="min-w-600px">
                {/* Render cards */}
                {theSchedules && theSchedules.map((theSchedule, index) => (
                  <div className="card mt-2 my-1 p-4 mx-4" key={index}>
                    <div className="row align-items-center">
                      <div className="col-2 text-start">
                        <label htmlFor="">{theSchedule.title}</label>
                      </div>
                      <div className="col-2 text-start">
                        <label htmlFor="">{theSchedule.contact[0].name}</label>
                      </div>
                      <div className="col-2 text-start">
                        <i className={getIconClass(theSchedule.ticket_channel)}></i>
                      </div>
                      <div className="col-2 text-start">
                        <label htmlFor="">{theSchedule.schedule_timer}</label>
                      </div>
                      <div className="col-2 text-start">
                        <label htmlFor="">{theSchedule.daily_time}</label>
                      </div>
                      <div className="col-1 text-center d-flex">
                        <button className=" text-hover-primary border-0 bg-white" onClick={()=>fetchOneSchedule(theSchedule.id)}>
                          <i className="bi bi-pencil-fill text-dark fs-4"></i>
                        </button>
                        <button className="text-hover-danger border-0 bg-white ms-4" onClick={()=>deleteSchedule(theSchedule.id)}>
                          <i className="bi bi-trash-fill text-dark fs-4"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
                <div className="container">
                <div className="row">
                <div className="col-12 col-md-6 mx-auto">
                <ReactModal
                  isOpen={isDeleteScheduleModalOpen}
                  onRequestClose={closeDeleteScheduleModal}
                  style={defaultDeleteDialogModalStyles}
                  contentLabel="Delete Schedule"
                >
                  <DeleteScheduleModal closeDeleteScheduleModal={closeDeleteScheduleModal} scheduleForDeletion = {scheduleForDeletion}/>
                </ReactModal>
                </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Scheduler;