import { useState, useEffect } from "react";
import ReactModal from "react-modal";
const AddFieldModal = ({closeModal})=> {
    const fieldTypes = ["Text", "Email", "Number", "Telephone", "Text area", "Checkbox", "Nested", "Date-Time", "Date", "Time", "Dropdown"];
    return (<>
      <div className="d-flex flex-column align-items-center">
        <div className="d-flex">
          <input className = "form-control mx-4 min-w-350px" type="text" placeholder="Label"/>
          <button className="btn btn-secondary min-w-150px"><i className="text-start fs-2 bi bi-plus-lg me-2"></i>Add</button>
        </div>
      </div>
      <label htmlFor="" className="form-label">Choose Type</label>
      <div className="row row-cols-6 mt-1">
        {fieldTypes.map((field, index)=>(<>
          <div className="col" key={index}>
          <input className="mt-2 form-check-input" type="radio" name="thefieldtype" id={`thefield${index}`} value={field}/>
          <label className="mx-1 mt-2 form-label" htmlFor={`thefield${index}`}>{field}</label>
          </div>
        </>))}
      </div>
      <div className="d-flex justify-content-center mt-6">
        <input className="ms-4 form-check-input" type="checkbox" name="markMandatory" id="markMandatory" />
        <label className="mx-1 form-label" htmlFor="markMandatory">Mark as mandatory</label>
        <input className="ms-4 form-check-input" type="checkbox" name="markUnique" id="markUnique" />
        <label className="mx-1 form-label" htmlFor="markUnique">Mark as unique</label>
        <input className="ms-4 form-check-input" type="checkbox" name="markBoth" id="markBoth"/>
        <label className="mx-1 form-label" htmlFor="markBoth">Mark as both</label>
      </div>
    </>)
  }

const FieldsPane = ({setOpenedPane}) => {
    const [isModalOpen, setIsModalOpen] = useState(false)
    const openModal = () => { setIsModalOpen(true) }
    const closeModal = () => { setIsModalOpen(false) }
    const [userprofilefields, setUserprofilefields] = useState([
        {label:"First Name", type:"Text", isMandatory:false, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:[]}},
        {label:"Last Name", type:"Text", isMandatory:false, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:[]}},
        {label:"Employee Id", type:"Number", isMandatory:true, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:[]}},
        {label:"Mobile number", type:"Number", isMandatory:false, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:[]}},
        {label:"Address", type:"Text area", isMandatory:false, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:[]}},
        {label:"Blood group", type:"Text", isMandatory:false, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:['A-negative','B-positive','O-positive', 'O-negative']}},
        {label:"Alternate no.", type:"Number", isMandatory:false, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:[]}},
        {label:"Designation", type:"Text", isMandatory:false, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:[]}},
        {label:"Education", type:"Dropdown", isMandatory:false, isUnique:false, isMasked:false, isEditable:false, moreSpecifications:{dropdownoptions:["Post Graduation", "Graduation","HSC","SSC"]}},
        ]);
    const [expandForMoreSpecifications, setExpandForMoreSpecifications] = useState({expansionOn:-1, expanded: false});
    const customModalStyles: ReactModal.Styles = {
        content: {
          top: '50%',
          left: '50%',
          width:'50%',
          right: 'auto',
          bottom: 'auto',
          marginRight: '-50%',
          transform: 'translate(-50%, -50%)'
        }
      }
    return (
    <>
        <div className="row">
            <div className="col">
                <span className="input-group-text mt-2" id="basic-addon1">User profile fields</span>
            </div>
        </div> 

        <div className="row">
          <div className="col-12">
            <span className="input-group-text mt-2 bg-white border-0" id="basic-addon1">
              <div className="ms-auto">
                <button 
                  className="input-group-text form-control form-control align-items-center text-hover-primary fw-bold min-w-150px"
                  onClick = {openModal}>
                    <i className="bi bi-plus-lg me-2"></i>
                    <span>Option</span>
                </button>
              </div>
            </span>
            <ReactModal
                isOpen={isModalOpen}
                onRequestClose={closeModal}
                style={customModalStyles}
                contentLabel='Add a new Tag'
                >
                <AddFieldModal closeModal={closeModal}/>
            </ReactModal>
          </div>
        </div>

        <div className="row mx-0 mb-2 mt-4 overflow-auto">
          <div className="col">
            <div className="row">
                <div className="col px-2">
                  <div className="card px-2 my-1 min-w-600px" style={{ backgroundColor: '#E4E4E47F' }}>
                    <div className="row align-items-center">
                      <div className="col-4 text-start ps-lg-10 ps-4"><label htmlFor="">Label</label></div>
                      <div className="col-2 text-end"><button className="btn btn-link text-dark">Type<i className="bi bi-chevron-down fs-5 mx-2"></i></button></div>
                      <div className="col text-center"><label htmlFor="">Mandatory</label></div>
                      <div className="col text-start"><label htmlFor="">Unique</label></div>
                      <div className="col text-center"><label htmlFor="">Masking</label></div>
                      <div className="col text-end"><label htmlFor="">IsEditable</label></div>
                      <div className="col text-start"></div>
                      <div className="col text-start"></div>
                    </div>
                  </div>
                </div>
            </div>
        
            <div className="row mx-0">
              <div className="col px-2">
                      {userprofilefields.map((field,index)=>(<>
                      <div className="card my-1 min-w-600px" key={index}>
                        <div className="row align-items-center">
                          <div className="col-4 text-start ps-lg-10 ps-4">
                            
                              {
                                field.type === 'Dropdown'
                                ? (<div className="d-flex align-items-center flex-nowrap">
                                    <label htmlFor=""><div>{field.label}</div></label>
                                    <div className={`d-flex mx-2 ${expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded?'':'d-none'}`}>
                                      <input type="text" placeholder="Add a new item to the list." className="form-control m-1"/><button className="btn btn-sm btn-primary m-1">Add</button></div>
                                  </div>)
                                : (<label htmlFor="">{field.label}</label>)
                              }
                            
                          </div>
                          <div className="col-2 d-flex justify-content-end align-items-end dropdown">
                          {/*STARTS dynamic UI in case if dropdown OR nested is selected as a field type*/}
                          {field.type === 'Dropdown' 
                            ? (<button 
                                className="btn btn-link" 
                                onClick={()=>setExpandForMoreSpecifications({expansionOn: index, expanded:!expandForMoreSpecifications.expanded})}
                                > 
                                <i className="fs-2 bi bi-plus-lg me-2"></i>
                              </button>)
                            : field.type === 'Nested' ? (<>
                                <button 
                                  className="btn btn-link d-flex" 
                                  onClick={()=>setOpenedPane('confNestedDD')}
                                  >
                                    <i className="text-primary fs-2 bi bi-arrow-bar-left me-2"></i>
                                    Configure
                                </button>
                              </>)
                              :<div></div>
                          }
                          {/*ENDS dynamic UI in case if dropdown OR nested is selected as a field type */}
                          {/* STARTS select menu to select a field type from */}
                          <div className="w-100 mw-150px">
                            <select className="form-select select2-hidden-accessible border-0" value={field.type}
                              onChange={ (e)=>{ setUserprofilefields(prev=>{
                                  const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,type:e.target.value}:thefield)
                                  return updated; })
                              }}
                              >
                              {["Text", "Email", "Number", "Telephone", "Text area", "Checkbox", "Nested", "Date-Time", "Date", "Time", "Dropdown"].map((option)=>(<>
                                <option value={option}>{option}</option>
                              </>))}
                              </select>
                          </div>
                          {/* ENDS select menu to select a field type from */}
                          </div>
                          <div className="col text-center d-flex align-items-center">
                            <div className="col text-center">
                              <i  className={`me-4 fs-2 bi bi-check2 ${field.isMandatory?'text-dark':'text-muted'}`} 
                              style={{cursor:"pointer"}}
                              onClick={() => {
                                setUserprofilefields(prev=>{
                                  const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isMandatory:!thefield.isMandatory}:thefield)
                                  return updated;
                                })
                              }}></i>
                            </div>
                          </div>
                          <div className="col text-start">
                            <i  className={`me-4 fs-2 bi bi-check2 ${field.isUnique?'text-dark':'text-muted'}`} 
                            style={{cursor:"pointer"}}
                            onClick={() => {
                              setUserprofilefields(prev=>{
                                const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isUnique:!thefield.isUnique}:thefield)
                                return updated;
                              })
                            }}></i>
                          </div>
                          <div className="col text-start">
                            <i  className={`me-4 fs-2 bi bi-check2 ${field.isMasked?'text-dark':'text-muted'}`} 
                            style={{cursor:"pointer"}}
                            onClick={() => {
                              setUserprofilefields(prev=>{
                                const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isMasked:!thefield.isMasked}:thefield)
                                return updated;
                              })
                            }}></i>
                          </div>
                          <div className="col text-start">
                            <i  className={`me-4 fs-2 bi bi-check2 ${field.isEditable?'text-dark':'text-muted'}`} 
                            style={{cursor:"pointer"}}
                            onClick={() => {
                              setUserprofilefields(prev=>{
                                const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isEditable:!thefield.isEditable}:thefield)
                                return updated;
                              })
                            }}></i>
                          </div>
                          <div className="col text-end d-flex"><i className="ms-auto text-dark bi bi-pencil-fill fs-2"></i><i className="bi text-dark bi-trash-fill fs-2 mx-4"></i></div>
                        </div>
                      </div>
                      {/* starts: the drophang options list for dropdown field customizations */}
                      {expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded ? (<>
                          <div className="card my-1 bg-light-primary" key={`${index}-moreSpec`}>
                            {field.moreSpecifications.dropdownoptions.map(option=>(<>
                              <div className="card-header my-0 min-h-40px d-flex align-items-center justify-content-lg-start" key={index}>
                                  <div className="d-flex">
                                    <button className="btn btn-link"><i className="ms-auto bi bi-pencil-fill fs-2"></i></button>
                                    <button className="btn btn-link"><i className="fs-2 bi bi-x"></i></button>
                                  </div>
                                  <div className="ps-4">
                                    <label htmlFor="">{option}
                                    </label>
                                  </div>
                              </div>
                            </>))}  
                          </div>                    
                        </>):(<></>)}
                      {/* ends: the drophang options list for dropdown field customizations */}
                      </>))}
                    </div>      
            </div>
          </div>
        </div>
</>
  )
}

export default FieldsPane