import { useState } from 'react';

interface IProps {
  closeModal: any
}

const AddTeamModal: React.FC<IProps> = ({ closeModal }) => {
    const payload = {
        name: "testing",
        agent_module_permissions: [
            "dashboard:enable",
            "email:enable",
            "ticket:enable",
        ],
        manager_module_permissions: [],
        role: ["agent"],
    };

    const [tagToBeAdded, setTagToBeAdded] = useState("");
    const [currentPage, setCurrentPage] = useState("teamlistings");
    const addTagToTheList = () => {
        console.log("Tag being added is ", tagToBeAdded);
        closeModal();
    };
    const [teamfields, setTeamFields] = useState([
        { enabled: true, moduleName: "Setting", canEdit: true, canView: false },
        { enabled: true, moduleName: "Email", canEdit: false, canView: true },
        { enabled: true, moduleName: "Ticket", canEdit: true, canView: true },
        {
            enabled: true,
            moduleName: "Dashboard",
            canEdit: false,
            canView: false,
        },
        {
            enabled: false,
            moduleName: "Dashboard",
            canEdit: false,
            canView: false,
        },
        { enabled: false, moduleName: "Chat", canEdit: true, canView: false },
        { enabled: false, moduleName: "Phone", canEdit: true, canView: false },
        {
            enabled: false,
            moduleName: "Campaign",
            canEdit: true,
            canView: true,
        },
    ]);

    const addTeam = async () => {
      axios
        .post("http://botgo.localhost:8000/api/v1/users/teams/", payload, {
          headers: {
            Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE3MTYwNjg3NjIsInRlYW0iOiIxIiwiYWNjb3VudCI6ZmFsc2V9.vQqc3QJjcATrYoSi6alN5jKTvAc6RTkyRDUgv0ItFJw`,
          },
        })
        .then((res: any) => {
          console.log(res);
        })
        .catch((error:any) => {console.log(error)})
    };
  return (
    <>
      <form action="">
        <div className="row">
          <div className="col-6">
            <div className="d-flex flex-column align-items-baseline">
              <label className="form-label" htmlFor="teamname">
                Team Name
              </label>
              <input
                className="form-control"
                name="teamname"
                placeholder="Enter text"
                type="text"
              />
            </div>
          </div>
          <div className="col-6">
            <div className="d-flex flex-column align-items-baseline">
              <label className="form-label" htmlFor="role">
                Select Role
              </label>
              <select
                className="form-control"
                name="role"
                id=""
                placeholder="Manager"
              >
                <option value="Manager">Manager</option>
              </select>
            </div>
          </div>
        </div>
        <div className="row mx-0 input-group-text mt-2">
          <div className="col-6 text-start">
            <span>Module Name</span>
          </div>
          <div className="col-6 text-start">
            <div>
              <i className="me-4 bi bi-pencil-fill fs-2"></i>
              <i className="ms-4 fs-2 bi bi-eye"></i>
            </div>
          </div>
        </div>
        {teamfields.map((teamfield, index) => (
          <>
            <div className="row mx-0 input-group-text bg-white mt-1" key={index}>
              <div className="col-6 text-start">
                <span
                  className={teamfield.enabled ? "text-dark" : "text-muted"}
                  style={{ cursor: "pointer" }}
                  onClick={() =>
                    setTeamFields((prev) => {
                      const updated = [...prev];
                      updated[index] = {
                        ...updated[index],
                        enabled: !updated[index].enabled,
                      };
                      return updated;
                    })
                  }
                >
                  {teamfield.moduleName}
                </span>
              </div>
              <div className="col-6 text-start">
                <div>
                  <i
                    className={`me-4 fs-2 bi bi-check2 ${
                      teamfield.canEdit ? "text-dark" : "text-muted"
                    }`}
                    style={{ cursor: "pointer" }}
                    onClick={() =>
                      setTeamFields((prev) => {
                        const updated = [...prev];
                        updated[index] = {
                          ...updated[index],
                          canEdit: !updated[index].canEdit,
                        };
                        return updated;
                      })
                    }
                  ></i>
                  <i
                    className={`ms-4 fs-2 bi bi-check2 ${
                      teamfield.canView ? "text-dark" : "text-muted"
                    }`}
                    style={{ cursor: "pointer" }}
                    onClick={() =>
                      setTeamFields((prev) => {
                        const updated = [...prev];
                        updated[index] = {
                          ...updated[index],
                          canView: !updated[index].canView,
                        };
                        return updated;
                      })
                    }
                  ></i>
                </div>
              </div>
            </div>
          </>
        ))}
        <div className="row mt-4">
          <div className="col-12 d-flex">
            <div className="ms-auto">
              <button className="btn btn-secondary w-150px" onClick={addTeam}>
                Save
              </button>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default AddTeamModal;