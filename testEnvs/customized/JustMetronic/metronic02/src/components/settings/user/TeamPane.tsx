import { useState, useEffect } from "react";
import ReactModal from "react-modal";
import { useTeamsQuery } from "../../../services/settingsAPIs/CommonAPI";
import { defaultModalStyles } from "../../common/CustomModalStyles";
import LoadingSpinner from "../../LoadingSpinner";
import AddTeamModal from "./AddTeamModal";
import { teamresponse } from "../../../services/apiresponses";

const TeamPane = () => {
  const [currentPage, setCurrentPage] = useState("teamlistings");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const {data, isLoading} = useTeamsQuery({});

  const openModal = () => { setIsModalOpen(true); };
  const closeModal = () => { setIsModalOpen(false); };
  const [theTeams, setTheTeams] = useState<any[]>([]);
  const [showAllTeams, setShowAllTeams] = useState<boolean>(false);
  const teamfields = [
    "Setting",
    "Email",
    "Ticket",
    "Dashboard",
    "Dashboard2",
    "Chat",
    "Phone",
    "Campaign",
  ];

  useEffect(() => { data ? setTheTeams(data) : setTheTeams(teamresponse)}, [data]);
  return (
    <>
      <div className="row">
        <div className="col-10">
          <div className="input-group mb-3">
            <span className="input-group-text" id="basic-addon1">
              <i className="fs-2 bi bi-search"></i>
            </span>
            <input
              type="text"
              className="form-control"
              placeholder="Search"
              aria-label="Search"
              aria-describedby="basic-addon1"
            />
          </div>
        </div>
        <div className="col-2">
          <button
            className="form-control form-control align-items-center text-hover-primary fw-bold min-w-150px"
            onMouseOver={() => setCurrentPage("addteam")}
            onClick={openModal}
          >
            <i className="bi bi-plus-lg me-2"></i>
            <span className="ms-auto">Team</span>
          </button>
        </div>
      </div>
      <ReactModal
        isOpen={isModalOpen}
        onRequestClose={closeModal}
        style={defaultModalStyles}
        contentLabel="Add a new Tag"
      >
        <AddTeamModal closeModal={closeModal} />
      </ReactModal>
      
      {currentPage === "teamlistings" ? (
        <>
          <div className="row">
            <div className="col">
              <span className="input-group-text mt-2" id="basic-addon1">
                Team
              </span>
            </div>
          </div>
          {isLoading ? (/*Render the loading spinner if isLoading is true */ <LoadingSpinner />) : (<>
            {theTeams && theTeams.map((team, index) =>
              index < 6 ? (
                <>
                  <div className="row" key={index}>
                    <div className="col-12">
                      <span
                        className="input-group-text bg-white mt-2"
                        id="basic-addon1"
                      >
                        {team.name}
                        <i className="ms-auto text-dark bi bi-pencil-fill fs-2"></i>
                        <i className="bi text-dark bi-trash-fill fs-2 mx-4"></i>
                      </span>
                    </div>
                  </div>
                </>
              ) : (
                showAllTeams && (
                  <>
                    <div className="row">
                      <div className="col-12">
                        <span
                          className="input-group-text bg-white mt-2"
                          id="basic-addon1"
                        >
                          {team.name}
                          <i className="ms-auto bi bi-pencil-fill fs-2"></i>
                          <i className="bi bi-trash-fill fs-2 mx-4"></i>
                        </span>
                      </div>
                    </div>
                  </>
                )
              )
            )}
            <div className="row">
              <div className="col-12">
                <span
                  className="input-group-text mt-2 bg-white border-0"
                  id="basic-addon1"
                >
                  <div className="ms-auto">
                    <button className="btn btn-link" onClick={() => setShowAllTeams(!showAllTeams)}>
                      {!showAllTeams ? (theTeams && theTeams.length>6 && <><i className="fs-2 bi bi-plus-lg me-2"></i>` ${theTeams && theTeams.length - 6} more`</>) : (<>collapse</>)}
                    </button>
                  </div>
                </span>
              </div>
            </div>            
          </>)}
        </>
      ) : currentPage === "addteam" ? (
        <>
          <div className="row">
            <div className="col-6">
              <div className="d-flex flex-column align-items-baseline">
                <label className="form-label" htmlFor="teamname">
                  Team Name
                </label>
                <input
                  className="form-control"
                  name="teamname"
                  placeholder="Enter text"
                  type="text"
                />
              </div>
            </div>
            <div className="col-6">
              <div className="d-flex flex-column align-items-baseline">
                <label className="form-label" htmlFor="role">
                  Select Role
                </label>
                <input
                  className="form-control"
                  name="role"
                  placeholder="Manager"
                  type="text"
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <span className="input-group-text mt-2" id="basic-addon1">
                Module Name
              </span>
            </div>
            <div className="col-6">
              <span className="input-group-text mt-2" id="basic-addon1">
                Access
              </span>
            </div>
          </div>
          {teamfields.map((teamfield, index) => (
            <>
              <div className="row" key={index}>
                <div className="col-12">
                  <span
                    className="input-group-text bg-white mt-2"
                    id="basic-addon1"
                  >
                    {teamfield}
                    <input className="mx-2 ms-auto" type="checkbox" />
                    <i className="mx-2 text-dark bi bi-pencil-fill fs-2"></i>
                    <i className="mx-2 fs-2 bi bi-eye"></i>
                  </span>
                </div>
              </div>
            </>
          ))}
          <div className="row mt-4">
            <div className="col-12 d-flex">
              <div className="ms-auto">
                <button
                  className="btn btn-secondary"
                  onClick={() => setCurrentPage("teamlistings")}
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        </>
      ) : (
        <>
          <h5>default page</h5>
        </>
      )}
    </>
  );
};

export default TeamPane;