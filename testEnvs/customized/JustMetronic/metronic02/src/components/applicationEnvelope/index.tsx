import { Suspense } from 'react'
import { Outlet } from 'react-router-dom'
import LoadingSpinner from '../LoadingSpinner'

const ApplicationEnvelope = () => {
	return (
		<Suspense fallback={<LoadingSpinner />}>
     		<Outlet />
	    </Suspense>
	)
}

export default ApplicationEnvelope