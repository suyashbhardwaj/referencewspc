import { toast } from "react-toastify";


export const showErrorToast = (str) => {
  toast.error(str, {
    style: {
      background: "red",
      color: "white",
    },
    theme: "dark",
  });
};

export const showSuccessToast = (str) => {
  toast.success(str, {
    style: {
      background: "green",
      color: "white",
    },
    theme: "dark",
  });
};
