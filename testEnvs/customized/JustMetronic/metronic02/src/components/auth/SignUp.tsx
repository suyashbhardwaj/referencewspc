import { useFormik } from 'formik'
import * as Yup from 'yup'
import { useState } from 'react';
import { SIGNUP, SIGNMEUP, GETTOKEN } from "../../graphql/mutations";
import { useMutation, useQuery } from "@apollo/client";
import { AUTH_TOKEN } from '../../constants';
import { useNavigate } from 'react-router-dom';

const initialValues = {
  username: '',
  useremail: '',
  userpass: '',
}

const registerSchema = Yup.object().shape({
  username: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('User name is required'),
  useremail: Yup.string()
    .email('Wrong email format')
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Email is required'),  
  userpass: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Password is required')
})

const SignUp = () => {
  const [, setLoading] = useState(false)
  const [responseMsg,setResponseMsg] = useState('');
  const [responseOne,setResponseOne] = useState();
  const [responseTwo,setresponseTwo] = useState();
  const navigate = useNavigate();
  const { loading: getTokenLoading, error: getTokenError, data: tokenData, refetch: refetchToken } = useQuery(GETTOKEN, {
    skip: true // Initially skip the query
  });
  const [signup] = useMutation(SIGNMEUP)/*,{
    refetchQueries: [
      {
        query: SIGNINQUERY,
        variables: {
            emailid: 'yash@yahoo.com',
            password: '123456'
        }
      }
    ]
  })*/
    const formik = useFormik({
      initialValues,
      validationSchema: registerSchema,
      onSubmit: (values) => {
        setLoading(true)
        signup({
          variables: {
            name: values.username,
            email: values.useremail,
            password: values.userpass,
          }
        })
          .then((res: any) => {
              console.log("response from create user ", res);
              setResponseOne(res);
              // setresponseMsg(res.data.signUp.message)
              setLoading(false)
              // localStorage.setItem("AccountId", res.data.createMyUser)
              /*navigate('/dashboard');
              document.location.reload()*/
              refetchToken({
                email: values.useremail,
                password: values.userpass,
              }).then((res2:any) => {
                setresponseTwo(res2)
                localStorage.setItem(AUTH_TOKEN, res2.data.getMyUserToken)
                navigate('/verify-email');
                document.location.reload()
              }).catch(err=>console.log("error is ", err));
            })
          .catch((err: any) => {
            setLoading(false)
            /*setSubmitting(false)*/
            console.error(err)
          }) 
      }
    })
  return (
    <div className="center-wrap">
            <div className="section text-center">
              <form className="form" onSubmit={formik.handleSubmit}>
                  <h4 className="mb-4 pb-3">Register Yourself</h4>
                  <div className="form-group">
                      <input type="text" 
                        className="form-style" 
                        placeholder="Your Full Name" 
                        id="username"
                        {...formik.getFieldProps('username')}/>
                      <i className="input-icon uil uil-user"></i>
                  </div>  
                  <div className="form-group mt-2">
                      <input type="email" 
                        className="form-style" 
                        placeholder="Your Email" 
                        id="useremail"
                        {...formik.getFieldProps('useremail')}/>
                      <i className="input-icon uil uil-at"></i>
                  </div>  
                  <div className="form-group mt-2">
                      <input type="password" 
                        className="form-style" 
                        placeholder="Your Password" 
                        id="userpass"
                        {...formik.getFieldProps('userpass')}/>
                      <i className="input-icon uil uil-lock-alt"></i>
                  </div>
                  <button type = 'submit' className='btn mt-4'>submit</button>
              </form>
            <h4 className="mb-0 mt-2 text-center text-danger">{responseMsg}</h4>
            </div>
    </div>
  )
}

export default SignUp

/*{
  "operationName": "GetUserById",
  "variables": {},
  "query": "query GetUserById {\n  getUserById {\n    id\n    name\n    email\n    status\n    contactNumber\n    Designation\n    Department\n    emailVerified\n    onboardingDone\n    UserRole {\n      roleId\n      userId\n      __typename\n    }\n    franchiseId\n    __typename\n  }\n}"
}

{
  "data": {
    "getUserById": {
      "id": "819",
      "name": "Pego Hip",
      "email": "pegohip946@rartg.com",
      "status": "Active",
      "contactNumber": null,
      "Designation": null,
      "Department": null,
      "emailVerified": false,
      "onboardingDone": null,
      "UserRole": [
        {
          "roleId": 1,
          "userId": 819,
          "__typename": "userRoles"
        }
      ],
      "franchiseId": null,
      "__typename": "UserById"
    }
  }
}


{"data":{"getBgAccountById":{"id":"627","__typename":"BgAccount"}}}


{
  "data": {
    "getcompletePackageDetailsbyBgAccountId": {
      "status": 1,
      "message": "complete Plan/Package details found.",
      "data": {
        "bgAccountId": 627,
        "clientPackageId": 30,
        "status": "ACTIVE",
        "startDate": "2024-01-29T00:00:00.000Z",
        "endDate": "2024-04-12T08:10:00.412Z",
        "package": {
          "package": {
            "packageId": 1,
            "packageName": "SIMPLE_BOT",
            "packageDuration": 28,
            "packageExpiryDate": "2024-01-29T16:19:11.000Z",
            "permissions": 1,
            "packageType": {
              "typeId": 1,
              "typeName": "FREE_PLAN"
            }
          },
          "feature": {
            "featuresId": 1,
            "numberOfBots": 50,
            "dashboardAccess": true,
            "sessionsLimit": 999999,
            "uniqueEndUsers": 999999,
            "additionalConversationCost": true,
            "supportUsers": 1,
            "reportingAnalytics": "ADVANCE",
            "language": "SINGLE",
            "CRMIntegration": true,
            "additionalChannelntegration": 1,
            "Intents": 999999,
            "paymentIntegration": true,
            "mapIntegraionAPI": true,
            "dataSecurity": true,
            "conversationHistory": 1,
            "thirdPartyIntegration": true,
            "callToAction": true,
            "exportsLeads": true,
            "exportReport": true,
            "templateCustomization": true,
            "emailAndCallSupport": true,
            "leadGenerationForm": true,
            "multipleChatWindowsForLive_ChatPerUser": 1,
            "ticketHistory": 1,
            "blockUser": true,
            "visitorInsight": true,
            "preDefinedResponses": true,
            "liveChat": true,
            "whiteLabelRemoveBranding": true,
            "fileUploads": true,
            "additionalIntegrationPerWebsiteSameBot": true,
            "askingPrice": 1,
            "createdBy": "SUPERADMIN",
            "createdAt": "2024-01-29T16:21:23.342Z",
            "updatedBy": "SUPERADMIN",
            "updatedAt": "2024-01-29T16:21:23.342Z"
          }
        }
      },
      "__typename": "DefaultPayload"
    }
  }
}*/