import { VERIFY_EMAIL, VERIFYEMAILBYOTP } from "../../graphql/mutations";
import { useMutation } from "@apollo/client";
// import { FINDUSERBYID } from '../../graphql/query';
import { useNavigate } from "react-router-dom";
import { useContext, useState } from 'react';
import UserContext from '../../context/UserContext';

interface VEprops {
user: any
}

const VerifyEmail: React.FC<VEprops> = ({user}) => {
	const { currentUser } = useContext(UserContext)
	const [theInput, setTheInput] = useState('');
	const navigate = useNavigate();
	// const loggedInUserAccountID = localStorage.getItem('AccountId')?.replaceAll('"','');
	const [verifyEmail] = useMutation(VERIFYEMAILBYOTP)

	const loggedInUserMailID = localStorage.getItem('MailId')?.replaceAll('"','');
	const verifyMailSubmit = (evt: any) => {
		evt.preventDefault()
		verifyEmail({
        variables: {
	        theotp: theInput
	        }
	      })
	        .then((res: any) => {
	        	if(res.data.verifyMail){ navigate('/onboarding/personal-details')}

	        })
	      	.catch((err) => {
              console.error(err)
        	}) 
	    }

	return (
		<div className="container mt-10">
			<form onSubmit={verifyMailSubmit}>
			  <div className="mb-3">
			    <label htmlFor="exampleInputEmail1" className="form-label">The OTP</label>
			    <input type="text" onChange = {(e)=>setTheInput(e.target.value)} value = {theInput} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
			    <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
			  </div>
			  <button type="submit" className="btn btn-primary">Submit</button>
			</form>
		</div>
	)
}

export default VerifyEmail