import { Link, useNavigate } from "react-router-dom";
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { useState } from "react";
import { AUTH_TOKEN, MYDASH_USER } from "../../constants";
import './login.css'
import { GETTOKEN, SIGNIN } from "../../graphql/mutations";
import { useMutation } from "@apollo/client";
import SignUp from "./SignUp";
import { toast } from "react-toastify";
import { useQuery } from '@apollo/client/react';

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Wrong email format')
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Email is required'),
  password: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Password is required')
})

const initialValues = {
  email: '',
  password: ''
}

export function Login() {
  const [, setLoading] = useState(false)
  const [incorrectpassUI, setIncorrectpassUI] = useState(false)
  const navigate = useNavigate();
  const [signin] = useMutation(SIGNIN)
  const { loading: getTokenLoading, error: getTokenError, data: tokenData, refetch: refetchToken } = useQuery(GETTOKEN, {
    skip: true // Initially skip the query
  });
  const formik = useFormik({
    initialValues,
    validationSchema: loginSchema,
    onSubmit: (values) => {
      console.log('submition initiated', values)
      console.log('submition initiated')
      setLoading(true)
      refetchToken({
        email: values.email,
        password: values.password,
      }).then((res:any) => {
          if (res.data.getMyUserToken !== null) {
            localStorage.setItem(AUTH_TOKEN, res.data.getMyUserToken)
            toast.success("Successfully logged in");
            navigate('/dashboard');
            document.location.reload()
          } 
            else {
                setIncorrectpassUI(true); 
              }
        })
        .catch((err) => {
          setIncorrectpassUI(true); 
          setLoading(false)
          /*setSubmitting(false)*/
          console.error(err)
        }) 
    }
  })
  return (
    <div className="loginscreen">
    <div className="section bg-black">
      <div className="container">
        {/*<div className="row full-height justify-content-center">*/}
        <div className="full-height justify-content-center">
          <div className="col-12 text-center align-self-center py-5">
            <div className="section pb-5 pt-5 pt-sm-2 text-center">
              {/*<h6 className="mb-0 pb-3 text-primary"><span>Log In </span><span>Sign Up</span></h6>*/}
            <h6 className="mb-0 pb-3 text-primary text-end"><span>Log In </span><span>Sign Up</span></h6>
              <input className="checkbox" type="checkbox" id="reg-log" name="reg-log"/>
              <label className="me-20" htmlFor="reg-log"></label>
              {/*<div className="card-3d-wrap mx-auto">*/}
              {/*<div className="card-3d-wrap">*/}
              <div className="card-3d-wrap mt-20">
                <div className="card-3d-wrapper">
                  <div className="card-front">
                    <div className="center-wrap">
                      <div className="section text-center">
                        <form className="form" onSubmit={formik.handleSubmit}>
                          <h4 className="mb-4 pb-3">Log In</h4>
                          
                          <div className="form-group">
                            <input 
                              type='email'
                              className="form-style" 
                              placeholder="Your Email" 
                              {...formik.getFieldProps('email')}
                              id="logemail"/>
                            <i className="input-icon uil uil-at"></i>
                          </div>  
                          <div className="form-group mt-2">
                            <input 
                              type="password" 
                              className="form-style" 
                              placeholder="Your Password" 
                              {...formik.getFieldProps('password')}
                              id="logpass"/>
                            <i className="input-icon uil uil-lock-alt"></i>
                          </div>
                        
                          <button
                            type="submit"
                            id="kt_sign_in_submit"
                            className="btn btn-lg btn-primary w-100 mt-4"
                          >
                            Continue
                          </button>

                          <p className="mb-0 mt-4 text-center">
                            {/* start::Link */}
                            <Link
                              to="/auth/forgot-password"
                              className="link link-primary fs-6 fw-bolder"
                              style={{ marginLeft: "5px" }}
                            > Forgot your password?
                            </Link>
                            {/* end::Link */}
                          </p>
                          {incorrectpassUI && (<h4 className="mb-0 mt-2 text-center text-danger">Incorrect Password!!!</h4>)}
                        </form>
                      </div>
                    </div>
                  </div>
                  <div className="card-back">
                    <SignUp/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  );
}