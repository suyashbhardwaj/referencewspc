import { useEffect } from 'react'
import { AUTH_TOKEN, MYDASH_USER } from '../../constants'

export function Logout() {
  useEffect(() => {
    localStorage.removeItem(AUTH_TOKEN)
    localStorage.removeItem(MYDASH_USER)
    document.location.reload()
  }, [])

  return <h1>See you soon!!</h1>
}
