import { MainAppRoutes } from './routes/MainAppRoutes'
import { ApolloProvider } from '@apollo/client'
import client from './shared/apollo-client'
// import { ApolloClient } from '@apollo/client';
// import { InMemoryCache } from '@apollo/client';
import { ToastContainer, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { ThemeProvider } from './context/ThemeContext'
import { Provider } from 'react-redux'
import { store } from './reduxStore/Store'
import { loadErrorMessages, loadDevMessages } from "@apollo/client/dev";

/*const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql',
    cache: new InMemoryCache(),
    });*/

  loadDevMessages();
  loadErrorMessages();

const SOhome = () => {
	return (
		<Provider store={store}>
			<ThemeProvider>
				<ApolloProvider client={client}>
				    <ToastContainer transition={Slide} />
				    <MainAppRoutes />
				</ApolloProvider>
			</ThemeProvider>
		</Provider>
	)
}

export default SOhome