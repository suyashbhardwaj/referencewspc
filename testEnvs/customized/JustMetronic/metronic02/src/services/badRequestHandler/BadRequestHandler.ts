import { BaseQueryFn, FetchArgs, fetchBaseQuery, FetchBaseQueryError } from '@reduxjs/toolkit/query/react';
import { showErrorToast } from "../../components/toast/Toast";
import { CIRCLEONE_AUTH_TOKEN, CIRCLEONE_USER } from "../../constants";

interface ErrorData {
  message: string;
}

interface Result {
  data?: any;
  error?: {
    status: number;
    data?: ErrorData;
  };
}

export const dynamicBaseQuery: BaseQueryFn<string | FetchArgs, unknown, FetchBaseQueryError> = async (args:any, api:any, extraOptions:any) => {
  const rawBaseQuery = fetchBaseQuery({
    baseUrl: `http://${localStorage.getItem(CIRCLEONE_USER)?`${localStorage.getItem(CIRCLEONE_USER)}.`:""}localhost:8000/api`,
    headers: {
      Authorization: `Bearer ${localStorage.getItem(CIRCLEONE_AUTH_TOKEN)}`,
    },
  });
  const result = await rawBaseQuery(args, api, extraOptions);

  if (result?.error) {
    const responseMessage = (result.error.data as ErrorData)?.message;
    const status = result?.error?.status;
    if (status === 401) {
      localStorage.clear();
    //   window.location.replace("/");
    } else {
      showErrorToast(responseMessage);
    }
  }
  return result;
  // if (result?.data.status === 200 || result?.data.status) {
  //   message.success(result?.data?.message);
  // } else if (result?.data.status === false) {
  //   message.error(result?.data?.message);
  // }
};