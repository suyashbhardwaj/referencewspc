export const teamresponse = [
	{
		"id": 1,
		"name": "zapak",
		"agent_module_permissions": [
			"dashboard:enable",
			"ticket:enable",
			"email:enable"
		],
		"manager_module_permissions": [
			"customers:enable",
			"settings:enable",
			"dashboard:enable",
			"email:enable",
			"ticket:enable"
		],
		"role": [
			"manager"
		],
		"auto_ticket_generate_enabled": true,
		"auto_ticket_notification_enabled": true,
		"manual_ticket_notification_enabled": true,
		"email_notification_enabled": true,
		"ticket_resolution_sla_emails": [
			"testOne@yoyo.com",
			"testOne@yoyo.com",
			"anotherOne@gmail.com",
			"moreOver@yahoo.com",
			"test01@gmail.com",
			"tarun@wayh.com"
		],
		"time_zone": "Asia/Kolkata",
		"open_24_7": true,
		"periodic_ticket_generation": false,
		"is_default": true
	},
	{
		"id": 2,
		"name": "Insurance team",
		"agent_module_permissions": [
			"campaigns:enable",
			"dashboard:enable",
			"reports:enable",
			"email:enable",
			"ticket:enable"
		],
		"manager_module_permissions": [
			"customers:enable",
			"settings:enable",
			"campaigns:enable",
			"dashboard:enable",
			"reports:enable",
			"users:enable",
			"email:enable",
			"ticket:enable"
		],
		"role": [
			"agent"
		],
		"auto_ticket_generate_enabled": true,
		"auto_ticket_notification_enabled": true,
		"manual_ticket_notification_enabled": true,
		"email_notification_enabled": false,
		"ticket_resolution_sla_emails": [
			"suyash.inprofessionaluse@gmail.com"
		],
		"time_zone": "Asia/Kolkata",
		"open_24_7": true,
		"periodic_ticket_generation": false,
		"is_default": false
	},
	{
		"id": 7,
		"name": "myUnder Writing Team",
		"agent_module_permissions": [
			"dashboard:enable",
			"ticket:enable",
			"email:enable"
		],
		"manager_module_permissions": [
			"customers:enable",
			"dashboard:enable",
			"reports:enable",
			"users:enable",
			"email:enable",
			"ticket:enable"
		],
		"role": [
			"manager"
		],
		"auto_ticket_generate_enabled": false,
		"auto_ticket_notification_enabled": false,
		"manual_ticket_notification_enabled": false,
		"email_notification_enabled": false,
		"ticket_resolution_sla_emails": [],
		"time_zone": "Asia/Kolkata",
		"open_24_7": true,
		"periodic_ticket_generation": false,
		"is_default": false
	},
	{
		"id": 8,
		"name": "GD Team",
		"agent_module_permissions": [
			"dashboard:enable",
			"ticket:enable",
			"email:enable"
		],
		"manager_module_permissions": [
			"settings:enable",
			"dashboard:enable",
			"reports:enable",
			"users:enable",
			"email:enable",
			"ticket:enable"
		],
		"role": [
			"manager"
		],
		"auto_ticket_generate_enabled": false,
		"auto_ticket_notification_enabled": false,
		"manual_ticket_notification_enabled": false,
		"email_notification_enabled": false,
		"ticket_resolution_sla_emails": [],
		"time_zone": "Asia/Kolkata",
		"open_24_7": true,
		"periodic_ticket_generation": false,
		"is_default": false
	},
	{
		"id": 10,
		"name": "TestTeam02",
		"agent_module_permissions": [
			"customers:enable",
			"settings:enable",
			"dashboard:enable",
			"users:enable",
			"email:enable"
		],
		"manager_module_permissions": [
			"settings:enable",
			"dashboard:enable",
			"ticket:enable",
			"email:enable"
		],
		"role": [
			"manager"
		],
		"auto_ticket_generate_enabled": false,
		"auto_ticket_notification_enabled": true,
		"manual_ticket_notification_enabled": true,
		"email_notification_enabled": true,
		"ticket_resolution_sla_emails": [],
		"time_zone": "Asia/Kolkata",
		"open_24_7": true,
		"periodic_ticket_generation": false,
		"is_default": false
	}
]

export const workflowresponse = {
	"count": 4,
	"next": null,
	"previous": null,
	"results": [
		{
			"id": 1,
			"name": "zapak",
			"active_status": "active",
			"in_progress_statuses": [],
			"is_assigned_to_creator_on_close": true,
			"resolved_status": null,
			"closed_status": "closed",
			"reopen_status": "Hey",
			"closed_assignment_team": null,
			"closed_visibility_team": null,
			"disposition": null,
			"all_statuses": [
				{
					"status": "active",
					"status_type": "active",
					"visibility_team": null
				},
				{
					"status": {
						"id": 1,
						"status": "closed",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": true
					},
					"status_type": "closed",
					"visibility_team": null
				},
				{
					"status": "Hey",
					"status_type": "reopen",
					"visibility_team": null
				}
			],
			"team": 1,
			"closed_status_visible": false,
			"closed_statuses": [
				{
					"status": {
						"id": 1,
						"status": "closed",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": true
					},
					"status_type": "closed",
					"visibility_team": null
				}
			]
		},
		{
			"id": 2,
			"name": "GDGSM Helpline",
			"active_status": "Enquiry is registered",
			"in_progress_statuses": [
				{
					"id": 13,
					"status": "Enquiry is validated",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				}
			],
			"is_assigned_to_creator_on_close": true,
			"resolved_status": null,
			"closed_status": "Enquiry is fulfilled",
			"reopen_status": "Enquiry resumed further",
			"closed_assignment_team": null,
			"closed_visibility_team": null,
			"disposition": null,
			"all_statuses": [
				{
					"status": "Enquiry is registered",
					"status_type": "active",
					"visibility_team": null
				},
				{
					"status": {
						"id": 13,
						"status": "Enquiry is validated",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 2,
						"status": "Enquiry is fulfilled",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": true
					},
					"status_type": "closed",
					"visibility_team": null
				},
				{
					"status": "Enquiry resumed further",
					"status_type": "reopen",
					"visibility_team": null
				}
			],
			"team": null,
			"closed_status_visible": false,
			"closed_statuses": [
				{
					"status": {
						"id": 2,
						"status": "Enquiry is fulfilled",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": true
					},
					"status_type": "closed",
					"visibility_team": null
				}
			]
		},
		{
			"id": 3,
			"name": "My Insurance Policies",
			"active_status": "New",
			"in_progress_statuses": [
				{
					"id": 7,
					"status": "Policy selection",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				},
				{
					"id": 8,
					"status": "Processing application",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				},
				{
					"id": 9,
					"status": "Medical examination",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				},
				{
					"id": 10,
					"status": "Document collection submission",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				},
				{
					"id": 18,
					"status": "Underwriting procedure",
					"assignment_team": {
						"id": 7,
						"name": "myUnder Writing Team",
						"agent_module_permissions": [
							"dashboard:enable",
							"ticket:enable",
							"email:enable"
						],
						"manager_module_permissions": [
							"customers:enable",
							"dashboard:enable",
							"reports:enable",
							"users:enable",
							"email:enable",
							"ticket:enable"
						],
						"role": [
							"manager"
						],
						"auto_ticket_generate_enabled": false,
						"auto_ticket_notification_enabled": false,
						"manual_ticket_notification_enabled": false,
						"email_notification_enabled": false,
						"ticket_resolution_sla_emails": [],
						"time_zone": "Asia/Kolkata",
						"open_24_7": true,
						"periodic_ticket_generation": false,
						"is_default": false
					},
					"visibility_team": {
						"id": 7,
						"name": "myUnder Writing Team",
						"agent_module_permissions": [
							"dashboard:enable",
							"ticket:enable",
							"email:enable"
						],
						"manager_module_permissions": [
							"customers:enable",
							"dashboard:enable",
							"reports:enable",
							"users:enable",
							"email:enable",
							"ticket:enable"
						],
						"role": [
							"manager"
						],
						"auto_ticket_generate_enabled": false,
						"auto_ticket_notification_enabled": false,
						"manual_ticket_notification_enabled": false,
						"email_notification_enabled": false,
						"ticket_resolution_sla_emails": [],
						"time_zone": "Asia/Kolkata",
						"open_24_7": true,
						"periodic_ticket_generation": false,
						"is_default": false
					},
					"ticket_name_mapped": [
						{
							"id": 5,
							"created": "2024-08-06T15:12:06.376704Z",
							"updated": "2024-08-06T15:12:06.376734Z",
							"name": "myUnderwriting process",
							"active_status": "new",
							"resolved_status": null,
							"reopen_status": "reopen",
							"is_assigned_to_creator_on_close": true,
							"closed_assignment_team_id": null,
							"closed_visibility_team_id": null,
							"created_by_id": 1,
							"team_id": null,
							"closed_status_visible": false
						}
					],
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": [
						{
							"id": 11,
							"topic": "document verificaiton"
						}
					],
					"user": null,
					"round_robin": false,
					"FIFO": false
				}
			],
			"is_assigned_to_creator_on_close": true,
			"resolved_status": null,
			"closed_status": "Policy Delivery",
			"reopen_status": "ReOpen",
			"closed_assignment_team": null,
			"closed_visibility_team": null,
			"disposition": null,
			"all_statuses": [
				{
					"status": "New",
					"status_type": "active",
					"visibility_team": null
				},
				{
					"status": {
						"id": 7,
						"status": "Policy selection",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 8,
						"status": "Processing application",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 9,
						"status": "Medical examination",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 10,
						"status": "Document collection submission",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 18,
						"status": "Underwriting procedure",
						"assignment_team": {
							"id": 7,
							"name": "myUnder Writing Team",
							"agent_module_permissions": [
								"dashboard:enable",
								"ticket:enable",
								"email:enable"
							],
							"manager_module_permissions": [
								"customers:enable",
								"dashboard:enable",
								"reports:enable",
								"users:enable",
								"email:enable",
								"ticket:enable"
							],
							"role": [
								"manager"
							],
							"auto_ticket_generate_enabled": false,
							"auto_ticket_notification_enabled": false,
							"manual_ticket_notification_enabled": false,
							"email_notification_enabled": false,
							"ticket_resolution_sla_emails": [],
							"time_zone": "Asia/Kolkata",
							"open_24_7": true,
							"periodic_ticket_generation": false,
							"is_default": false
						},
						"visibility_team": {
							"id": 7,
							"name": "myUnder Writing Team",
							"agent_module_permissions": [
								"dashboard:enable",
								"ticket:enable",
								"email:enable"
							],
							"manager_module_permissions": [
								"customers:enable",
								"dashboard:enable",
								"reports:enable",
								"users:enable",
								"email:enable",
								"ticket:enable"
							],
							"role": [
								"manager"
							],
							"auto_ticket_generate_enabled": false,
							"auto_ticket_notification_enabled": false,
							"manual_ticket_notification_enabled": false,
							"email_notification_enabled": false,
							"ticket_resolution_sla_emails": [],
							"time_zone": "Asia/Kolkata",
							"open_24_7": true,
							"periodic_ticket_generation": false,
							"is_default": false
						},
						"ticket_name_mapped": [
							{
								"id": 5,
								"created": "2024-08-06T15:12:06.376704Z",
								"updated": "2024-08-06T15:12:06.376734Z",
								"name": "myUnderwriting process",
								"active_status": "new",
								"resolved_status": null,
								"reopen_status": "reopen",
								"is_assigned_to_creator_on_close": true,
								"closed_assignment_team_id": null,
								"closed_visibility_team_id": null,
								"created_by_id": 1,
								"team_id": null,
								"closed_status_visible": false
							}
						],
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": [
							{
								"id": 11,
								"topic": "document verificaiton"
							}
						],
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": 7
				},
				{
					"status": {
						"id": 4,
						"status": "Policy Delivery",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": true
					},
					"status_type": "closed",
					"visibility_team": null
				},
				{
					"status": {
						"id": 5,
						"status": "Application declined",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": false
					},
					"status_type": "closed",
					"visibility_team": null
				},
				{
					"status": "ReOpen",
					"status_type": "reopen",
					"visibility_team": null
				}
			],
			"team": null,
			"closed_status_visible": false,
			"closed_statuses": [
				{
					"status": {
						"id": 4,
						"status": "Policy Delivery",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": true
					},
					"status_type": "closed",
					"visibility_team": null
				},
				{
					"status": {
						"id": 5,
						"status": "Application declined",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": false
					},
					"status_type": "closed",
					"visibility_team": null
				}
			]
		},
		{
			"id": 5,
			"name": "myUnderwriting process",
			"active_status": "new",
			"in_progress_statuses": [
				{
					"id": 14,
					"status": "Completed application rececived",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				},
				{
					"id": 15,
					"status": "Medical examination check in progress",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				},
				{
					"id": 16,
					"status": "Inspection report",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				},
				{
					"id": 17,
					"status": "Application on hold",
					"assignment_team": null,
					"visibility_team": null,
					"ticket_name_mapped": null,
					"main_status": null,
					"wip_status": null,
					"closed_status": null,
					"status_type": null,
					"disposition": null,
					"user": null,
					"round_robin": false,
					"FIFO": false
				}
			],
			"is_assigned_to_creator_on_close": true,
			"resolved_status": null,
			"closed_status": "application processed",
			"reopen_status": "reopen",
			"closed_assignment_team": null,
			"closed_visibility_team": null,
			"disposition": null,
			"all_statuses": [
				{
					"status": "new",
					"status_type": "active",
					"visibility_team": null
				},
				{
					"status": {
						"id": 14,
						"status": "Completed application rececived",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 15,
						"status": "Medical examination check in progress",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 16,
						"status": "Inspection report",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 17,
						"status": "Application on hold",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false
					},
					"status_type": "wip",
					"visibility_team": null
				},
				{
					"status": {
						"id": 7,
						"status": "application processed",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": true
					},
					"status_type": "closed",
					"visibility_team": null
				},
				{
					"status": {
						"id": 8,
						"status": "Application declined",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": false
					},
					"status_type": "closed",
					"visibility_team": null
				},
				{
					"status": "reopen",
					"status_type": "reopen",
					"visibility_team": null
				}
			],
			"team": null,
			"closed_status_visible": false,
			"closed_statuses": [
				{
					"status": {
						"id": 7,
						"status": "application processed",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": true
					},
					"status_type": "closed",
					"visibility_team": null
				},
				{
					"status": {
						"id": 8,
						"status": "Application declined",
						"assignment_team": null,
						"visibility_team": null,
						"ticket_name_mapped": null,
						"main_status": null,
						"wip_status": null,
						"closed_status": null,
						"status_type": null,
						"disposition": null,
						"user": null,
						"round_robin": false,
						"FIFO": false,
						"is_default": false
					},
					"status_type": "closed",
					"visibility_team": null
				}
			]
		}
	]
}

export const dynamicFieldDataTicketNameResponse = [
	{
		"id": 2,
		"field_type": "text",
		"label": "Name of Applicant",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [],
		"identifier": "field_2",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 1,
				"name": "zapak",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [
					"testOne@yoyo.com",
					"testOne@yoyo.com",
					"anotherOne@gmail.com",
					"moreOver@yahoo.com",
					"test01@gmail.com",
					"tarun@wayh.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": true
			},
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 3,
		"field_type": "date-time",
		"label": "DOB",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [],
		"identifier": "field_3",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 1,
				"name": "zapak",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [
					"testOne@yoyo.com",
					"testOne@yoyo.com",
					"anotherOne@gmail.com",
					"moreOver@yahoo.com",
					"test01@gmail.com",
					"tarun@wayh.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": true
			},
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 4,
		"field_type": "multi-level",
		"label": "place of living",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [],
		"identifier": "field_4",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 1,
				"name": "zapak",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [
					"testOne@yoyo.com",
					"testOne@yoyo.com",
					"anotherOne@gmail.com",
					"moreOver@yahoo.com",
					"test01@gmail.com",
					"tarun@wayh.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": true
			},
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 5,
		"field_type": "check-box",
		"label": "Notify You On",
		"is_required": true,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [],
		"identifier": "field_5",
		"campaign_name": null,
		"multiple_choices": [
			{
				"id": 1,
				"choice": "electronic mail",
				"sort_order": 1,
				"field": 5
			},
			{
				"id": 3,
				"choice": "letter",
				"sort_order": 1,
				"field": 5
			}
		],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 1,
				"name": "zapak",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [
					"testOne@yoyo.com",
					"testOne@yoyo.com",
					"anotherOne@gmail.com",
					"moreOver@yahoo.com",
					"test01@gmail.com",
					"tarun@wayh.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": true
			},
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 7,
				"name": "myUnder Writing Team",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": false,
				"manual_ticket_notification_enabled": false,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 8,
				"name": "GD Team",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"settings:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": false,
				"manual_ticket_notification_enabled": false,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 10,
				"name": "TestTeam02",
				"agent_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"users:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"settings:enable",
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 6,
		"field_type": "drop-down",
		"label": "Education",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [
			{
				"id": 1,
				"choice": "High School",
				"sort_order": 1,
				"field": 6
			},
			{
				"id": 2,
				"choice": "Intermediatte",
				"sort_order": 1,
				"field": 6
			},
			{
				"id": 3,
				"choice": "Post Graduate",
				"sort_order": 1,
				"field": 6
			},
			{
				"id": 6,
				"choice": "Graduate",
				"sort_order": 1,
				"field": 6
			}
		],
		"identifier": "field_6",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 1,
				"name": "zapak",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [
					"testOne@yoyo.com",
					"testOne@yoyo.com",
					"anotherOne@gmail.com",
					"moreOver@yahoo.com",
					"test01@gmail.com",
					"tarun@wayh.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": true
			},
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 7,
				"name": "myUnder Writing Team",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": false,
				"manual_ticket_notification_enabled": false,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 8,
				"name": "GD Team",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"settings:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": false,
				"manual_ticket_notification_enabled": false,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 10,
				"name": "TestTeam02",
				"agent_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"users:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"settings:enable",
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 7,
		"field_type": "text",
		"label": "Mailing Address",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [],
		"identifier": "field_7",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 1,
				"name": "zapak",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [
					"testOne@yoyo.com",
					"testOne@yoyo.com",
					"anotherOne@gmail.com",
					"moreOver@yahoo.com",
					"test01@gmail.com",
					"tarun@wayh.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": true
			},
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 7,
				"name": "myUnder Writing Team",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": false,
				"manual_ticket_notification_enabled": false,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 8,
				"name": "GD Team",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"settings:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": false,
				"manual_ticket_notification_enabled": false,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			{
				"id": 10,
				"name": "TestTeam02",
				"agent_module_permissions": [
					"customers:enable",
					"settings:enable",
					"dashboard:enable",
					"users:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"settings:enable",
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": true,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 30,
		"field_type": "tel-phone",
		"label": "Contact number",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [],
		"identifier": "field_30",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 32,
		"field_type": "drop-down",
		"label": "Activity",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [
			{
				"id": 7,
				"choice": "Cricket",
				"sort_order": 1,
				"field": 32
			}
		],
		"identifier": "field_32",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 33,
		"field_type": "check-box",
		"label": "Venue",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 1,
		"choices": [],
		"identifier": "field_33",
		"campaign_name": null,
		"multiple_choices": [
			{
				"id": 18,
				"choice": "Farm house",
				"sort_order": 1,
				"field": 33
			}
		],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 37,
		"field_type": "text",
		"label": "Name of patient MIP",
		"is_required": true,
		"model_name": "ticket_name",
		"ticket_name": 3,
		"choices": [],
		"identifier": "field_37",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 30,
		"is_masked": false
	},
	{
		"id": 38,
		"field_type": "number",
		"label": "Age of patient MIP",
		"is_required": true,
		"model_name": "ticket_name",
		"ticket_name": 3,
		"choices": [],
		"identifier": "field_38",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 2,
		"is_masked": false
	},
	{
		"id": 39,
		"field_type": "tel-phone",
		"label": "Contact number MIP",
		"is_required": true,
		"model_name": "ticket_name",
		"ticket_name": 3,
		"choices": [],
		"identifier": "field_39",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [
			{
				"id": 2,
				"name": "Insurance team",
				"agent_module_permissions": [
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"email:enable",
					"ticket:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"settings:enable",
					"campaigns:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"agent"
				],
				"auto_ticket_generate_enabled": true,
				"auto_ticket_notification_enabled": true,
				"manual_ticket_notification_enabled": true,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [
					"suyash.inprofessionaluse@gmail.com"
				],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			}
		],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 40,
		"field_type": "text-area",
		"label": "Medical history MIP",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 3,
		"choices": [],
		"identifier": "field_40",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 41,
		"field_type": "check-box",
		"label": "Location preference MIP",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 3,
		"choices": [],
		"identifier": "field_41",
		"campaign_name": null,
		"multiple_choices": [
			{
				"id": 20,
				"choice": "New delhi",
				"sort_order": 1,
				"field": 41
			},
			{
				"id": 21,
				"choice": "Noida",
				"sort_order": 1,
				"field": 41
			},
			{
				"id": 22,
				"choice": "Ghaziabad",
				"sort_order": 1,
				"field": 41
			}
		],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 42,
		"field_type": "drop-down",
		"label": "Got referred to by",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 3,
		"choices": [
			{
				"id": 9,
				"choice": "Print ad",
				"sort_order": 1,
				"field": 42
			},
			{
				"id": 10,
				"choice": "Organic references",
				"sort_order": 1,
				"field": 42
			},
			{
				"id": 11,
				"choice": "Social media",
				"sort_order": 1,
				"field": 42
			}
		],
		"identifier": "field_42",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [],
		"field_length": 0,
		"is_masked": false
	},
	{
		"id": 43,
		"field_type": "text",
		"label": "Test Field MUP",
		"is_required": false,
		"model_name": "ticket_name",
		"ticket_name": 5,
		"choices": [],
		"identifier": "field_43",
		"campaign_name": null,
		"multiple_choices": [],
		"multiple_level": null,
		"is_unique": false,
		"is_editable": true,
		"team": [],
		"field_length": 0,
		"is_masked": false
	}
]

export const dispositionsForCollectionsTeamResponse = [
	{
		"id": 2,
		"topic": "myInsurance",
		"sub_topics": [
			{
				"id": 3,
				"topic": "meInterested",
				"sub_topics": [],
				"priority": "low",
				"ticket_name": 3,
				"team": 2,
				"hide": false
			},
			{
				"id": 4,
				"topic": "mePotential",
				"sub_topics": [],
				"priority": "low",
				"ticket_name": 3,
				"team": 2,
				"hide": false
			}
		],
		"priority": "low",
		"ticket_name": null,
		"team": 2,
		"hide": false
	}
]

export const emailTempalteResponse = [
	{
		"id": 1,
		"subject": "Template 01",
		"content": "<strong>Lorem ipsum </strong><br>dolor sit amet consectetur adipisicing elit. Tempora est quas atque modi enim minus, pariatur blanditiis. Eius maiores est ratione quos nam! Voluptatum vitae sequi perferendis laborum dolorum deleniti.<br>Yihaaoo01",
		"attachments": [],
		"team": 1
	},
	{
		"id": 2,
		"subject": "Template 02",
		"content": "<strong>Lorem ipsum </strong><em>dolor sit amet consectetur adipisicing elit. Tempora est quas atque modi enim minus, pariatur blanditiis. Eius maiores est ratione quos nam! Voluptatum vitae sequi perferendis laborum dolorum deleniti.</em><br>Yihaaoo02",
		"attachments": [],
		"team": 1
	}
]
/*not from my set up tenants*/
export const smtpMailConfigurationsResponse = [
	{
		"id": 6,
		"username": "testDeveloper06@testdomain.com",
		"password": "Test@1234",
		"host_name": "smtp.zapak.in",
		"port": 871,
		"team": [
			1
		],
		"user": [
			1
		],
		"email_auto_response_enabled": true,
		"email_auto_response_template": "to Assignee from {{Sender Name}} for {{Ticket Id}}",
		"client_id": "",
		"client_secret": "",
		"server_tenant_id": "",
		"email_server": ""
	},
	{
		"id": 7,
		"username": "testDeveloper07@testdomain.com",
		"password": "Test@1234",
		"host_name": "smtp.check.in",
		"port": 811,
		"team": [
			1
		],
		"user": [
			2
		],
		"email_auto_response_enabled": true,
		"email_auto_response_template": "something to test further {{Ticket Id}}",
		"client_id": "451",
		"client_secret": "yoyohoneysingh",
		"server_tenant_id": "jigsag",
		"email_server": "microsoft"
	}
]
/*not from my set up tenants*/
export const teamActiveEmailNotificationResponse = [
	{
		"id": 16,
		"team": 1,
		"status_type": "active",
		"send_to": [
			"Customer",
			"Customer",
			"Creator",
			"Assignee"
		],
		"notification_type": "email",
		"creator_template": "Hey Creator!\nA ticket has been created & assigned by {{Assigned By}}\n\n{{Sender Name}}",
		"assignee_template": "The assignee {{Sender Name}}",
		"customer_template": "{{Sender Name}} yihaaoo {{Assigned By}} !!"
	}
]

export const oneTeamConfigResponse = {
	"id": 2,
	"name": "Insurance team",
	"agent_module_permissions": [
		"campaigns:enable",
		"dashboard:enable",
		"reports:enable",
		"email:enable",
		"ticket:enable"
	],
	"manager_module_permissions": [
		"customers:enable",
		"settings:enable",
		"campaigns:enable",
		"dashboard:enable",
		"reports:enable",
		"users:enable",
		"email:enable",
		"ticket:enable"
	],
	"role": [
		"agent"
	],
	"auto_ticket_generate_enabled": true,
	"auto_ticket_notification_enabled": true,
	"manual_ticket_notification_enabled": true,
	"email_notification_enabled": false,
	"ticket_resolution_sla_emails": [
		"suyash.inprofessionaluse@gmail.com"
	],
	"time_zone": "Asia/Kolkata",
	"open_24_7": true,
	"periodic_ticket_generation": false,
	"is_default": false
}

export const dispositionsForInsuranceTeamResponse = [
	{
		"id": 2,
		"topic": "myInsurance",
		"sub_topics": [
			{
				"id": 3,
				"topic": "meInterested",
				"sub_topics": [],
				"priority": "low",
				"ticket_name": null,
				"team": 2,
				"hide": false
			},
			{
				"id": 4,
				"topic": "mePotential",
				"sub_topics": [],
				"priority": "low",
				"ticket_name": null,
				"team": 2,
				"hide": false
			}
		],
		"priority": "low",
		"ticket_name": null,
		"team": 2,
		"hide": false
	},
	{
		"id": 5,
		"topic": "myInsurance",
		"sub_topics": [
			{
				"id": 6,
				"topic": "meInterested",
				"sub_topics": [],
				"priority": "low",
				"ticket_name": null,
				"team": 2,
				"hide": false
			},
			{
				"id": 7,
				"topic": "mePotential",
				"sub_topics": [],
				"priority": "low",
				"ticket_name": null,
				"team": 2,
				"hide": false
			},
			{
				"id": 8,
				"topic": "maintainence",
				"sub_topics": [],
				"priority": "low",
				"ticket_name": null,
				"team": 2,
				"hide": false
			},
			{
				"id": 9,
				"topic": "scheduled",
				"sub_topics": [
					{
						"id": 23,
						"topic": "scheduledForCheck",
						"sub_topics": [],
						"priority": "low",
						"ticket_name": null,
						"team": 2,
						"hide": false
					}
				],
				"priority": "low",
				"ticket_name": null,
				"team": 2,
				"hide": false
			}
		],
		"priority": "low",
		"ticket_name": null,
		"team": 2,
		"hide": false
	}
]

export const statusesForAWorkflowResponse = {
	"id": 3,
	"name": "My Insurance Policies",
	"active_status": "New",
	"in_progress_statuses": [
		{
			"id": 7,
			"status": "Policy selection",
			"assignment_team": null,
			"visibility_team": null,
			"ticket_name_mapped": null,
			"main_status": null,
			"wip_status": null,
			"closed_status": null,
			"status_type": null,
			"disposition": null,
			"user": null,
			"round_robin": false,
			"FIFO": false
		},
		{
			"id": 8,
			"status": "Processing application",
			"assignment_team": null,
			"visibility_team": null,
			"ticket_name_mapped": null,
			"main_status": null,
			"wip_status": null,
			"closed_status": null,
			"status_type": null,
			"disposition": null,
			"user": null,
			"round_robin": false,
			"FIFO": false
		},
		{
			"id": 9,
			"status": "Medical examination",
			"assignment_team": null,
			"visibility_team": null,
			"ticket_name_mapped": null,
			"main_status": null,
			"wip_status": null,
			"closed_status": null,
			"status_type": null,
			"disposition": null,
			"user": null,
			"round_robin": false,
			"FIFO": false
		},
		{
			"id": 10,
			"status": "Document collection submission",
			"assignment_team": null,
			"visibility_team": null,
			"ticket_name_mapped": null,
			"main_status": null,
			"wip_status": null,
			"closed_status": null,
			"status_type": null,
			"disposition": null,
			"user": null,
			"round_robin": false,
			"FIFO": false
		},
		{
			"id": 18,
			"status": "Underwriting procedure",
			"assignment_team": {
				"id": 7,
				"name": "myUnder Writing Team",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": false,
				"manual_ticket_notification_enabled": false,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			"visibility_team": {
				"id": 7,
				"name": "myUnder Writing Team",
				"agent_module_permissions": [
					"dashboard:enable",
					"ticket:enable",
					"email:enable"
				],
				"manager_module_permissions": [
					"customers:enable",
					"dashboard:enable",
					"reports:enable",
					"users:enable",
					"email:enable",
					"ticket:enable"
				],
				"role": [
					"manager"
				],
				"auto_ticket_generate_enabled": false,
				"auto_ticket_notification_enabled": false,
				"manual_ticket_notification_enabled": false,
				"email_notification_enabled": false,
				"ticket_resolution_sla_emails": [],
				"time_zone": "Asia/Kolkata",
				"open_24_7": true,
				"periodic_ticket_generation": false,
				"is_default": false
			},
			"ticket_name_mapped": [
				{
					"id": 5,
					"created": "2024-08-06T15:12:06.376704Z",
					"updated": "2024-08-06T15:12:06.376734Z",
					"name": "myUnderwriting process",
					"active_status": "new",
					"resolved_status": null,
					"reopen_status": "reopen",
					"is_assigned_to_creator_on_close": true,
					"closed_assignment_team_id": null,
					"closed_visibility_team_id": null,
					"created_by_id": 1,
					"team_id": null,
					"closed_status_visible": false
				}
			],
			"main_status": null,
			"wip_status": null,
			"closed_status": null,
			"status_type": null,
			"disposition": [
				{
					"id": 11,
					"topic": "document verificaiton"
				}
			],
			"user": null,
			"round_robin": false,
			"FIFO": false
		}
	],
	"is_assigned_to_creator_on_close": true,
	"resolved_status": null,
	"closed_status": "Policy Delivery",
	"reopen_status": "ReOpen",
	"closed_assignment_team": null,
	"closed_visibility_team": null,
	"disposition": null,
	"all_statuses": [
		{
			"status": "New",
			"status_type": "active",
			"visibility_team": null
		},
		{
			"status": {
				"id": 7,
				"status": "Policy selection",
				"assignment_team": null,
				"visibility_team": null,
				"ticket_name_mapped": null,
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": null,
				"user": null,
				"round_robin": false,
				"FIFO": false
			},
			"status_type": "wip",
			"visibility_team": null
		},
		{
			"status": {
				"id": 8,
				"status": "Processing application",
				"assignment_team": null,
				"visibility_team": null,
				"ticket_name_mapped": null,
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": null,
				"user": null,
				"round_robin": false,
				"FIFO": false
			},
			"status_type": "wip",
			"visibility_team": null
		},
		{
			"status": {
				"id": 9,
				"status": "Medical examination",
				"assignment_team": null,
				"visibility_team": null,
				"ticket_name_mapped": null,
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": null,
				"user": null,
				"round_robin": false,
				"FIFO": false
			},
			"status_type": "wip",
			"visibility_team": null
		},
		{
			"status": {
				"id": 10,
				"status": "Document collection submission",
				"assignment_team": null,
				"visibility_team": null,
				"ticket_name_mapped": null,
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": null,
				"user": null,
				"round_robin": false,
				"FIFO": false
			},
			"status_type": "wip",
			"visibility_team": null
		},
		{
			"status": {
				"id": 18,
				"status": "Underwriting procedure",
				"assignment_team": {
					"id": 7,
					"name": "myUnder Writing Team",
					"agent_module_permissions": [
						"dashboard:enable",
						"ticket:enable",
						"email:enable"
					],
					"manager_module_permissions": [
						"customers:enable",
						"dashboard:enable",
						"reports:enable",
						"users:enable",
						"email:enable",
						"ticket:enable"
					],
					"role": [
						"manager"
					],
					"auto_ticket_generate_enabled": false,
					"auto_ticket_notification_enabled": false,
					"manual_ticket_notification_enabled": false,
					"email_notification_enabled": false,
					"ticket_resolution_sla_emails": [],
					"time_zone": "Asia/Kolkata",
					"open_24_7": true,
					"periodic_ticket_generation": false,
					"is_default": false
				},
				"visibility_team": {
					"id": 7,
					"name": "myUnder Writing Team",
					"agent_module_permissions": [
						"dashboard:enable",
						"ticket:enable",
						"email:enable"
					],
					"manager_module_permissions": [
						"customers:enable",
						"dashboard:enable",
						"reports:enable",
						"users:enable",
						"email:enable",
						"ticket:enable"
					],
					"role": [
						"manager"
					],
					"auto_ticket_generate_enabled": false,
					"auto_ticket_notification_enabled": false,
					"manual_ticket_notification_enabled": false,
					"email_notification_enabled": false,
					"ticket_resolution_sla_emails": [],
					"time_zone": "Asia/Kolkata",
					"open_24_7": true,
					"periodic_ticket_generation": false,
					"is_default": false
				},
				"ticket_name_mapped": [
					{
						"id": 5,
						"created": "2024-08-06T15:12:06.376704Z",
						"updated": "2024-08-06T15:12:06.376734Z",
						"name": "myUnderwriting process",
						"active_status": "new",
						"resolved_status": null,
						"reopen_status": "reopen",
						"is_assigned_to_creator_on_close": true,
						"closed_assignment_team_id": null,
						"closed_visibility_team_id": null,
						"created_by_id": 1,
						"team_id": null,
						"closed_status_visible": false
					}
				],
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": [
					{
						"id": 11,
						"topic": "document verificaiton"
					}
				],
				"user": null,
				"round_robin": false,
				"FIFO": false
			},
			"status_type": "wip",
			"visibility_team": 7
		},
		{
			"status": {
				"id": 4,
				"status": "Policy Delivery",
				"assignment_team": null,
				"visibility_team": null,
				"ticket_name_mapped": null,
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": null,
				"user": null,
				"round_robin": false,
				"FIFO": false,
				"is_default": true
			},
			"status_type": "closed",
			"visibility_team": null
		},
		{
			"status": {
				"id": 5,
				"status": "Application declined",
				"assignment_team": null,
				"visibility_team": null,
				"ticket_name_mapped": null,
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": null,
				"user": null,
				"round_robin": false,
				"FIFO": false,
				"is_default": false
			},
			"status_type": "closed",
			"visibility_team": null
		},
		{
			"status": "ReOpen",
			"status_type": "reopen",
			"visibility_team": null
		}
	],
	"team": null,
	"closed_status_visible": false,
	"closed_statuses": [
		{
			"status": {
				"id": 4,
				"status": "Policy Delivery",
				"assignment_team": null,
				"visibility_team": null,
				"ticket_name_mapped": null,
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": null,
				"user": null,
				"round_robin": false,
				"FIFO": false,
				"is_default": true
			},
			"status_type": "closed",
			"visibility_team": null
		},
		{
			"status": {
				"id": 5,
				"status": "Application declined",
				"assignment_team": null,
				"visibility_team": null,
				"ticket_name_mapped": null,
				"main_status": null,
				"wip_status": null,
				"closed_status": null,
				"status_type": null,
				"disposition": null,
				"user": null,
				"round_robin": false,
				"FIFO": false,
				"is_default": false
			},
			"status_type": "closed",
			"visibility_team": null
		}
	]
}

export const activeAutoTicketNotificationsResponse = [
	{
		"id": 2,
		"team": 2,
		"status_type": "active",
		"send_to": [
			"Assignee"
		],
		"notification_type": "auto_ticket",
		"creator_template": "",
		"assignee_template": "ओ भाई! नू न समझ लियो की थारा कोई काम कोन्या, इब तन्ने ही संभालाना सै। {{ticket_id}}: {{ticket_description}}",
		"customer_template": ""
	},
	{
		"id": 3,
		"team": 2,
		"status_type": "active",
		"send_to": [
			"Creator"
		],
		"notification_type": "auto_ticket",
		"creator_template": "Hi {{assigned_by}}, we will discuss about {{ticket_id}}:{{ticket_description}} more so",
		"assignee_template": "",
		"customer_template": ""
	},
	{
		"id": 19,
		"team": 2,
		"status_type": "active",
		"send_to": [
			"Customer"
		],
		"notification_type": "auto_ticket",
		"creator_template": "",
		"assignee_template": "",
		"customer_template": "Hello Customer X, a ticket with the ticket ID {{ticket_id}}: {{ticket_description}} is currently being worked upon by {{sender_name}} monitored over by {{assigned_by}}. You may refer to {{assigned_by}} for being notified around the progress"
	}
]

export const activeManualTicketNotificationsResponse = [
	{
		"id": 4,
		"team": 2,
		"status_type": "active",
		"send_to": [
			"Creator"
		],
		"notification_type": "manual_ticket",
		"creator_template": "Hi There! It's a manual notification being set by {{send_by}} to you",
		"assignee_template": "",
		"customer_template": ""
	},
	{
		"id": 5,
		"team": 2,
		"status_type": "active",
		"send_to": [
			"Customer"
		],
		"notification_type": "manual_ticket",
		"creator_template": "Hello Ji! Your ticket has been assigned by  {{assigned_by}} with ticket description as {{ticket_description}}\nHum ne isko badal diya hai",
		"assignee_template": "",
		"customer_template": ""
	},
	{
		"id": 21,
		"team": 2,
		"status_type": "active",
		"send_to": [
			"Assignee"
		],
		"notification_type": "manual_ticket",
		"creator_template": "",
		"assignee_template": "भाई नू auto-generated कोन्या। it is manually set alert. beware... ticket id is {{ticket_id}}",
		"customer_template": ""
	}
]

export const ticketRulesResponse = {
	"ticket_auto_allocation_enabled": false,
	"ticket_resolution_sla_emails": [
		"suyash.inprofessionaluse@gmail.com"
	],
	"sla_rules": {
		"low": 0,
		"medium": 0,
		"high": 120000,
		"urgent": 0
	},
	"ticket_email_notifications_enabled": false
}

export const schedulerListResponse = [
	{
		"id": 1,
		"title": "routine check",
		"description": "the circular #441 has to be aligned with the procedure anytime before closing upon the same",
		"contact": [
			{
				"id": 2,
				"name": "Suyash Bhardwaj",
				"email": "suyash.inpersonaluse@gmail.com",
				"account_id": null
			}
		],
		"date_from": "2024-08-10T18:30:00Z",
		"date_to": "2024-08-20T18:30:00Z",
		"schedule_timer": "daily",
		"team": [
			{
				"id": 2,
				"name": "Insurance team"
			}
		],
		"disposition": [
			{
				"id": 3,
				"topic": "meInterested"
			}
		],
		"status_type": "active",
		"status": null,
		"ticket_channel": "Phone",
		"disposition_description": "only interesteed candidate's right to be protected with the circular #441",
		"daily_time": "12:12:00",
		"day_of_week": null,
		"day_of_month": null,
		"status_display": "New"
	}
]