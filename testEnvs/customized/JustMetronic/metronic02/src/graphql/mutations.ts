import { gql } from '@apollo/client'

export const SIGNIN = gql`
mutation SignInmutation($emailid: String!, $password: String!) {
  signInmutation(emailid: $emailid, password: $password) {
    token
    status
    message
    data {
      emailid
      name
      password
      profileData {
        id
        status
        contact
        designation
        department
        emailVerified
        onboardingDone
        userRole {
          roleId
          userId
        }
        franchiseId
      }
    }
  }
}
`;

export const SIGNUP = gql`
  mutation signUp($name: String!, $emailid: String!, $password: String!) {
    signUp(name: $name, emailid: $emailid, password: $password) {
      message
      status
      token
      data {
        name
        emailid
        password
        profileData {
          id
          status
          contact
          designation
          department
          emailVerified
          onboardingDone
          userRole {
            roleId
            userId
          }
          franchiseId
        }
      }
    }
  }
`

export const SIGNMEUP = gql`
  mutation CreateMyUser($name: String!, $email: String!, $password: String!) {
    createMyUser(name: $name, email: $email, password: $password)
  }
`

export const GETTOKEN = gql`
  query Query($email: String!, $password: String!) {
    getMyUserToken(email: $email, password: $password)
  }
`

export const VERIFY_EMAIL = gql`
  mutation VerifyEmail($emailid: String!, $otp: String!) {
    verifyEmail(emailid: $emailid, otp: $otp) {
      status
      message
      data
    }
  }
`

export const VERIFYEMAILBYOTP = gql`
  mutation VerifyEmail($theotp: String!) {
    verifyMail(theotp: $theotp)
  }
`

export const ONBOARDUSER = gql`
  mutation OnboardUser($status: String!, $contact: String!, $designation: String!, $department: String!) {
    onboardUser(status: $status, contact: $contact, designation: $designation, department: $department) {
      status
      message
      data {
        name
        emailid
        password
        mailOTP
        id
        status
        contact
        designation
        department
        emailVerified
        onboardingDone
        roleId
        userId
        franchiseId
      }
    }
  }
`