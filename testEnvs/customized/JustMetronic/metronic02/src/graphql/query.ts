import { gql } from '@apollo/client'

export const SIGNINQUERY = gql`
query SignInQuery($emailid: String!, $password: String!) {
  signInQuery(emailid: $emailid, password: $password) {
    message
    status
    token
    data {
      name
      emailid
      password
      profileData {
        id
        status
        contact
        designation
        department
        emailVerified
        onboardingDone
        userRole {
          roleId
          userId
        }
        franchiseId
      }
    }
  }
}
`;

export const FINDUSERBYID = gql`
  query FindUserById($id: String!) {
  findUserById(id: $id) {
    name
    emailid
    password
    profileData {
      id
      status
      contact
      designation
      department
      emailVerified
      onboardingDone
      userRole {
        roleId
        userId
      }
      franchiseId
    }
  }
}
`

export const GETMYCURRENTLOGGEDINUSER = gql`
  query Query {
  getMyCurrentLoggedInUser {
    name
    emailid
    password
    mailOTP
    id
    status
    contact
    designation
    department
    emailVerified
    onboardingDone
    roleId
    userId
    franchiseId
  }
}
`

export const GET_USER_BY_ID = gql`
query GetUserbyId {
  getUserById {
    id
    name
    email
    status
    contact
    designation
    department
    emailVerified
    onboardingDone
    userRole {
      roleId
      userId
    }
    franchiseId
  }
}
`

