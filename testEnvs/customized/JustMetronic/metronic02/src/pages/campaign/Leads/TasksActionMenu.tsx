import React, {useState} from 'react'
import { Dropdown } from '../../../helpers/Dropdown'
import Modal from 'react-modal'
import { Dropdown } from '../../../helpers/Dropdown'

Modal.setAppElement('#root')

const customModalStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-30%, -60%)'
  }
}

interface Props {
    taskResActionMenuRef: React.MutableRefObject<any[]>
    user: any
    index: number
    dditems:string[]
  }

  
const TasksActionMenu: React.FC<Props> = ({
    taskResActionMenuRef,
    user,
    index,
    dditems,
  }) => {
    const [parseResActionMenuDroppedDown, setParseResActionMenuDroppedDown] = useState<boolean>(false)
    const [isEditModalOpen, setIsEditModalOpen] = useState<boolean>(false)
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState<boolean>(false)
    const [isInformationOneModalOpen, setIsInformationOneModalOpen] = useState<boolean>(false)
    const [isInformationTwoModalOpen, setIsInformationTwoModalOpen] = useState<boolean>(false)

    const openEditModal = () => {
        setIsEditModalOpen(true)
        setParseResActionMenuDroppedDown(false)
    }

    const afterOpenEditModal = () => {
        // subtitle.style.color = '#f00'
    }

    const closeEditModal = () => {
        setIsEditModalOpen(false)
    }

    const openDeleteModal = () => {
        setIsDeleteModalOpen(true)
        setParseResActionMenuDroppedDown(false)
    }

    const afterOpenDeleteModal = () => {
        // subtitle.style.color = '#f00'
    }

    const closeDeleteModal = () => {
        setIsDeleteModalOpen(false)
    }

    const openInformationOneModal = () => {
        setIsInformationOneModalOpen(true)
        setParseResActionMenuDroppedDown(false)
    }

    const afterOpenInformationOneModal = () => {
        // subtitle.style.color = '#f00'
    }

    const closeInformationOneModal = () => {
        setIsInformationOneModalOpen(false)
    }

    const openInformationTwoModal = () => {
        setIsInformationTwoModalOpen(true)
        setParseResActionMenuDroppedDown(false)
    }

    const afterOpenInformationTwoModal = () => {
        // subtitle.style.color = '#f00'
    }

    const closeInformationTwoModal = () => {
        setIsInformationTwoModalOpen(false)
    }

  return (
    <>
      <button
        className={`btn btn-sm btn-icon btn-active-light-primary`}
        onClick={() => {
          setParseResActionMenuDroppedDown(!parseResActionMenuDroppedDown)
        }}
      >
        <i className='bi bi-three-dots-vertical fs-2'></i>
      </button>
      <Dropdown
        menuRef={taskResActionMenuRef.current[index]}
        droppedDown={parseResActionMenuDroppedDown}
        setDroppedDown={setParseResActionMenuDroppedDown}
        width = {150}
        height = {80}
      >
        <>
        {dditems.map((dditem:string)=>(
            <>
              <p className='link-primary hoverable' role='button' onClick={openInformationOneModal}>
                {/* <i className='bi bi-info-circle-fill'></i> */}
                <span className='px-2'>{dditem}</span>
              </p>
              <Modal
                isOpen={isInformationOneModalOpen}
                onAfterOpen={afterOpenInformationOneModal}
                onRequestClose={closeInformationOneModal}
                style={customModalStyles}
                contentLabel={dditem}
              >
                {/* <ResendInviteModal
                      user={user}
                      setIsLoading={setIsLoading}
                      closeRsendInviteModal={closeInformationOneModal}
                    /> */}
                <h4>{dditem}</h4>
              </Modal>
            </>
          ))}
        </>
      </Dropdown>
    </>

  )
}

export default TasksActionMenu
