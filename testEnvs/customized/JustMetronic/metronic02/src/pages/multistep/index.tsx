import Wizard from './Wizard'

const NewClientPage = () => {
    const clearLstr = () => {
        localStorage.removeItem(AUTH_TOKEN); 
        localStorage.removeItem(MYDASH_USER); 
        document.location.reload()
    }
    return (
        <div className = 'container'>
            {/*start:: The 3rd stack */}
            <div className="d-flex justify-content-between container-fluid mt-4">
                <div className="row w-100">
                    {/* start:: The main central stack */}
                    <div className="col-lg-12">
                        <div className="d-flex flex-column">
                            <div className="card mt-1 min-h-400px">
                                <Wizard/>
                            </div>
                            <div className='d-flex'>
                            <div><button className = 'btn btn-warning' onClick={clearLstr}>Clear LS</button></div>
                            <div><button className = 'mx-2 btn btn-secondary'>Save Defaults</button></div>
                            </div>
                        </div>
                    </div>
                    {/* end:: The main central stack */}
                </div>
            </div>
        </div>
    )
}

export default NewClientPage