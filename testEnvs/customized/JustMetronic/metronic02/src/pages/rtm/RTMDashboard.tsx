import { useState } from "react";
import PieChartRTM from "./charts/PieChartRTM"

const RTMDashboard = () => {
  const statusCards = [
    {iconClass: "fa-phone", label:"Logged In", arrowClass:"bi-arrow-down", count:4},
    {iconClass: "fa-phone-volume", label:"On Call", arrowClass:"bi-arrow-down", count:0},
    {iconClass: "fa-phone", label:"Ready", arrowClass:"bi-arrow-down", count:1},
    {iconClass: "fa-phone-slash", label:"Paused", arrowClass:"", count:6},
    {iconClass: "", label:"ACW", arrowClass:"", count:7},
    {iconClass: "fa-blender-phone", label:"Call Waiting", arrowClass:"", count:11}
  ]
  
  const [isHover, setIsHover] = useState({hovered: false, cardIndex:-1});
	const [isPressed, setIsPressed] = useState({pressState: false, cardIndex:-1});
  
	const handleMouseEnter = (index: number) => {
      setIsHover({hovered: true, cardIndex:index});
   	};
   	
   	const handleMouseLeave = (index: number) => {
      setIsHover({hovered: false, cardIndex:index});
   	};

   	const handleMouseClick = (index: number) => {
      setIsPressed({...isPressed, pressState: !isPressed.pressState, cardIndex:index});
   	};
    
  const boxStyle = (index: number) => {
    return {
      backgroundColor: (isPressed.cardIndex===index && isPressed.pressState) ? '#3C94FD' : (isHover.cardIndex === index && isHover.hovered ? '#e9edf1' : 'white'),
      color: (isPressed.cardIndex===index && isPressed.pressState) ? 'white' : (isHover.cardIndex === index && isHover.hovered ? '#303030' : '#959595'),
      /* color: isHover.cardIndex === index && isHover.hovered ? '#303030': '#A2A2A2', */
      cursor: 'pointer',
      transition: "background-color 0.5s"
      }
  };

  return (
    <div id="kt_app_content_container" className="app-container mx-5">
      {/*begin::Dashboard*/}
      <div className="card card-flush">
        {/*begin::Card header*/}
        <div className="card-header align-items-center py-1 my-1 gap-2 gap-md-5">
          {/*begin::Card title*/}
          <div className="card-title">
            {/*begin::Search*/}
            <div className="d-flex align-items-center position-relative my-1">
              <i className="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                <span className="path1"></span>
                <span className="path2"></span>
              </i>
              <label>RTM Dashboard</label>
            </div>
            {/*end::Search*/}
          </div>
          {/*end::Card title*/}

          {/*begin::Card toolbar*/}
          <div className="card-toolbar flex-row-fluid justify-content-end gap-5">
            <div className="w-100 mw-150px">
              {/*begin::Select2*/}
              <select
                className="form-select select2-hidden-accessible"
                data-control="select2"
                data-hide-search="true"
                data-placeholder="Status"
                data-kt-ecommerce-product-filter="status"
                data-select2-id="select2-data-9-cpv9"
                tabIndex={-1}
                aria-hidden="true"
                data-kt-initialized="1"
              >
                <option data-select2-id="select2-data-11-qb6b"></option>
                <option value="select queue" selected>
                    Select Queue
                </option>
                <option value="queue 01">Queue 01</option>
                <option value="queue 02">Queue 02</option>
                <option value="queue 03">Queue 03</option>
              </select>
              {/*end::Select2*/}
            </div>
            <div className="w-100 mw-150px">
              {/*begin::Select2*/}
              <select
                className="form-select select2-hidden-accessible"
                data-control="select2"
                data-hide-search="true"
                data-placeholder="Status"
                data-kt-ecommerce-product-filter="status"
                data-select2-id="select2-data-9-cpv9"
                tabIndex={-1}
                aria-hidden="true"
                data-kt-initialized="1"
              >
                <option data-select2-id="select2-data-11-qb6b"></option>
                        <option value="team" selected>
                        Select Team
                        </option>
                        <option value="team 01">Team 01</option>
                        <option value="team 02">Team 02</option>
                        <option value="team 03">Team 03</option>
              </select>
              {/*end::Select2*/}
            </div>
            <div className="w-100 mw-150px">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Individual"
              />
            </div>
          </div>
          {/*end::Card toolbar*/}
        </div>
        {/*end::Card header*/}
        {/*begin::Card body*/}
        <div className="card-body pt-0">
          <section className="section dashboard">

            <div className="row">
              {/*<!-- Start RTM cardset -->*/}
              <div className="col-lg-10 d-flex flex-column justify-content-center">
                <div className="row row-cols-sm-3">
                        {statusCards.map((sc,index)=>(
                          <div className="col">
                            <div 
                              className="card d-flex align-items-center mt-2" 
                              style={boxStyle(index)}
                              onMouseEnter={()=>handleMouseEnter(index)}
                              onMouseLeave={()=>handleMouseLeave(index)}
                              onClick={()=>handleMouseClick(index)}>
                              <div className="card-body py-4 d-flex">
                                  <i className={`fa fa-solid ${sc.iconClass} fa-2x mx-6 pt-2`} style={{transform:'rotate(360deg)'}}></i>
                                  <div className="d-flex flex-column align-items-center">
                                      <span className="fw-bold fs-2 d-block">{sc.label}</span> 
                                      <span className="fw-semibold fs-1">{sc.count}</span>
                                  </div>
                                  <i className={`bi ${sc.arrowClass} fa-2x`}></i> 
                              </div>
                          </div>
                        </div>
                        ))}
                </div>
              </div>
              {/*<!-- End RTM cardset -->*/}
              
              {/*<!-- Start distribution chart-->*/}
              <div className="col-lg-2">
                <div className="row">
                  <div className="col-xxl-12 col-md-12 pb-2">
                    <div className="card info-card sales-card py-3">
                      <PieChartRTM />
                    </div>
                  </div>
                </div>
              </div>
              {/*<!-- End distribution chart-->*/}
            </div>
            <div className="h6">Real Time Agent Status</div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="card mt-2">
                        <div className="card-body">
                        <div className="table-responsive text-center">
                        <table className="table table-row-bordered table-row-gray-300  align-middle gs-10">
                            <thead className="table-light fw-bolder">
                            <tr className="bg-primary">
                                <th scope="col">Queue Name</th>
                                <th scope="col">User</th>
                                <th scope="col">Status</th>
                                <th scope="col">Pause Code</th>
                                <th scope="col">Extension</th>
                                <th scope="col">Call Duration</th>
                                <th scope="col">Force Logout</th>
                            </tr>
                            </thead>
                            <tbody className="table-group-divider">
                            <tr>
                                <th scope="row">English</th>
                                <td>Govind</td>
                                <td>ACW</td>
                                <td></td>
                                <td>7026</td>
                                <td></td>
                                <td>
                                    <div><button className="btn btn-outline-light bg-primary">Logout</button></div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">English</th>
                                <td>Dheeraj</td>
                                <td>Paused</td>
                                <td>ACW</td>
                                <td>7026</td>
                                <td></td>
                                <td>
                                    <div><button className="btn btn-outline-light bg-primary">Logout</button></div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">English</th>
                                <td>Megha</td>
                                <td>ACW</td>
                                <td></td>
                                <td>7026</td>
                                <td></td>
                                <td>
                                    <div><button className="btn btn-outline-light bg-primary">Logout</button></div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Support</th>
                                <td>IAX2/5002</td>
                                <td></td>
                                <td></td>
                                <td>7026</td>
                                <td></td>
                                <td>
                                    <div><button className="btn btn-outline-light bg-primary">Logout</button></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row mt-2">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-body">
                        <div className="table-responsive">
                        <table className="table table-row-bordered table-row-gray-300  align-middle gs-10">
                            <thead className="table-light fw-bolder text-center">
                            <tr className="bg-primary">
                                <th scope="col">Offerred</th>
                                <th scope="col">Answered</th>
                                <th scope="col">Abandoned</th>
                                <th scope="col">Service Level %</th>
                                <th scope="col">Abandoned %</th>
                            </tr>
                            </thead>
                            <tbody className="table-group-divider text-center">
                            <tr>
                                <th scope="row">32</th>
                                <td>21</td>
                                <td>11</td>
                                <td>150</td>
                                <td>3438</td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
          </section>
        </div>
        {/*end::Card body*/}
      </div>
      {/*end::Dashboard*/}
    </div>
    )
}

export default RTMDashboard
