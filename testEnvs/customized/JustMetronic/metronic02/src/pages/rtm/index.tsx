import RTMDashboard from './RTMDashboard'
import { Navigate, Route, Routes, Outlet } from 'react-router-dom'

const CampaignRoutes = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route path='dashboard' element={<RTMDashboard/>} />
        <Route index element={<Navigate to='/rtm/dashboard' />} />
      </Route>
    </Routes>
  )
}

export default CampaignRoutes