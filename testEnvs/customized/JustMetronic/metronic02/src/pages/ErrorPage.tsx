import { useState } from "react"

const ErrorPage = () => {
	const textArea = document.querySelector('textarea')
	const textRowCount = textArea ? textArea.value.split("\n").length : 0
	const rows = textRowCount + 1
	const [keyspressed, setKeyspressed] = useState(0);
	return (
		<div className = 'container'>
			<textarea
			  style={{minHeight:'2rem', height:'unset'}}
		      rows={rows}
		      placeholder="Enter text here."
		      onKeyPress={()=> setKeyspressed(keyspressed+1)}
		      />
		</div>
	)
}

export default ErrorPage