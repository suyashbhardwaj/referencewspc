import { Link, useLocation } from 'react-router-dom'

let menuItem = [
  {
    icon: '/media/icons/duotune/general/gen001.svg',
    title: 'Users',
    to: 'users'
  },
  {
    icon: '/media/icons/duotune/communication/com014.svg',
    title: 'Customer',
    to: 'customer'
  },
  {
    icon: '/media/icons/duotune/general/gen025.svg',
    title: 'General',
    to: 'general'
  },
  {
    icon: '/media/icons/duotune/communication/com014.svg',
    title: 'Email',
    to: 'email'
  },
  {
    icon: '/media/icons/duotune/communication/com006.svg',
    title: 'Ticket',
    to: 'ticket'
  },
  {
    icon: '/media/icons/duotune/communication/com007.svg',
    title: 'Campaign',
    to: 'campaign'
  }
]

const ListMenu = () => {
  const location = useLocation()

  return (
    <>
      <div className='card-body px-4'>
        <div className='menu-item'>
          <div className='menu-content pb-3'>
            <span className='menu-section fw-bolder text-uppercase fs-3 ls-1'>
              Settings Configuration
            </span>
          </div>
        </div>
        {menuItem.map((value, index) => (
          <div className='menu-item' key={index}>
            <div
              className={`menu-link without-sub ${
                location.pathname.includes(value.to) ? 'text-primary' : ''
              }`}
            >
              <Link to={value.to} className='d-flex'>
                <span className='menu-icon'>
                    <i className="fa fa-solid fa-users"></i>
                </span>
                <span
                  className={`menu-title fs-4 fw-bold  text-hover-primary ${
                    location.pathname.includes(value.to) ? 'text-primary' : 'text-muted'
                  }`}
                >
                  {value.title}
                </span>
              </Link>
            </div>
          </div>
        ))}
      </div>
    </>
  )
}

export default ListMenu