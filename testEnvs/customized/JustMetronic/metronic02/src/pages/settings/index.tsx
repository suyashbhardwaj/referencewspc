import { Navigate, Route, Routes } from 'react-router-dom'
import UserSettings from './UserSettings'
import ListMenu from './ListMenu'
import CustomerSettings from './CustomerSettings'
import EmailSettings from "./EmailSettings";
import TicketSettings from './TicketSettings'
import GeneralSettings from './GeneralSettings'

const SettingRoutes = () => {
  return(
    <section className='row mx-lg-10'>
        <div className='card px-2 col-lg-2 h-lg-800px  border border-secondary rounded '>
          <ListMenu />
        </div>
        <div className='card col ms-xl-6 d-flex flex-column h-lg-800px scroll-y '>
          <Routes>
            <Route path='users' element={<UserSettings />} />
            <Route path='customer' element={<CustomerSettings />} />
            <Route path="email" element={<EmailSettings />} />
            <Route path='ticket' element={<TicketSettings />} />
            <Route path='general' element={<GeneralSettings />} />
            <Route index element={<Navigate to='users' />} />
          </Routes>
        </div>
    </section>
  )
}

export default SettingRoutes