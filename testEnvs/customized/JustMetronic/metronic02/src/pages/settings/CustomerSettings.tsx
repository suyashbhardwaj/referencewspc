import React, { useState } from 'react'
import ReactModal from 'react-modal';

interface AddTagModalProps {
    closeModal: any
}

const AddTagModal: React.FC<AddTagModalProps> = ({closeModal}) => {
    const [tagToBeAdded, setTagToBeAdded] = useState('');
    const addTagToTheList = () => {console.log("Tag being added is ", tagToBeAdded); closeModal();};
    return(<>
        <form action=''>
            <div className='d-flex align-items-center'>
            <label className='form-label me-4' htmlFor=''>
                Tag
            </label>
            <input
                onChange={(evt)=>setTagToBeAdded(evt.target.value)}
                className='form-control me-4'
                type='text'
                placeholder={`Enter text`}
                value={tagToBeAdded}
            />
            <button type='submit' className='btn btn-primary' onClick={addTagToTheList}>Save</button>
            </div>
        </form>
</>)
}

const NestedFieldsConfigurationModal: React.FC<AddTagModalProps> = ({closeModal}) => {
    return(<>
        <div className='d-flex align-items-center'>
            <button className='btn btn-link'><i className="mx-2 fs-2 bi bi-plus-lg"></i></button>
            <input className = "form-control" type="text" placeholder='Enter the list item to be added'/>
            <button className='btn btn-link'><i className="mx-2 fs-2x bi bi-node-plus-fill"></i></button>
        </div>
        <button className = "btn btn-secondary mt-2 float-lg-end" onClick={closeModal}>Save Nesting</button>
    </>)
}

const initialAccountFields = [
    {label:"Business Name", type:"Text", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Address", type:"Text area", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"State", type:"Text", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Country", type:"Text", subtype:"", isMandatory:true, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Place", type:"Number", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:['north','east','west','south']}},
    {label:"Phone", type:"Number", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Address 2", type:"Text", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Unit name (select dropdown)", type:"Number", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Pincode", type:"Number", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}}
    ]
const initialContactFields = [
    {label:"Name", type:"Text", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Email", type:"Email", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Address", type:"Text area", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"State", type:"Text", subtype:"", isMandatory:true, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Country", type:"Text", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Education", type:"Text", subtype:"Text", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"phone no.", type:"Number", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}},
    {label:"Register", type:"Dropdown", subtype:"", isMandatory:false, isUnique:false, moreSpecifications:{dropdownoptions:[]}}
    ];
const initialRegistrations = [
    {account:"Bluewhirl technologies private limited", username:"Prajakta"},
    {account:"RS technologies private limited", username:"Raj"},
    ]

const CustomerSettings = () => {
    const customModalStyles: ReactModal.Styles = {
        content: {
          top: '50%',
          left: '50%',
          right: 'auto',
          bottom: 'auto',
          marginRight: '-50%',
          transform: 'translate(-40%, -50%)'
        }
      }
    const openModal = () => { setIsModalOpen(true) }
    const openNestingDDModal = () => { setIsNestingDDModalOpen(true) }
    const closeModal = () => { setIsModalOpen(false) }
    const closeNestingDDModal = () => { setIsNestingDDModalOpen(false) }
    const fieldTypes = ["Text", "Email", "Number", "Telephone", "Text area", "Checkbox", "Nested", "Date-Time", "Date", "Time", "Dropdown"];
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [isNestingDDModalOpen, setIsNestingDDModalOpen] = useState(false)
    const [openedPane, setOpenedPane] = useState('');
    const [, setCurrentPage] = useState('fieldlistings');
    const [accountFields, setAccountFields] = useState(initialAccountFields);
    const [contactFields, setContactFields] = useState(initialContactFields);
    const [registrations, ] = useState(initialRegistrations);
    const [expandForMoreSpecifications, setExpandForMoreSpecifications] = useState({expansionOn:-1, expanded: false});   
    const [showAllcustomerTags, setShowAllcustomerTags] = useState<boolean>(false);
    const customerTags = [
        "Platinum", 
        "Frequent buyer", 
        "Gold", 
        "Silver", 
        "Interested", 
        "Passive", 
        "Active", 
        "Trainable", 
        "Unaware",
        "Classy",
        "Influencer",
        "Networked",
        "Negative",
        "Positive"
      ]; 
    return (
        <div>
            <div className="text-center">
                <div className="card-header">
                    <ul className="nav card-header-pills d-flex align-items-center">
                    <li className="nav-item">
                        <a className="nav-link active" href="#" onClick={()=>setOpenedPane('accountFields')}>Account fields</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#" onClick={()=>setOpenedPane('ContactFields')}>Contact fields</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#" onClick={()=>setOpenedPane('Tag')}>Tag</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#" onClick={()=>setOpenedPane('ClientLogin')}>Client login</a>
                    </li>
                    </ul>
                </div>
                <div className="card-body">
                    {(() => {
                    switch (openedPane) {
                        case 'accountFields': return (<>
                                <div className="row">
                                    <div className="col">
                                    <span className="input-group-text mt-2" id="basic-addon1">Customer account fields</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                    <span className="input-group-text mt-2 bg-white border-0" id="basic-addon1">
                                        <div className="ms-auto">
                                        <button 
                                        className="input-group-text form-control form-control align-items-center text-hover-primary fw-bold min-w-150px"
                                        onClick = {()=>setCurrentPage('addteam')}>
                                            <i className="bi bi-plus-lg me-2"></i>
                                            <span className="ms-auto">Option</span>
                                        </button>
                                        </div>
                                    </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                    {/* The Table Header */}
                                    <div className="card my-1" style={{ backgroundColor: '#E4E4E47F' }}>
                                        <div className="row align-items-center">
                                        <div className="col-4 text-start ps-10"><label htmlFor="">Label</label></div>
                                        <div className="col-3 text-start"><button className="btn btn-link text-dark">Type<i className="bi bi-chevron-down fs-5 mx-2"></i></button></div>
                                        <div className="col-3 text-start"><button className="btn btn-link text-dark">Sub type<i className="bi bi-chevron-down fs-5 mx-2"></i></button></div>
                                        <div className="col text-start"><label htmlFor="">Mandatory</label></div>
                                        <div className="col text-start"><label htmlFor="">Unique</label></div>
                                        <div className="col text-start"></div>
                                        <div className="col text-start"></div>
                                        </div>
                                    </div>
                                    {/* The Table Data */}
                                    {accountFields.map((field,index)=>(<>
                                        <div className="card my-1" key={index}>
                                            <div className="row align-items-center">
                                                <div className="col-4 text-start ps-10">
                                                    {
                                                        field.type === 'Dropdown'
                                                        ? (<div className="d-flex align-items-center flex-nowrap">
                                                            <label htmlFor=""><div>{field.label}</div></label>
                                                            <div className={`d-flex mx-2 ${expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded?'':'d-none'}`}>
                                                            <input type="text" placeholder="Add a new item to the list." className="form-control m-1"/><button className="btn btn-sm btn-primary m-1">Add</button></div>
                                                        </div>)
                                                        : (<label htmlFor="">{field.label}</label>)
                                                    }
                                                </div>
                                                <div className="col-3 text-end dropdown">
                                                    <div className="w-100 mw-150px">
                                                    <select
                                                        className="form-select select2-hidden-accessible border-0"
                                                        data-placeholder="Status"
                                                        value={field.type}
                                                        onChange={(e)=>{/*if(e.target.value==='Dropdown') console.log('dropdown selecetd')}*/
                                                        setAccountFields(prev=>{
                                                            const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,type:e.target.value}:thefield)
                                                            return updated;
                                                        })
                                                    }}
                                                    >
                                                        {fieldTypes.map((option)=>(<> <option value={option}>{option}</option> </>))}
                                                    </select>
                                                </div>
                                                
                                                </div>
                                                <div className="col-3 text-start">blank</div>
                                                <div className="col text-end d-flex align-items-center justify-content-end">
                                                    {field.type === 'Dropdown' ? (<button className="btn btn-link" onClick={()=>setExpandForMoreSpecifications({expansionOn: index, expanded:!expandForMoreSpecifications.expanded})}><i className="fs-2 bi bi-plus-lg me-2"></i></button>):<div></div>}
                                                    <input type="checkbox"
                                                        style={{outline:'1px solid red', border:"none", backgroundPosition:'center'}} 
                                                        name = {`${field.label}-mandatory-${index}`} 
                                                        defaultChecked={field.isMandatory} 
                                                        className="form-check-input"
                                                        onChange={()=>{
                                                            setAccountFields(prev=>{
                                                                const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isMandatory:!thefield.isMandatory}:thefield)
                                                                return updated;
                                                            })
                                                        }}/>
                                                </div>
                                                <div className="col text-start">
                                                    <input type="checkbox" 
                                                        style={{outline:'1px solid yellow', border:"none", backgroundPosition:'center'}}
                                                        name = {`${field.label}-unique-${index}`} 
                                                        defaultChecked={field.isUnique} 
                                                        className="form-check-input"
                                                        onChange={()=>{
                                                            setAccountFields(prev=>{
                                                                const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isUnique:!thefield.isUnique}:thefield)
                                                                return updated;
                                                            })
                                                        }}
                                                    />
                                                </div>  
                                                <div className="col text-start"><i className="ms-auto bi bi-pencil-fill fs-2"></i><i className="bi bi-trash-fill fs-2 mx-4"></i></div>
                                            </div>
                                        </div>
                                    {/* The dynamic dropdown that expands to set up options to be listed in dropdown */} 
                                    {expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded ? (<>
                                        <div className="card my-1 bg-light-primary" key={`${index}-moreSpec`}>
                                            {field.moreSpecifications.dropdownoptions.map(option=>(<>
                                            <div className="card-header my-0 min-h-40px d-flex align-items-center justify-content-lg-start" key={index}>
                                                <div className="d-flex">
                                                    <button className="btn btn-link"><i className="ms-auto bi bi-pencil-fill fs-2"></i></button>
                                                    <button className="btn btn-link"><i className="fs-2 bi bi-x"></i></button>
                                                    <button className="btn btn-link" onClick={openNestingDDModal}><i className="fs-2 bi bi-airplane-fill"></i></button>
                                                </div>
                                                <div className="ps-4">
                                                    <label htmlFor="">{option}
                                                    </label>
                                                </div>
                                            </div>
                                            </>))}
                                        <ReactModal
                                            isOpen={isNestingDDModalOpen}
                                            onRequestClose={closeNestingDDModal}
                                            style={customModalStyles}
                                            contentLabel='Add a new submenu'
                                        >
                                            <NestedFieldsConfigurationModal
                                                closeModal={closeNestingDDModal}
                                            />
                                        </ReactModal>
                                        </div>                    
                                        </>):(<></>)}
                                    </>))}
                                    </div>      
                                </div>
                                </>);
                        case 'ContactFields': return (<>
                                <div className="row">
                                    <div className="col">
                                    <span className="input-group-text mt-2" id="basic-addon1">Customer contact fields</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                    <span className="input-group-text mt-2 bg-white border-0" id="basic-addon1">
                                        <div className="ms-auto">
                                        <button 
                                        className="input-group-text form-control form-control align-items-center text-hover-primary fw-bold min-w-150px"
                                        onClick = {()=>setCurrentPage('addContactField')}>
                                            <i className="bi bi-plus-lg me-2"></i>
                                            <span className="ms-auto">Option</span>
                                        </button>
                                        </div>
                                    </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                    <div className="card my-1" style={{ backgroundColor: '#E4E4E47F' }}>
                                        <div className="row align-items-center">
                                        <div className="col-4 text-start ps-10"><label htmlFor="">Label</label></div>
                                        <div className="col-3 text-start"><button className="btn btn-link text-dark">Type<i className="bi bi-chevron-down fs-5 mx-2"></i></button></div>
                                        <div className="col-3 text-start"><button className="btn btn-link text-dark">Sub type<i className="bi bi-chevron-down fs-5 mx-2"></i></button></div>
                                        <div className="col text-start"><label htmlFor="">Mandatory</label></div>
                                        <div className="col text-start"><label htmlFor="">Unique</label></div>
                                        <div className="col text-start"></div>
                                        <div className="col text-start"></div>
                                        </div>
                                    </div>
                                    {contactFields.map((field,index)=>(<>
                                        <div className="card my-1" key={index}>
                                        <div className="row align-items-center">
                                        <div className="col-4 text-start ps-10">
                                            
                                            {
                                                field.type === 'Dropdown'
                                                ? (<div className="d-flex align-items-center flex-nowrap">
                                                    <label htmlFor=""><div>{field.label}</div></label>
                                                    <div className={`d-flex mx-2 ${expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded?'':'d-none'}`}>
                                                    <input type="text" placeholder="Add a new item to the list." className="form-control m-1"/><button className="btn btn-sm btn-primary m-1">Add</button></div>
                                                </div>)
                                                : (<label htmlFor="">{field.label}</label>)
                                            }
                                            
                                        </div>
                                        <div className="col-3 text-end dropdown">
                                            {/* <button className="btn btn-link text-dark dropdown-toggle">{field.type}</button> */}
                                            <div className="w-100 mw-150px">
                                            {/*begin::Select2*/}
                                            <select
                                            className="form-select select2-hidden-accessible border-0"
                                            data-placeholder="Status"
                                            value={field.type}
                                            onChange={(e)=>{/*if(e.target.value==='Dropdown') console.log('dropdown selecetd')}*/
                                                setContactFields(prev=>{
                                                const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,type:e.target.value}:thefield)
                                                return updated;
                                                })
                                            }}
                                            >
                                            
                                            {["Text", "Email", "Number", "Telephone", "Text area", "Checkbox", "Nested", "Date-Time", "Date", "Time", "Dropdown"].map((option)=>(<>
                                                <option value={option}>{option}</option>
                                            </>))}
                                            </select>
                                            {/*end::Select2*/}
                                        </div>
                                        
                                        </div>
                                        <div className="col-3 text-start">blank</div>
                                        <div className="col text-end d-flex align-items-center justify-content-end">
                                            {field.type === 'Dropdown' ? (<button className="btn btn-link" onClick={()=>setExpandForMoreSpecifications({expansionOn: index, expanded:!expandForMoreSpecifications.expanded})}><i className="fs-2 bi bi-plus-lg me-2"></i></button>):<div></div>}
                                            <input type="checkbox"
                                            style={{outline:'1px solid red', border:"none", backgroundPosition:'center'}} 
                                            name = {`${field.label}-mandatory-${index}`} 
                                            defaultChecked={field.isMandatory} 
                                            className="form-check-input"
                                            onChange={()=>{
                                                setContactFields(prev=>{
                                                const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isMandatory:!thefield.isMandatory}:thefield)
                                                return updated;
                                                })
                                            }}/>
                                        </div>
                                        <div className="col text-start">
                                            <input type="checkbox" 
                                            style={{outline:'1px solid yellow', border:"none", backgroundPosition:'center'}}
                                            name = {`${field.label}-unique-${index}`} 
                                            defaultChecked={field.isUnique} 
                                            className="form-check-input"
                                            onChange={()=>{
                                                setContactFields(prev=>{
                                                const updated = prev.map(thefield=>thefield.label===field.label?{...thefield,isUnique:!thefield.isUnique}:thefield)
                                                return updated;
                                                })
                                            }}
                                            />
                                        </div>
                                        <div className="col text-start"><i className="ms-auto bi bi-pencil-fill fs-2"></i><i className="bi bi-trash-fill fs-2 mx-4"></i></div>
                                        </div>
                                    </div>
                                    {expandForMoreSpecifications.expansionOn === index && expandForMoreSpecifications.expanded ? (<>
                                        <div className="card my-1 bg-light-primary" key={`${index}-moreSpec`}>
                                            {field.moreSpecifications.dropdownoptions.map(option=>(<>
                                            <div className="card-header my-0 min-h-40px d-flex align-items-center justify-content-lg-start" key={index}>
                                                <div className="d-flex">
                                                    <button className="btn btn-link"><i className="ms-auto bi bi-pencil-fill fs-2"></i></button>
                                                    <button className="btn btn-link"><i className="fs-2 bi bi-x"></i></button>
                                                </div>
                                                <div className="ps-4">
                                                    <label htmlFor="">{option}
                                                    </label>
                                                </div>
                                            </div>
                                            </>))}  
                                        </div>                    
                                        </>):(<></>)}
                                    </>))}
                                    </div>      
                                </div>
                                </>);
                        case 'Tag': return (<>
                                <div className="row">
                                    <div className="col-10">
                                    <div className="input-group mb-3">
                                        <span className="input-group-text" id="basic-addon1"><i className="fs-2 bi bi-search"></i></span>
                                        <input type="text" className="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon1"/>
                                    </div>
                                    </div>
                                    <div className="col-2">
                                        <button 
                                        className="form-control form-control align-items-center text-hover-primary fw-bold min-w-150px"
                                        onClick = {openModal}>
                                            <i className="bi bi-plus-lg me-2"></i>
                                            <span className="ms-auto">Tag</span>
                                        </button>
                                    </div>
                                </div>
                                <div className="row">
                                <div className="col">
                                    <span className="input-group-text mt-2" id="basic-addon1">Customer tags</span>
                                </div>
                                </div>
                                <ReactModal
                                    isOpen={isModalOpen}
                                    onRequestClose={closeModal}
                                    style={customModalStyles}
                                    contentLabel='Add a new Tag'
                                >
                                    <AddTagModal
                                        closeModal={closeModal}
                                    />
                                </ReactModal>
                                {customerTags.map((team, index)=>index<6 ? (<>
                                    <div className="row">
                                        <div className="col-12">
                                        <span className="input-group-text bg-white mt-2" id="basic-addon1">{team}
                                            <i className="ms-auto bi bi-pencil-fill fs-2"></i>
                                            <i className="bi bi-trash-fill fs-2 mx-4"></i>
                                        </span>
                                        </div>
                                    </div>
                                    </>):(showAllcustomerTags && <>
                                    <div className="row">
                                        <div className="col-12">
                                        <span className="input-group-text bg-white mt-2" id="basic-addon1">{team}
                                            <i className="ms-auto bi bi-pencil-fill fs-2"></i>
                                            <i className="bi bi-trash-fill fs-2 mx-4"></i>
                                        </span>
                                        </div>
                                    </div>
                                    </>))}
                                    <div className="row">
                                        <div className="col-12">
                                        <span className="input-group-text mt-2 bg-white border-0" id="basic-addon1">
                                            <div className="ms-auto">
                                            <button className="btn btn-link" onClick={()=>setShowAllcustomerTags(!showAllcustomerTags)}>
                                                {!showAllcustomerTags ? (<><i className="fs-2 bi bi-plus-lg me-2"></i> {customerTags.length-6} more</>): <>collapse</>}
                                            </button>
                                            </div>
                                        </span>
                                        </div>
                                    </div>
                            </>);
                        case 'ClientLogin': return (<>
                                <div className="row">
                                <div className="col">
                                <span className="input-group-text mt-2" id="basic-addon1">Client Registration</span>
                                </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                    <span className="input-group-text mt-2 bg-white border-0" id="basic-addon1">
                                        <div className="ms-auto">
                                        <button 
                                        className="input-group-text form-control form-control align-items-center text-hover-primary fw-bold min-w-150px"
                                        onClick = {()=>setCurrentPage('addContactField')}>
                                            <i className="bi bi-plus-lg me-2"></i>
                                            <span className="ms-auto">Registration</span>
                                        </button>
                                        </div>
                                    </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                    <div className="card my-1" style={{ backgroundColor: '#E4E4E47F' }}>
                                        <div className="row align-items-center py-3">
                                        <div className="col-5 text-start ps-10"><label htmlFor="">Account</label></div>
                                        <div className="col-5 text-start ps-10"><label htmlFor="">Username</label></div>
                                        <div className="col text-start"></div>
                                        <div className="col text-start"></div>
                                        </div>
                                    </div>
                                    {registrations.map((field,index)=>(<>
                                        <div className="card my-1" key={index}>
                                        <div className="row align-items-center py-3">
                                            <div className="col-5 text-start ps-10"><label htmlFor="">{field.account}</label></div>
                                            <div className="col-5 text-start ps-10"><label htmlFor="">{field.username}</label></div>                                
                                            <div className="col text-end"><i className="ms-auto bi bi-pencil-fill fs-2"></i><i className="bi bi-trash-fill fs-2 mx-4"></i></div>
                                        </div>
                                    </div>
                                    </>))}
                                    </div>      
                                </div>    
                            </>);
                        default:
                            return <h4>select a tab to proceed</h4>;
                        }
                    })()} {/* This code block uses an immediately invoked function expression (IIFE) inside the curly braces to switch between different cases based on the value of openedPane and render the corresponding JSX elements accordingly. */}
                </div>
            </div>
        </div>
        )
    }

export default CustomerSettings