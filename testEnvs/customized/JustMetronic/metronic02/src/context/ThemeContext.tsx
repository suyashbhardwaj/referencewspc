import React, { createContext, useEffect, useState, PropsWithChildren, ReactNode } from 'react';
interface Props {
	children?: ReactNode;
  }
/*"भाई, एक context बना कर उसका reference दे दो और उसे export भी कर देना। क्यो? "*/
export const ThemeContext: React.Context<any> = createContext({ isDarkMode: false, toggleTheme:"" });

export const ThemeProvider: React.FC<PropsWithChildren<Props>> = ({children}) => {
	const [isDarkMode, setIsDarkMode] = useState(false);
	const toggleTheme = () => {setIsDarkMode(!isDarkMode)}

	useEffect(() => {
		/*if(isDarkMode) document.body.classList.remove("white-content");
		else document.body.classList.add("white-content");*/
		if(isDarkMode) document.documentElement.style.setProperty('--app-primaryColor', 'dark');
		else document.documentElement.style.setProperty('--app-primaryColor', 'default');
	}, [isDarkMode])

	return ( <ThemeContext.Provider value = {{isDarkMode, toggleTheme: toggleTheme}}> {children} </ThemeContext.Provider> );
}

export const ThemeConsumer = ThemeContext.Consumer;