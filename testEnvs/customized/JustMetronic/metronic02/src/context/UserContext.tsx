import React, { PropsWithChildren, ReactNode, createContext, useEffect, useRef } from 'react'
import { useQuery } from '@apollo/client/react';
import { FINDUSERBYID, GETMYCURRENTLOGGEDINUSER } from '../graphql/query'
import { USWCB } from '../utils/hooks/uswcb';

const UserContext = createContext<any>({})
interface IProps {
	children?: ReactNode
}

export const UserProvider: React.FC<PropsWithChildren<IProps>> = ({children}) => {
	let user: any = {}
	// const loggedInUserAccountID = localStorage.getItem('AccountId')?.replaceAll('"','');
	const [currentUser, setCurrentUser] = USWCB<any>({})
  	const [currentUserRole, setCurrentUserRole] = USWCB<{ [key: string]: boolean }>({
	    admin: false,
	    superAdmin: false,
	    agent: false,
	    operator: false,
	    salesManager: false,
	    salesAssociate: false,
	    franchiseAdmin: false
	  	})
  	const [, setIsLoading] = USWCB<boolean>(true)
	const { loading, error, data } = useQuery(GETMYCURRENTLOGGEDINUSER)

	const isUserFetchedRef = useRef<boolean>(false)
	
	useEffect(()=>{
		if (isUserFetchedRef.current === true) {
	      isUserFetchedRef.current = false
	      setCurrentUser(
	        (prevUser: any) => ({ ...user }),
	        (updatedUser: any) => {
	          // Logic to properly define currentUserRole based on roleId
	          if (updatedUser?.roleId === 1) {
	            setCurrentUserRole((prev: any) => ({...prev,admin: true}), (updatedUserRole: any) => { setIsLoading(false)})
	          } else if (updatedUser?.roleId === 2) {
	          	setCurrentUserRole((prev: any) => ({...prev,superAdmin: true}), (updatedUserRole: any) => { setIsLoading(false)})
	          } else if (updatedUser?.roleId === 3) {
	          	setCurrentUserRole((prev: any) => ({...prev,agent: true}), (updatedUserRole: any) => { setIsLoading(false)})
	          } else if (updatedUser?.roleId === 4) {
	          	setCurrentUserRole((prev: any) => ({...prev,operator: true}), (updatedUserRole: any) => { setIsLoading(false)})
	          } else {
	          	setCurrentUserRole((prev: any) => ({...prev,fanchiseAdmin: true}), (updatedUserRole: any) => { setIsLoading(false)})
	          }
	})}},[data]);

	if (!loading && !error && data) {
	    isUserFetchedRef.current = true
	    user = data.getMyCurrentLoggedInUser
	}

	return (
		<UserContext.Provider
			value = {{
				/*provide here the variables to be floated via context to the wrapped components*/
				currentUser,
				currentUserRole
			}}
		> {children}
		</UserContext.Provider>
	)
}

export default UserContext