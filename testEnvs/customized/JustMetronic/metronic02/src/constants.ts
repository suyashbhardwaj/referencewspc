export const AUTH_TOKEN = 'auth-token'
export const MYDASH_USER = 'application-user'
export const CIRCLEONE_AUTH_TOKEN = 'circleone-auth-token'
export const CIRCLEONE_USER = 'circleone-user'
export const CIRCLEONE_USERNAME = "circleone-username"