import React from 'react'
import useInputState from './hooks/useInputState'

const EditField = ({id, task, editTodo, toggleEditMode}) => {
	const [editedTodo, setEditedTodo, resetEditedTodo] = useInputState(task);
	return (
		<div>
			<form style = {{marginLeft: '0.8rem',paddingTop: '0px', paddingBottom: '0px' , width: 'inherit', marginBlock: "1rem"}} 
				onSubmit = {(e) => {
					e.preventDefault();
					editTodo(id,editedTodo);
					resetEditedTodo();
					toggleEditMode();
				}}>
			<input type="text" value = {editedTodo} onChange = {setEditedTodo} autoFocus className='form-control'/>
			</form>
		</div>
	)
}

export default EditField