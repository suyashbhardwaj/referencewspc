import React from 'react'
import useToggleState from './hooks/useToggleState';
import EditField from './EditField';

const Todo = ({task, completed, id, removeTodo, toggleCheck, editTodo}) => {
	const [isEditing, toggleEditMode] = useToggleState();
	return (
		<>
			{isEditing ? (<EditField id={id} editTodo={editTodo} task={task} toggleEditMode = {toggleEditMode}/>) : (
				<div class="d-flex my-1">
					<div className='card flex-row align-items-center w-100 mb-0'>
						<input 
							className='form-check-input' 
							type="checkbox" 
							id={`task${id}`} 
							checked = {completed} 
							onClick = {()=> toggleCheck(id)}
						/>
						<label className = "fs-2 form-check-label ms-2" htmlFor={`task${id}`} style = {{textDecorationLine: completed ? 'line-through': 'none' }}>{task}</label>
						<i className="bi bi-pencil-fill ms-2 ms-auto fs-1" onClick = {()=> toggleEditMode()}></i>
						<i className="bi bi-trash-fill ms-2 fs-1" onClick = {()=> removeTodo(id)}></i>
					</div>
				</div>
				)}
			
			
		</>
		)
	}

export default Todo