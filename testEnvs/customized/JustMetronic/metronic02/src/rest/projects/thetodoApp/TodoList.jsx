import React from 'react';
import Todo from './Todo';

const TodoList = (props) => {
	const { todos, removeTodo, toggleCheck, editTodo } = props;
	return (
		<>
			{todos.map(todo => (
				<>
					<Todo 
						{...todo}
						key = {todo.id}
						removeTodo = {removeTodo}
						toggleCheck = {toggleCheck}
						editTodo = {editTodo}
					/>
				</>
				))}
		</>
	)
}

export default TodoList