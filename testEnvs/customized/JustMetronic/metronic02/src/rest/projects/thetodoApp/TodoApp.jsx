import React, { useEffect, useState } from 'react'
import TodoList from './TodoList';
import TodoForm from './TodoForm'
import useMyTodoHook from './hooks/useMyTodoHook'

const TodoApp = () => {
	const initialTodos = JSON.parse(window.localStorage.getItem('todos') || "[]");
	const { todos, toggleCheck, addTodo, removeTodo, editTodo } = useMyTodoHook(initialTodos);
	
	useEffect(() => {
		console.log('you made me render')
		window.localStorage.setItem("todos", JSON.stringify(todos));
	}, [todos])

	return (
		<div className='container'>
			<TodoForm addTodo = {addTodo}/>
			<TodoList todos = {todos} removeTodo = {removeTodo} toggleCheck = {toggleCheck} editTodo = {editTodo}/>
		</div>
	)
}

export default TodoApp