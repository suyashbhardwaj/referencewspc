import { Navigate, Route, Routes, Outlet } from 'react-router-dom'
import ProgressSteps from './ProgressSteps/ProgressSteps';

const StylepickRoutes = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/progresssteps/*'
          element={
            <>
              <ProgressSteps />
            </>
          }
        />
        {/*<Route path='dashboard' element={<CampaignDashboard/>} />
        <Route path='lead/:uuid' element={<LeadInfo/>} />*/}
        <Route index element={<Navigate to='/stylepicks/progresssteps' />} />
      </Route>
    </Routes>
  )
}

export default StylepickRoutes
