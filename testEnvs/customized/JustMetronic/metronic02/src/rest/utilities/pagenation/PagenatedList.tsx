import { createRef, useEffect, useRef, useState } from 'react'
import ReactModal from 'react-modal'
import clsx from 'clsx'
/*import { Link } from 'react-router-dom'*/

const initialticketlist = [
  {
    ticketId: '314d-23cr-vsk3-ld3r',
    raisedByUser: {
      name: 'yash',
      email: 'suyash.inpersonaluse@gmail.com',
      contact: '9319661081'
    },
    ticketDet: {
      subject: 'radio toggle crash',
      category: 'business logic',
      priority: 5,
      description:
        'when we try to toggle a radio button while editing a form in for-builder, it crashes'
    },
    enclosed: '',
    additionalInfo: '',
    pickedByAgent: 'Shivam',
    resolutionStatus: 'open',
    progress: [
      { action: 'comprehend the issue', currentlyAt: false, iterationNo: 1 },
      { action: 'figure out root cause', currentlyAt: false, iterationNo: 1 },
      { action: 'formulate a solution', currentlyAt: true, iterationNo: 1 },
      { action: 'work on it', currentlyAt: false, iterationNo: 0 },
      { action: 'done', currentlyAt: false, iterationNo: 0 }
    ]
  },

  {
    ticketId: '314d-23cr-vsk3-ld4r',
    raisedByUser: {
      name: 'abhirup',
      email: 'abhirup.basu@gmail.com',
      contact: '9564684254'
    },
    ticketDet: {
      subject: 'chatlist not getting updated in chat history',
      category: 'business logic, backend',
      priority: 5,
      description:
        "when we assing a chat from business account to an operator, post having the chat session closed the same is not getting listed in the business login's chat history section"
    },
    enclosed: '',
    additionalInfo: '',
    pickedByAgent: 'kiran',
    resolutionStatus: 'in progress',
    progress: [
      { action: 'comprehend the issue', currentlyAt: false, iterationNo: 1 },
      { action: 'figure out root cause', currentlyAt: false, iterationNo: 1 },
      { action: 'formulate a solution', currentlyAt: true, iterationNo: 3 },
      { action: 'work on it', currentlyAt: false, iterationNo: 2 },
      { action: 'done', currentlyAt: false, iterationNo: 0 }
    ]
  },

  {
    ticketId: '314d-23cr-vsk3-ld5r',
    raisedByUser: {
      name: 'Abhinav',
      email: 'abhinav.qasba@gmail.com',
      contact: '9566433263'
    },
    ticketDet: {
      subject: 'captcha image can be copied',
      category: 'UI logic',
      priority: 3,
      description:
        'The captcha image that appears in the captcha section of rendered form can easily be copied & pasted. This is insecure'
    },
    enclosed: '',
    additionalInfo: '',
    pickedByAgent: 'Suyash',
    resolutionStatus: 'closed',
    progress: [
      { action: 'comprehend the issue', currentlyAt: false, iterationNo: 1 },
      { action: 'figure out root cause', currentlyAt: false, iterationNo: 2 },
      { action: 'formulate a solution', currentlyAt: true, iterationNo: 3 },
      { action: 'work on it', currentlyAt: false, iterationNo: 3 },
      { action: 'done', currentlyAt: true, iterationNo: 1 }
    ]
  },

  {
    ticketId: '314d-23cr-vske-ld3r',
    raisedByUser: {
      name: 'avinash',
      email: 'suyash.inpersonaluse@gmail.com',
      contact: '9319661081'
    },
    ticketDet: {
      subject: 'checkbox toggle crash',
      category: 'UI logic',
      priority: 3,
      description:
        'when we try to toggle a radio button while editing a form in for-builder, it crashes'
    },
    enclosed: '',
    additionalInfo: '',
    pickedByAgent: 'Shivam',
    resolutionStatus: 'open',
    progress: [
      { action: 'comprehend the issue', currentlyAt: false, iterationNo: 1 },
      { action: 'figure out root cause', currentlyAt: false, iterationNo: 1 },
      { action: 'formulate a solution', currentlyAt: true, iterationNo: 1 },
      { action: 'work on it', currentlyAt: false, iterationNo: 0 },
      { action: 'done', currentlyAt: false, iterationNo: 0 }
    ]
  },

  {
    ticketId: '314d-43cr-vsk3-ld4r',
    raisedByUser: {
      name: 'arvind',
      email: 'abhirup.basu@gmail.com',
      contact: '9564684254'
    },
    ticketDet: {
      subject: 'botlist not getting updated in chat history',
      category: 'business logic, backend',
      priority: 5,
      description:
        "when we assing a chat from business account to an operator, post having the chat session closed the same is not getting listed in the business login's chat history section"
    },
    enclosed: '',
    additionalInfo: '',
    pickedByAgent: 'kiran',
    resolutionStatus: 'in progress',
    progress: [
      { action: 'comprehend the issue', currentlyAt: false, iterationNo: 1 },
      { action: 'figure out root cause', currentlyAt: false, iterationNo: 1 },
      { action: 'formulate a solution', currentlyAt: true, iterationNo: 3 },
      { action: 'work on it', currentlyAt: false, iterationNo: 2 },
      { action: 'done', currentlyAt: false, iterationNo: 0 }
    ]
  },

  {
    ticketId: '319d-23cr-vsk3-ld5r',
    raisedByUser: {
      name: 'Vikram',
      email: 'abhinav.qasba@gmail.com',
      contact: '9566433263'
    },
    ticketDet: {
      subject: 'my image can be copied',
      category: 'UI logic',
      priority: 3,
      description:
        'The captcha image that appears in the captcha section of rendered form can easily be copied & pasted. This is insecure'
    },
    enclosed: '',
    additionalInfo: '',
    pickedByAgent: 'Suyash',
    resolutionStatus: 'closed',
    progress: [
      { action: 'comprehend the issue', currentlyAt: false, iterationNo: 1 },
      { action: 'figure out root cause', currentlyAt: false, iterationNo: 2 },
      { action: 'formulate a solution', currentlyAt: true, iterationNo: 3 },
      { action: 'work on it', currentlyAt: false, iterationNo: 3 },
      { action: 'done', currentlyAt: true, iterationNo: 1 }
    ]
  }
]

const PagenatedList = () => {
  const [isOpenNewTicketModal, setIsOpenNewTicketModal] = useState(false)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [ticketlist, setTicketList] = useState(initialticketlist)
  const [population, setPopulation] = useState(ticketlist)
  const userActionDropdownMenuRefs = useRef([])

  const [totalPages, setTotalPages] = useState(initialticketlist.length)
  const [currentPage, setCurrentPage] = useState(1)
  const [itemsPerPage, setItemsPerPage] = useState(1) // You can adjust this based on your requirement

  const indexOfLastPage = currentPage * itemsPerPage
  const indexOfFirstPage = indexOfLastPage - itemsPerPage
  const currentData = population.slice(indexOfFirstPage, indexOfLastPage)

  const paginate = (pageNumber: any) => {
    setCurrentPage(pageNumber)
  }

  const nextPage = () => {
    setCurrentPage((prev) => prev + 1)
  }

  const prevPage = () => {
    setCurrentPage((prev) => prev - 1)
  }

  // Generate snake handle pagination numbers
  const generatePaginationNumbers = () => {
    const numbers = []
    const maxVisibleNumbers = 4 // Maximum visible pagination numbers

    if (totalPages <= maxVisibleNumbers) {
      // If total pages are less than or equal to max visible numbers, show all
      for (let i = 1; i <= totalPages; i++) {
        numbers.push(i)
      }
    } else {
      const middleIndex = Math.ceil(maxVisibleNumbers / 2) // Middle index of the pagination numbers
      let startPage = currentPage - middleIndex + 1
      let endPage = currentPage + middleIndex - 1

      if (startPage < 1) {
        // If start page is less than 1, adjust start and end page accordingly
        endPage = startPage + maxVisibleNumbers - 1
        startPage = 1
      } else if (endPage > totalPages) {
        // If end page is greater than total pages, adjust start and end page accordingly
        startPage = endPage - maxVisibleNumbers + 1
        endPage = totalPages
      }

      for (let i = startPage; i <= endPage; i++) {
        numbers.push(i)
      }
    }
    console.log('total pages set to be as ', numbers)
    return numbers
  }

  const customModalStyles = {
    content: {
      inset: '50% 30% 5% 50%',
      /* top: '50%',
          left: '30%',
          right: '5%',
          bottom: '50%', */
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)'
    }
  }

  const closeCreateTeamModal = () => {
    setIsOpenNewTicketModal(false)
  }

  const reloadTicketsFilteredBy = (param: any) => {
    setPopulation(ticketlist)
    param &&
      setPopulation((prevSet: any) => {
        const updatedSet = [...prevSet]
        return updatedSet.filter((tkt) => tkt.resolutionStatus === param)
      })
  }

  /* useEffect(()=> {
        async function fetchTickets() {
            axios
                .get(`${process.env.REACT_APP_GET_TICKETS_URL}`)
                .then((res:any) => {
                    userActionDropdownMenuRefs.current = res.data.data.getAllTickets.data.map(
                        (item: any, i: any) => userActionDropdownMenuRefs.current[i] ?? createRef()
                      )
                    setTicketList(res.data.data.getAllTickets.data);
                    setTotalPages(res.data.data.getAllTickets.data.length)
                });
        }
        fetchTickets();
    },[]); */

  return (
    <>
      <div id='kt_app_content' className='app-content  flex-column-fluid '>
        <div id='kt_app_content_container' className='app-container'>
          <div className='card card-flush' data-select2-id='select2-data-124-gb9b'>
            <div
              className='card-header align-items-center py-5 gap-2 gap-md-5'
              data-select2-id='select2-data-123-gilb'
            >
              <div className='card-title'>
                <div className='d-flex align-items-center position-relative my-1'>
                  <i className='ki-duotone ki-magnifier fs-3 position-absolute ms-4'>
                    <span className='path1'></span>
                    <span className='path2'></span>
                  </i>
                  <input
                    type='text'
                    data-kt-ecommerce-product-filter='search'
                    className='form-control form-control-solid w-250px ps-12'
                    placeholder='Search Ticket'
                  />
                </div>
              </div>

              <div
                className='card-toolbar flex-row-fluid justify-content-end gap-5'
                data-select2-id='select2-data-122-vmtv'
              >
                <div className='w-100 mw-150px' data-select2-id='select2-data-121-xiks'>
                  <select
                    className='form-select form-select-solid select2-hidden-accessible'
                    onChange={(evt) => reloadTicketsFilteredBy(evt.target.value)}
                    data-control='select2'
                    data-hide-search='true'
                    data-placeholder='Status'
                    data-kt-ecommerce-product-filter='status'
                    data-select2-id='select2-data-9-r626'
                    tabIndex={-1}
                    aria-hidden='true'
                    data-kt-initialized='1'
                  >
                    <option data-select2-id='select2-data-11-z2qa'></option>
                    <option value='' data-select2-id='select2-data-126-xf6u'>
                      All
                    </option>
                    <option value='open' data-select2-id='select2-data-127-eoua'>
                      Open
                    </option>
                    <option value='in progress' data-select2-id='select2-data-128-z9uc'>
                      In Progress
                    </option>
                    <option value='closed' data-select2-id='select2-data-129-gznk'>
                      Closed
                    </option>
                  </select>
                </div>

                <button onClick={() => setIsOpenNewTicketModal(true)} className='btn btn-primary'>
                  Create Ticket
                </button>
              </div>
            </div>

            <div className='card-body pt-0'>
              <div
                id='kt_ecommerce_products_table_wrapper'
                className='dataTables_wrapper dt-bootstrap4 no-footer'
              >
                <div className='table-responsive'>
                  <table
                    className='table align-middle table-row-dashed fs-6 dataTable no-footer'
                    id='kt_ecommerce_products_table'
                  >
                    <thead>
                      <tr className='text-start text-gray-500 fw-bold fs-7 text-uppercase gs-0'>
                        {/* <th
                          className='w-10px pe-2 sorting_disabled'
                          rowSpan={1}
                          colSpan={1}
                          aria-label=''
                          style={{ width: '29.8785px' }}
                        >
                          <div className='form-check form-check-sm form-check-custom form-check-solid me-3'>
                            <input
                              className='form-check-input'
                              type='checkbox'
                              data-kt-check='true'
                              data-kt-check-target='#kt_ecommerce_products_table .form-check-input'
                              value='1'
                            />
                          </div>
                        </th> */}
                        <th
                          className='min-w-200px sorting'
                          tabIndex={0}
                          aria-controls='kt_ecommerce_products_table'
                          rowSpan={1}
                          colSpan={1}
                          aria-label='Product: activate to sort column ascending'
                          style={{ width: '255.451px' }}
                        >
                          Ticket
                        </th>
                        <th
                          className='text-end min-w-100px sorting'
                          tabIndex={0}
                          aria-controls='kt_ecommerce_products_table'
                          rowSpan={1}
                          colSpan={1}
                          aria-label='SKU: activate to sort column ascending'
                          style={{ width: '129.01px' }}
                        >
                          Subject
                        </th>
                        <th
                          className='text-end min-w-70px sorting'
                          tabIndex={0}
                          aria-controls='kt_ecommerce_products_table'
                          rowSpan={1}
                          colSpan={1}
                          aria-label='Qty: activate to sort column ascending'
                          style={{ width: '116.215px' }}
                        >
                          Category
                        </th>
                        <th
                          className='text-center min-w-100px sorting'
                          tabIndex={0}
                          aria-controls='kt_ecommerce_products_table'
                          rowSpan={1}
                          colSpan={1}
                          aria-label='Price: activate to sort column ascending'
                          style={{ width: '129.01px' }}
                        >
                          Priority
                        </th>
                        <th
                          className='text-end min-w-100px sorting'
                          tabIndex={0}
                          aria-controls='kt_ecommerce_products_table'
                          rowSpan={1}
                          colSpan={1}
                          aria-label='Rating: activate to sort column ascending'
                          style={{ width: '129.01px' }}
                        >
                          Attachment
                        </th>
                        <th
                          className='text-end min-w-100px sorting'
                          tabIndex={0}
                          aria-controls='kt_ecommerce_products_table'
                          rowSpan={1}
                          colSpan={1}
                          aria-label='Status: activate to sort column ascending'
                          style={{ width: '129.01px' }}
                        >
                          Status
                        </th>
                        <th
                          className='text-end min-w-70px sorting_disabled'
                          rowSpan={1}
                          colSpan={1}
                          aria-label='Actions'
                          style={{ width: '123.16px' }}
                        >
                          Actions
                        </th>
                      </tr>
                    </thead>
                    <tbody className='fw-semibold text-gray-600'>
                      {currentData.map((ticket, index) => (
                        <>
                          <tr className={index % 2 === 0 ? `even` : `odd`}>
                            {/* <td>
                              <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                <input className='form-check-input' type='checkbox' value='1' />
                              </div>
                            </td> */}
                            <td>
                              <div className='d-flex align-items-center'>
                                {/* <a
                                  href='/metronic8/demo1/../demo1/apps/ecommerce/catalog/edit-product.html'
                                  className='symbol symbol-50px'
                                >
                                  <span
                                    className='symbol-label'
                                    style={{
                                      backgroundImage:
                                        'url(/metronic8/demo1/assets/media//stock/ecommerce/1.png)'
                                    }}
                                  ></span>
                                </a> */}

                                <div className='ms-5'>
                                  {/*<Link
                                    to={`/support/ticket/${ticket.ticketId}`}
                                    className='btn btn-sm btn-light-primary'
                                  >
                                    {ticket.ticketId}
                                  </Link>*/}
                                {ticket.ticketId}

                                  {/* <a
                                    href='/metronic8/demo1/../demo1/apps/ecommerce/catalog/edit-product.html'
                                    className='text-gray-800 text-hover-primary fs-5 fw-bold'
                                    data-kt-ecommerce-product-filter='product_name'
                                  >
                                    {ticket.ticketId}
                                  </a> */}
                                </div>
                              </div>
                            </td>
                            <td className='text-end pe-0'>
                              <span className='fw-bold'>{ticket.ticketDet.subject}</span>
                            </td>
                            <td className='text-end pe-0' data-order='7'>
                              <span
                                className={clsx(
                                  'badge',

                                  { 'badge-light-danger': ticket.ticketDet.priority === 1 },
                                  { 'badge-light-warning': ticket.ticketDet.priority === 2 },
                                  { 'badge-light-info': ticket.ticketDet.priority === 3 },
                                  { 'badge-light-primary': ticket.ticketDet.priority > 3 }
                                )}
                              >
                                {ticket.ticketDet.category}
                              </span>
                            </td>
                            <td className='text-center pe-0'>{ticket.ticketDet.priority}</td>
                            <td className='text-end pe-0' data-order='rating-4'>
                              <div className='rating justify-content-end'>
                                <div className='rating-label checked'>
                                  <i className='ki-duotone ki-star fs-6'></i>{' '}
                                </div>
                                <div className='rating-label checked'>
                                  <i className='ki-duotone ki-star fs-6'></i>{' '}
                                </div>
                                <div className='rating-label checked'>
                                  <i className='ki-duotone ki-star fs-6'></i>{' '}
                                </div>
                                <div className='rating-label checked'>
                                  <i className='ki-duotone ki-star fs-6'></i>{' '}
                                </div>
                                <div className='rating-label '>
                                  <i className='ki-duotone ki-star fs-6'></i>{' '}
                                </div>
                              </div>
                            </td>
                            <td className='text-end pe-0' data-order='Published'>
                              <div
                                className={clsx(
                                  'badge fs-6 ',
                                  { 'badge-light-danger': ticket.resolutionStatus === 'open' },
                                  {
                                    'badge-light-warning': ticket.resolutionStatus === 'in progress'
                                  },
                                  { 'badge-light-success': ticket.resolutionStatus === 'closed' }
                                )}
                              >
                                {ticket.resolutionStatus}
                              </div>
                            </td>
                            {/*<td className='text-end'>
                              <TicketAction
                                userActionDropdownMenuRefs={userActionDropdownMenuRefs}
                                ticketInfo={ticket}
                                index={index}
                                setIsLoading={setIsLoading}
                                ticketlist={ticketlist}
                                setTicketList={setTicketList}
                              />
                            </td>*/}
                          </tr>
                        </>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className='row'>
                  <div className='col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'>
                    <div className='dataTables_length' id='kt_ecommerce_products_table_length'>
                      <label>
                        <select
                          name='kt_ecommerce_products_table_length'
                          aria-controls='kt_ecommerce_products_table'
                          className='form-select form-select-sm form-select-solid'
                          onChange={(evt) => setItemsPerPage(Number(evt.target.value))}
                        >
                          <option value='1'>1</option>
                          <option value='2'>2</option>
                          <option value='5'>5</option>
                          <option value='10'>10</option>
                        </select>
                      </label>
                    </div>
                  </div>
                  <div className='col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'>
                    <div
                      className='dataTables_paginate paging_simple_numbers'
                      id='kt_ecommerce_products_table_paginate'
                    >
                      <ul className='pagination'>
                        <li
                          className={`paginate_button page-item previous ${
                            currentPage === 1 ? 'disabled' : ''
                          }`}
                          id='kt_ecommerce_products_table_previous'
                        >
                          <button
                            type='button'
                            aria-controls='kt_ecommerce_products_table'
                            data-dt-idx='0'
                            tabIndex={0}
                            className='btn primary page-link'
                            onClick={prevPage}
                          >
                            <i className='previous'></i>
                          </button>
                        </li>

                        {generatePaginationNumbers().map((pageNumber) => (
                          <li
                            key={pageNumber}
                            className={`paginate_button page-item ${
                              currentPage === pageNumber ? 'active' : ''
                            }`}
                          >
                            <button
                              type='button'
                              aria-controls='kt_table_users'
                              data-dt-idx={pageNumber}
                              className='btn primary page-link'
                              onClick={() => paginate(pageNumber)}
                            >
                              {pageNumber}
                            </button>
                          </li>
                        ))}

                        <li
                          className={`paginate_button page-item ${
                            currentPage === totalPages ? 'disabled' : ''
                          }`}
                          id='kt_ecommerce_products_table_next'
                        >
                          <button
                            type='button'
                            aria-controls='kt_ecommerce_products_table'
                            className='btn primary page-link'
                            data-dt-idx='6'
                            tabIndex={0}
                            onClick={nextPage}
                          >
                            <i className='next'></i>
                          </button>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {isOpenNewTicketModal && (
        <>
          <ReactModal
            isOpen={isOpenNewTicketModal}
            /* onAfterOpen={afterOpenCreateTeamModal}
            onRequestClose={closeCreateTeamModal} */
            /* style={customModalStyles} */
            contentLabel='Create Ticket'
          >
            {/*<CreateTicketModal
              setIsLoading={setIsLoading}
              closeCreateTeamModal={closeCreateTeamModal}
              ticketlist={ticketlist}
              setTicketList={setTicketList}
            />*/}
            Create Ticket Modal
          </ReactModal>
        </>
      )}
    </>
  )
}

export default PagenatedList