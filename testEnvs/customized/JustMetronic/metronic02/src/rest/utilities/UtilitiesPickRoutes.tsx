import { Navigate, Route, Routes, Outlet } from 'react-router-dom'
import CaptchaDiv from './captcha/CaptchaDiv'
import PagenatedList from './pagenation/PagenatedList'
import Wizard from './wizard/Wizard'
import DropdownWizard from './customDropdown'
const UtilitiesPickRoutes = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/captcha/*'
          element={
            <>
              <CaptchaDiv />
            </>
          }
        />
        <Route path='/pagenatedlist' element={<PagenatedList/>} />
        <Route path='/wizard' element={<Wizard/>} />
        <Route path='/configuredropdown' element={<DropdownWizard/>} />
        {/*<Route path='lead/:uuid' element={<LeadInfo/>} />*/}
        <Route index element={<Navigate to='/utilitypicks/captcha' />} />
      </Route>
    </Routes>
  )
}

export default UtilitiesPickRoutes