import { setupListeners } from '@reduxjs/toolkit/query'
import {configureStore} from '@reduxjs/toolkit'
import { commonAPI } from '../services/settingsAPIs/CommonAPI'
import { emailConfigSettingsApi } from '../services/settingsAPIs/EmailConfiguration'
import { ticketSettingsApi } from '../services/settingsAPIs/TicketSettingsAPI'



export const store = configureStore({
    reducer : {
        [commonAPI.reducerPath]: commonAPI.reducer,
        [emailConfigSettingsApi.reducerPath]: commonAPI.reducer,
        [ticketSettingsApi.reducerPath]:ticketSettingsApi.reducer,
    },

    middleware: (getDefaultMiddleware) => 
        getDefaultMiddleware()
            .concat(commonAPI.middleware)
            .concat(emailConfigSettingsApi.middleware)
            .concat(ticketSettingsApi.middleware)
})

setupListeners(store.dispatch)