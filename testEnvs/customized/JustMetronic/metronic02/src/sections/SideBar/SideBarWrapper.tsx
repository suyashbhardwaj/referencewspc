import { PropsWithChildren } from 'react'
import { Link } from 'react-router-dom'
import '../../assets/dashboardSpecificStyles/style.css'

interface IProps {
  showSideBar: any
}

const SideBarWrapper: React.FC<PropsWithChildren<IProps>> = ({showSideBar}) => {
	const sidemenulist = [
			{ main:"Components", sub: [
				{theroute:"/utilitypicks/wizard", thelabel:"Wizazrd"},
				{theroute:"/utilitypicks/captcha", thelabel:"Captcha"},
				{theroute:"/utilitypicks/configuredropdown", thelabel:"Dropdown Wizazrd"},
				{theroute:"/utilitypicks/pagenatedlist", thelabel:"Pagenated List"}] },
			{ main:"Projects", sub: [
				{theroute:"/projectpicks/todoapp", thelabel:"ToDoApp"},
				{theroute:"/projectpicks/browserdbapp", thelabel:"IndexedDB app"}] },
			{ main:"ShunyaOne", sub: [
				{theroute:"/campaign", thelabel:"Campaign List"},
				{theroute:"/campaign/dashboard", thelabel:"Campaign Dashboard"},
				{theroute:"/rtm", thelabel:"RTM Dashboard"},
				{theroute:"/settings", thelabel:"Settings"} ]},
			{ main:"StylePicks", sub: [
				{theroute:"/stylepicks/progresssteps", thelabel:"Progress Steps"} ]},
		];

	return (
		<div className='thesidebarNtheheader'>
		{/*sidebar starts*/}
			    <aside id="sidebar" className={`sidebar ${showSideBar?'':'toggle-sidebar'}`}>
				    <ul className="sidebar-nav" id="sidebar-nav">
				    	{sidemenulist.map((listitem:any)=>(<>
					    	<li className="nav-item">
					        <a className="nav-link collapsed" data-bs-target={`#${listitem.main}-nav`} data-bs-toggle="collapse" href="#">
					          <i className="bi bi-gem"></i><span>{listitem.main}</span><i className="bi bi-chevron-down ms-auto"></i>
					        </a>
					        <ul id={`${listitem.main}-nav`} className="nav-content collapse " data-bs-parent="#sidebar-nav">
					        {listitem.sub.map((thesub:any)=>(<>
						          <Link to = {thesub.theroute}>
		                    <i className='bi bi-circle'></i><span>{thesub.thelabel}</span>
		                  </Link>
					        </>))}
					        </ul>
					      </li>
				    	</>))}
				    </ul>
  				</aside>
  				{/*sidebar ends*/}
		</div>
	)
}

export default SideBarWrapper