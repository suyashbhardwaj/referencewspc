import clsx from 'clsx'
import React, { useRef, useState } from 'react'
import { Link } from 'react-router-dom'
import { MYDASH_USER } from '../../../constants'
/* import { KTSVG } from '../../../helpers' */
import { Dropdown } from '../../../helpers/Dropdown'
/*import { useLayout } from '../../../layout/core'*/
import { createInitials } from '../../../utils/functions/createInitials'

const TopRightTools: React.FC = () => {
/*  const { config } = useLayout()*/
  const [isUserMenuOpen, setIsUserMenuOpen] = useState<boolean>(false)
  const headerUserDropdownMenuRef = useRef<HTMLDivElement>(null)
  const user = !!localStorage.getItem(MYDASH_USER)
    ? JSON.parse(localStorage.getItem(MYDASH_USER)!)
    : null
  let initials = null
  if (!!user) {
    initials = createInitials(user.name)
  }

  const closeUserMenu = () => {
    setIsUserMenuOpen(false)
  }

  const toolbarButtonMarginClass = 'ms-1 ms-lg-3',
    toolbarUserAvatarHeightClass = `symbol-30px symbol-md-40px ${
      isUserMenuOpen ? 'show menu-dropdown' : ''
    }`

  return (
    <>
      {/* begin::User */}
      <div
        className={clsx('d-flex align-items-center', toolbarButtonMarginClass)}
        ref={headerUserDropdownMenuRef}
      >
        {/* begin::Toggle */}
        <div
          className={clsx('cursor-pointer symbol d-flex', toolbarUserAvatarHeightClass)}
          onClick={() => setIsUserMenuOpen(!isUserMenuOpen)}
        >
          <h1 className='d-flex align-items-center text-dark fw-bolder my-1 fs-3'>
            {user.name} <i className="bi bi-chevron-compact-down mx-4"></i>
          </h1>
          <span className={`symbol-label bg-light-dark text-primary fs-6 fw-bolder`}>
          {initials?.label}
        </span>
        </div>
        {/* <UserMenu isUserMenuOpen={isUserMenuOpen} setIsUserMenuOpen={setIsUserMenuOpen} /> */}
        <Dropdown
          menuRef={headerUserDropdownMenuRef}
          droppedDown={isUserMenuOpen}
          setDroppedDown={setIsUserMenuOpen}
        >
          <div className='card-body'>
            <div className='menu-content d-flex align-items-center px-3'>
              <div className='symbol symbol-50px me-5'>
                <span className={`symbol-label bg-light-dark text-primary fs-6 fw-bolder`}>
                  {initials?.label}
                </span>
              </div>

              <div className='d-flex flex-column'>
                <div className='fw-bolder d-flex align-items-center fs-5'>
                  {!!user ? user.name : 'Unknown User'}
                </div>
                <span className='fw-bold text-muted text-hover-primary fs-7'>
                  {!!user ? user.email : 'abc@xyz.com'}
                </span>
              </div>
            </div>
          </div>

          <div className='separator my-2'></div>

          <div className='menu-item px-5'>
            <Link to='/profile' onClick={closeUserMenu} className='menu-link px-5'>
              My Profile
            </Link>
          </div>

          <div className='separator my-2'></div>

          <div className='menu-item px-5'>
            <Link to='/logout' className='menu-link px-5'>
              Sign Out
            </Link>
          </div>
        </Dropdown>
        {/* end::Toggle */}
      </div>
      {/* end::User */}

      {/* begin::Aside Toggler */}
      {true && (
        <div className='d-flex align-items-center d-lg-none ms-2 me-n3' title='Show header menu'>
          <div
            className='btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px'
            id='kt_header_menu_mobile_toggle'
          >
            Some KTSVG Icon
            {/* <KTSVG path='/media/icons/duotune/text/txt001.svg' className='svg-icon-1' /> */}
          </div>
        </div>
      )}
    </>
  )
}

export default TopRightTools