/*import App from './App.tsx'*/
/*import './index.css' */
// import React from 'react'
import ReactDOM from 'react-dom/client'
import './assets/scss/style.scss'
import SOhome from './SOhome'

/*ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
     <SOhome/>
  </React.StrictMode>,
)*/

ReactDOM.createRoot(document.getElementById('root')!).render(<SOhome/>)


/*01 understand the role & implementation of prisma ORM to back an application (groundwork for step 02)
02 understand configuration of JWT based authorization middleware in a graphql based API
03 setup 
04 verify email
05 configure profile info by user at the time of 1st time login
06 role based dynamic loading of sidebar/header contents & styles
07 dark mode toggling correction
08 setting up accounts upon acceptance of mail inviations
09 special access for admin to pages that configure dynamically loaded contents respective to the user types*/