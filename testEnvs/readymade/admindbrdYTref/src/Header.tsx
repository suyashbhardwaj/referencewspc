import {BsFillBellFill, BsFillEnvelopeFill, BsPersonCircle, BsSearch, BsJustify} from 'react-icons/bs'

interface Header {
  OpenSidebar: any;
}

const Header: React.FC<Header> = ({OpenSidebar}) => {
	return (
		<header className='header'>
	        <div className='menu-icon'>
	            <BsJustify className='icon' onClick={OpenSidebar}/>
	        </div>
	        <div className='header-left'>
	            <BsSearch  className='icon'/>
	        </div>
	        <div className='header-right'>
	            <BsFillBellFill className='icon'/>
	            <BsFillEnvelopeFill className='icon'/>
	            <BsPersonCircle className='icon'/>
	        </div>
	    </header>
	)
}

export default Header