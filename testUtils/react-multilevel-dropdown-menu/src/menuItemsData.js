export const rishavbhartimenu = [
    {
      name: "Documentary",
      link: "/page/documentary",
      type: "page"
    },
    {
      name: "Filmein",
      link: "/page/movies",
      type: "page",
      children: [
        {
          name: "Hindi Movies",
          link: "/pages/movies/hindi",
          type: "page",
          children: [
            {
              name: "Action Movies",
              link: "/pages/movies/hindi/action",
              type: "page",
              children: [
                {
                  name: "2021",
                  link: "/pages/movies/hindi/action/2021",
                  type: "page"
                }
              ]
            },
            {
              name: "Romantic Movies",
              link: "/pages/movies/hindi/romantic",
              type: "page",
              children: [
                {
                  name: "Adult",
                  link: "/pages/movies/hindi/romantic/adult",
                  type: "page"
                }
              ]
            }
          ]
        },
        {
          name: "Telugu Movies",
          link: "/pages/movies/telugu",
          type: "page",
          children: [
            {
              name: "Action Movies",
              link: "/pages/movies/telugu/action",
              type: "page"
            }
          ]
        }
      ]
    },
    {
      name: "series",
      link: "/page/series",
      type: "page",
      children: [
        {
          name: "Comedy",
          link: "/pages/series/comedy",
          type: "page"
        }
      ]
    }
  ];

export const menuItemsData = [
  {
    title: "Home",
    url: "/",
  },
  {
    title: "Services",
    url: "/services",
    submenu: [
      {
        title: "Web Design",
        url: "web-design",
      },
      {
        title: "Web Development",
        url: "web-dev",
        submenu: [
          {
            title: "Frontend",
            url: "frontend",
          },
          {
            title: "Backend",
            submenu: [
              {
                title: "NodeJS",
                url: "node",
              },
              {
                title: "React",
                url: "react",
                submenu: [
                  {
                    title: "FrontEnd",
                    url: "frontend",
                  },
                  {
                    title: "BackEnd",
                    url: "backend",
                  }
                  ]
              },
              {
                title: "PHP",
                url: "php",
              },
            ],
          },
        ],
      },
      {
        title: "SEO",
        url: "seo",
      },
    ],
  },
  {
    title: "About",
    url: "/about",
  },
];
