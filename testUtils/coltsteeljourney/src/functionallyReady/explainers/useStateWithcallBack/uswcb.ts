/*import React from "react";*/
import React, { useEffect, useRef, useState } from "react";

/*it basically is a hook that takes in the instructions as 
"तुम्हारे setState से value तो update तुम कर के return दे ही देना, लेकिन साथ ही साथ जो function तुम्हे दिया जा रहा है उसे वही value दे कर execute भी करवा लेना"*/
export const USWCB = 
	<T>(initialState: T): [state: T, setState: (updatedState: React.SetStateAction<T>, callback?: (updatedState: T) => void) => void] => {
    const [state, setState] = useState<T>(initialState);
    const callbackRef = useRef<(updated: T) => void>();

    const handleSetState = (updatedState: React.SetStateAction<T>, callback?: (updatedState: T) => void) => {
        callbackRef.current = callback;
        setState(updatedState);
    };

    useEffect(() => {
        if (typeof callbackRef.current === "function") {
            callbackRef.current(state);
            callbackRef.current = undefined;
        }
    }, [state]);

    return [state, handleSetState];
}

/*export const USWCB = () => { console.log("I am useStateWithCallBack executed");  return 'yoyo'; }*/