import React from 'react'
import Die from './Die'
import './RollDice.css'

export class RollDice extends React.Component {
	constructor(props) {
		super(props);
		this.state = { diceOnenum: 2, diceTwonum: 4, isRolling: false};
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(e) {
		console.log('button clickwa');
		let rand1 = Math.floor(Math.random()*6)+1;
		let rand2 = Math.floor(Math.random()*6)+1;
		this.setState({ diceOnenum: rand1, diceTwonum: rand2, isRolling: true });
		
		/*wait for a second & then set the isRolling to false*/
		setTimeout(()=>{
			this.setState({isRolling: false});
		},1000);
	}

	render() {
		console.log('re-rendering once done');
		return (
			<>
				<div className = 'Die'>
					<Die number = {this.state.diceOnenum} rolling = {this.state.isRolling}/>
					<Die number = {this.state.diceTwonum} rolling = {this.state.isRolling}/>				
				</div>
				<div>
					<button 
						onClick = {this.handleClick}
						disabled = {this.state.isRolling}
					>{this.state.isRolling ? 'Rolling..' : 'RollDie'}</button>
				</div>
			</>
		)
	}
}

export default RollDice