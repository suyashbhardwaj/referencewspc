import RollDice from './RollDice'

function RunMe() {
  
  return (
    <div className="RunMe">
      <RollDice />
    </div>
  );
}

export default RunMe;
