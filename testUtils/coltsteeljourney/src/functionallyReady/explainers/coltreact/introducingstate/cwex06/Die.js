import React from 'react'
import dieOne from './images/dieOne.png'
import dieTwo from './images/dieTwo.png'
import dieThree from './images/dieThree.png'
import dieFour from './images/dieFour.png'
import dieFive from './images/dieFive.png'
import dieSix from './images/dieSix.png'

import './Die.css'

export class Die extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		console.log('re-rendered with props: ', this.props.rolling);
		return (
			<div className = {this.props.rolling ? 'shaking' : 'notshaking'}>
				<img src = {this.props.number == 1 ? dieOne
					 : this.props.number == 2 ? dieTwo
					 : this.props.number == 3 ? dieThree
					 : this.props.number == 4 ? dieFour
					 : this.props.number == 5 ? dieFive
					 : dieSix
					} />
			</div>
		)
	}
}

export default Die