import React from 'react'

export class Game2 extends React.Component {
	/*---------------------------------------------------------*/
	state = {
		score: 99,
		gameOver: false
		}
	/*---------------------------{initializing a state: shorter notation}--------*/
	render() {
		return (
			<div>
				<h1>Your score is {this.state.score}</h1>
			</div>
		)
	}
}

export default Game2