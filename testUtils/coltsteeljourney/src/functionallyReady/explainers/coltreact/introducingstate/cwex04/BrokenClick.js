import React from 'react'

export class BrokenClick extends React.Component {

	constructor(props){
		super(props);
		this.state = { clickStatus: 'नही दबाया' };
		this.handleClick = this.handleClick.bind(this); /*setting the context of handleClick..*/
	}

	/*the below method - handleClick is being called out of context. bind it's 
	context in the constructor सबसे पहले*/
	handleClick(e){
		console.log('clicked for the binded handleClick');
		this.setState({ clickStatus: 'दबा दिया' });
	}

	/*arrow function need not be binded explicitly*/
	notBindedHandleClick = (e) => {
		console.log('clicked for the notBindedHandleClick');
		this.setState({ clickStatus: 'दबा दिया' });	
	}

	render() {
		console.log('re-rendering done once!');
		return (
			<div>
				{/*<button onClick = {this.handleClick}>जोर के दबाओ</button>*/}
				<button onClick = {this.notBindedHandleClick}>जोर के दबाओ</button>
				<h1>{this.state.clickStatus}</h1>
			</div>
		)
	}
}

export default BrokenClick