import Button from './Button'
import BrokenClick from './BrokenClick'

function RunMe() {
  
  return (
    <div className="RunMe">
      <BrokenClick />
    </div>
  );
}

export default RunMe;
