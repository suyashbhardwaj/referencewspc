import Demo from './Demo'

function RunMe() {
  
  return (
    <div className="RunMe">
      <Demo animal = 'dog' food = 'bone'/>
    </div>
  );
}

export default RunMe;
