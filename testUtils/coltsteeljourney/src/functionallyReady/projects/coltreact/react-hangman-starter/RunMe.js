import React, { Component } from "react";
import Hangman from "./Hangman";

class RunMe extends Component {
  render() {
    return (
      <div className="RunMe">
        <Hangman />
      </div>
    );
  }
}

export default RunMe;
