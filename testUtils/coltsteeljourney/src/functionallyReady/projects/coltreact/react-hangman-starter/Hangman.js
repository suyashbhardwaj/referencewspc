import React, { Component } from "react";
import { randomWord } from './words'
import "./Hangman.css";
import img0 from "./0.jpg";
import img1 from "./1.jpg";
import img2 from "./2.jpg";
import img3 from "./3.jpg";
import img4 from "./4.jpg";
import img5 from "./5.jpg";
import img6 from "./6.jpg";

class Hangman extends Component {
  /** by default, allow 6 guesses and use provided gallows images. */
  static defaultProps = {
    maxWrong: 7,
    images: [img0, img1, img2, img3, img4, img5, img6]
  };

  constructor(props) {
    super(props);
    /*Set() gives us a collection wherein contents are ensured to be unique, perhaps...*/
    this.state = { nWrong: 0, guessed: new Set(), answer: randomWord() };
    this.handleGuess = this.handleGuess.bind(this);
    this.resetGame = this.resetGame.bind(this);
  }

  /** guessedWord: show current-state of word:
    if guessed letters are {a,p,e}, show "app_e" for "apple"
  */
  guessedWord() {
    return this.state.answer
      .split("")
      .map(ltr => (this.state.guessed.has(ltr) ? ltr : "_"));
  }

  /** handleGuest: handle a guessed letter:
    - add to guessed letters
    - if not in answer, increase number-wrong guesses
  */
  handleGuess(evt) {
    let ltr = evt.target.value;
    this.setState(st => ({
      guessed: st.guessed.add(ltr),
      nWrong: st.nWrong + (st.answer.includes(ltr) ? 0 : 1)
    }));
  }

  /** generateButtons: return array of letter buttons to render */
  generateButtons() {
    return "abcdefghijklmnopqrstuvwxyz".split("").map((ltr, idx) => (
      <button
        value={ltr}
        onClick={this.handleGuess}
        disabled={this.state.guessed.has(ltr)}
        key = {idx}
      >
        {ltr}
      </button>
    ));
  }

  resetGame() {
    this.setState({ nWrong: 0, guessed: new Set(), answer: randomWord() });
  }

  /** render: render game */
  render() {
    let gameOver = this.state.nWrong >= this.props.maxWrong;
    let gameWon = this.guessedWord().join("") === this.state.answer;
    return (
      <div className = 'Gamebox'>
  .      <section className='Hangman'>
          <h1>Hangman</h1>
          { !gameOver && !gameWon ?
            <div>
              <img 
                src={this.props.images[this.state.nWrong]} 
                alt = {`${this.state.nWrong} arms drawn!`}
              />
              <p className='Hangman-word'>{this.guessedWord()}</p>
              
            </div>
            :
            <div>
              <h2> {gameWon ? 'You Won!' : 'You Lose!'}</h2>
              <h6>The correct answer is {this.state.answer}</h6>
            </div>
          }
        </section>

        <aside>
          <p className='Hangman-btns'>{!gameOver && !gameWon && this.generateButtons()}</p>
          <button id = 'resetbutton' onClick = {this.resetGame}>Reset Game</button>
          <h4>{this.state.nWrong} incorrect attempts</h4>
        </aside>
      </div>
    );
  }
}

export default Hangman;
