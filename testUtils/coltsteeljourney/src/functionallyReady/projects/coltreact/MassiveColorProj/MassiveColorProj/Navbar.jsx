import React from 'react'
import Slider from 'rc-slider'
import 'rc-slider/assets/index.css'
import './Navbar.css'
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import NativeSelect from '@mui/material/NativeSelect';
import { IconButton, Snackbar } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { Link } from 'react-router-dom';

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  'label + &': {
    marginTop: theme.spacing(3),
  },
  '& .MuiInputBase-input': {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}));

export class Navbar extends React.Component {
	static defaultProps = {
		level:0,
		changeLevel: function() {console.log('defaultProps geneated')}
	}

	constructor(props) {
		super(props);
		this.state = { format:'hex', formatJustChanged: false }
		this.handleChange = this.handleChange.bind(this);
		this.closeSnackbar = this.closeSnackbar.bind(this);
	}

	handleChange(evt) {
		this.setState({format:evt.target.value, formatJustChanged:true});
		this.props.handleFormatChange(evt.target.value);
	}

	closeSnackbar() {
		this.setState({formatJustChanged:false});
	}

	render() {
		const selectMenuStyles = {
			minWidth: 120, 
			marginLeft:'auto',
			marginRight: '1rem',
			padding: '0.1em 2em 0.1em',
			backgroundColor:'black',
			borderRadius:'7px', 
			'.MuiNativeSelect-select': {backgroundColor: 'grey', color:'black', paddingLeft: '4px'}
		}

		const {level, changeLevel, handleFormatChange, showingColors} = this.props;
		const {format, formatJustChanged} = this.state;

		return (
			<header className = 'Navbar'>
				<div className = 'logo'>
					<Link to = '/'>reactcolorslider</Link>
				</div>
				
				{showingColors && (
					<>
					<span>Level: {this.props.level}</span>
					<div className = 'slider-container'>
						<div className = 'slider'>
							<Slider 
								defaultValue = {level}
								min = {100}
								max = {900}
								step = {100}
								onAfterChange = {changeLevel}
							/>
						</div>
					</div>
					</>
					)}
				
				<Box sx = {selectMenuStyles}>
				    <NativeSelect onChange = {this.handleChange}
				    	defaultValue={format}
				    	// inputProps={{
				     	//    name: 'age',
				     	//    id: 'uncontrolled-native',
				    	// }}
				    	// input={<BootstrapInput />}
				    >
				    	<option value='hex'>HEX-#ffffff</option>
				        <option value='rgb'>RGB - rgb(255,255,255)</option>
				        <option value='rgba'>RGBA - rgb(255,255,255,1.0)</option>
				    </NativeSelect>
				</Box>
				<Snackbar 
					anchorOrigin = {{vertical:'bottom', horizontal:'left'}}
					open = {formatJustChanged}
					autoHideDuration = {3000}
					onClose = {this.closeSnackbar}
					message = {`Format changed to ${format.toUpperCase()}`}
					action = {[
						<IconButton onClick = {this.closeSnackbar} key = 'close'>
							<CloseIcon/>
						</IconButton>
					]}
					/>
			</header>
		)
	}
}

export default Navbar