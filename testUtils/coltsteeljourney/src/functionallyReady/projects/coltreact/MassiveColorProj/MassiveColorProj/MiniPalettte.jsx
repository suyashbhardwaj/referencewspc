import React from 'react'
// import { withStyles } from '@mui/material';
import './Minipalettte.css'

const styles = {
		main: {
			backgroundColor:'orange',
			border:'3px solid red',
			'& h1': {
				color:'orangered'
			}
		},

		secondary: {
			backgroundColor:'skyblue',
			'& h1': {
				color:'blue',
				'& span': {
					backgroundColor:'yellow'
				}
			}
		}
	};

const MiniPalettte = (props) => {
	const {paletteName,emoji,colors} = props;
	const miniColorBoxes = colors.map(color => {
		return (<div className = 'miniColor' style = {{backgroundColor:color.color}} key = {color.name}/>)
	});

	return (
		<>
		{/*<div className = {props.classes.main}>
			<h1>The Palettes</h1>
			<section className = {props.classes.secondary}>
				<h1>
					Mini Palette<span>cacash</span>
				</h1>
			</section></div>*/}

		<div className="root">
			<div className="colors">{miniColorBoxes}</div>
			<h5 className = 'title'>{paletteName}<span className = 'emoji'>{emoji}</span></h5>
		</div>
		</>
	)
}

// export default withStyles(styles)(MiniPalettte)
export default MiniPalettte