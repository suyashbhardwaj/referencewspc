import React from 'react'

const DraggableColorBox = (props) => {
	const draggableBoxStyles = {
		backgroundColor: props.theBackgroundColor,
		display: 'inline-block',
		width: '20%',
		height: "20vh",
		cursor: 'pointer',
		// margin: '0 auto',
		position: 'relative',
		// marginBottom: '-0.6rem',
	}
	
	return (
		<li style = {draggableBoxStyles} >
			{props.theColorName}
		</li>
	)
}

export default DraggableColorBox