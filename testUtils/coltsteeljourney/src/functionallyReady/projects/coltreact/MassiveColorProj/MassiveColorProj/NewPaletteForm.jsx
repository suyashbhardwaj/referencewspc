import React from 'react'
import { styled } from '@mui/material/styles';
import FuncCompForHooks from './FuncCompForHooks'

import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import {ChromePicker} from 'react-color'
import { Button } from '@mui/material';
import DraggableColorBox from './DraggableColorBox'
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { Link } from 'react-router-dom';

const drawerWidth = 400;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

/* All in All - <DrawerHeader/> is a component that just helps us space things out |
styled is a function available for our use in '@mui/material/styles' library that 
	returns another function upon being invoked with an argument ('div' here) that function
		requires a callback function as its argument wherein
			what is passed as an argument is a theme identifier & 
				what is returned from invokation of that function is a JSON notation descriptions of style properties*/

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));


export class NewPaletteForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = { open: false, currentColor: '', colorName: '', colors: [] } 
		this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
		this.handleDrawerClose = this.handleDrawerClose.bind(this);
		this.updateCurrentColor = this.updateCurrentColor.bind(this);
		this.addNewColor = this.addNewColor.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.savePalette = this.savePalette.bind(this);
	}

	handleDrawerOpen() { this.setState({ open:true }); }

  handleDrawerClose() { this.setState({ open:false }); }

  updateCurrentColor(newColor) { this.setState({currentColor: newColor.hex}); }

  addNewColor() { 
  	const newColor = { color:this.state.currentColor, name: this.state.colorName }; 
  	this.setState({ colors: [...this.state.colors, newColor], colorName:'' }) 
  }

  handleSubmit() { this.addNewColor()}

  	handleChange(evt) { this.setState({colorName:evt.target.value}) }

  componentDidMount() {
  	ValidatorForm.addValidationRule('isNameUnique', (value) => {
    	return this.state.colors.every( ({name}) => name.toLowerCase() !== value.toLowerCase() );
    });

  	ValidatorForm.addValidationRule('isColorUnique', (value) => {
    	return this.state.colors.every( ({color}) => color !== this.state.currentColor );
    });    
  }

  savePalette() {	
  	let newName = 'New Test Palette'
  	const newPalette = { paletteName: newName, id: newName.toLowerCase().replace(/ /g, '-'), emoji:'<3', colors: this.state.colors }
  	this.props.savePalette(newPalette); 
  }

	render() {
			const theme = this.props.thetheme;
		return (
			<Box sx={{ display: 'flex' }}>
      			<CssBaseline />
      			<AppBar position="fixed" open={this.state.open} color = 'default'>
			        <Toolbar>
			          <IconButton
			            color="inherit"
			            aria-label="open drawer"
			            onClick={this.handleDrawerOpen}
			            edge="start"
			            sx={{ mr: 2, ...(this.state.open && { display: 'none' }) }}
			          >
			            <MenuIcon />
			          </IconButton>
			          <Typography variant="h6" noWrap component="div">
			            Persistent drawer | स्थाई दराज
			          </Typography>
			          <Button variant = 'contained' color = 'primary' onClick = {this.savePalette}>Save Palette</Button>
			          <Link to = '/'>Back</Link>
			        </Toolbar>
			      </AppBar>
			      <Drawer
			        sx={{
			          width: drawerWidth,
			          flexShrink: 0,
			          '& .MuiDrawer-paper': {
			            width: drawerWidth,
			            boxSizing: 'border-box',
			          },
			        }}
			        variant="persistent"
			        anchor="left"
			        open={this.state.open}
			      >
			        <DrawerHeader>
			          <IconButton onClick={this.handleDrawerClose}>
			            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
			          </IconButton>
			        </DrawerHeader>
			        <Divider />
			        <Typography variant= 'h4'>Design Your Palette</Typography>
			        <Button variant = 'contained' color = 'secondary'>Clear Palette</Button>
			        <Button variant = 'contained' color = 'primary'>Random Palette</Button>
			        <ChromePicker color = {this.state.currentColor} onChangeComplete = {this.updateCurrentColor} />
			        <ValidatorForm onSubmit = {this.handleSubmit}>
			        	<TextValidator 
			        		label = 'ColorName'
			        		onChange = {this.handleChange}
			        		name = 'colorName'
			        		value = {this.state.colorName}
			        		validators = {['required', 'isNameUnique', 'isColorUnique']}
			        		errorMessages = {['A color name has to be given!', 'You entered a color name that already exists!', 'color already there in palette!']}
			        	/>
				        <Button 
				        	style = {{backgroundColor:this.state.currentColor}} 
				        	variant = 'contained' 
				        	color = 'primary'
				        	type = 'submit'
				        >Add Color
				        </Button>
			        </ValidatorForm>
			      </Drawer>
			      <Main open={this.state.open}>
			        <DrawerHeader />
			        	<ul>
			        		{this.state.colors.map(thecolor => (
			        			<DraggableColorBox theBackgroundColor = {thecolor.color} theColorName = {thecolor.name} />
			        			))}
			        	</ul>
			      </Main>
      		</Box>
		)
	}
}

export default FuncCompForHooks(NewPaletteForm)