import React from 'react'
import { generatePalette } from '../ColorHelper'
import seedColors from '../seedColors'
import Palette from './Palette'
import SubPalette from './SubPalette'
import { useParams } from 'react-router-dom';

const DynamicRoute = (props) => {
	
	const findPalette = (id) => {
    	return props.currentPalettes.find(function(palette) {return palette.id === id});
  	}

	const {paletteId, subPaletteId} = useParams();

	return (
		<div>
			{ 
				!subPaletteId ? 
						(<Palette palette = {generatePalette(findPalette(paletteId))} />) 
					: (<SubPalette colorId = {subPaletteId} palette = {generatePalette(findPalette(paletteId))} />) 
			}
		</div>
	)
}

export default DynamicRoute