import React from 'react'
import { Link } from 'react-router-dom';
import MiniPalettte from './MiniPalettte'
import './PaletteList.css'

export default class PaletteList extends React.Component {
	render() {
		const {palettes} = this.props;
		return (
			<div className = 'palettelist'>
				<div className = 'container'>
					<div className = 'nav'>
						<Link to = '/palette/new'>Create New Palette</Link>
					</div>
					<div className = 'palettes'>
						{palettes.map(apalette => {
							let thePalette = apalette;
							return (
								<Link to = {`palette/${apalette.id}`}>
									<MiniPalettte {...thePalette} />
								</Link>
							)
						})
					}
					</div>				
				</div>
			</div>
		)
	}
}