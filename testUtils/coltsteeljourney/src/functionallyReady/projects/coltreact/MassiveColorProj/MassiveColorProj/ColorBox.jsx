import React from 'react'
import './ColorBox.css'
import {CopyToClipboard} from 'react-copy-to-clipboard'
import { Link } from 'react-router-dom';
import chroma from 'chroma-js'
/*import { withStyles } from '@mui/material/styles';
import { withStyles } from '@mui/material'; NO LONGER SUPPORTED*/

/*const styles = {
	copyText: {
		color: "purple"
	}
}*/

export class ColorBox extends React.Component {
	constructor(props){
		super(props);
		this.state = { copied:false }
		this.changeCopyState = this.changeCopyState.bind(this);
	}

	changeCopyState() {
		this.setState({copied:true},()=>{
			setTimeout(()=>this.setState({ copied:false }),1500);
		});
	}

	render() {
		const { background, name, moreUrl, showLink } = this.props;
		const { copied } = this.state;
		const urlPartToSubPalette = name.slice(0,-3);
		const isDarkColor = chroma(background).luminance() <= 0.05;
		const isLightColor = chroma(background).luminance() > 0.7;
		return (
			<CopyToClipboard text = {background} onCopy = {this.changeCopyState}>
				<div className = 'ColorBox' style = {{background}}>
					<div className = {`copy-overlay ${copied && "show"}`} style = {{background}} />
					<div className = {`copy-msg ${copied && "show"}`}>
						<h1>copied!</h1>
						<p className = {isLightColor && 'dark-text'} >{background}</p>
					</div>
					<div className = 'copy-container'>
						<div className = 'box-content'>
							<span className = {isDarkColor && 'light-text'}>{name}</span>
						</div>
						<button className = {`copy-button ${isLightColor && 'dark-text'}`}>Copy</button>
						{showLink && (
							<Link to = {moreUrl}  
								onClick = {e => e.stopPropagation()}
							>
								<span className = {`see-more ${isLightColor && 'dark-text'}`}>More</span>
							</Link>
							)}
					</div>
				</div>
			</CopyToClipboard>

		)
	}
}

/*export default withStyles(styles)(ColorBox)*/
export default ColorBox