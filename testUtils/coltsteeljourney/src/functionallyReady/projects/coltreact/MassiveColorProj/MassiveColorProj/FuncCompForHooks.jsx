/*using hooks in class components*/
import React from 'react'
import { useTheme } from '@mui/material/styles';

const FuncCompForHooks = WrappedComponent => props => {
  /*जो कोई भी hooks तुझे किसी भी class component मे use करने हो वे यहां लिख दे। */
  const thetheme = useTheme();

  return (
    <WrappedComponent
      {...props}
      {...{thetheme}}
    />
  );
};

export default FuncCompForHooks