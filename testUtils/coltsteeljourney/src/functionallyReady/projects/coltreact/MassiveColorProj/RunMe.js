import React from 'react'
import Palette from './MassiveColorProj/Palette'
import seedColors from './seedColors'
import { generatePalette } from './ColorHelper'
import {Routes, Route} from 'react-router-dom'
import DynamicRoute from './MassiveColorProj/DynamicRoute'
import PaletteList from './MassiveColorProj/PaletteList'
import NewPaletteForm from './MassiveColorProj/NewPaletteForm'

// import TestMaterialUI from './UIlibraries/MaterialUI/TestMaterialUI'
// import PLlandingPage from './UIlibraries/MaterialUI/PostLoginSectionProj/PLlandingPage'

export class RunMe extends React.Component {
  constructor(props) {
    super(props);
    this.state = { palettes: seedColors }
    this.savePalette = this.savePalette.bind(this);
  }

  savePalette(newPalette) {
    this.setState({ palettes: [...this.state.palettes, newPalette] });
  }

  render() {

    return (
      <div className = 'RunMe'>        
          {/*<Palette {...seedColors[4]} palette = {generatePalette(seedColors[4])} />*/}
          {/*<TestMaterialUI/>*/}
          {/*<PLlandingPage/>*/}
          <Routes>
            <Route path = '/' element = {<PaletteList palettes = {this.state.palettes}/>}/>
            <Route path = '/palette/new' element = {<NewPaletteForm savePalette = {this.savePalette}/>}/>
            <Route path = '/palette/:paletteId' element = {<DynamicRoute currentPalettes = {this.state.palettes} />}/>
            <Route path = '/palette/:paletteId/:subPaletteId' element = {<DynamicRoute currentPalettes = {this.state.palettes} />}/>
          </Routes>
      </div>  
    );
  }
}

export default RunMe