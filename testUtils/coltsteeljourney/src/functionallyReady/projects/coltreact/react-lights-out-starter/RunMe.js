/*01 what are the components we need
02 how many could we get away with 
03 how much state do we need to store
04 where do we store it*/

/*import './RunMe.css';*/
import React from 'react'
import LightsOut from './LightsOut';
/*import ProgressSteps from './ProgressSteps';*/

export class RunMe extends React.Component {
  render() {
    return (
      <div className = "RunMe">
        <LightsOut gridSize = {4} />
        {/*<ProgressSteps />*/}
      </div>
    )
  }
}

export default RunMe