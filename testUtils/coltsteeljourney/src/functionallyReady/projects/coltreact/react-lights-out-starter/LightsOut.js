import React from 'react'
import GridBlock from './GridBlock';
import './LightsOut.css'

export class LightsOut extends React.Component {
	static defaultProps = {
		gridSize : 4,
		colorScheme : ['blue', 'skyblue', 'orangered', 'orange']
	}

	constructor(props) {
		super(props);
		this.state = { 
			lightsOnStatus: Array.from({ length: this.props.gridSize*this.props.gridSize }).map(x => Math.floor(Math.random()*2) ? true : false),
			hasWon: false
			}
		
		this.toggleLight = this.toggleLight.bind(this);
	}

	toggleLight(arg) {
		console.log('received request to toggle lights of ',arg);
		this.setState(curState => {
			curState.lightsOnStatus.splice(arg-1, 1, (curState.lightsOnStatus[arg-1] === true ? false : true ));

			if(arg%this.props.gridSize!=1) {console.log('leftbox'); curState.lightsOnStatus.splice(arg-2, 1, (curState.lightsOnStatus[arg-2] === true ? false : true ));}
			if(arg%this.props.gridSize!=0) {console.log('rightbox'); curState.lightsOnStatus.splice(arg, 1, (curState.lightsOnStatus[arg] === true ? false : true ));}

			if(arg>this.props.gridSize) {console.log('topbox'); curState.lightsOnStatus.splice(arg-1-this.props.gridSize, 1, (curState.lightsOnStatus[arg-1-this.props.gridSize] === true ? false : true ));}
			if(arg+this.props.gridSize<=this.props.gridSize*this.props.gridSize) {console.log('bottombox'); curState.lightsOnStatus.splice(arg-1+this.props.gridSize, 1, (curState.lightsOnStatus[arg-1+this.props.gridSize] === true ? false : true ));}
			return { lightsOnStatus: [...curState.lightsOnStatus] }
		})
	}

	render() {
		const boxes = Array.from({ length: this.props.gridSize*this.props.gridSize }).map((x,idx) => {
				return (
					<>
						<div>
						<GridBlock 
							scheme = {this.props.colorScheme} 
							key = {idx+1} 
							blockId = {idx+1}
							gridSize = {this.props.gridSize}
							lightsOnStatus = {this.state.lightsOnStatus[idx]}
							toggleLight = {this.toggleLight}
						/>
						</div>
					</>
					);
				}
			);
		return (
			<div className = 'GameBox'>
				{boxes}
			</div>
		)
	}
}

export default LightsOut