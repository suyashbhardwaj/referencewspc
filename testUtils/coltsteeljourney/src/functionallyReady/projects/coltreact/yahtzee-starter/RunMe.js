 /*
 |
 |
 - <RunMe/>
"render the game component"
 */

import React, { Component } from "react";
import Game from "./Game";
import "./RunMe.css";

class RunMe extends Component {
  render() {
    return (
      <div className='RunMe'>
        <Game />
      </div>
    );
  }
}

export default RunMe;
