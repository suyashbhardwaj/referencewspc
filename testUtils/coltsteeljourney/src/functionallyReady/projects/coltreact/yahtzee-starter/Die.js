import React, { Component } from "react";
import "./Die.css";

class Die extends Component {
  static defaultProps = {
    numberWords: ['one','two','three','four','five','six'],
    val:5
  }

  constructor(props){
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(evt) {
    this.props.handleClick(this.props.idx);
  }

  render() {
    const { locked, val, numberWords, disabled, rolling } = this.props;
    let respClass = `Die fa-solid fa-dice-${numberWords[val-1]} fa-5x `;
    if (locked)  respClass += 'Die-locked ';
    if (rolling && !locked) {respClass += 'Die-rolling'}
    return ( <i className={respClass} onClick={this.handleClick} disabled = {disabled}/> );
  }
}

export default Die;
