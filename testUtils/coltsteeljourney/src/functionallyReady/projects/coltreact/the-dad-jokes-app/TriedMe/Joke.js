import React from 'react'
import './Joke.css'

export class Joke extends React.Component {
	constructor(props) {
		super(props);
		this.handleUpClick = this.handleUpClick.bind(this);
		this.handleDnClick = this.handleDnClick.bind(this);
	}

	handleUpClick(evt) {
		evt.preventDefault();
		this.props.upvote(this.props.jokeid);
	}

	handleDnClick() {
		this.props.downvote(this.props.jokeid);
	}

	render() {
		let styleringcolor = { borderColor: `#00${this.props.jokescore}00` }
		return (
			<div className = 'onejokediv'>
				<tr>
			        <td>
			        	<button onClick = {this.handleUpClick}> <i className="fa fa-arrow-up"/> </button>
			        	<div className = 'jokescorediv' style = {styleringcolor}>{this.props.jokescore}</div>
			        	<button onClick = {this.handleDnClick}> <i className="fa fa-arrow-down"/> </button>
			        </td>
			        <td>{this.props.jokedesc}</td>
			        <td><img src = {this.props.smiley} alt = 'something'/></td>
			    </tr>
			</div>
		)
	}
}

export default Joke