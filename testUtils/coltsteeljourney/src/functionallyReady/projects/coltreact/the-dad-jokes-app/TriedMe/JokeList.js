import React from 'react'
import axios from 'axios'
import Joke from './Joke'
import './JokeList.css'
import laugh from './laugh.svg';
import smile from './smile.svg';
import whatnonsense from './whatnonsense.svg';
import rofl from './rofl.png';

/*function removeDuplicates(arr) { 
	const hasId = (rec,id) => rec.value.data.id === id;
    return arr.filter((item, index) => arr.findIndex(hasId(index)) === index);
    }*/

export class JokeList extends React.Component {
	constructor(props) {
		super(props);
		this.state = { jokes: [] }
		this.upvote = this.upvote.bind(this);
		this.downvote = this.downvote.bind(this);
	}

	async componentDidMount() {
		let jokesBunch = [];
		if (localStorage.getItem("jokes")) {
			jokesBunch = JSON.parse(localStorage.getItem("jokes") || "[]");
			this.setState({ jokes: [...this.state.jokes, ...jokesBunch] })
			return;
		}
		
		const jokesPromises = [];
		let i=0;
		do {
			let jokeJSON = axios.get('https://icanhazdadjoke.com/', { headers: { Accept: 'application/json' } });
			jokesPromises.push(jokeJSON);
			i++;
		} while(i<10);

		await Promise.allSettled(jokesPromises)
			.then(results => {
				// removeDuplicates(results)
				results.forEach(res => jokesBunch.push({ id: res.value.data.id, jokedesc: res.value.data.joke, jokescore: 0, smiley: whatnonsense }));
			});
		console.log(jokesBunch);
		localStorage.setItem("jokes", JSON.stringify(jokesBunch));
		this.setState({ jokes: [...this.state.jokes, ...jokesBunch] })
	}

	componentDidUpdate(prevProps, prevState) {
		localStorage.setItem("jokes", JSON.stringify(this.state.jokes));
		// console.log('componentDidUpdate invoked with prevState as ', prevState.jokes)
	}

	upvote(jokeid) {
		let sortedJokes;
		let revisedJokes = this.state.jokes.map(jk => {
			if (jk.id === jokeid) { 
				jk.jokescore++;
				jk.jokescore > 10 ? jk.smiley = rofl : (jk.jokescore > 7 ? jk.smiley = laugh : (jk.jokescore > 0 ? jk.smiley = smile : jk.smiley = whatnonsense));
			}
			return jk;
		});
		sortedJokes = [...revisedJokes].sort((a,b) => b.jokescore - a.jokescore);
		this.setState({ jokes: [...sortedJokes] })
	}

	downvote(jokeid) {
		let sortedJokes;
		let revisedJokes = this.state.jokes.map(jk => {
			if (jk.id === jokeid) {
				jk.jokescore--;
				jk.jokescore > 10 ? jk.smiley = rofl : (jk.jokescore > 7 ? jk.smiley = laugh : (jk.jokescore > 0 ? jk.smiley = smile : jk.smiley = whatnonsense));
			}
			return jk;
		});
		sortedJokes = [...revisedJokes].sort((a,b) => b.jokescore - a.jokescore);
		this.setState({ jokes: [...sortedJokes] })
	}

	render() {
		const jokesFetched = this.state.jokes.map(jk => {
			return (
				<Joke 
					jokedesc = {jk.jokedesc}
					jokescore = {jk.jokescore}
					upvote = {this.upvote}
					downvote = {this.downvote}
					smiley = {jk.smiley}
					jokeid = {jk.id}
					key = {jk.id}
				/>
			)
		})
		return (
			<div className = 'modalspace'>
				<div className = 'leftblock'>
					<h2>Jokes at your disposal..</h2>
				</div>
				<div className = 'rightblock'>
					<table cellSpacing="0">
	            		<tbody>
	            			{jokesFetched}
	            		</tbody>
	          		</table>
				</div>
			</div>
		)
	}
}

export default JokeList