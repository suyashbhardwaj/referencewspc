import React from 'react'
import './RotatingNavInstructorVer.css'
import theimage from '../assets/images/paperCutoutSignatureBikeLBN.jpg'

export class RotatingNavInstructorVer extends React.Component {
	constructor(props) {
		super(props);
		this.state = { navOpen:false }
		this.handleNavToggle = this.handleNavToggle.bind(this);
	}

	handleNavToggle(evt) {
		this.setState( {navOpen:!this.state.navOpen});
	}

	render() {
		const navOpenClass = this.state.navOpen ? "container show-nav" : "container";
		return (
			<div>
				<div class={navOpenClass}>
					<div class="circle-container">
						<div class="circle">
							<button class="fa fa-times" id="close" onClick = {this.handleNavToggle}></button>
							<button class="fa fa-bars" id="open" onClick = {this.handleNavToggle}></button>
						</div>
					</div>
					<div class="content">
						<h1>Amazing Article</h1>
						<small>Florin Pop</small>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus reiciendis voluptates, a asperiores. Nostrum dolor, autem? Repellat provident doloribus voluptate amet. Nihil mollitia, consectetur laboriosam itaque iusto dolorem error alias modi doloribus sequi perspiciatis, quos! Quos repellendus delectus, reiciendis eligendi molestiae ab dignissimos, est odit quibusdam possimus similique commodi natus. Similique obcaecati totam nihil, quam non assumenda aut facilis eaque eum magnam expedita, optio nemo beatae impedit, perspiciatis minima fuga reiciendis eveniet aliquid. Quas commodi recusandae quisquam maiores vel repellat dolore earum, fugit eum. Nostrum, aspernatur, rem. Nemo debitis mollitia odit ab, minima hic impedit! Quidem eum iste numquam earum.</p>
						<h3>My Dog</h3>
						<img src={theimage} alt="the imagwa" />
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum, saepe laudantium autem nemo maiores accusantium earum cupiditate aliquam in corrupti cumque excepturi explicabo asperiores tempora repellendus sunt perferendis, dicta facere deleniti tenetur quis? Quos fuga, natus veniam modi? Perferendis nisi dolore ut ipsa est distinctio voluptas quaerat porro ad quisquam praesentium, totam officiis reiciendis fugit voluptatibus repellat deserunt corporis eligendi sunt molestias labore quae odit obcaecati? Laudantium optio accusantium rem recusandae nemo illum totam natus.</p>
					</div>
				</div>
				<nav>
					<ul>
						<li><i class="fa fa-home"></i>Home</li>
						<li><i class="fa fa-television"></i>About</li>
						<li><i class="fa fa-envelope"></i>Contact</li>
					</ul>
				</nav>
			</div>
		)
	}
}

export default RotatingNavInstructorVer