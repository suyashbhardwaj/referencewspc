import React from 'react'
import './RotatingNav.css'
import theimage from './paperCutoutSignatureBikeLBN.jpg'

export class RotatingNav extends React.Component {
	render() {
		// const rotorstyle = { transform: rotate(`-${15}deg`) }
		return (
			<div className = 'rotatingitems show-nav'>
				<div className = 'pagecontents'>
					<div className='sidenav'>
						<div className = 'navbar'>
							<button>Yo</button>
							<button>Yo</button>
							<button>Yo</button>
						</div>
					</div>
					<div className = 'article'>
						<h1>Amazing Article</h1>
						<h6><i>Florin Pop</i></h6>
						<p>
							<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis rem vel molestias fuga aliquid obcaecati nam laboriosam a, architecto consectetur accusantium, ducimus, atque iste quaerat voluptas, at corporis quasi cupiditate.</span>
							<span>Soluta reiciendis, omnis culpa eum quos odio similique obcaecati in, excepturi consectetur reprehenderit qui voluptatibus dolor neque quibusdam dolore repellat nam nulla atque nemo expedita ab ducimus porro pariatur. Explicabo.</span>
							<span>Officia officiis, sapiente atque sint eum delectus, dolore deleniti pariatur rerum repellat nesciunt velit earum cum impedit omnis quasi incidunt similique. Illum fugiat velit vero tenetur unde quas vitae harum.</span>
							<span>Esse quod quis libero velit magni amet vitae consequatur placeat sapiente beatae cum sed doloremque asperiores dolore atque aspernatur, aliquam incidunt alias, quaerat cumque architecto quo sit id. Velit, alias.</span>
						</p>
						<h4>My Dog</h4>
						<img src = {theimage}/>
						<p>
							<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis rem vel molestias fuga aliquid obcaecati nam laboriosam a, architecto consectetur accusantium, ducimus, atque iste quaerat voluptas, at corporis quasi cupiditate.</span>
							<span>Soluta reiciendis, omnis culpa eum quos odio similique obcaecati in, excepturi consectetur reprehenderit qui voluptatibus dolor neque quibusdam dolore repellat nam nulla atque nemo expedita ab ducimus porro pariatur. Explicabo.</span>
							<span>Officia officiis, sapiente atque sint eum delectus, dolore deleniti pariatur rerum repellat nesciunt velit earum cum impedit omnis quasi incidunt similique. Illum fugiat velit vero tenetur unde quas vitae harum.</span>
							<span>Esse quod quis libero velit magni amet vitae consequatur placeat sapiente beatae cum sed doloremque asperiores dolore atque aspernatur, aliquam incidunt alias, quaerat cumque architecto quo sit id. Velit, alias.</span>
						</p>
					</div>
				</div>
			</div>
		)
	}
}

export default RotatingNav