import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import SignaturePad from "react-signature-canvas";

const useStyles = makeStyles((theme) => ({
  canvas: {
    background: "#FAFBFC",
    border: "1px solid #D4E1F2",
    borderRadius: "3px",
    width: "100%",
    height: "300px"
  }
}));

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired
};

function SimpleDialog(props) {
  const classes = useStyles();

  const { open, signautreRef, onClose, onSave } = props;

  const handleClose = () => {
    onClose();
  };

  const handleSave = () => {
    onSave();
    onClose();
  };

  return (
    <Dialog
      fullWidth
      maxWidth="sm"
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <DialogTitle id="simple-dialog-title">Signature Pad</DialogTitle>
      <SignaturePad
        canvasProps={{
          className: classes.canvas
        }}
        ref={signautreRef}
      />
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Close
        </Button>
        <Button onClick={handleSave} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const SignatureDiv = () => {
	const [open, setOpen] = React.useState(false);
	const [imageURL, setImageURL] = React.useState(null);
  	const [pointsArray, setPointsArray] = React.useState(null);

  	const sigCanvas = React.useRef({});

	const handleClickOpen = () => {
    setOpen(true);
    setTimeout(() => {
      if (pointsArray) {
        sigCanvas.current.fromData(pointsArray);
      }
    });
  };

  const handleClose = () => {
    setOpen(false);
    setPointsArray(sigCanvas.current.toData());
  };

  const save = () => {
    setImageURL(sigCanvas.current.getTrimmedCanvas().toDataURL("image/png"));
  };

	return (
		<div>
	      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
	        Open Modal
	      </Button>
	      <SimpleDialog
	        signautreRef={sigCanvas}
	        open={open}
	        onClose={handleClose}
	        onSave={save}
	      />
	      
	      {imageURL ? (
	        <img
	          src={imageURL}
	          alt="my signature"
	          style={{
	            display: "block",
	            margin: "0 auto",
	            border: "1px solid black",
	            width: "150px"
	          }}
	        />
	      ) : null}
	    </div>
	)
}

export default SignatureDiv