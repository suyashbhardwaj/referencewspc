import React, { useEffect, useRef, useState } from 'react'
import './StarRating.css'

const StarRating = () => {
const inputStyles = {
	display: "none",
	color: "#ffca08",
	
}

const labelStyles = {
	backgroundColor: "green",
	color: "#222222",
	hover: {
		backgroundColor:"yellow"
	}
}
	return (
		<>
			<h1 className="text-center" alt="Simple">Simple Pure CSS Star Rating Widget Bootstrap 4</h1>
 				<div className="container" style={{background: "#4a4a4c"}}>
				        <div className="starrating risingstar d-flex justify-content-center flex-row-reverse">
				            <input type="radio" id="star5" name="rating" value="5" className='d-none'/><label for="star5" title="5 star">5</label>
				            <input type="radio" id="star4" name="rating" value="4" className='d-none'/><label for="star4" title="4 star" >4</label>
				            <input type="radio" id="star3" name="rating" value="3" className='d-none'/><label for="star3" title="3 star" >3</label>
				            <input type="radio" id="star2" name="rating" value="2" className='d-none'/><label for="star2" title="2 star" >2</label>
				            <input type="radio" id="star1" name="rating" value="1" className='d-none'/><label for="star1" title="1 star" >1</label>
				        </div>
				  </div>	
				<h2 className="text-center" alt="Simple"><a href="http://themastercut.co"/>Check also WordPress Plugins on TheMasterCut.co</h2>
		</>
	)
}

export default StarRating