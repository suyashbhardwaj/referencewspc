import React, { useState, useEffect } from 'react'

const RoughArea = () => {

    return (
        <>
          <h1>This is a rough area</h1>        
        </>
    )
}

export default RoughArea

{
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
// const initialState = {
//   appId:'d41d8cd98f00b204e9800998ecf8427e',
//   token: "",
//   transcript: "",
//   conversation: [],
//   chatInput: { value: "", error: "", name: "chatInput" },
//   enableUserInput: false,
//   inputOptions: null,
//   previousValue: [],
//   activeIntentId: null,
//   nextIntentId: null,
//   enableHuman: false,
//   selectValue: [],
//   initiallyShow:false,
//   msgLoading: { active: false, timer: 1500 },
//   reconnectChat: false,
//   agentAssigned: { status: false, assignedTo: "" },
// };

// const initWSResolveHandlerRef = useRef((value) => {return;});
// 	const initWSRejecthandlerRef = useRef((value) => {return;});
// 	const socketRef = useRef(null);
// 	const [numOfTimesWebsocketDisconnected, setNumOfTimesWebsocketDisconnected] = useState(0);
// 	const [isSocketOpen, setIsSocketOpen] = useState(false);
// 	const [state, setState] = useState(initialState);
// 	const [isOnline, setIsOnline] = useState(navigator.onLine);

// 	const onConnOpen = () => {
// 	/* purpose: reset numOfTimesWebsocketDisconnected to 0, now connection is open */
// 	    console.log("  > invoked onConnOpen()")
// 	    initWSResolveHandlerRef.current(true);
// 	    if (numOfTimesWebsocketDisconnected > 0) {
// 		    //console.warn("Attempt No: " + numOfTimesWebsocketDisconnected);
// 		      setNumOfTimesWebsocketDisconnected(0);
// 	    }
// 	};
// 	const onMessage = (data) => {
// 		console.log("  > invoked onMessage() with data as ", data);
// 	}
// 	const onConnClose = () => {
// 	    console.log("  > invoked onConnClose()")
// 	    setIsSocketOpen(false);
// 	    setState((prevState) => ({
// 	      ...prevState,
// 	      reconnectChat: true,
// 	    }));
// 	    setNumOfTimesWebsocketDisconnected((prev) => prev + 1);
// 	};
// 	const initWS = async () => {
// 	    console.log("  > invoked initWs()");
// 	    const promise = new Promise((resolve, reject) => {
// 	      initWSResolveHandlerRef.current = resolve;
// 	      initWSRejecthandlerRef.current = reject;
// 	    });
// 	    socketRef.current = new WebSocket('wss://dev.botgo.io/websocket/');
// 	    socketRef.current.onopen = onConnOpen;
// 	    socketRef.current.onmessage = onMessage;
// 	    socketRef.current.onclose = onConnClose;
// 	    return promise;
// 	  };

// 	useEffect(() => {
// 		console.log("initWs has been executed ");
// 		initWS().then((res)=>console.log("the res is ", res));
// 	}, []);
// 	useEffect(() => {
//     /*purpose: add a common event listener to window obj both for online & offline event triggers that sets/unsets up a state var - 'isOnline' to 
//     let us know of the internet connectivity status any given time. Remove the event listener post having taken down the status*/

//     console.log("  > invoked useEffect() ~ [isOnline] with isOnline having ", isOnline);
//     const handleStatusChange =()=> { 
//     	setIsOnline(
//     		(prevValue) => navigator.onLine, 
//     		(updatedValue) => {/*console.log({ isOnline: updatedValue*/ }
//     	);
//     }

//     window.addEventListener("online", handleStatusChange); // Listen to the online status
//     window.addEventListener("offline", handleStatusChange); // Listen to the offline status

//     return () => {
//       window.removeEventListener("online", handleStatusChange);
//       window.removeEventListener("offline", handleStatusChange);
//     };
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, [isOnline]);

 /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
}