import React, { useState } from 'react';

const ChatGPTDid = () => {
  const [rating, setRating] = useState(0);

  const rate = (stars) => {
    setRating(stars);
    resetStars();
    alert('Rated: ' + stars + ' stars');
  };

  const highlightStars = (stars) => {
    for (let i = 1; i <= stars; i++) {
      document.querySelector(`.star:nth-child(${i})`).classList.add('active');
    }
  };

  const resetStars = () => {
    for (let i = 1; i <= 5; i++) {
      document.querySelector(`.star:nth-child(${i})`).classList.remove('active');
    }
  };

  return (
    <div style={{ display: 'inline-block', fontSize: '24px' }}>
      {[1, 2, 3, 4, 5].map((index) => (
        <div
          key={index}
          className="star"
          onClick={() => rate(index)}
          onMouseOver={() => highlightStars(index)}
          onMouseOut={resetStars}
          style={{
            display: 'inline-block',
            margin: '0 4px',
            cursor: 'pointer',
            color: rating >= index ? '#ffdd00' : '#ccc',
          }}
        >
          ★
        </div>
      ))}
    </div>
  );
};

export default ChatGPTDid;