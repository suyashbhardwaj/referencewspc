import { fade } from "@material-ui/core/styles/colorManipulator";
/*style is a reference to a function that takes in a theme as a prop & returns an object with key-value pairs depicting style properties*/
/*theme has a lot of standardized specifications, take theme everywhere & observe uniformity & control on overall styling of application*/ 
const styles = theme => ({
  root: {
    width: "100%",
    marginBottom: 0
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    /*the below styles would apply from the small breakpoints & up, the theme coming to us as a prop features responsive design this way instead of media queries*/
    display: "none",                  /*do not show below small screen size*/
    [theme.breakpoints.up("sm")]: {
      display: "block"                /*show above small screen size*/
    }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),  /*theme.palette.common.colors lets us have easy uniformity of colors across application*/
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit,
      width: "auto"
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: "100%",
    position: "absolute",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  /*when you say padding: _ _ _ _ < these 4 values are supposed to be in clockwise order: that's for you to remember it next time you use it*/
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: 120,
      "&:focus": {
        width: 200  /*it grows as you focus on */
      }
    }
  }
});

export default styles;