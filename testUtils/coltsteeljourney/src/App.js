/*-------------------------------------------------------------------------------------*/
      /*<ThemeProvider>
        <LanguageProvider>
          <PageContent> इस <PageContent> के पास Navbar और Form, इसके props.children मे आ जायेंगे
            <Navbar/>
            <Form/>
          </PageContent>
        </LanguageProvider>
      </ThemeProvider>*/

/*-------------------------------------------------------------------------------------*/
/*const App = () => { return ( <Xpl_clientMutation/> ); };*/
/*-------------------------------------------------------------------------------------*/
/*const client = new ApolloClient({
    uri: 'http://localhost:4000',
    cache: new InMemoryCache(),
    })

  <ApolloProvider client = {client}>
      <Exz08_08client12 />
  </ApolloProvider>*/

/*======================================================================================*/
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
/*import Exz08_08client12 from './ForProject/fullstackopen/graphQL/exercises/Exz08_08client12/Exz08_08client12';*/

/*import ChatbotApp from './ForProject/chatbotbase/ChatbotApp';*/
/*import Navbar from './Navbar'
import Form from './Form'
import PageContent from './PageContent'*/

/*"ThemeContext द्वारा उप्लब्ध कराई गई props को ThemeProvider के माध्यम से nested components मे float करा देना है, उसके लिय import करो उसे" */
/*import ThemeProvider from './contexts/ThemeContext'
import LanguageProvider from './contexts/LanguageContext'*/

/*import RunMe from './functionallyReady/projects/coltreact/MassiveColorProj/RunMe';*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

import './App.css';
/*import TodoApp from './ThetodoApp/TodoApp';*/
/*import Xpl_clientMutation from './ForProject/fullstackopen/graphQL/explainers/xpl_clientMutation';*/
/*import RoughArea from './functionallyReady/snippets/RoughArea';*/
/*import ChatGPTDid from './functionallyReady/snippets/ChatGPTDid';*/
import SignatureDiv from './functionallyReady/snippets/SignatureDiv';
/*import React, { useState } from 'react';
import CaptchaDiv from './functionallyReady/snippets/CaptchaDiv';*/
import StarRating from './functionallyReady/snippets/StarRating';
import RenderForm from './ForProject/renderedformwa/RenderForm';

const sampleJSON = [
  {
    type: 'heading',
    formHeading: 'Organization Details Page',
    alignment: 'center',
    color: '#000000',
    size: 'h4'
  },
  {
    type: 'subheading',
    formSubHeading: 'Tell us a little bit about your business.'
  },
  {
    type: 'text',
    label: 'Organization:',
    placeholder: 'Organization Name'
  },
  {
    type: 'text',
    label: 'Registered Address:',
    placeholder: 'Registered Address'
  },
  {
    type: 'select',
    label: 'What is the employee size of your company?',
    color: '#000000',
    name: 'companySize',
    options: [
      { value: 'belowTwenty', text: 'Below 20' },
      { value: 'twentyOneToFifty', text: '21 to 50', selected: true },
      { value: 'fiftyOneToTwoHundred', text: '51 to 100' },
      { value: 'twoHundredOneToFiveHundred', text: '201 to 500' },
      { value: 'fiveHundredOneToThousand', text: '501 to 1000' },
      { value: 'thousandPlus', text: '1000+' }
    ]
  },
  {
    type: 'switch',
    label: 'Valid information',
    color: '#000000'
  },

  {
    type: 'checkbox',
    label: 'Gender',
    color: '#000000',
    name: 'checkbox',
    options: [
      { value: 'Male', text: 'Male' },
      { value: 'Female', text: 'Female' },
      { value: 'Other', text: 'Other' }
    ]
  },
  {
    type: 'divider',
    color: '#000000',
    thickness: 1
  },

  {
    type: 'radio',
    label: 'Select from these Options',
    color: '#000000',
    name: 'radio',
    options: [
      { value: 'Option A', text: 'Option A' },
      { value: 'Option B', text: 'Option B' },
      { value: 'Option C', text: 'Option C' }
    ]
  },

  {
    type: 'file',
    label: 'Upload File',
    color: '#000000',
    name: 'file',
    accept: '.pdf, .doc, .docx'
  },

  {
    type: 'button',
    buttonType: 'submit',
    id: 'kt_sign_up_submit',
    text: 'Next'
  }
];

const client = new ApolloClient({
    uri: 'http://localhost:4000',
    cache: new InMemoryCache(),
    });

const App = () => { 
  /*const [formElements, setFormElements] = useState(sampleJSON);*/

  return (
      <ApolloProvider client = {client}>
        {/*<Xpl_clientMutation/>*/}
        <RenderForm/>
      </ApolloProvider>
      /*<RoughArea />*/
      /*<TodoApp/>*/
      /*<SignatureDiv/>*/
      /*<CaptchaDiv/>*/
      /*<StarRating/>*/
    );
  };

export default App;

