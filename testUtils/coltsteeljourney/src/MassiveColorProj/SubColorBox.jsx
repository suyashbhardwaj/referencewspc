import React from 'react'
import './SubColorBox.css'
import {CopyToClipboard} from 'react-copy-to-clipboard'

export class SubColorBox extends React.Component {
	constructor(props){
		super(props);
		this.state = { copied:false }
		this.changeCopyState = this.changeCopyState.bind(this);
	}

	changeCopyState() {
		this.setState({copied:true},()=>{
			setTimeout(()=>this.setState({ copied:false }),1500);
		});
	}

	render() {
		const { background, name } = this.props;
		const { copied } = this.state;
		return (
			<CopyToClipboard text = {background} onCopy = {this.changeCopyState}>
				<div className = 'subcolorbox' style = {{backgroundColor:background}}>
					<div className = {`subcolorbox-copy-overlay ${copied && "show"}`} style = {{background}} />
					<div className = {`subcolorbox-copy-msg ${copied && "show"}`}>
						<h1>copied!</h1>
						<p>{background}</p>
					</div>
					<div className = 'subcolorbox-copy-container'>
						<div className = 'subcolorbox-box-content'>
							<span>{name}</span>
						</div>
						<button className = 'subcolorbox-copy-button'>Copy</button>
					</div>
				</div>
			</CopyToClipboard>
		)
	}
}

export default SubColorBox