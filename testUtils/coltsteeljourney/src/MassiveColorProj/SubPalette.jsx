import React from 'react'
import './SubPalette.css'
import Navbar from './Navbar'
import ColorBox from './ColorBox';
import PaletteFooter from './PaletteFooter'
import { Link } from 'react-router-dom';

export class SubPalette extends React.Component {
	constructor(props) {
		super(props);
		this.state = { format:'hex' }
		this.changeFormat = this.changeFormat.bind(this);
		this._shades = this.gatherShades(this.props.palette, this.props.colorId);
	}

	gatherShades(palette, colorToFilterBy) {
		let allColors = palette.colors;
		let shades = [];
		for(const level in allColors) {
			let colorMatch = allColors[level].find(obj=>obj.id.toLowerCase() === colorToFilterBy.toLowerCase());
			colorMatch.OfLevel = level;
			shades.push(colorMatch)
		}
		return shades.slice(1);
	}

	changeFormat(val) {
		console.log('format changed to ',val);
		this.setState({format:val})
	}

	render() {
		const {paletteName, id, colors, emoji} = this.props.palette;
		const {colorId} = this.props;
		const {format} = this.state;

		const colorBoxes = this._shades.map(shade => 
			<ColorBox
				background = {shade[format]}
				name = {`${colorId}-${shade.OfLevel}`}
				key = {`${colorId}-${shade}`}
			/>
			)

		return (
			<div className = 'subpalette'>
				<Navbar handleFormatChange = {this.changeFormat}/>
				<div className = 'subpalette-colors'> 
					{colorBoxes} 
					<div className = 'gobackdiv ColorBox'>
						<Link to = {`/palette/${id}`} className = 'back-button'>GO BACK</Link>
					</div>
				</div>
				<PaletteFooter paletteName = {paletteName} emoji = {emoji}/>
			</div>
		)
	}
}

export default SubPalette