import React from 'react';
import { Divider } from '@mui/material';
import Todo from './Todo';

const TodoList = (props) => {
	const { todos, removeTodo, toggleCheck, editTodo } = props;
	return (
		<div>
			{todos.map(todo => (
				<>
					<Todo 
						{...todo}
						key = {todo.id}
						removeTodo = {removeTodo}
						toggleCheck = {toggleCheck}
						editTodo = {editTodo}
					/>
					<Divider/>
				</>
				))}
		</div>
	)
}

export default TodoList