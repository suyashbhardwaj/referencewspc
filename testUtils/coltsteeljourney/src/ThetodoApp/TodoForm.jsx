import React from 'react'
import { Paper, styled } from '@mui/material';
import useInputState from './hooks/useInputState'
import TextField from '@mui/material/TextField';

const DemoPaper = styled(Paper)(({ theme }) => ({
  width: 'inherit',
  height: 60,
  padding: '0px',
  textAlign: 'center',
}));

const TodoForm = ({addTodo}) => {
	const [input, setInput, resetInput] = useInputState();
	return (
		<DemoPaper style = {{margin: '1rem 0', padding: '0 1rem'}}>
			<form onSubmit = {(evt)=> {
				evt.preventDefault();
				addTodo(input)
				resetInput();
			}}>
				<TextField
			        label="Size"
			        id="outlined-size-small"
			        defaultValue="Small"
			        size="small"

			        onChange = {setInput} 
					value = {input} 
					margin= 'normal' 
					label = 'Add a new Todo'
					fullWidth
					/>
			</form>
		</DemoPaper>
	)
}

export default TodoForm