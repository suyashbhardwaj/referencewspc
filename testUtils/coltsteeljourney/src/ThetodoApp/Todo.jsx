import { Checkbox, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText } from '@mui/material';
import { Delete, Edit } from '@mui/icons-material';
import useToggleState from './hooks/useToggleState';
import EditField from './EditField';

const Todo = ({task, completed, id, removeTodo, toggleCheck, editTodo}) => {
	const [isEditing, toggleEditMode] = useToggleState();
	return (
		<div>
			<List style = {{height:'20px', backgroundColor:'dimgrey'}}>
				<ListItem style = {{height:'inherit'}}>
					{isEditing ? (<EditField id={id} editTodo={editTodo} task={task} toggleEditMode = {toggleEditMode}/>) : (
						<>
						<Checkbox tabIndex ={-1} checked = {completed} onClick = {()=> toggleCheck(id)} />
						<ListItemText style = {{textDecorationLine: completed ? 'line-through': 'none' }}>{task}</ListItemText>
						<ListItemSecondaryAction>
							<IconButton onClick = {()=> removeTodo(id)}>
								<Delete/>
							</IconButton>
							<IconButton onClick = {()=> toggleEditMode()}>
								<Edit/>
							</IconButton>
						</ListItemSecondaryAction>
						</>
					)}
				</ListItem>
			</List>
		</div>
		)
	}

export default Todo