import { Form, Link, Outlet, useLoaderData } from "react-router-dom";
import { createContact, getContacts } from "../contacts";


export async function loader() {
  const contacts = await getContacts();
  return { contacts };
}

export async function action() {
  const contact = await createContact();
  return { contact };
}

export default function Root() {
  const { contacts } = useLoaderData(); /*special hook by react-router-dom to make data available at routes*/
  return (
    <>
      <div id="sidebar">
        <h1>React Router Contacts</h1>
        <div>
          <form id="search-form" role="search">
            <input
              id="q"
              aria-label="Search contacts"
              placeholder="Search"
              type="search"
              name="q"
            />
            <div
              id="search-spinner"
              aria-hidden
              hidden={true}
            />
            <div
              className="sr-only"
              aria-live="polite"
            ></div>
          </form>
          {/*<form method="post">
            <button type="submit">New</button>
          </form>*/}
          {/*यह <form> से भिन्न है special है, send post data to the receiver route's action instead*/}
          <Form method="post">
            <button type="submit">New</button>
          </Form>
        </div>
        <nav>
          {/*<ul>
            <li>*/}
              {/*<a href={`/contacts/1`}>Your Name</a>*/}
              {/*<Link to = {`/contacts/1`}>Your Name</Link>*/} {/*यह Link component page reload होने से बचा लेता है। ~ client-side routing*/}
            {/*</li>
            <li>*/}
              {/*<a href={`/contacts/2`}>Your Friend</a>*/}
              {/*<Link to = {`/contacts/2`}>Your Friend</Link>
            </li>
          </ul>*/}
        {contacts.length ? (
            <ul>
              {contacts.map((contact) => (
                <li key={contact.id}>
                  <Link to={`contacts/${contact.id}`}>
                    {contact.first || contact.last ? (
                      <>
                        {contact.first} {contact.last}
                      </>
                    ) : (
                      <i>No Name</i>
                    )}{" "}
                    {contact.favorite && <span>★</span>}
                  </Link>
                </li>
              ))}
            </ul>
          ) : (
            <p>
              <i>No contacts</i>
            </p>
          )}
        </nav>
      </div>
      <div id="detail"><Outlet/></div>
    </>
  );
}