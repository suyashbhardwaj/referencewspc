import { Form, useLoaderData } from "react-router-dom";
import { getContacts } from '../contacts';

/*  इस component के route के hit होने पर जो भी get params को <Form> component द्वारा पहुंचाया जायेगा वह यहां loader (जो की वहां main.jsx मे import किया हुआ है) मे स्वतः ही मिल जायेंगी । */
export async function loader({params}) {
  const contacts = await getContacts(params.contactId);
  return { contacts };
}

export default function Contact() {
  const contact = {
    first: "Your",
    last: "Name",
    avatar: "https://placekitten.com/g/200/200",
    twitter: "your_handle",
    notes: "Some notes",
    favorite: true,
  };

/*  इस component के route के hit होने पर जो भी post data <Form> component द्वारा पहुंचाया जायेगा वह यहां useLoaderData hook के माध्यम से प्राप्त कर लिया जायेगा। */
  // const { contact } = useLoaderData();
  console.log("contact received is ", contact);
  return (
    <div id="contact">
      <div>
        <img
          key={contact.avatar}
          src={contact.avatar || null}
        />
      </div>

      <div>
        <h1>
          {contact.first || contact.last ? (
            <>
              {contact.first} {contact.last}
            </>
          ) : (
            <i>No Name</i>
          )}{" "}
          <Favorite contact={contact} />
        </h1>

        {contact.twitter && (
          <p>
            <a
              target="_blank"
              href={`https://twitter.com/${contact.twitter}`}
            >
              {contact.twitter}
            </a>
          </p>
        )}

        {contact.notes && <p>{contact.notes}</p>}

        <div>
          <Form action="edit">
            <button type="submit">Edit</button>
          </Form>
          <Form
            method="post"
            action="destroy"
            onSubmit={(event) => {
              if (
                !confirm(
                  "Please confirm you want to delete this record."
                )
              ) {
                event.preventDefault();
              }
            }}
          >
            <button type="submit">Delete</button>
          </Form>
        </div>
      </div>
    </div>
  );
}

function Favorite({ contact }) {
  // yes, this is a `let` for later
  let favorite = contact.favorite;
  return (
    <Form method="post">
      <button
        name="favorite"
        value={favorite ? "false" : "true"}
        aria-label={
          favorite
            ? "Remove from favorites"
            : "Add to favorites"
        }
      >
        {favorite ? "★" : "☆"}
      </button>
    </Form>
  );
}