export interface ExReport{ periodLength: number,
  trainingDays: number,
  success: boolean,
  rating: number,
  ratingDescription: string,
  target: number,
  average: number }

export function calculateExercises(dailyExHours: number[], targetDailyHours: number): ExReport {
	let initialValue = 0;
	const netExHours = dailyExHours.reduce((accumulator, currentValue)=> accumulator + currentValue, initialValue);
	const dayCountEx = dailyExHours.filter(exhr => exhr > 0).length;

	return { periodLength: dailyExHours.length,
	  trainingDays: dayCountEx,
	  success: netExHours<targetDailyHours ? false: true,
	  rating: netExHours<targetDailyHours ? 2 : 5,
	  ratingDescription: netExHours<targetDailyHours ? 'not too bad but could be better' : 'बस हो गया बहुत, और के लेवेगा?',
	  target: targetDailyHours,
	  average: netExHours/dailyExHours.length };
	}

const logs = [3, 0, 2, 4.5, 0, 3, 1];
console.log(calculateExercises(logs, 2));