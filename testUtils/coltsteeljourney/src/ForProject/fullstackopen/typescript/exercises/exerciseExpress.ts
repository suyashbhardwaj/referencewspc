/*Exercise 9.4, 9.5, 9.6 & 9.7 in this program*/

import express = require('express');
import {calculateBmi} from './bmiCalculator';
import { calculateExercises, ExReport } from '../exercises/exerciseCalculator'


const app = express();
app.use(express.json());

app.get('/hello', (_req, res) => {
  res.send('Hello FullStack!');
});

const parseForNumber = (testvar: any) => {
	if(!isNaN(Number(testvar))) return testvar;
	else throw new Error('Provided value(s) were not numbers!');
}

app.get('/bmi', (_req, res) => {
  	try{
		const height: number = parseForNumber(_req.query.height);
		const weight: number = parseForNumber(_req.query.weight);
		const healthStatus: String = calculateBmi(height,weight)
		res.json({ height:height, weight: weight, healthStatus: healthStatus });
		}catch(error: unknown) {
			let errorMessage = 'Something bad happened.'
		  	if (error instanceof Error) { errorMessage += ' Error: ' + error.message; }
	  		console.log(errorMessage);
	  		res.json({ error: errorMessage });
			}
	});

app.post('/exercises', (req, res) => {
  try{  
  /*eslint-disable-next-line @typescript-eslint/no-unsafe-assignment*/
  const { daily_exercises, target } = req.body;
  
  if ( !daily_exercises || !target ) { res.status(400).send({ error: 'parameters missing'}); }
  else if (daily_exercises.find((ex:any)=>isNaN(Number(ex))) || isNaN(Number(target))) { res.status(400).send({ error: 'malformatted parameters'}) }
  
  /*eslint-disable-next-line @typescript-eslint/no-unsafe-assignment*/
  const result: ExReport = calculateExercises(daily_exercises, Number(target));
  
  res.send(result);
  } catch (error: unknown) {
    let errorMessage = 'There is an error: ';
    if (error instanceof Error) { errorMessage += error.message; }
    console.log(errorMessage); res.send(errorMessage);
    }
  });

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});