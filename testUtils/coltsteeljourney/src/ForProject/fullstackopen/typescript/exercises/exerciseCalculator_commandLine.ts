interface ExReport{ periodLength: number,
  trainingDays: number,
  success: boolean,
  rating: number,
  ratingDescription: string,
  target: number,
  average: number }

function calculateExercises(dailyExHours: number[], targetDailyHours: number): ExReport {
	console.log("received dailyExHours as ", dailyExHours);
	console.log("received targetDailyHours as ", targetDailyHours);
	let initialValue = 0;
	const netExHours = dailyExHours.reduce((accumulator, currentValue)=> accumulator + currentValue, initialValue);
	const dayCountEx = dailyExHours.filter(exhr => exhr > 0).length;

	return { periodLength: dailyExHours.length,
	  trainingDays: dayCountEx,
	  success: netExHours<targetDailyHours ? false: true,
	  rating: netExHours<targetDailyHours ? 2 : 5,
	  ratingDescription: netExHours<targetDailyHours ? 'not too bad but could be better' : 'बस हो गया बहुत, और के लेवेगा?',
	  target: targetDailyHours,
	  average: netExHours/dailyExHours.length };
	}

const parseArguments = (args: string[]): number[] => {
		args.shift();
		args.shift();
		if (!args.find((arg:any) => isNaN(arg))) { return args.map(arg=>Number(arg)); }
		else { throw new Error('Provided values were not numbers!');}
		}
		
// const logs = [3, 0, 2, 4.5, 0, 3, 1];
try {
	const argfeeds = parseArguments(process.argv);
	const secondarg = argfeeds.pop();
	console.log("Your report looks like this \n", calculateExercises(argfeeds, secondarg))
}catch(error: unknown)	{ let errorMessage = "Something bad happenned "; if(error instanceof Error) errorMessage += "Error: "+ error; console.log(errorMessage)}