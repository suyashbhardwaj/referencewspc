export type Gender = 'male' | 'female' | 'other';

export interface PatientEntry {
  id: string,
  name: string,
  dateOfBirth: string,
  ssn: string,
  gender: Gender,
  occupation: string
}

export interface DiagnosesEntry {
  code: string,
  name: string,
  latin?: string
}

/*यहां एक ऐसि typespecification export की जा रहीं है जो यह बतायेगा की जो data source किया जा रहा है उसमे comment field omitted(नही लेनी) है */
export type NonSensitivePatientEntry = Omit<PatientEntry, 'ssn'>;
export type newPatientEntry = Omit<PatientEntry, 'id'>;