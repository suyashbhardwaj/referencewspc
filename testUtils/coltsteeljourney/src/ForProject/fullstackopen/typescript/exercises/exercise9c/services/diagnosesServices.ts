/*उप्योग मे लाया जाने वाला पूरा application json data जहां से लिया जा र्हा है वही पहले से ही typedefined मिलेगा जिस्से की typescript check यहा issue न create करे */
import diagnosesEntries from '../data/diagnosesTypeChecked'
import { DiagnosesEntry } from '../types';

/*अब मिलने क्या वाला है - DiagnosesEntry वह तो specify करना ही पडेगा यहा इसैलिय उप्पर एक interface import कर लिया गया है। */
const diagnoses: DiagnosesEntry[] = diagnosesEntries;

/*क्योंकी typescript check कोई भी अतिरिक्त field पर check लगाने का काम नही करता इसैलिय सम्भव है की unnecessarily extra fields फ़ल्तू source हो जाये, वह map के द्वारा यहीं छांट ली जाएं  */
const getEntries = (): DiagnosesEntry[] => {  return diagnoses.map(({ code, name, latin }) => ({ code, name, latin }));};


const adddiagnoses = () => {
  return null;
};

export default {
  adddiagnoses,
  getEntries
};