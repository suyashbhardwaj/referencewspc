import { newPatientEntry } from './types';

export enum Gender {
  male = 'male',
  female = 'female',
  other = 'other',
}

const isString = (text: unknown): text is string => { return typeof text === 'string' || text instanceof String; };
const parseName = (name: unknown): string => {
  if (!name || !isString(name)) { throw new Error('Incorrect or missing name'); }
  return name;
};

const isDate = (date: string): boolean => { return Boolean(Date.parse(date)); };
const parseDOB = (date: unknown): string => {
  if (!date || !isString(date) || !isDate(date)) { throw new Error('Incorrect or missing date: ' + date); }
  return date;
};

const parseSSN = (ssn: unknown): string => {
  if (!ssn || !isString(ssn)) { throw new Error('Incorrect or missing ssn'); }
  return ssn;
};

const parseOccupation = (occupation: unknown): string => {
  if (!occupation || !isString(occupation)) { throw new Error('Incorrect or missing occupation'); }
  return occupation;
};

const isGender = (param: string): param is Gender => { return Object.values(Gender).map(v => v.toString()).includes(param); };
const parseGender = (gender: unknown): Gender => {
  if (!gender || !isString(gender) || !isGender(gender)) { throw new Error('Incorrect or missing gender: ' + gender); }
  return gender;
};

const parseFeed = (object: unknown): newPatientEntry => {
  if ( !object || typeof object !== 'object' ) { throw new Error('Incorrect or missing data'); }

  if ('name' in object && 'dateOfBirth' in object && 'ssn' in object && 'gender' in object && 'occupation' in object)  {
    const newEntry: newPatientEntry = {
      name: parseName(object.name),
	  dateOfBirth: parseDOB(object.dateOfBirth),
	  ssn: parseSSN(object.ssn),
	  gender: parseGender(object.gender),
	  occupation: parseOccupation(object.occupation),
    	/*name: "sample singh",
		dateOfBirth: "1966-29-02",
		ssn: "samplke124",
		gender: "other",
		occupation: "somejob",*/
    };

    return newEntry;
  }

  throw new Error('Incorrect data: some fields are missing');
};

export default parseFeed