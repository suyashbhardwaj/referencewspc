/*उप्योग मे लाया जाने वाला पूरा application json data जहां से लिया जा र्हा है वही पहले से ही typedefined मिलेगा जिस्से की typescript check यहा issue न create करे */
import patientEntries from '../data/patientsTypeChecked'
import { NonSensitivePatientEntry, PatientEntry, newPatientEntry } from '../types';
const { v1: uuid } = require('uuid')

/*अब मिलने क्या वाला है - DiaryEntry वह तो specify करना ही पडेगा यहा इसैलिय उप्पर एक interface import कर लिया गया है। */
const patients: PatientEntry[] = patientEntries;

/*क्योंकी typescript check कोई भी अतिरिक्त field पर check लगाने का काम नही करता इसैलिय सम्भव है की unnecessarily extra fields फ़ल्तू source हो जाये, वह map के द्वारा यहीं छांट ली जाएं  */
const getNonSensitiveEntries = (): NonSensitivePatientEntry[] => {  return patients.map(({ id, name, dateOfBirth, gender, occupation }) => ({ id, name, dateOfBirth, gender, occupation }));};

const addPatient = (newp: newPatientEntry): PatientEntry => {
  const patientRegistered: PatientEntry = { ...newp, id: uuid()};
  patients.push(patientRegistered);
  return patientRegistered;
};

export default {
  addPatient,
  getNonSensitiveEntries
};