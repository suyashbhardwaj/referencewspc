/*Exercises 9.8, 9.9, 9.10, 9.11 in this program*/
import express from 'express';
import patientRouter from './routes/patients';
import diagnosesRouter from './routes/diagnoses';
import cors from 'cors';


const app = express();

app.use(cors());
app.use(express.json());
app.use('/api/patients', patientRouter);
app.use('/api/diagnoses', diagnosesRouter);

const PORT = 3001;

app.get('/ping', (_req, res) => { res.send('pong');  });

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});