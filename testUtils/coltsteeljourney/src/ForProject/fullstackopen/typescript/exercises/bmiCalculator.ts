export const calculateBmi = (height: number, weight: number) : string => {
	height = height/100;
	const bmi: number = weight/(height*height);
	let status: string;

	if(bmi < 18.4) status = "abnormal (underweight)";
	else if (bmi < 24.9)	status = "normal (healthy weight)";
	else if (bmi < 39.9)	status = "abnormal (overweight)";
	else	status = "abnormal (obese)";
	
	return status;
	}

console.log(calculateBmi(178,65));