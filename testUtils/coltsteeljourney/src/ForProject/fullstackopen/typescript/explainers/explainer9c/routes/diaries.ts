/*सभी routes ने किस request पर करना क्या है वह सब यहां निर्देशित है। */
import express from 'express';
import toNewDiaryEntry from '../utils';

/*प्रत्येक route को अपने response render करने के लिय जो भी services/executioncode आवश्याक हैं वे यहां stored रहेंगे*/
import diaryService from '../services/diaryService'; 

const router = express.Router();

router.get('/', (_req, res) => { res.send(diaryService.getNonSensitiveEntries()); });

router.post('/', (_req, res) => { 
	try{
		const newDiaryEntry = toNewDiaryEntry(_req.body);
    	const addedEntry = diaryService.addDiary(newDiaryEntry);
		res.json(addedEntry);
		} catch (error: unknown) { let errorMessage = 'Something went wrong.'; if (error instanceof Error) {	errorMessage += ' Error: ' + error.message;	} res.status(400).send(errorMessage); }
	});

router.get('/:id', (_req, res) => { const diary = diaryService.findById(Number(_req.params.id)); diary ? res.send(diary) : res.sendStatus(404); });

export default router;