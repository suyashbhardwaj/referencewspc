export type Weather = 'sunny' | 'rainy' | 'cloudy' | 'windy' | 'stormy';
export type Visibility = 'great' | 'good' | 'ok' | 'poor';

export interface DiaryEntry {
  id: number;
  date: string;
  weather: Weather;
  visibility: Visibility;
  /*प्रश्न चिन्ह का माने है की यह field optional है। */
  comment?: string;
}

/*यहां एक ऐसि typespecification export की जा रहीं है जो यह बतायेगा की जो data source किया जा रहा है उसमे comment field omitted(नही लेनी) है */
export type NonSensitiveDiaryEntry = Omit<DiaryEntry, 'comment'>;
export type NewDiaryEntry = Omit<DiaryEntry, 'id'>;