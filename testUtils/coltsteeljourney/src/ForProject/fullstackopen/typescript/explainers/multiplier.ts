/*const multiplicator = (a, b, printText) => {
  console.log(printText,  a * b);
}

multiplicator("two", 4, 'Multiplied numbers 2 and 4, the result is:');*/




/*const multiplicator = (a: number, b: number, printText: string) => {
  console.log(printText,  a * b);
}

multiplicator("two", 4, 'Multiplied numbers 2 and 4, the result is:');*/




/*with command-line parameters*/
/*const multiplicator = (a: number, b: number, printText: string) => {
  console.log(printText,  a * b);
}

const a: number = Number(process.argv[2])
const b: number = Number(process.argv[3])
multiplicator(a, b, `Multiplied ${a} and ${b}, the result is:`);*/



/*क्योंकी runtime के दौरान क्या value arguments के माध्यम से हमारे पास आ जाये इसकी कोई निश्चितत्ता नही है & 
typeScript has no power to rescue us from this kind of situation इसीलिय its better to keep provisions to validate data proactively 
& throw errors against invalid data */
interface MultiplyValues {
  value1: number;
  value2: number;
}

const parseArguments = (args: string[]): MultiplyValues => {
  if (args.length < 4) throw new Error('Not enough arguments');
  if (args.length > 4) throw new Error('Too many arguments');

  if (!isNaN(Number(args[2])) && !isNaN(Number(args[3]))) {
    return {
      value1: Number(args[2]),
      value2: Number(args[3])
    }
  } else {
    throw new Error('Provided values were not numbers!');
  }
}

const multiplicator = (a: number, b: number, printText: string) => {
  console.log(printText,  a * b);
}

try {
  const { value1, value2 } = parseArguments(process.argv);
  multiplicator(value1, value2, `Multiplied ${value1} and ${value2}, the result is:`);
} catch (error: unknown) {
  let errorMessage = 'Something bad happened.'
  if (error instanceof Error) {     /*क्योकी error instanceOf Error type narrowing क्र् रखा है इसीलिया error का इस्तेमाल बतौर् variable हम अब कर सकते है */
    errorMessage += ' Error: ' + error.message;
  }
  console.log(errorMessage);
}