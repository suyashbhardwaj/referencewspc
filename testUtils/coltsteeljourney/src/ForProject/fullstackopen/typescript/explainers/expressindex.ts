/*This is an explainer code from the section 9b of typescript from fullstackopen*/
import cors from 'cors';
import express = require('express');
import { calculator, Operation } from '../explainers/operation';
const app = express();

app.use(cors()); 
app.use(express.json());

app.get('/ping', (_req, res) => {
  res.send('pong');
});

app.post('/calculate', (req, res) => {
  try{  
  /*eslint-disable-next-line @typescript-eslint/no-unsafe-assignment*/
  const { value1, value2, op } = req.body;
  if ( !value1 || isNaN(Number(value1)) ) {    res.status(400).send({ error: '...'});  }
  if ( !value2 || isNaN(Number(value2)) ) {    res.status(400).send({ error: '...'});  }
  /*eslint-disable-next-line @typescript-eslint/no-unsafe-assignment*/
  const result = calculator(Number(value1), Number(value2), op as Operation);
  res.send({ result });
  } catch (error: unknown) {
    let errorMessage = 'There is an error: ';
    if (error instanceof Error) { errorMessage += error.message; }
    console.log(errorMessage); res.send(errorMessage);
    }
  });

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});