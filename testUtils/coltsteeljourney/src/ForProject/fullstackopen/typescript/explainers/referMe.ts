const birthdayGreeter = (name: string, age: number): string => {
  return `Happy birthday ${name}, you are now ${age} years old!`;
};

const birthdayHero = "Jane User";
const age = 22;

console.log(birthdayGreeter(birthdayHero, age));

/*Type inference would lead to the return type as Number*/
const add = (a: number, b: number) => {
  /* The return value is used to determine
     the return type of the function */
  return a + b;
}

/*npm run ts-node file.ts -- -s --someoption*/
/*https://www.typescriptlang.org/play/index.html*/ /*online playground for typescript*/
