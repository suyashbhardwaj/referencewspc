import { gql } from '@apollo/client';

export const ALL_AUTHORS = gql`
query {
	allAuthors {
		name
		born
		bookCount
	}
}
`;

export const ALL_BOOKS = gql`
query {
	allBooks {
		title
		published
		author
		id
	}
}
`;

export const CREATE_BOOK = gql`
mutation createBook($title: String!, $published: Int!, $author: String, $genres: [String!]!) {
  addBook(
  	title: $title
  	published: $published
	author: $author
  	genres: $genres
  ) {
    published
    title
	author
	id
  }
}
`;

export const UPDATE_AUTHOR = gql`
  mutation updateAuthor($naam: String!, $janmvarsh: Int!) {
    editAuthor(name: $naam, setBornTo: $janmvarsh) {
      name
      id
      born
      bookCount
    }
  }
`;