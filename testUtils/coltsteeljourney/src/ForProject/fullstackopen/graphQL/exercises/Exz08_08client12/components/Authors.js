/*Exercise 8.08, 8.11 in this program - थोडा फ़ालतू ही बना दिया*/
import { useMutation, useQuery } from '@apollo/client';
import { ALL_AUTHORS, UPDATE_AUTHOR } from './Exzqueries'
import { useState } from 'react';

const UpdateForm = () => {
  const [error, setError] = useState(null);
  const [ updateAuthor, result ] = useMutation(UPDATE_AUTHOR, {
    refetchQueries: [ALL_AUTHORS],
    onError: (error) => { const messages = error.graphQLErrors[0].message; setError(messages); }
  })
  
  const [naam, setNaam] = useState("");
  const [janmvarsh, setJanmvarsh] = useState(0);

  const submit = async (event) => {
    event.preventDefault();
    updateAuthor( {variables: {naam, janmvarsh}} );
    setnaam("");
    setJanmvarsh(0);
  }

  return(
      <>
        <hr/>
        <h4>set birthyear</h4>
        <form onSubmit={submit}>
          <div>
            name
            <input
              value={naam}
              onChange={({ target }) => setNaam(target.value)}
            />
          </div>

          <div>
            born
            <input
              type="number"
              value={janmvarsh}
              onChange={({ target }) => setJanmvarsh(parseInt(target.value))}
            />
          </div>

          <button type="submit">update author</button>
      </form>
      <div style={{color: 'red'}}> {error} </div>
      </>
    );
}

const AuthorsList = ({authors}) => {
  return (
      <>
      <h4>browser showing {authors.length} authors with buttons</h4>
      {authors.map(auth => (
        <div>
          <div style= {{
              width:'300px', 
              height:'30px', 
              backgroundColor:'darkslategray', 
              color:'white',
              border: "1px dotted white",
              textAlign: "center"
            }}>{auth.name} | {auth.born}</div>
        </div>
        ))}
      </>
    )}

const Authors = (props) => {
  if (!props.show) {
    return null
  }
  const authors = []
  const response = useQuery(ALL_AUTHORS);
  if (response.loading) { return <div>loading...</div> }
  response.data.allAuthors.forEach(author => authors.push(author));

  return (
    <div>
      <h2>authors</h2>
      <AuthorsList authors = {authors}/>
      <UpdateForm/>
    </div>
  )
}

export default Authors
