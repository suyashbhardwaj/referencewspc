/*Exercise 8.10 in this program*/
import { useState } from 'react'
import { useMutation } from '@apollo/client';
import { CREATE_BOOK, ALL_BOOKS, ALL_AUTHORS} from './Exzqueries'

const NewBook = (props) => {
  const [ createBook ] = useMutation(CREATE_BOOK, {
    refetchQueries: [ ALL_BOOKS, ALL_AUTHORS ],
    onError: (error) => { const messages = error.graphQLErrors[0].message; setError(messages); }
    }); 

  const [title, setTitle] = useState('')
  const [author, setAuthor] = useState('')
  const [published, setPublished] = useState(0)
  const [genre, setGenre] = useState('')
  const [genres, setGenres] = useState([])
  const [error, setError] = useState(null);

  if (!props.show) {
    return null
  }

  const submit = async (event) => {
    event.preventDefault()
    createBook( {variables: {title, published, author, genres}} );
    setTitle('')
    setPublished(0)
    setAuthor('')
    setGenres([])
    setGenre('')
  }

  const addGenre = () => {
    setGenres(genres.concat(genre))
    setGenre('')
  }

  return (
    <div>
      <form onSubmit={submit}>
        <div>
          title
          <input
            value={title}
            onChange={({ target }) => setTitle(target.value)}
          />
        </div>
        <div>
          author
          <input
            value={author}
            onChange={({ target }) => setAuthor(target.value)}
          />
        </div>
        <div>
          published
          <input
            type="number"
            value={published}
            onChange={({ target }) => setPublished(parseInt(target.value))}
          />
        </div>
        <div>
          <input
            value={genre}
            onChange={({ target }) => setGenre(target.value)}
          />
          <button onClick={addGenre} type="button">
            add genre
          </button>
        </div>
        <div>genres: {genres.join(' ')}</div>
        <button type="submit">create book</button>
      </form>
      <div style={{color: 'red'}}> {error} </div>
    </div>
  )
}

export default NewBook