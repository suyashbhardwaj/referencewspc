/*Exercise 8.09 in this program*/
import { useQuery } from '@apollo/client';
import { ALL_BOOKS } from './Exzqueries'
const Books = (props) => {
  if (!props.show) {
    return null
  }

  const books = [];
  const response = useQuery(ALL_BOOKS);
  if (response.loading) { return <div>loading...</div> }
  response.data.allBooks.forEach(book => books.push(book));

  return (
    <div>
      <h2>books</h2>

      <table>
        <tbody>
          <tr>
            <th></th>
            <th>author</th>
            <th>published</th>
          </tr>
          {books.map((a) => (
            <tr key={a.title}>
              <td>{a.title}</td>
              <td>{a.author}</td>
              <td>{a.published}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default Books
