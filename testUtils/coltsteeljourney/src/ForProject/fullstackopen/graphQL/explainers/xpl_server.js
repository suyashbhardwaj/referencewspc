// This one is the explainer code
const { v1: uuid } = require('uuid')
const { GraphQLError } = require('graphql')

const { ApolloServer } = require('@apollo/server')
const { startStandaloneServer } = require('@apollo/server/standalone')

// THE DATA: in json form
let persons = [
  {
    name: "Arto Hellas",
    phone: "040-123543",
    street: "Tapiolankatu 5 A",
    city: "Espoo",
    id: "3d594650-3436-11e9-bc57-8b80ba54c431"
  },
  {
    name: "Matti Luukkainen",
    phone: "040-432342",
    street: "Malminkaari 10 A",
    city: "Helsinki",
    id: '3d599470-3436-11e9-bc57-8b80ba54c431'
  },
  {
    name: "Venla Ruuska",
    street: "Nallemäentie 22 C",
    city: "Helsinki",
    id: '3d599471-3436-11e9-bc57-8b80ba54c431'
  },
]

/* THE MAPPING SCHEMATICS: OF DATA, OF QUERY, OF QUERY WITH ENUMS, OF MUTATIONS (ADD, UPDATE): i.e how the data is 
formatted, what all can be inquired, what all updations in data can be made ~ basically, API contracts*/
const typeDefs = `
    type Address {
        street: String!
        city: String!
    }

    type Person {
        name: String!
        phone: String
        address: Address!
        id: ID!
    }

    enum YesNo {YES NO}

    type Query {
        personCount: Int!
        allPersons(phone: YesNo): [Person!]!
        findPerson(name: String!): Person
    }

    type Mutation {
        addPerson(
          name: String!
          phone: String
          street: String!
          city: String!
        ): Person
        
        editNumber(
            name: String!
            phone: String!
            ): Person
    }
`

// EXECUTIONERS OF QUERY: how are we gonna go about each query, mutation, data accesses specified in the schemas
const resolvers = {
    Query: {
        personCount: () => persons.length,
        // allPersons: () => persons,
        allPersons: (root, args) => {
            if (!args.phone) { return persons }
            const byPhone = (person) => args.phone === 'YES' ? person.phone : !person.phone
            return persons.filter(byPhone)
            },
        findPerson: (root, args) => persons.find(p => p.name === args.name)
    },
  
    /*default resolvers for each field of the Person object are automatically made by graphQL internally, however, for user defined
    field like Address, we've got to define our own resolver for the same.*/
    Person: {
        address: (root) => {
            return { street: root.street, city: root.city }
            }
    },

    Mutation: {
        addPerson: (root, args) => {
            if (persons.find(p => p.name === args.name)) {
                throw new GraphQLError('Bhai, Name must be unique!', { extensions:{code: 'BAD_USER_INPUT', invalidArgs: args.name} })
                }
            const person = { ...args, id: uuid() }
            persons = persons.concat(person)
            return person
        },
        editNumber: (root, args) => {
            const person = persons.find(p => p.name === args.name)
            if (!person) {
              return null
            }
        
            const updatedPerson = { ...person, phone: args.phone }
            persons = persons.map(p => p.name === args.name ? updatedPerson : p)
            return updatedPerson
          } 
    }
}

// THE QUERY ITSELF: in an agreed to format to trigger executioners (~resolvers) both for enquiring & mutating the data
/*
    { 
        "variables": {"<vkey01>":"<vVal01>"},
        "query": "query <queryAlias>($aliasvKey01: <typespec>) { <queryName>(vKey01: $aliasvKey01) {<retfield01> <retfield02> ..} }"
    }
*/

/*prepare a reference to a server script with an endpoint aimed to receive graphQl POST requests that are intended to be catered to by the 
resolvers being provided to you here alongwith the needed typeDefs to be assisted meanwhile*/
const server = new ApolloServer({
  typeDefs,
  resolvers,
})

/*"start a standalone server (the one from the @apollo/server library) with listening parameters - port needs 2b 4000.
And, make sure that the server waiting for requests sitting over there understands the typeDefs & their corrosponding
resolvers (as is being described in the 'server' reference). now if this promise gets fulfilled then just console log 
that the server is ready at whatever url the Promise gives you back"*/
startStandaloneServer(server, {
  listen: { port: 4000 },
}).then(({ url }) => {
  console.log(`Server ready at ${url}`)
})