/*using useQuery hook from @apollo/client library to make query (this ensures firing of query upon component's render)
give names to queries, dynamically give the parameters to query using GraphQL variables */
/*Note*/ //despite the same query being done again by the code, the query is not sent to the backend, it is cached

import { gql, useMutation, useQuery } from '@apollo/client'
import React, { useEffect, useState } from 'react'
import { ALL_PERSONS, FIND_PERSON, CREATE_PERSON, EDIT_NUMBER } from './allqueries';

const Notify = ({errorMessage}) => {
  if ( !errorMessage ) {return null}  
  return (<div style={{color: 'red'}}> {errorMessage} </div> )
}

// this, Persons component, dedicated to just the display of persons, now, on the trigger of setting up of a state variable.
const Persons = ({ persons }) => {
  const [nameToSearch, setNameToSearch] = useState(null)
  // nameToSearch state var के set होने पर ही (जो की button press करने पर होगा) query fire होगी, अन्यथा नही होगी।  
  const result = useQuery(FIND_PERSON, { variables:{nameToSearch}, skip: !nameToSearch})
  if (nameToSearch && result.data) {
    return(
      <Person 
        person={result.data.findPerson}
        onClose={() => setNameToSearch(null)}
      />
      )
    }
  return (
    <div>
      <h2>Persons</h2>
      {persons.map(p =>
        <div key={p.name}>
          {p.name} {p.phone}
          {/* यह एक फ़ालतू add कर दिया बस।  */}
          <button onClick={() => setNameToSearch(p.name)}> show address </button>
        </div>  
      )}
    </div>
  )
}

/*display the address details of a person.*/
const Person = ({ person, onClose }) => {
  return (
    <div>
      <h2>{person.name}</h2>
      <div>
        {person.address.street} {person.address.city}
      </div>
      <div>{person.phone}</div>
      <button onClick={onClose}>close</button>
    </div>
  )
}

/*display a form to the user to feed in new person entry*/
const PersonForm = ({ setError }) => {
  const [name, setName] = useState('')
  const [phone, setPhone] = useState('')
  const [street, setStreet] = useState('')
  const [city, setCity] = useState('')

  /*here, createPerson is gonna refer to a function that'll eventually cause the mutation
  -is made available as the 1st element of the array being returned by the useMutation hook here*/
  const [ createPerson ] = useMutation(CREATE_PERSON, {
    refetchQueries: [ {query: ALL_PERSONS} ], /*pass as many queries to refetchQueries as you wish to reflect*/
    onError: (error) => { const messages = error.graphQLErrors[0].message; setError(messages); }
    }); 
  
  const submit = (event) => {
    event.preventDefault()

    // name, phone, street, city query variables है। 
    /*fire the query that corrosponds to the createPerson function with arguments as defined in passed json. refetch queries & error exceptions
      have already been taken care of*/
    createPerson({  variables: { name, phone, street, city } })
    setName('')
    setPhone('')
    setStreet('')
    setCity('')
  }

  return(
    <div>
      <h2>create new</h2>
      <form onSubmit={submit}>
        <div>
          name <input value={name}
            onChange={({ target }) => setName(target.value)}
          />
        </div>
        <div>
          phone <input value={phone}
            onChange={({ target }) => setPhone(target.value)}
          />
        </div>
        <div>
          street <input value={street}
            onChange={({ target }) => setStreet(target.value)}
          />
        </div>
        <div>
          city <input value={city}
            onChange={({ target }) => setCity(target.value)}
          />
        </div>
        <button type='submit'>add!</button>
      </form>
    </div>
  );
}

const PhoneForm = ({ setError }) => {
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [ changeNumber, result ] = useMutation(EDIT_NUMBER)
  const submit = (event) => {
    event.preventDefault()
    changeNumber({ variables: { name, phone } })
    setName('')
    setPhone('')
  }

  useEffect(() => {
    if (result.data && result.data.editNumber === null) { setError('person not found') } 
  }, [result.data], setError)

  return(
    <div>
      <h2>change number</h2>

      <form onSubmit={submit}>
        <div>
          name <input
            value={name}
            onChange={({ target }) => setName(target.value)}
          />
        </div>
        <div>
          phone <input
            value={phone}
            onChange={({ target }) => setPhone(target.value)}
          />
        </div>
        <button type='submit'>change number</button>
      </form>
    </div>
  );  
}

/*--------------------------------------------------------------------------------------
	display the interactive persons list, 
	display a form to create new person, & 
	display a form to update contact. Also would notify of error
----------------------------------------------------------------------------------------*/
const Xpl_clientMutation	 = () => {
	const [errorMessage, setErrorMessage] = useState(null) 
	const result = useQuery(ALL_PERSONS)
	if (result.loading) { return <div>loading...</div> }

	/*flashes error messages for 10s*/
	const notify = (message) => {
		console.log("received a message by notify()", message);
		setErrorMessage(message);
		setTimeout(() => {setErrorMessage(null)}, 10000)  
		}

	return (
		<div>
	        <Notify errorMessage={errorMessage} />
	        <Persons persons={result.data.allPersons}/>
	        <hr/>
	        <PersonForm setError={notify}/> {/* PersonForm मे notify() का trigger होगा useMutation hook के माध्यम से।  */}
	        <PhoneForm setError={notify}/>	
		</div>
		)
	}

export default Xpl_clientMutation