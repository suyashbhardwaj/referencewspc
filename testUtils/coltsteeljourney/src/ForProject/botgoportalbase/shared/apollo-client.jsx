// Use ApolloProvider from @apollo/client library to float the client obj to all the components as a prop by wrapping the root App component
import { ApolloClient, ApolloProvider,InMemoryCache, gql } from '@apollo/client'

const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: 'http://localhost:4000',
  })

export default client