import React, { useEffect } from 'react'
import { AUTH_TOKEN, BOTGO_USER } from '../../constants'

export function Logout() {
	useEffect(() => {
		localStorage.removeItem(AUTH_TOKEN);
		localStorage.removeItem(BOTGO_USER);
		document.location.reload();
	}, []);

	return <h1>See You Soon</h1>
}