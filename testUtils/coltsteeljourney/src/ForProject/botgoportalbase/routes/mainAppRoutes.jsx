import React from 'react'
import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom'
import App from '../components/app/app'
import PrivateRoutes from './PrivateRoutes';
import { AuthPage, Logout } from '../components/auth';
import { AUTH_TOKEN } from '../constants';
import ErrorPage from '../pages/ErrorPage/ErrorPage';

/*[clueless]*/const { PUBLIC_URL } = process.env;

const mainAppRoutes = () => {
	const isAuthorized = localStorage.getItem(AUTH_TOKEN);

	return (
		<>
			<BrowserRouter basename = {PUBLIC_URL}>
				<Routes>
					<Route element = {<App/>} />
						{isAuthorized ? (
							<>
								<Route path = "/*" element = { <PrivateRoutes/> }/>	
								<Route index element = { <Navigate to = "/bots/all-bots"/> }/>	
								<Route path = "logout" element = { <Logout/> }/>	
							</>
							):(
							<>
								<Route path = "auth/*" element = { <AuthPage/> }/>	
								<Route path = "*" element = { <Navigate to = "/auth"/> }/>	
							</>
							)}
					<Route path = "error/*" element = { <ErrorPage/> }/>	
				</Routes>
			</BrowserRouter>
		</>
	)
}

export default mainAppRoutes