import React, { createContext, useEffect, useState } from 'react'
import configFile from "../presets/config.json"
import { CHATBOT_ID, ENVIRONMENT } from "../env";
import axios from 'axios';

const defaultValue = configFile;
export const ConfigContext = createContext(defaultValue);

export const ConfigProvider = ({children}) => {
	const [config, setConfig] = useState(defaultValue);
	useEffect(() => {
		async function fetchConfig() {
			let appId;
			if(ENVIRONMENT === "production") { appId = window.botgo._globals.appId;	}
			else { appId = CHATBOT_ID ? CHATBOT_ID : ""	}
			axios
				.get(`${process.env.REACT_APP_GET_CONFIG_URL}/?applicationId=${appId}`)
				.then((res) => {
					const config = JSON.parse(res.data.body).config;
					if(!!config) {
						setConfig(config);
						if(!!config.theme.colorPalette) {
							const colorNames = Object.keys(config.theme.colorPalette);
							const colorValues = Object.values(config.theme.colorPalette);
							colorNames.forEach((colorName, index) => {
								document.documentElement.style.setProperty(
									`--${colorName}`,
									colorValues[index]
									);
								});
							}
						}
					})
			}
		fetchConfig();
		}, []);

	return ( <ConfigContext.Provider value = {config}> {children} </ConfigContext.Provider> );
}

export const ConfigConsumer = ConfigContext.Consumer;