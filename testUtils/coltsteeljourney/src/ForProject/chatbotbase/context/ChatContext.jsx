import React, { createContext, useContext, useEffect } from 'react'
import { CHATBOT_ID, ENVIRONMENT } from '../env';
import { useStateWithCallback } from '../hooks/useStateWithCallback';
import { ConfigContext } from './configContext';

const initialState = {
	appId: ENVIRONMENT === "produciton"
		? window.botgo._globals.appId
		: CHATBOT_ID
		? CHATBOT_ID 
		: "",
	token: "",
	transcript: "",
	conversation: [],
	chatInput: { value: "", error: "", name: "chatInput" },
}

const defaultChatContext = {
	state: initialState,
}

export const ChatContext = createContext(defaultChatContext);

export const ChatProvider = ({children}) => {
	const [state, setState] = useStateWithCallback(initialState);
	const [isOnline, setIsOnline] = useStateWithCallback(navigator.online);
	const config = useContext(ConfigContext);
	const toggleFloatIcon = () => {console.log("toggle pressed")}

	useEffect(() => {}, []);
	useEffect(() => {}, []);
	useEffect(() => {}, []);
	useEffect(() => {}, []);
	useEffect(() => {}, []);
	useEffect(() => {}, []);
	useEffect(() => {}, []);
	useEffect(() => {}, []);

	return (
		<ChatContext.Provider value = {
			{
				state,
				isOnline,
				config,
				toggleFloatIcon
			}
		}> {children} </ChatContext.Provider>
	)
}

export default ChatContext