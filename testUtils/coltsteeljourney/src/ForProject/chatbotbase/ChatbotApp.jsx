import React from 'react'
import { ConfigProvider } from './context/configContext';
import { ChatProvider } from './context/ChatContext';
import Chatbot from './components/chatbot/chatbot';

const ChatbotApp = () => {
	/*if (!(typeof(window).botgo === undefined)) {*/
	return (
		<ConfigProvider>
			<ChatProvider>
				<div className = 'chatapp'> <Chatbot /> </div>
			</ChatProvider>
		</ConfigProvider>
		);
	/*}*/
	/*else { window.location.href = "https://globtierinfotech.com/chatbot-landing"; return null; }*/
}

export default ChatbotApp