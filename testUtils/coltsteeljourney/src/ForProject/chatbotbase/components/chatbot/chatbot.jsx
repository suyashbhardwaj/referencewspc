import React, { useContext } from 'react'
import ChatContext from '../../context/ChatContext';

const Chatbot = () => {
	const {state, isOnline, config, toggleFloatIcon} = useContext(ChatContext);
	const overlayStyles = {
		filter: "blur(4px)",
		pointerEvents: "none" };
	const handleMarkMessageAsRead = () => {
		if (flagToTriggerMarkMessageAsRead === true && unreadMessagesArray.length > 0) {
			const messageIds = unreadMessagesArray.map(message => message.messageId);
			markMessageAsRead(messageIds);
			setFlagToTriggerMarkMessageAsRead(false);
			setUnreadMessagesArray([]);
		}};

	return (
		<div>
			{/*<div
				id = "chat-circle"
				onClick = {toggleFloatIcon}
				className = "btn btn-raised"
				>
				<h4>Everything will come soon. Have patience!</h4>
				<img alt="icon" src = {config.theme.iconHref} className = "IconHref" />
			</div>
			<div className = {state.initiallyShow ? "form-popup" : "form-popup hide" } id = "myForm">
				<div class="card">
					<div class="pt-2 card-header d-flex justify-content-between align-items-center">
						<div class="d-flex justify-content-between align-items-center">
							<img src= {config.theme.headerAvatarStyle} alt="header-avatar" style={config.theme.headerAvatar} />
							<div class="d-flex flex-column align-items-start">
								<h1 class="h5 mb-0 font-weight-normal flaot-left headerTitle">{config.theme.headerTitle}</h1>
								<OnOffStatus />
							</div>
						</div>
						
						<div class="d-flex flex-row-reverse justify-content-xxl-between">
							{
								!config.theme.hideCloseButton && (
									<button onClick = {toggleFloatIcon} className = "window-close">
										<img src = "https://chatbot-project.s3" />
									</button>
									)
							}
							<button class="undo" onClick={restart}>
								<img src="https://chatbot-project.s3" alt="" />
							</button>
						</div>
					</div>

					<div class="chat-body">
						<div class="chat-container">
							{
								!isOnline ? (
									<>
										<div style={overlayStyles}>
											<IntentRenderer
												previousValue={state.previousValue}
												token={state.token}
												conversation={state.conversation}
												trigger={trigger}
												onButtonClicked={onButtonClicked}
											/>
										</div>
									</>
									) : (
									<>
										<IntentRenderer
											previousValue={state.previousValue}
											token={state.token}
											conversation={state.conversation}
											trigger={trigger}
											onButtonClicked={onButtonClicked}
										/>
									</>
									)
							}
							<div style={{ width: "100%", height:"1.25rem"}}>&nbsp</div>
							<div style={{ width: "100%", height:"2px"}} ref={bottomRef}>&nbsp</div>
						</div>
					</div>
					<div class="chat_glb mb-3">
						<a href="http://botgo.io/" target="_blank" rel="noreferrer">Botgo</a>
					</div>
				</div>
			</div>*/}
		yoyo
		</div>
	)
}

export default Chatbot