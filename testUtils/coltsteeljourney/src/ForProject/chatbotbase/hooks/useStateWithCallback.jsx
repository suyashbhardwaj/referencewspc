/*यह हमारा एक hook है */
import { useEffect, useRef, useState } from 'react';

export const useStateWithCallback = (initialState) => {
	const [state, setState] = useState(initialState);
	/*const callbackRef = useRef();
	const handleSetState = (updatedState, callback) => { callbackref.current = callback; setState(updatedState); };

	useEffect(() => {
    	if (typeof callbackRef.current === "function") {
	      	callbackRef.current(state);
	      	callbackRef.current = undefined;
	    	}
  		}, [state]);

	return [state, handleSetState];*/
	return [state, setState];
}