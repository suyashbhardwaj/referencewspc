import React, { useEffect, useState, useRef } from 'react';
import { io } from 'socket.io-client'
import './IOsocketClient.css';

let connectionId = "no connection for now";
const mysocket2 = io('http://localhost:4000');
/*भाइ 'mysocket2' नाम के socket, अपने connect event के listener के trigger होने पर console par message likh dena/messageToDisplayRef का state फ़लाना text 'connection has been established' से set कर दियो */
mysocket2.on('connect', () => {connectionId = mysocket2.id; console.log("connection is made to a girl", connectionId);});

let count = 0;
setInterval(()=> {
	mysocket2.emit('ping', ++count)
}, 1000);

/*एक socket भाई हमारा adminSocket भी रहेगा जिस पर connect करने के लिय authorization token लगेगा, इस socket पर connect_error raise होने पर वह error display कर दिया जायेगा। धन्यवाद!*/
const adminSocket = io('http://localhost:4000/admin', {auth: { token: 'mainhundon'} });
adminSocket.on('connect_error', error => {
	messageToDisplayRef.current = error;
})

const IOsocketClient = () => {
    const [room, setRoom] = useState("");
    const [message, setMessage] = useState("");
    const [messageToSend, setMessageToSend] = useState("");
    const messageToDisplayRef = useRef(`connection to the girl - ${connectionId} has been establisheD`);

    const handleClick2Send = (event) => { 
    	event.preventDefault(); 
    	mysocket2.emit('msgFromClient',`${message} -to roomNo ${room}`, room);
    	if(message !== "") setMessageToSend(message); 
    	setMessage("");
    	mysocket2.on('serveracknowledgement', (themessage) => { messageToDisplayRef.current = `${messageToDisplayRef.current} \n ${themessage}` });
    	};

    const handleClickToJoinARoom = (event) => { 
    	event.preventDefault(); 
    	mysocket2.emit('join-room', room, msg2callback => {messageToDisplayRef.current = msg2callback});
    	alert(`you have requested to join ${room}`); };

    useEffect(() => {
    	return () => {
    		console.log('websocket component refreshed with no dependency!');
    	};
    }, [])

    useEffect(() => {
    	messageToDisplayRef.current = `${messageToDisplayRef.current} ${'\n'} ${messageToSend}`;
    }, [messageToSend])

	return (
		<div className='myinterface'>
	        <h4>websocket implementation</h4><hr />
	        <div>
	            <div className='message-container'>{messageToDisplayRef.current}</div>
	            <form id='form'>
	                <table>
	                	<tbody>
	                    <tr>
	                    	<td> <label htmlFor="message-input">Message</label> </td>
	                        <td> <input type="text" value = {message} onChange = {(e) => setMessage(e.target.value)} id='message-input'/> 
	                         	<button onClick={handleClick2Send}>Send</button> 
	                         	<button onClick={()=> mysocket2.connect()}>Connect</button> </td>
	                    </tr>
	                    <tr>
	                    	<td> <label htmlFor="room-input">Room</label> </td>
	                        <td> <input type="text" value={room} onChange={(e) => setRoom(e.target.value)} id='room-input'/> 
	                        	<button onClick={handleClickToJoinARoom} >Join</button> 
	                        	<button onClick={()=> mysocket2.disconnect()}>Disconnect</button> </td>
	                    </tr>
	                    </tbody>
	                </table>
	            </form>
	        </div>
	    </div>
		);
};

export default IOsocketClient