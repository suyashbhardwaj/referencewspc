import React from 'react'
import { useState } from 'react';
/*import {io} from 'socket.io-client'*/
import './SocketIOImpl.css'

const SocketIOImpl = () => {
    // const room = 'kwcwmcc_n69-4UHJAAAG';   // is basically the socket.id ~ i.e. represents a single chat client/converted into a room
    // const msg2bdisplayedonjoinbyserver = 'wow! you have joined the rrom you had wished to';
    // const messageToSend = "Hi! Just testing if it works!";
    // const socket = io('http://localhost:3001'); //get me an io socket that would connect us with the provided url
    // socket.on('connect', ()=> setConnected(true));
    // socket.on('connect', ()=> console.log(`you connected with an id ${socket.id}`));
    // socket.emit('myevent',10,'hi',{'name': 'yash'});
    // socket.emit('sendMsgToServerEvt', messageToSend);   //doing an emit here sends a message to server side socket
    // socket.emit('sendMsgToServerEvt', messageToSend, room);   //doing an emit here sends a message to server side socket
    // socket.emit('joinroom', room);
    // socket.emit('sendMsgToServerEvt', messageToSend, room);   //doing an emit here sends a message to server side socket with a specific room id(~socket.id of some client)
    // socket.on('receiveMsgFromServerEvt', message => console.log('a message received from server socket is ', message));
    // socket.emit('joinroom', room, msg2bdisplayedonjoinbyserver => {console.log('client system got this executed by the server: ', msg2bdisplayedonjoinbyserver)});
    // -------------------------------------------------------------------------------------

    const [messageToSend, setMessageToSend] = useState("");
    const [room, setRoom] = useState("");
    const [messageToDisplay, setMessageToDisplay] = useState("");
    // const socket = io('http://localhost:3001'); //get me an io socket that would connect us with the provided url
    
    const handleClick2Send = (event) => { 
        event.preventDefault(); /*socket.emit('sendMsgToServerEvt', messageToSend)*/; 
        console.log("messageToSend has been updated to ", messageToSend);
        setMessageToDisplay(`${messageToDisplay} ; ${messageToSend}`);
    }

    const handleClickToJoinARoom = (event) => {
        event.preventDefault(); 
        /*socket.emit('joinroom', room, msg2bdisplayedonjoinbyserver => {
            setMessageToDisplay(`client system got this executed by the server: ${msg2bdisplayedonjoinbyserver}`)
        });*/
        console.log("room has been updated to ", room);
     }

    // socket.on('receiveMsgFromServerEvt', message => setMessageToDisplay(`a message received from server socket is: ${message}`));

    return (
    <div className='myinterface'>
        <h4>websocket implementation</h4><hr />
        {/* {connected && <h6>{`you connected with an id ${socket.id}`}</h6>} */}
        <div>
            <div className='message-container'>{messageToDisplay}</div>
            <form id='form'>
                <table>

                    <tr><td> <label htmlFor="message-input">Message</label> </td>
                        <td> <input type="text" value={messageToSend} onChange={(e) => setMessageToSend(e.target.value)} id='message-input'/> 
                        <button onClick={handleClick2Send}>Send</button> </td>
                    </tr>

                    <tr><td> <label htmlFor="room-input">Room</label> </td>
                        <td> <input type="text" value={room} onChange={(e) => setRoom(e.target.value)} id='room-input'/> 
                        <button onClick={handleClickToJoinARoom} >Join</button> </td>
                    </tr>

                </table>
            </form>
        </div>
    </div>
  )
}

export default SocketIOImpl
