import React from 'react'
import Grid from '@mui/material/Grid';
import { BasicSnackbar } from '../../components/reusables/BasicSnackbar/BasicSnackbar';
import { MyButton } from '../../components/reusables/MyButton';
import UserTable from '../../components/UserTable/UserTable'
import { BasicCard } from '../../components/reusables/BasicCard';

export class Security extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = { open:false }
	    this.handleClick = this.handleClick.bind(this);
	    this.handleClose = this.handleClose.bind(this);
	  }

	  handleClick(evt) {
	    this.setState({open:true})
	  }

	  handleClose(evt, reason) {
	    if (reason === 'clickaway') { return; }
	    this.setState({open:false})
	  }

	render() {
		return (
			<Grid item xs={4}>
				This is the security page.
				<MyButton variant="outlined" onClick={this.handleClick}>
		          Open success snackbar
		        </MyButton>
				<BasicSnackbar
					open = {this.state.open}
					onClose = {this.handleClose}
					severity = 'warning'
					message = 'A sex siren is on the way! Beware..'
				/>
				<BasicCard header = 'जानकारी लो' content = {<UserTable onError = {()=> this.setState({open:true})}/>} />
				<BasicSnackbar 
					open = {this.state.open} 
					severity = 'error' 
					message = 'data could not be fetched' 
					onClose = {this.handleClose}/>
			</Grid>
		)
	}
}

export default Security