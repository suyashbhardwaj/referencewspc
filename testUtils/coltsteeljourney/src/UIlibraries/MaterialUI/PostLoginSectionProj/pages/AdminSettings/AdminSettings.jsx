import React from 'react'
import Header from './Header'
import BasicCard from '../../components/reusables/BasicCard'
import SearchBar from '../../components/reusables/SearchBar'
import GridWrapper from '../../components/reusables/GridWrapper'
import MyButton from '../../components/reusables/MyButton'
import NewUserModalF from '../../components/Modals/NewUserModalF'
import RefreshIcon from '@mui/icons-material/Refresh';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';

export class AdminSettings extends React.Component {
	constructor(props){
		super(props);
		this.state = { open: false, users:[], searchResults:[] }
		this.addUser = this.addUser.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.addNewUser = this.addNewUser.bind(this);
		this.handleSearchChange = this.handleSearchChange.bind(this);
	}

	addUser(evt) {
		this.setState({ open:true })
	}

	handleClose(evt) {
		this.setState({ open:false })
	}

	addNewUser(data) {
		console.log('received in parent component- ',data)
		this.state.users.push(data);
		this.setState({ open:false });
	}

	handleSearchChange(val) {
				this.filteredData(val);
			}

	filteredData(value) {
		const lowerCasedValue = value.toLowerCase().trim();
		if (lowerCasedValue === '') this.setState({searchResults: this.state.users})
		else {
			const filteredData = this.state.users.filter(user => {
				return Object.keys(user).some((key)=> user[key].toString().toLowerCase().includes(lowerCasedValue));
			});
			this.setState({users:filteredData});
		}
	}

	componentDidMount() {
		this.setState({searchResults: this.state.users})
	}

	render() {
		const getHeader = () => {

			const headerStyles = {
				wrapper: {
					display:'flex',
					alignItems:'center',
					justifyContent:'space-between',
					paddingLeft: '20px',
					paddingRight: '20px',
					height:'65px',
					backgroundColor:'#f5f5f5',
					borderBottom: '1px solid rgba(0,0,0,0.12)'
				},
				addUserButton: {
					fontSize:'0.65rem'
				}
			}

			return (
				<Box sx = {headerStyles.wrapper}>
					<SearchBar 
						placeholder = 'Enter a search key' 
						onChange = {(event) => this.handleSearchChange(event.target.value)} 
						searchBarWidth = '500px'
					/>
					<Box sx = {{display:'flex'}}>
						<MyButton 
							variant = 'contained'
							onClick = {this.addUser} 
							size = 'large' 
							sx = {headerStyles.addUserButton}>
								Add User
						</MyButton>
						<IconButton>
							<RefreshIcon/>
						</IconButton>
					</Box>
				</Box>
				)
		}
		const getContent = () => {
			return(
				<>
				{this.state.users.length ? this.state.users.map(user => (
						<Box sx = {{marginBottom: '10px'}}>
							<Typography>UserID: {user.userId}</Typography>
							<Typography>EmailID: {user.email}</Typography>
							<Typography>PhoneNumber: {user.phoneNumber}</Typography>
						</Box>
					))
					:
					<Typography 
						align = 'center' 
						sx = {{
							margin: '40px 16px',
							color: 'rgba(0,0,0,0.6)',
							fontSize:'1.3rem'
						}}>
							No users for this project yet.
					</Typography>
				}
				</>
			);
		}
		return (
			<>
			<Header title = 'AdminSettings'/>
			<GridWrapper>
				<BasicCard 
					header = {getHeader()} 
					content = {getContent()}
				/>
				<NewUserModalF open = {this.state.open} onClose = {this.handleClose} addNewUser = {this.addNewUser}/>
			</GridWrapper>
			</>
		)
	}
}

export default AdminSettings