import { createTheme } from '@mui/material/styles';

export const postloginTheme = createTheme({
  components: {
    // Name of the component
    MuiButton: {
      defaultProps: {
      	disabled:true,
      	disableRipple:true,
      	variant:'outline',
      	color:'secondary'
      },
      styleOverrides: {
        // Name of the slot
        root: {
          // Some CSS
          fontSize: '1rem',
        },
      },
    },
  },

  palette: {
  	primary: {
  		main: '#bada55'
  	}
  },

  typography: {
  	fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },

  spacing: [0,4,8,16,32,64],

});