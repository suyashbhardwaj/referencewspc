import React from 'react'
import BasicModal from '../reusables/BasicModal/BasicModal'
import Box from '@mui/material/Box';
import Input from '@mui/material/Input';
import { TextField } from '@mui/material';
import * as yup from 'yup';
import withRouter from '../withRouter';

export class NewUserModal extends React.Component {
	render() {
		const { open, onClose, useForm, yupResolver } = this.props;
		const modalStyles = {
			inputFields: {
				display:'flex',
				flexDirection:'column',
				marginTop: '20px',
				marginBottom:'15px',
				'.MuiInput-root': {
					marginBottom:'20px'
				}
			}
		}

		const validationSchema = yup
		  .object()
		  .shape({
		    userId: yup.string().required('user ID has to be there').min(6,'UserId has to be atleast 6 characters long'),
		    email: yup.string().required('email is required').email('entered mail id is invalid'),
		    phoneNumber: yup.string()/*.matches(phoneRegEx,'entered invalid phone number')*/
		  });

		const { register, handleSubmit, formState: {errors} } = this.props.useForm({ resolver: this.props.yupResolver(validationSchema) });

		const getContent = () => {
			return (
				<Box sx={modalStyles.inputFields}>
				    <Input placeholder = 'Email' />
				    <Input placeholder = 'Phone no' />
				    <Input placeholder = 'User ID' />
				    <TextField
				    	placeholder = 'UserID'
				    	name = 'userId'
				    	label = 'User ID'
				    	required
				    	error
				    	// helperText = 'There is an error'
				    	{...register('userId')}
				    	error = { errors.userId? true: false }
				    	helperText = { errors.userId?.message }
				    />
				</Box>
				)
		}
		return (
			<BasicModal
				open = {open}
				handleClose = {onClose}
				title = 'New User'
				subtitle = "Fill out inputs and hit the 'submit' button"
				content = {getContent}
				onSubmit = {()=>{}}
			>
			</BasicModal>
		)
	}
}

export default withRouter(NewUserModal)