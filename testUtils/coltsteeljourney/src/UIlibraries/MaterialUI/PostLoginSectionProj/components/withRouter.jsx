import React from 'react'
import { useNavigate } from 'react-router-dom'
import { useTheme } from '@mui/material/styles';

const withRouter = WrappedComponent => props => {
	const navigate = useNavigate();
	const navogate = (msg) => {alert(msg)}
	/*और जो कोई भी hooks तुझे किसी भी class component मे use करने हो वे यहां लिख दे। */
	const thetheme = useTheme();

	return (
		<WrappedComponent
			{...props}
			{...{navigate, navogate, thetheme}}
		/>
	);
};

export default withRouter