import React from 'react'
import DataTable from '../reusables/DataTable/DataTable';

export class UserTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = { users:[] }
	}

	async componentDidMount() {
	  	await fetch('https://jsonplaceholder.typicode.com/users')
	      .then(response => response.json())
	      .then(json => this.setState({ users:json }))
	      .catch(()=>this.props.onError())
	  }

	render() {
		const columns = [
			{ field: 'id', headerName: 'UserID', width: 150 },
			{ field: 'name', headerName: 'Name', width: 150 },
			{ field: 'username', headerName: 'UserName', width: 150 },
			{ field: 'email', headerName: 'EmailId', width: 300 },
		];	

		const userTableStyles = {
			width:'70vw', 
			height: '80vh'
		}

		return (
			<div>
				<DataTable 
					rows = {this.state.users} 
					columns = {columns}
					loading = {!this.state.users.length}
					mystyleoverride = {userTableStyles}
					/>	
			</div>
		)
	}
}

export default UserTable