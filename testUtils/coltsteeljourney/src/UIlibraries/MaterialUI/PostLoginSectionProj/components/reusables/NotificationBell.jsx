import React from 'react'
import Badge from '@mui/material/Badge';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import BasicMenu from './BasicMenu'

export class NotificationBell extends React.Component {
	render() {
		const { onClick, handleClose, anchorEl, iconColor, badgeContent } = this.props;
		return (
			<div>
				<Tooltip title={`You have ${badgeContent} notifications`}>
					<IconButton aria-label="notifications" onClick = {onClick}>
						<Badge badgeContent={badgeContent} color={iconColor}>
					      <NotificationsIcon color="action" />
					    </Badge>
				    </IconButton>
				</Tooltip>
				<BasicMenu anchorEl = {anchorEl} handleCloseMenu = {handleClose}/>
			</div>
		)
	}
}

export default NotificationBell