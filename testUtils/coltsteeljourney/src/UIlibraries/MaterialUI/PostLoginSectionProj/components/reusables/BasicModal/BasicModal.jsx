import React from 'react'
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import MyButton from '../MyButton'
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {modalStyles} from './styles'
import Input from '@mui/material/Input';

export class BasicModal extends React.Component {
	render() {
		const style = {
		  position: 'absolute',
		  top: '50%',
		  left: '50%',
		  transform: 'translate(-50%, -50%)',
		  width: 400,
		  bgcolor: 'background.paper',
		  border: '2px solid #000',
		  boxShadow: 24,
		  p: 4,
		};

		const { open,handleClose,title,subtitle,content,onSubmit } = this.props;

		return (
			<div>
			    <Modal open={open} onClose={handleClose}>
			    	<Box sx = {modalStyles.wrapper}>
				    	<Typography variant="h6" component="h2">
				            {title}
				          </Typography>
				        <Typography sx={{ mt: 2 }}>
				        	{subtitle}
				          </Typography>

				        {content}
				        
				        <Box sx = {modalStyles.buttons}>
				        	<MyButton variant = 'contained' onClick = {onSubmit}> Submit</MyButton>
				        	<MyButton onClick = {handleClose}>Cancel</MyButton>
				        </Box>
				    </Box>
			    </Modal>				
			</div>
		)
	}
}

export default BasicModal