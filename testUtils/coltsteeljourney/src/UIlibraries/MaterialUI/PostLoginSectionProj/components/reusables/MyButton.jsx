import React from 'react'
import Button from '@mui/material/Button';

export class MyButton extends React.Component {
	static defaultProps = {
		children:'',
		color:'primary',
		disabled:false,
		size:'',
		variant:'contained',
		styleoverride:'',
		onClick:null
	}
	render() {
		const {variant,size,styleoverride,disabled,color,children,onClick} = this.props;
		return (
			<div>
				<Button 
					variant = {variant}
					color = {color}
					disabled = {disabled}
					size = {size}
					sx = {styleoverride}
					onClick = {onClick}
				>{children}</Button>
			</div>
		)
	}
}

export default MyButton