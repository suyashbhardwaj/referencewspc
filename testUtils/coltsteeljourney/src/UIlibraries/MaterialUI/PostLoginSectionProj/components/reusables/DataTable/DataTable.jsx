import React from 'react'
import { DataGrid } from '@mui/x-data-grid';

export class DataTable extends React.Component {
	constructor(props){
		super(props);
		this.state = { pageSize:2 }
	}
	render() {
		const {rows, columns, loading, mystyleoverride} = this.props;
		return (
			<DataGrid 
				sx = {mystyleoverride} 
				rows = {rows} 
				columns = {columns}
				loading = {loading}
				checkboxSelection
				pagination
				pageSize = {this.state.PageSize}
				onPageSizeChange = {(newPageSize)=>this.setState({pageSize:newPageSize})}
				rowsPerPageOptions = {[2,5,10]}
				/>
		)
	}
}

export default DataTable