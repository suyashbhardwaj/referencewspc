import React from 'react'
import Button from '@mui/material/Button';

export class TestMaterialUI extends React.Component {
	render() {
		return (
			<div>
				<Button 
					variant = 'contained'
					color='secondary'
					onClick = {()=> console.log('clicked')}
				>YoYO</Button>
			</div>
		)
	}
}

export default TestMaterialUI