import logo from './logo.svg';
import './App.css';
import ProgressSteps from './Projects/ProgressSteps/ProgressSteps';

function App() {
  return (
    <div className="App">
      <ProgressSteps />
    </div>
  );
}

export default App;
