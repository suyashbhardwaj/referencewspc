import React from 'react'
import ColorBox from './ColorBox'
import './Palette.css'
import Navbar from './Navbar'
import PaletteFooter from './PaletteFooter'

export class Palette extends React.Component {
	constructor(props) {
		super(props);
		this.state = { level:500, format:'hex' }
		this.changeLevel = this.changeLevel.bind(this);
		this.changeFormat = this.changeFormat.bind(this);
	}

	changeLevel(level) {
		this.setState({level});
	}

	changeFormat(val) {
		console.log('format changed to ',val);
		this.setState({format:val})
	}

	render() {
		const {colors, paletteName, emoji, id} = this.props.palette;
		const {level,format} = this.state;
		const colorBoxes = colors[level].map(color => 
			<ColorBox 
				background = {color[format]} 
				name = {color.name}
				key = {color.id}
				moreUrl = {`/palette/${id}/${color.id}`}
				showLink
			/>
			)
		return (
			<div className = 'Palette'>
				<Navbar level = {level} changeLevel = {this.changeLevel} handleFormatChange = {this.changeFormat} showingColors/>
				<div className = 'Palette-colors'> {colorBoxes} </div>
				<PaletteFooter paletteName = {paletteName} emoji = {emoji}/>
			</div>
		)
	}
}

export default Palette