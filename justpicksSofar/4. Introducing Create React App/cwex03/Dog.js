import React, {Component} from 'react'
import './Dog.css'
import mapillustration from './map.png'

export class Dog extends Component {
	render() {
		return (
			<div className = 'Dog'>
				<h1>Dog!</h1>
				<img className = 'illustration' src = {mapillustration} />
			</div>
		)
	}
}

export default Dog