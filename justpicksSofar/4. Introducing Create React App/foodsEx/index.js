import fooditems from './foods'
import { choice, remove } from './helpers'

console.log(`I would like to have one ${choice(fooditems)} please`);
let remaining = remove(fooditems, '🍿');
console.log('Delicious! May I have another?');
console.log(`I am sorry. We are all out. We are left out with ${remaining} fruits only`);