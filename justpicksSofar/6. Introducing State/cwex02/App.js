import './App.css';
import Demo from './Demo'

function App() {
  
  return (
    <div className="App">
      <Demo animal = 'dog' food = 'bone'/>
    </div>
  );
}

export default App;
