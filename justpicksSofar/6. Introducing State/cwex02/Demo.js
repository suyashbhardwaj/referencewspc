import React from 'react'

export class Demo extends React.Component {
	constructor(props) {
		// super();
		super(props);	/*जब super को props दोगे तो ही constructor function मे props मिलेंगी।*/ 
		console.log('within constructor this.props is ', this.props); //would show undefined!
		/*if we wish to have access to this.props right here in the constructor then we constructor
				then we have to have props passed into super()*/
	}
	render() {
		console.log('within render this.props is ', this.props);
		return (
			<div>
				<h1>DEMO COMPONENT</h1>
			</div>
		)
	}
}

export default Demo