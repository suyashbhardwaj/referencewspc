import React from 'react'

export class Rando extends React.Component {
	constructor(props) {
		super(props);
		this.state = { num:0, color: 'orangered' };
		// this.makeTimer();
	}

	makeTimer() {
		setInterval(()=>{
			let randno = Math.floor(Math.random()*this.props.maxNum);
			// if (randno === 5) 
			this.setState({num: randno});
		}, 1000);
	}
	render() {
		console.log("render() invoked! Component re-rendered..");
		return (
			<div>
				<h1>{this.state.num}</h1>
			</div>
		)
	}
}

export default Rando