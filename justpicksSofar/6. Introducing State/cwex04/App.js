import './App.css';
import Button from './Button'
import BrokenClick from './BrokenClick'

function App() {
  
  return (
    <div className="App">
      <BrokenClick />
    </div>
  );
}

export default App;
