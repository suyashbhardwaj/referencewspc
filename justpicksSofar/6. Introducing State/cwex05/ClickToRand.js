import React from 'react'

export class ClickToRand extends React.Component {
	constructor(props){
		super(props);
		this.state = { randno: 0, stop: false}
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(e) {
		let genrand = Math.floor(Math.random()*10);
		genrand === 7 ? this.setState({ randno: genrand, stop:true }) : this.setState({randno: genrand});
	}

	render() {
		const finishtext = <h2>YOU WIN!</h2>;
		return (
			<div>
				<h1>Number is {this.state.randno}</h1>
				{this.state.stop? finishtext : (
					<button 
						onClick = {this.handleClick}
						display = 'none'
						>
						बटन दबाओ
					</button>
					)}
			</div>
		)
	}
}

export default ClickToRand