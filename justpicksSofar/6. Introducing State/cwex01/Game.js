import React from 'react'

export class Game extends React.Component {
	/*---------------------------------------------------------*/
	constructor(props) {
		super(props);
		this.state = {
			score: 0,
			gameOver: false
		}
	}
	/*---------------------------{initializing a state}--------*/
	render() {
		return (
			<div>
				<h1>Your score is {this.state.score}</h1>
			</div>
		)
	}
}

export default Game