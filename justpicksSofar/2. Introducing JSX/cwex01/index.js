function getMood() {
	const moods = ['sad', 'happy', 'angry', 'romantic', 'blank', 'funny'];
	return moods[Math.floor(Math.random()*moods.length)];
}

class JSXDemo extends React.Component {
	render(){
		return (
			<div>
				<h1>Long Lasting Bunk</h1>
				<h5>And my mood is {getMood()} </h5>
				<img src = '18.jpg' />
			</div>
		); 

		/*OR - The actual JS code (as transpiled to by Babel engine)*/
				
			/*(
			React.createElement("div", null, 
				React.createElement("h3", null, "Long Lasting Bunk"), React.createElement("img", {src: "18.jpg"}))
		);*/

		/*OR - The actual JS code (as transpiled to by Babel engine)*/
				
			/*React.createElement("h3", null, 'I AM AN H3');*/
	}
}

ReactDOM.render(<JSXDemo />, document.getElementById('root'));