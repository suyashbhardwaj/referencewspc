/*purpose: to showcase the standered app structure*/
class App extends React.Component {
	render() {
		return (
			<div>
				<Hello />
				<NumPicker />
				<Hello />
			</div>
		)
	}
}

ReactDOM.render(<App />, document.getElementById('root'));