/*01 what are the components we need: <ToDoList />, <ToDoListForm />, <ToDoItem />
02 how many could we get away with 
03 how much state do we need to store: 
  { todolist: [{ id: "", todotask: "", doneStatus: false },{ id: "", todotask: "", doneStatus: false }]}
  { todotask: "", doneStatus: false, isBeingEdited: false}
04 where do we store it*/

import './App.css';
import React from 'react'
import TestBootstrap from './TestBootstrap'
import whiskey from './assets/images/whiskey.jpg'
import hazel from './assets/images/hazel.jpg'
import tubby from './assets/images/tubby.jpg'
import Navbar from './CurrentProj/Navbar'
import MyRoutes from './CurrentProj/MyRoutes'

export class App extends React.Component {
  static defaultProps = {
    dogs: [
        {
          name: 'Whiskey',
          age: 5,
          src: whiskey,
          facts: [
              "whiskey loves popcorn..",
              "whiskey is risky. Is a terrible guard dog..",
              "whiskey wants to fuck everybody.."
            ]
        },
        {
          name: 'Hazel',
          age: 3,
          src: hazel,
          facts: [
              "बेवकॊफ़ कुत्ता..",
              "only barks..",
              "runs behind her tail.."
            ]
        },
        {
          name: 'Tubby',
          age: 4,
          src: tubby,
          facts: [
              "टबी सडक के बीचो-बीच टट्टी करना पसंद करता है। हराम-ज्यादा।।..",
              "tubby is a चुत्ता..",
              "tubby goes to park daily.."
            ]
        }
      ]
  }

  render() {
    return (
      <div className = "App">
        {/*<h1 className = 'hinditext'>।। प्रतिक्रिया परियावरण मे आपका स्वागत है ।।</h1>
        <i className = 'fa fa-4x fa-television' />*/}
        <Navbar alldogs = {this.props.dogs} />
        <MyRoutes dogs = {this.props.dogs} />
      </div>
    );
  }
}

export default App