import React from 'react'
import whiskey from '../assets/images/whiskey.jpg'
import hazel from '../assets/images/hazel.jpg'
import tubby from '../assets/images/tubby.jpg'
import { Route, Switch, Redirect } from 'react-router-dom'
import DogCard from './DogCard'
import HomePage from './HomePage'

export class MyRoutes extends React.Component {
	render() {
		const getDog = (props) => {
	      let name = props.match.params.name;
	      let currentDog = this.props.dogs.find(dog => dog.name.toLowerCase() === name.toLowerCase());
	      return <DogCard theimage = {currentDog.src} dogname = {currentDog.name} dogage = {currentDog.age} dogfacts = {currentDog.facts} />
	    }

		return (
		<Switch>
          <Route exact path = '/' render = {()=> <HomePage alldogs = {this.props.dogs} />} />
          <Route exact path = '/dogs/:name' render = {getDog} />
          {/*or*/}
          {/*{this.props.dogs.map(onedog => 
            <Route exact path = {`/dogs/${onedog.name}`} render = {(routeProps)=> 
              <DogCard 
                theimage = {onedog.src}
                dogname = {onedog.name}
                dogage = {onedog.age}
                dogfacts = {onedog.facts}
                key = {onedog.name}
              />} 
            />
            )}*/}
          <Route render = {()=><Redirect to = '/' />} />
        </Switch>
		)
	}
}

export default MyRoutes