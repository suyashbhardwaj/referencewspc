import React from 'react'
import {Link} from 'react-router-dom'
import './HomePage.css'

export class HomePage extends React.Component {
	render() {
		return (
			<div className = 'container'>
				<h1 className = 'display-1 text-center'>Click a Dog!</h1>
				<div className = 'doglinks mt-lg'>
					{this.props.alldogs.map(dog=> 
						<div key = {dog.name}>
							<Link to = {`/dogs/${dog.name}`}> <img src = {dog.src} alt = {dog.name}/></Link>
							<h4>{dog.name}</h4>
						</div>
						)}
				</div>
			</div>
		)
	}
}

export default HomePage