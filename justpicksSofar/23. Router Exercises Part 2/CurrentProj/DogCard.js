import React from 'react'
import './DogCard.css'
import {Link} from 'react-router-dom'

export class DogCard extends React.Component {
	render() {
		return (
			<div className = 'dogcardcontainer'>
				<div className="card dogcard" style={{width: '18rem'}}>
				  <img src={this.props.theimage} className="card-img-top" alt="dog image" />
				  <div className="card-body">
				    <h4 className="card-title fw-bold hinditext">{this.props.dogname}</h4>
				    <h6 className = "card-subtitle mb-2 text-muted">{this.props.dogage} years old</h6>
				    {/*<hr className = "mb-0"/>*/}
				    {/*{this.props.dogfacts.map(fact => {
						return ( <>	<p className="card-text my-1">{fact}</p> <hr className = "my-0 py-0"/> </> );
					})}*/}

					<ul className = "list-group list-group-flush">
						{this.props.dogfacts.map((fact,i) => {
						return ( <li className="list-group-item" key={i}>{fact}</li> );
						})}
					</ul>
					<div className="card-body">	<Link to="/" className="btn btn-primary mt-3">Go Back</Link> </div>
				    {/*<a href="#" className="btn btn-primary mt-3">Go Back</a>*/}
				  </div>
				</div>
			</div>
		)
	}
}

export default DogCard