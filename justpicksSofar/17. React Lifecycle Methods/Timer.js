import React from 'react'

export class Timer extends React.Component {
	constructor(props) {
		super(props);
		this.state = { time: new Date() }
		console.log("inside the constructor with time as ", this.state.time);
	}
	
	componentDidMount() {
		console.log("inside the componentDidMount");
		const timeint = setInterval(()=> {
			this.setState({ time: new Date()});
		},1000);
	}

	componentDidUpdate(prevProps, prevState) {
		console.log(`COMPONENT DID UPDATE with previous props as ${prevProps} & previous state as ${prevState.time.getSeconds()}`);
	}
	
	render() {
		console.log("inside the render")
		return (
			<div>
				<h2>This is the Timer component</h2>
				<h6>{ this.state.time.getSeconds() }</h6>
			</div>
		)
	}
}

export default Timer