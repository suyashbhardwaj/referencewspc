import React from 'react'
import axios from 'axios'
import MyLoaders from './MyLoaders'

export class Quote extends React.Component {
	constructor(props) {
		/*Bad practice to 
			01 load  any data via AJAX/ from API
			02 set state using setState*/
		console.log('CONSTRUCTOR');
		super(props);
		this.state = { quote: "", isLoaded: false }
	}

	componentDidMount() {
		/*Good place to
			01 load any data via AJAX/ from API
			02 set up subscriptions/timers*/
		
		let apiurl = 'https://api.github.com/zen';
		setTimeout(()=>{
			axios.get(apiurl).then(response => { this.setState({ quote:response.data, isLoaded:true }) });
		},3000);
	}

	/*or*/

	/*async componentDidMount() {
		console.log('COMPONENT DID MOUNT');
		let apiurl = 'https://api.github.com/zen';
		let response = await axios.get(apiurl);
		this.setState({ quote:response.data, isLoaded:true });
	}*/

	componentDidUpdate(prevProps, prevState) {
		/*Suitable place to implement any side-effect operations like
			01 syncing up with localStorage
			02 auto-saving
			03 updating DOM for uncontrolled components*/
		console.log(`COMPONENT DID UPDATE with previous props as ${prevProps} & previous state as ${prevState}`);
	}	

	componentWillUnmount() {
		console.log('COMPONENT WILL UNMOUNT');
	}
	
	render() {
		console.log('RENDER');
		return (
			<div>
				{ this.state.isLoaded ? 
					(
						<div>
							<h4>Always remember...</h4>	
							<h6>{this.state.quote}</h6>
						</div>
					)
				
				: <MyLoaders />
				}
			</div>
		)
	}
}

export default Quote