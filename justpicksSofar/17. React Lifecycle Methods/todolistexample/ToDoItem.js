import React from 'react'
import './todoitem.css'
// import myicon from './images/avatarLBN.svg'
import myicondel from './images/delete.png'
import myiconedit from './images/path847.png'

export class ToDoItem extends React.Component {
	constructor(props){
		super(props);
		this.state = { taskdesc: this.props.taskdesc, doneStatus: false, isBeingEdited: false }
		this.handleEdit = this.handleEdit.bind(this);
		this.handleDel = this.handleDel.bind(this);
		this.handleToggle = this.handleToggle.bind(this);
		this.handleUpdate = this.handleUpdate.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleEdit(evt){
		this.setState(curState => {
			return { taskdesc: this.props.taskdesc, isBeingEdited: !curState.isBeingEdited }
		})
	}

	handleDel(evt) {
		console.log(evt);
		this.props.deltodo(this.props.todoid);
	}

	handleToggle(evt){
		this.props.toggleTodoStatus(this.props.todoid);
	}

	handleChange(evt){
		evt.preventDefault();
		this.setState( {
			[evt.target.name]: evt.target.value
		} );
	}

	handleUpdate(evt) {
		evt.preventDefault();
		this.props.updatetodo(this.props.todoid, this.state.taskdesc);
		this.setState({ isBeingEdited:false });

	}

	render() {
		const todoclassinfo = this.props.doneStatus ? "todoDescClosed" : "todoDescOpen";
		const taskInMode = this.state.isBeingEdited ? 
			(
			<div className = "todoDescEdit">
				<form onSubmit = {this.handleUpdate} >
					<input 
						type="text" 
						name = "taskdesc" 
						value={this.state.taskdesc} 
						onChange = {this.handleChange} 
						// onBlur = {()=> this.props.updatetodo(this.props.todoid, this.state)}
					/>
					<button>Save</button>
				</form>
			</div>):
			
			(<div className = {todoclassinfo} onClick = {this.handleToggle}>{this.props.taskdesc}</div>);

		return (
			<div className = 'oneToDoItem'>
				{taskInMode}	
				<button name = "deletebtn" onClick = {this.handleDel}><img src={myicondel} /></button>
				<button name = "editbtn" onClick = {this.handleEdit}><img src={myiconedit} /></button>
			</div>
		)
	}
}

export default ToDoItem