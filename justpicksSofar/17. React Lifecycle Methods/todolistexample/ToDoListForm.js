/*#FD5B5C*/
import React from 'react'
import './ToDoListForm.css'
import {v4 as uuid} from 'uuid' 

export class ToDoListForm extends React.Component {
	constructor(props){
		super(props);
		this.state = { taskdesc: "", doneStatus: false }
		this.handleInput = this.handleInput.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInput(evt){
		this.setState({
			[evt.target.name]: evt.target.value
		})
	}

	handleSubmit(evt){
		evt.preventDefault();
		this.props.addtodo({ ...this.state, id:uuid() })
		this.setState({ taskdesc:"", doneStatus:false });
	}

	render() {
		return (
			<div className = "taskentryform">
				<h4>New Todo</h4>
				<form onSubmit = {this.handleSubmit}>
					<input 
						autoFocus
						name = "taskdesc" 
						type="text" 
						placeholder="Enter the task" 
						onChange = {this.handleInput} 
						value = {this.state.taskdesc}
						/>
					{/*<input type='color' />	*/}
					<button>ADD TODO</button>
				</form>
			</div>
		)
	}
}

export default ToDoListForm