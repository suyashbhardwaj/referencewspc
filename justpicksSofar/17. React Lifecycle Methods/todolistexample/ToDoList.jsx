import React from 'react'
import ToDoItem from './ToDoItem'
// import {v4 as uuid} from 'uuid' 
import ToDoListForm from './ToDoListForm'
import './ToDoList.css'

export class ToDoList extends React.Component {
	constructor(props) {
		super(props);
		this.state = { 
			todolist: []
		}
		this.toggleTodoStatus = this.toggleTodoStatus.bind(this);
		this.deltodo = this.deltodo.bind(this);
		this.addtodo = this.addtodo.bind(this);
		this.updatetodo = this.updatetodo.bind(this);
	}

	deltodo(todoid){
		console.log(todoid);
		/*this.setState(curState => {
			return { todolist: curState.todolist.filter(tdi => tdi.id !== todoid) }
		});*/
		this.setState({ todolist: this.state.todolist.filter(tdi => tdi.id!== todoid) })
	}

	addtodo(task){
		/*this.setState(curState => {
			let idfiedtask = { id:uuid(), ...task }
			curState.todolist.push(idfiedtask);
			return curState;
		})*/
		this.setState({ todolist: [...this.state.todolist, task] });
	}

	updatetodo(taskid, updatedTask) {
		console.log(`received an update for ${taskid} to be changed to ${updatedTask}`);
		/*this.setState(curState => {
			let revisedToDoList = curState.todolist.map(entry => entry.id == taskid ? entry.taskdesc = updatedTask: entry = entry);
			return { todolist: revisedToDoList }
		})*/
		const updatedTodos = this.state.todolist.map(todo => {
			if (todo.id === taskid) {
				return { ...todo, taskdesc: updatedTask }
			}
			return todo;
		});
		this.setState({ todolist: updatedTodos });
	}

	toggleTodoStatus(todoid){
		console.log(todoid);
		/*this.setState(curState => {
			curState.todolist.find(tdi => tdi.id == todoid).doneStatus = true;
			return { ...curState }
		});*/

		const updatedToDos = this.state.todolist.map(todo => {
			if (todo.id === todoid) { return { ...todo, doneStatus: !todo.doneStatus } }
			return todo;
		});
		this.setState({ todolist: updatedToDos });
	}

	componentDidMount() {
		console.log('COMPONENT DID MOUNT');
	}

	componentDidUpdate(prevProps, prevState) {
		console.log(`COMPONENT DID UPDATE WITH PREVIOUS PROPS AS ${prevProps} PREVIOUS STATE AS ${prevState.todolist}`);
	}

	componentWillUnmount() {
		console.log('COMPONENT WILL UNMOUNT INVOKED');
	}

	render() {
		const currenttodolist = this.state.todolist.map(entry => {
			return <ToDoItem 
				taskdesc = {entry.taskdesc} 
				doneStatus = {entry.doneStatus}
				deltodo = {this.deltodo}
				toggleTodoStatus = {this.toggleTodoStatus}
				key = {entry.id}
				todoid = {entry.id}
				updatetodo = {this.updatetodo}
				/>
		})

		return (
			<div className = "tododialog">
				<h1>Todo List!</h1>
				<h6>A Simple React Todo List App.</h6>
				
				<div className = 'todolistpopulation'>{currenttodolist}</div>
				
				<ToDoListForm addtodo = {this.addtodo}/>
			</div>
		)
	}
}

export default ToDoList