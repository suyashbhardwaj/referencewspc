import React from 'react'
import './ProgressSteps.css'

export class ProgressSteps extends React.Component {
	constructor(props) {
		super(props);
		this.state = { actives: 1, circles: 4, currentActive: 1 }
		this.handleNextClick = this.handleNextClick.bind(this);
		this.handlePrevClick = this.handlePrevClick.bind(this);
		this.nodestyle = this.nodestyle.bind(this);
	}

	handleNextClick(evt) {
		if (this.state.currentActive > 3) { this.setState({ currentActive: 4 }) }
			else { this.setState({ currentActive: this.state.currentActive+1 }) }
	}

	handlePrevClick(evt) {
		if (this.state.currentActive < 1) { this.setState({ currentActive: 1 }) }
			else { this.setState({ currentActive: this.state.currentActive-1 }) }
	}

	nodestyle(x) {
			if (this.state.currentActive >= x) return 'circle active';
			else return 'circle';
		}

	render() {
		const linethrough = { width: `${((this.state.currentActive-1)/3)*100}%` }
		return (
			<div className = 'container'>
				<div className="progress-container">
					<div className="progress" id="progress" style = {linethrough}></div>
					<div className={this.nodestyle(1)}>1</div>
					<div className={this.nodestyle(2)}>2</div>
					<div className={this.nodestyle(3)}>3</div>
					<div className={this.nodestyle(4)}>4</div>
				</div>

				<button className="btn" name="prev" disabled = {this.state.currentActive <= 1} onClick = {this.handlePrevClick}>Prev</button>
				<button className="btn" name="next" disabled = {this.state.currentActive > 3} onClick = {this.handleNextClick}>Next</button>
			</div>
		)
	}
}

export default ProgressSteps