import React from 'react'
import axios from 'axios'
import ColtCard from './ColtCard'
import './CardDrawer.css'

const API_BASEURL = 'https://deckofcardsapi.com/api/deck'
export class ColtDeck extends React.Component {
	constructor(props) {
		super(props);
		this.state = { deck:null, drawn:[] }
		this.getCard = this.getCard.bind(this);
	}

	async componentDidMount() {
		let newDeck = await axios.get(`${API_BASEURL}/new/shuffle`);
		this.setState({ deck: newDeck.data })
	}

	async getCard(){
		let deckId = this.state.deck.deck_id;
		console.log('cardid is ',deckId);
		let cardRes;
		try {
			let cardUrl = `${API_BASEURL}/${deckId}/draw/`;
			cardRes = await axios.get(cardUrl);
			if (!cardRes.data.success) { throw new Error('No card remaining!') }
		} catch(er) {console.log(er)}
		let newCard = cardRes.data.cards[0];
		console.log(newCard);
		this.setState(curState => {
			return { drawn: [...curState.drawn, { id:newCard.code, image: newCard.image, name: `${newCard.suit} ${newCard.value}` }] };
		});
	}

	render() {
		const cardimages = this.state.drawn.map(crd => (
			<ColtCard 
				image = {crd.image} 
				name = {crd.name}
				key= {crd.id}
			/>)
		);

		return (
			<div className = 'CardDrawer'>
				<h1>👓CARD DEALER🔈</h1>
				<h6>👓A LITTLE DEMO MADE WITH REACT🔈</h6>
				<button onClick = {this.getCard}>Get a Card!</button>
				<div className = 'cardstack'>{cardimages}</div>
			</div>
		)
	}
}

export default ColtDeck