import React from 'react'
import axios from 'axios'
import './CardDrawer.css'

export class CardDrawer extends React.Component {
	constructor(props) {
		super(props);
		this.state = { carddeckid: "", imgsrc: "", remainingCards:undefined }
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		// if (this.state.remainingCards!==0) {
			axios.get(`https://deckofcardsapi.com/api/deck/${this.state.carddeckid}/draw/`).then(res => { 
				console.log(res.data.cards[0].images.png);
				this.setState({ imgsrc: res.data.cards[0].images.png, remainingCards: res.data.remaining });
			}).catch(er=>{console.log(er)})
		// } else { alert('no more cards left') }
	}

	async componentDidMount() {
		let response = await axios.get('https://deckofcardsapi.com/api/deck/new/shuffle');
		this.setState({ carddeckid: response.data.deck_id });
	}

	componentDidUpdate() {
		console.log('component updated');
		const imgblock = document.getElementById('cardimage');
		imgblock.style.rotate = `${Math.floor(Math.random()*360+1)}deg`;
	}

	render() {
		return (
			<div className = 'CardDrawer'>
				<h1>👓CARD DEALER🔈</h1>
				<h6>👓A LITTLE DEMO MADE WITH REACT🔈</h6>
				<button onClick = {this.handleClick} >DEAL ME A CARD</button>
				<div id = "cardimage"><img src = { this.state.imgsrc }/></div>
			</div>
		)
	}
}

export default CardDrawer