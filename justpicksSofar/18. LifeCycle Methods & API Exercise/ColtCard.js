import React from 'react'
import './ColtCard.css'

export class ColtCard extends React.Component {
	constructor(props){
		super(props);
		let angle = Math.random()*90-45;
		let xtranslation = Math.random()*40-20;
		let ytranslation = Math.random()*40-20;
		this._transform = `translate(${xtranslation}px,${ytranslation}px) rotate(${angle}deg)`;
	}

	render() {
		return (
			<img className = "card" style = {{transform: this._transform}} src = {this.props.image} alt = {this.props.name} />
		)
	}
}

export default ColtCard