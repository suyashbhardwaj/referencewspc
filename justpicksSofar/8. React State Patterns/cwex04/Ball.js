import React from 'react'
import './ball.css'

export class Ball extends React.Component {
	render() {
		return (
			<div className = 'ball'> {this.props.num} </div>
		)
	}
}

export default Ball