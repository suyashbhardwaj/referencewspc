/*01 what are the components we need
02 how many could we get away with 
03 how much state do we need to store
04 where do we store it*/

import './App.css';
import Lottery from './Lottery'

function App() {
  
  return (
    <div className="App">
      <Lottery title = 'लूट लो माल'/>
      <Lottery title = 'Mini Daily' numBalls = {4} maxNum = {10}/>
    </div>
  );
}

export default App;
