import React from 'react'
import './Lottery.css'
import Ball from './Ball'

export class Lottery extends React.Component {
	
	static defaultProps = {
		title : 'yash',
		numBalls: 6,
		maxNum : 40
	}

	constructor(props) {
		super(props);
		this.state = { randnums: Array.from({ length: this.props.numBalls }) }; 
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick() {
		console.log('handleClick invoked');
		this.generate();
	}

	generate(){
		/*const randseries = [];
		let genrandno;
		for (let i = 0; i < this.props.numBalls; i++) {
			genrandno = Math.floor(Math.random()*this.props.maxNum)+1;
			randseries.push(genrandno);
		}
		this.setState({ randnums: randseries });*/

		/*OR better*/

		/*setState to an object that is returned by a callback Fn passed to you that takes current state
		value's randnums & for each of it's element it sets up a new value from scratch.*/
		this.setState(curState => ({randnums: curState.randnums.map(
			n=>Math.floor(Math.random()*this.props.maxNum)+1)
			}));

		console.log('randnums in state currently as ', this.state.randnums);
	}

	render() {
		return (
			<div className = 'Lottery'>
				<div className = 'balls'>
					<h4>{this.props.title}</h4>
					{this.state.randnums.map(rdno=> <Ball num = {rdno} />)}
				</div>
				<br/>
				<br/>
				<hr/>
				<button onClick = {this.handleClick}>Generate</button>
				<br/>
			</div>
		)
	}
}

export default Lottery