import React from 'react'

export class ToDo extends React.Component {
	render() {
		console.log('received props ',this.props);
		return (
			<div>
				<table>
					<th>id</th>
					<th>taskItem</th>
					<th>status</th>
					<th>priority</th>
					<th>remarks</th>
					<tr>
						<td>{this.props.task.id}</td>
						<td>{this.props.task.taskitem}</td>
						<td>{this.props.task.status}</td>
						<td>{this.props.task.priority}</td>
						<td>{this.props.task.remarks}</td>
					</tr>
				</table>
			</div>
		)
	}
}

export default ToDo