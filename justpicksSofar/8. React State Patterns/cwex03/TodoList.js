import React from 'react'
import ToDo from './ToDo'

export class TodoList extends React.Component {
	constructor(props){
		super(props);
		this.state = { todo: [
			{ id:1, taskitem: 'do the dishes', status: 'open', priority: 'low', remarks: 'xx'},
			{ id:2, taskitem: 'do the washes', status: 'in process', priority: 'mid', remarks: 'x'},
			{ id:3, taskitem: 'do the other needfuls', status: 'closed', priority: 'high', remarks: 'on hold complete'},],
			completed: 1 }
	}

	render() {
		return (
			<div>
				<ToDo task = {this.state.todo[1]} />
			</div>
		)
	}
}

export default TodoList