import React from 'react'
import dieOne from './images/dieOne.png'
import dieTwo from './images/dieTwo.png'
import dieThree from './images/dieThree.png'
import dieFour from './images/dieFour.png'
import dieFive from './images/dieFive.png'
import dieSix from './images/dieSix.png'
import './IconList.css'

export class IconList extends React.Component {
	constructor(props) {
		super(props);
		this.state = { stImglist: [] }
		this.addIcon = this.addIcon.bind(this);
	}

	/*Instead of mutating the state obj itself*/
	/*addIcon() {
		const imglist = [dieOne,dieTwo,dieThree,dieFour,dieFive,dieSix];
		let idx = Math.floor(Math.random()*imglist.length)
		let newIcon = imglist[idx];
		let stImages = this.state.stImglist;
		stImages.push(newIcon);
		this.setState( {stImglist:stImages } );
	}*/

	/*The better alternative: Is to create a copy of the current state, modify it as needed & put into setState*/
	addIcon() {
		const imglist = [dieOne,dieTwo,dieThree,dieFour,dieFive,dieSix];
		let idx = Math.floor(Math.random()*imglist.length)
		let newIcon = imglist[idx];
		this.setState( { stImglist: [...this.state.stImglist, newIcon] } );
	}

	render() {
		const Icons = this.state.stImglist.map(image => <img src = {image} />);
		return (
			<div className = 'renderedbody'>
				{Icons}
				<br/><hr/>
				<button onClick = {this.addIcon} >AddIcon</button>
			</div>
		)
	}
}

export default IconList