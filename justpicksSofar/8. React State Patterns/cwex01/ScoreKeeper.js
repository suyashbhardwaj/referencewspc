import React from 'react'

function incrementByOne(prevState) {
		return { score: prevState.score +1 }
	}

export class ScoreKeeper extends React.Component {
	constructor(props) {
		super(props);
		this.state = { score: 0 };
		this.singleKill = this.singleKill.bind(this);
		this.tripleKillMistaken = this.tripleKillMistaken.bind(this);
		this.tripleKillRectified = this.tripleKillRectified.bind(this);
		this.neatTetraKill = this.neatTetraKill.bind(this)
	}

	singleKill(e) {
		/*Not a good idea to update the state like this when it depends on the previous value*/
		/*setState is asynchronous, you never know what the previous value would be 
			by the time this setState gets a chance for execution*/
		this.setState({ score: this.state.score + 1 });
	}

	tripleKillMistaken(e) {
		/*should work!.. no?*/
		this.setState({ score: this.state.score + 1 });
		this.setState({ score: this.state.score + 1 });
		this.setState({ score: this.state.score + 1 });	/*This last update on state is what perhaps is likely to be considered by react to solely execute. pick the latest update न यार। */
		// this.setState({ score: this.state.score + 7 }); /*check..*/
	}

	tripleKillRectified(e) {
	/*Alternate is to use the callback form of setState. i.e. setState((currentState) => {return newState});*/
		this.setState((currentState) => { return { score: currentState.score + 1 }});
		this.setState((currentState) => { return { score: currentState.score + 1 }});
		this.setState((currentState) => { return { score: currentState.score + 1 }});
	}

	neatTetraKill(e) {
	/*This one is a neat code - it clearly mentions what is being done*/
		this.setState(incrementByOne);
		this.setState(incrementByOne);
		this.setState(incrementByOne);
		this.setState(incrementByOne);
	}

	render() {
		return (
			<div>
				<h1>Your score is: {this.state.score}</h1>
				<button onClick = {this.singleKill}>Single Kill!</button>
				<button onClick = {this.tripleKillMistaken}>Triple Kill Mistaken!</button>
				<button onClick = {this.tripleKillRectified}>Triple Kill Rectified!</button>
				<button onClick = {this.neatTetraKill}>Neat TetraKill!</button>
			</div>
		)
	}
}

export default ScoreKeeper