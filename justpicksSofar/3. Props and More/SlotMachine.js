class SlotMachine extends React.Component {
	render() {
		// const {s1,s2,s3} = this.props;	//won't work - same names as in destructured from obj
		const {slotOne,slotTwo,slotThree} = this.props;
		const winstatus = slotOne === slotTwo && slotTwo === slotThree;
		const mystyledef = {fontSize: '50px', backgroundColor: 'grey'};

		return (
			<div className = 'slotmachine'>
				<p style = {mystyledef}> {slotOne}  {slotTwo}  {slotThree} </p>
				<br />
				{/*{this.props.slotOne === this.props.slotTwo ? (
					this.props.slotTwo === this.props.slotThree ? (<h4>You Win!!</h4> : <h4>You Lose!!</h4>)
						: <h4>You Lose!!</h4> 
					)}*/}

				<p className = {winstatus ? 'win' : 'lose'}>
					{ winstatus ? <h4>You Win!!</h4> : <h4>You Lose!!</h4> }
				</p>
				
				<hr/>

			</div>
		)
	}
}