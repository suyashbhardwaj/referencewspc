/*जिसको भी आप ने react component बना दिया, वह अब एक नया tag है। */
class Hello extends React.Component {
	render() {
		console.log(this.props);
		// this.props.to = 'aditi'; /*no! we cannot alter the value of a prop. it is read-only*/
		let bangs = '!'.repeat(this.props.bangs);
		return (
			<div>
				<h1>Hi {this.props.to} from {this.props.from}{bangs}</h1>
				<img src = {this.props.img} />
			</div>
		)
	}
}