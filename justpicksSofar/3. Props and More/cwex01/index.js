/*purpose: to showcase the standered app structure*/
class App extends React.Component {
	render() {
		return (
			<div>
				<Hello 
					from = 'Yash' //strings
					to = 'Ananya'
					// num = 3 	// numbers - invalid
					num = '3'	// numbers - valid
					// num = {3}	// numbers - valid
					data = {[12,23,34,45,56]}	//arrays
					// isFunny = {true}	// boolean - valid
					isFunny				// boolean - valid (by default will take it as true)
					bangs = '7'
					img = '154086_L.png'
					/>
			</div>
		)
	}
}

ReactDOM.render(<App />, document.getElementById('root'));