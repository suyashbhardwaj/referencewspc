function getNum(){
	return Math.floor((Math.random()*10) + 1);
}

class NumPicker extends React.Component {
	render() {
		const num = getNum();
		let msg;
		if (num!==7) {
			msg = 
				<div>
					<h2>You deserve this!</h2>
					<img src = '7GklNKy2gfpII.gif' />
				</div>
			}
		else {
			msg = 'nothing from if-else';
		}

		return (
			<div>
				<h1>Your number is ... {num}</h1>
				<p>{num===7 ? 'Congratz' : 'Unlucky' }</p>
				{num===7 && <img src = '6WCwBOWQQ0iHe.gif' />}
				{msg}
			</div>
		)
	}
}