class SlotMachine extends React.Component {
	render() {
		// const {s1,s2,s3} = this.props;	//won't work - same names as in destructured from obj
		const {slotOne,slotTwo,slotThree} = this.props;
		const winstatus = slotOne === slotTwo && slotTwo === slotThree;
		return (
			<div>
				{slotOne}  {slotTwo}  {slotThree}
				<br />
				{/*{this.props.slotOne === this.props.slotTwo ? (
					this.props.slotTwo === this.props.slotThree ? (<h4>You Win!!</h4> : <h4>You Lose!!</h4>)
						: <h4>You Lose!!</h4> 
					)}*/}

				{ winstatus ? <h4>You Win!!</h4> : <h4>You Lose!!</h4> }
				<hr/>

			</div>
		)
	}
}