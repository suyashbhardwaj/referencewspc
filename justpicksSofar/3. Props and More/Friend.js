class Friend extends React.Component {

	static defaultProps = {
		name: 'BehenJi',
		hobbies: ['listening to music', 'bathing', 'sleeping', 'watching TV', 'web surfing', 'phone talking']
	}

	render() {
		const { name, hobbies } = this.props;
		return (
			<div>
				<h4>{name}</h4>
				<ul>
					{/*{hobbies.map( h => <li>h</li>)} this won't work */} 
					{hobbies.map( h => <li>{h}</li>)}
				</ul>
			</div>
		)
	}
}