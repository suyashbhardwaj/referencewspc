/*showcase the use of map function to iterate over an array inside JSX*/
class App extends React.Component {
	render() {
		return (
			<div>
				<SlotMachine 
					slotOne = '👑'
					slotTwo = '🎓'
					slotThree = '🎓'
					/>

				<SlotMachine 
					slotOne = '🎓'
					slotTwo = '🎓'
					slotThree = '🎓'
					/>

				<Friend 
					name = 'Yash'
					// we need to set object having array as a value, not an array itself - not acceptable
					hobbies = {['cricket', 'video gaming', 'embedded systems', 'cooking']}
				/>

				<Friend 
					name = 'Sushma'
					// we need to set object having array as a value, not an array itself - not acceptable
					// hobbies = {['film', 'work', 'angular','phone talk']}
				/>

			</div>
		)
	}
}

ReactDOM.render(<App />, document.getElementById('root'));