import React from 'react'
import head from './images/headCoin.png'
import tail from './images/tailCoin.png'
import Coin from './Coin'
import './Toss.css'

export class Toss extends React.Component {
	constructor(props){
		super(props);
		this.state = { netFlips: 0, totalHeads: 0, totalTails: 0, side: null }
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick() {
		this.flipCoin();
	}

	flipCoin() {
		let tossres = Math.floor(Math.random()*2);
		console.log('tossres is ', tossres);
		this.setState(curState => (
			tossres ? {
				netFlips: curState.netFlips+1, 
				totalHeads: curState.totalHeads+1, 
				totalTails: curState.totalTails,
				side: 'heads'
				}
				:
				{
				netFlips: curState.netFlips+1, 
				totalHeads: curState.totalHeads, 
				totalTails: curState.totalTails+1,
				side: 'tails'
				}
		));
	}

	render() {
		return (
			<div className = 'Toss'>
				<h2>Let's flip a coin</h2>
				<Coin side = {this.state.side === 'tails' ? tail : head} />
				<button onClick = {this.handleClick}>Flip Me!</button>
				<h6>Out of {this.state.netFlips} flips, there have been {this.state.totalHeads} heads and {this.state.totalTails} tails.</h6>
			</div>
		)
	}
}

export default Toss