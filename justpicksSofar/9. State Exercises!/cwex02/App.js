/*01 what are the components we need
02 how many could we get away with 
03 how much state do we need to store
04 where do we store it*/

import './App.css';
import React from 'react'
import Palette from './Palette'

export class App extends React.Component {
  render() {
    const rang = ['black','gray','ivory','blue'];
    return (
      <div className = "App">
        <Palette />
        {/*<Palette numBoxes = {6} />*/}
        {/*<Palette numBoxes = {6} allColors = {rang}/>*/}
      </div>
    )
  }
}

export default App