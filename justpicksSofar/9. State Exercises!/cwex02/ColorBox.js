import React from 'react'
import './ColorBox.css'

export class ColorBox extends React.Component {

	constructor(props) {
		super(props);
		this.state = { color: 'darkslategrey'}
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(){ this.setRandomColorFromList(); }

	setRandomColorFromList(){ 
		let newColor;
		do{	
			newColor = this.props.colorList[Math.floor(Math.random()*this.props.colorList.length)];
		} while(newColor === this.state.color);
		this.setState((curColorState) => { return { color: newColor } });
	}

	render() {
		return (
			<div 
				className = 'boxdiv' 
				style = {{backgroundColor: `${this.state.color}`}} 
				onClick = {this.handleClick}>
			</div>

		);
	}
}

export default ColorBox