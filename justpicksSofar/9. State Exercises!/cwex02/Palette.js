import React from 'react'
import ColorBox from './ColorBox'

export class Palette extends React.Component {

	static defaultProps = {
		numBoxes: 10,
		allColors: ['red','orangered','orange','yellow','green','purple','teal',
			'blue','skyblue','black','darkslatgray', 'ivory', 'tomato']
	}

	render() {
		const boxes = Array.from({length: this.props.numBoxes}).map(
			() => <ColorBox colorList = {this.props.allColors} />
			);
		return (
			<div>
				{boxes}
			</div>
		);
	}
}

export default Palette