import React from 'react'
import { NavLink } from 'react-router-dom'
import './Navbar.css'

export class Navbar extends React.Component {
	render() {
		return (
			<div className = 'navbar'>
				<NavLink exact activeClassName = 'navbaractive' to = '/'>Homewa</NavLink>
				<NavLink exact activeClassName = 'navbaractive' to = '/patanjali'>Patanjali</NavLink>
				<NavLink exact activeClassName = 'navbaractive' to = '/funfoods'>Crax</NavLink>
				<NavLink exact activeClassName = 'navbaractive' to = '/amul'>IceCream</NavLink>
			</div>
		)
	}
}

export default Navbar