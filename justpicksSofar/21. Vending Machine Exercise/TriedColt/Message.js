import React from 'react'
import './Message.css'

export class Message extends React.Component {
	render() {
		return (
			<div className = 'message'>
				{/*<h2>This is a message!</h2>*/}
				{ this.props.children }
			</div>
		)
	}
}

export default Message