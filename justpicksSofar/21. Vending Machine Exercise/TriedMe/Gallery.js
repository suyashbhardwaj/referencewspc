import React from 'react'
import './Gallery.css'
import whitewall from '../assets/images/whiteWall.jpg'
import vendingMachine from '../assets/images/vendingM.png'
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom'
import Patanjali from './Patanjali'

export class Gallery extends React.Component {
	render() {
		return (
			<div className = 'Gallery'>
				<img className='background' src = {whitewall} />
				<img className='object01' src = {vendingMachine} />
				<section className = 'banner'>
					<Switch>
						<Route exact path = '/' component = '' />
						<Route exact path = '/patanjali' component = {Patanjali} />
						<Route exact path = '/funfoods' component = {Patanjali} />
						<Route exact path = '/amul' component = {Patanjali} />
					</Switch>
					<NavLink exact to = '/'><h1>Get yourself a favour!</h1></NavLink>
					<NavLink exact to = '/patanjali'><h3>Patanjali Noodles</h3></NavLink>
					<NavLink exact to = '/amul'><h3>Amul Chhaas</h3></NavLink>
					<NavLink exact to = '/funfoods'><h3>Natkhat</h3></NavLink>
				</section>
				<section className = 'navbar'>
					<NavLink exact activeClassName = 'active-link' to = '/'><i className = 'fa fa-heart'/>H</NavLink>
					<NavLink exact activeClassName = 'active-link' to = '/patanjali'><i className = 'fa fa-heart'/>P</NavLink>
					<NavLink exact activeClassName = 'active-link' to = '/amul'><i className = 'fa fa-heart'/>A</NavLink>
					<NavLink exact activeClassName = 'active-link' to = '/funfoods'><i className = 'fa fa-heart'/>F</NavLink>
				</section>
			</div>
		)
	}
}

export default Gallery