import React from 'react'
import './Patanjali.css'
import Gallery from './Gallery'
import { Route, Switch, Link, NavLink } from 'react-router-dom'

export class Patanjali extends React.Component {
	render() {
		return (
			<div className = 'patanjalimodal'>
				<h1>लो भाइ पतंजलि खाओ</h1>
				<Switch>
					<Route exact path = '/' component = {Gallery} />
				</Switch>
				<NavLink exact to = '/'><h1>Go Back!</h1></NavLink>
			</div>
		)
	}
}

export default Patanjali