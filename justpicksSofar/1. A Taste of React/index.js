/*functional components - traditionally used for creating components with minimal features & powers (not anymore now with hooks)
is just a simple function that usually returns JSX code*/

/*its basically like.. 'inherit a component creating template from class - Component, made available to you via 
React module from react.development.js library' Fill in your component details within..*/
class Hello extends React.Component {
	render(){

		return (
			<div>
				<h1>Hello there!</h1>
				<h1>Hello there!</h1>
				<h1>Hello there!</h1>
			</div>
		);

		// return <h1> Hello There! </h1>;
	}
}

/*ReactDOM is coming from that react-dom script tag
basically says like.. 'Use render method from class ReactDOM (made available to us via react-dom.development.js) to
 get a component of your choice transpiled to JS & rendered by babel engine (made available to us via babel-standalone JS) in an 
 HTML element specified by you (i.e. with id = 'root' here)*/

// ReactDOM.render(<Hello/>, document.getElementById('root'));
/*ReactDOM! अपने render() methods से Hello Component के returned UI in JSX को root id के element मे display करा दो। */
ReactDOM.render(<Hello />, document.getElementById('root'));