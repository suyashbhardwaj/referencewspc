
/*}==========The Latest react-router-dom Way===================================================================================================================================================================================================={*/
import React from 'react'
import ReactDOM from 'react-dom/client'
// import "./ForProject/reactroutertut/index.css";  /*SPECIFIC TO REACT ROUTER TUTORIAL*/
import './index.css'; /*CREATE-REACT-APP PROVIDED CSS*/

import { createBrowserRouter, RouterProvider, } from "react-router-dom";
import ErrorPage from './ForProject/reactroutertut/error-page';

import App from './App';
import MainAppRoutes from './ForProject/botgoportalbase/routes/mainAppRoutes'
import Root, { loader as rootLoader, action as rootAction } from './ForProject/reactroutertut/routes/root';
import Contact, { loader as contactLoader} from './ForProject/reactroutertut/routes/contact';
import EditContact, { loader as editLoader, action as editAction} from './ForProject/reactroutertut/routes/edit';
import TheDefaultViteApp from './ForProject/ViteHome/TheDefaultViteApp';

const router = createBrowserRouter( [{ 
  path: "/",
  element: <h4>This is where we start from</h4>,
  errorElement: <ErrorPage/>,
  },
  
  {
  path: "/app",
  element: <App/>,
  errorElement: <ErrorPage/>,
  },

  {
  path: "/vitehome",
  element: <TheDefaultViteApp/>,
  errorElement: <ErrorPage/>,
  },

  {
  path: "/botgoportalbase",
  element: <MainAppRoutes/>,
  errorElement: <ErrorPage/>,
  },  
  
  {
    path: "/learnreactrouter/", 
    element: <Root />, /*<Root/> component तो / पर render हो जायगा*/
    errorElement: <ErrorPage/>,
    loader: rootLoader, /*Every route is likely to be associated with some form of data, hence, provision by reactrouterdom to list it here, useLoaderData() hook के माध्यम से relevant component here (यानी <Root/>) मे उप्लब्ध हो जायेगा। */
    action: rootAction, /*Similarly, another provision is to catch the post data triggereed by a react-router-dom's <Form> component*/
    children: [
      {
        path: "/learnreactrouter/contacts/:contactId",
        element: <Contact />, /*<Contact/> component कहां पर render होगा? parent path प्र render होने वाले <Root /> component के return(..) मे जहां भी <Outlet/> component है वहां पर। */
        loader: contactLoader,
      },
      {
        path: "/learnreactrouter/contacts/:contactId/edit",
        element: <EditContact />, /*यह भी <Outlet/> की जगह पर ही render होगा  */
        loader: editLoader,
        action: editAction
      },
    ],
  },

  ] );

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

/*}==========The Last Left Way===================================================================================================================================================================================================={*/
/*import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './assets/fonts/Rubik-VariableFont_wght.ttf'
import './assets/fonts/Gotu-Regular.ttf'
import './assets/fonts/Zeyada-Regular.ttf'
import {BrowserRouter} from 'react-router-dom'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();*/