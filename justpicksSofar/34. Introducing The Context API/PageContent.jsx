import React from 'react'
import { ThemeContext } from './contexts/ThemeContext';

export class PageContent extends React.Component {
	static contextType = ThemeContext;
	render() {
		const { isDarkMode } = this.context;
		const styles = {
			height: "100vh",
			width: "100vw",
			backgroundColor: isDarkMode ? 'grey' : 'darkslategrey',
			};
		
		return (
			<div style = {styles}>
				{this.props.children}	
				{/*Navbar & Form are gonna land in props.children. It basically is just sticking them in a div that has styles*/}
			</div>
		)
	}
}

export default PageContent