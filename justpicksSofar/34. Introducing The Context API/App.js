import React from 'react';
import Navbar from './Navbar'
import Form from './Form'
import PageContent from './PageContent'
/*"ThemeContext द्वारा उप्लब्ध कराई गई props को ThemeProvider के माध्यम से nested components मे float करा देना है, उसके लिय import करो उसे" */
import ThemeProvider from './contexts/ThemeContext'
import LanguageProvider from './contexts/LanguageContext'

const App = () => {
  return(
      <ThemeProvider>
        <LanguageProvider>
          <PageContent> {/*इस <PageContent> के पास Navbar और Form, इसके props.children मे आ जायेंगे*/}
            <Navbar/>
            <Form/>
          </PageContent>
        </LanguageProvider>
      </ThemeProvider>
    )
}

export default App
