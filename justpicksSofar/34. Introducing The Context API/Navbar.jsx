import React, { Component } from 'react'

/*यह दोनो साथ मे ही प्रयोग मे लाय जाते हैं*/
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

import IconButton from "@material-ui/core/IconButton";  /*for the flag button*/
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import Switch from "@material-ui/core/Switch";      /*this would give us the toggle switch*/

/*यह withStyles हमे classes provide करेगा is a HOC (Higher Order Component). 
It basically gets those style key-value pairs in styles exported from the associated JS styling sheet available to us here 
as a classes object's properties in CSS, however, finally in end code the class names would be observed to be udpated as 
.Navbar-root-324681370 i.e <This component name>-<className>-<someUIDgenerated>, when you'll check from the developer tools. Hence,
letting us free to use the same class names in another component without being affected from class naems here*/
import { withStyles } from "@material-ui/core/styles";  /*"generate a bunch of stuff (~properties to classes obj)"  */
import styles from "./styles/NavBarStyles";   /*"take the styles from NavBarStyles.js"*/

import { ThemeContext } from './contexts/ThemeContext';
import { LanguageContext, withLanguageContext } from './contexts/LanguageContext';

class Navbar extends Component {
  /*"Look Up & see if you're nested inside of a ThemeContext Provider, if more than 1 then find the nearest one"*/
  static contextType = ThemeContext;
  render() {
    const { classes, languageContext } = this.props;  /*classes coming as props from withStyles wrapper & languageContext coming from withLanguageContext custom HOF wrapper*/
    const { isDarkMode, toggleTheme } = this.context; /*this, coming from the state in ThemeContext*/
    return (
        <div className={classes.root}>
          <AppBar position='static' color= { isDarkMode ? 'default' : 'primary' } /*this color = would change on toggling of dark mode*/>
            <Toolbar>
              <IconButton className={classes.menuButton} color='inherit'>
                <span>🇫🇷</span>
              </IconButton>
              <Typography className={classes.title} variant='h6'/*denotes size*/ color='inherit'>
                App {languageContext.language}
              </Typography>
              <Switch onChange = {toggleTheme} />
              <div className={classes.grow} />
              <div className={classes.search}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                {/*more than one HTML element is created by InputBase - wrapper & text field itself*/}
                <InputBase
                  placeholder='Search...'
                  classes={{
                    root: classes.inputRoot,    /*this woudl go to the parent of input for application*/
                    input: classes.inputInput   /*this would go to the input itself for application*/
                  }}
                />
              </div>
            </Toolbar>
          </AppBar>
        </div>
    );
  }
}

/*"give whatever you have generated as props to our component"*/
export default withLanguageContext(withStyles(styles)(Navbar));  /*कुछ classes withStyles से हमे NavBar मे (as props) मिल जायेंगी styling के लिय: This is called wrapping */
/*This woudl basically get us a new version of the NavBar*/