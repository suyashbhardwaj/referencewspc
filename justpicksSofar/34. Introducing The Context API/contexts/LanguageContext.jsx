import React, { createContext } from 'react';
export const LanguageContext = createContext();
/*This reference to a created context will get 
	01 Provider, &  "enables us with a possibility to float the state within this context to nested components as props"
	02 Consumer 	"enables us with a possibility to automatically get the state within this context available to use as an argument (~value) to a function nested it it"
	available to us for use*/

export class LanguageProvider extends React.Component {
	constructor(props) {
		super(props);
		this.state = { language: 'french' }
		this.handleLangChange = this.handleLangChange.bind(this);
	}
	handleLangChange(evt) { this.setState({ language: evt.target.value }); }
	render() {
		return (
			<LanguageContext.Provider value = {{...this.state, handleLangChange: this.handleLangChange}} >
				{this.props.children}
			</LanguageContext.Provider>
		)
	}
}

export default LanguageProvider

/*So, this becomes a HOF that basically gets a Component as argument the 2brendered code of which is to be provided this Language Context's state for its use*/
export const withLanguageContext = Component => props => {
	return (
		<LanguageContext.Consumer>
			{value => <Component 
				languageContext = {value} 
				{...props}
				/>}
		</LanguageContext.Consumer>
		)
	}