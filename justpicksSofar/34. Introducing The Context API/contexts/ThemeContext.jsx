import React, { createContext } from 'react';

/*"भाई, एक context बना कर उसका reference दे दो और उसे export भी कर देना। क्यो? "*/
export const ThemeContext = createContext();

/*all that this component is supposed to do is to return a wrapper to float the state etc into nested components ahead*/
export class ThemeProvider extends React.Component {
	constructor(props) {
		super(props);
		this.state = { isDarkMode: true }
		this.toggleTheme = this.toggleTheme.bind(this);
	}
	toggleTheme() { this.setState({ isDarkMode: !this.state.isDarkMode }); }

	render() {
		return (
			/*"ThemeContext वाले reference to a created context से मिला Provider Component 
			फ़लाना (जिसमे हमारा state भी है) props के साथ जो भी children (nested components) मिले उन पर render कर दो "*/
			<ThemeContext.Provider value = {{...this.state, toggleTheme: this.toggleTheme}} >
				{this.props.children}
			</ThemeContext.Provider>
		)
	}
}

export default ThemeProvider