import React from 'react'
import './GridBlock.css'

export class GridBlock extends React.Component {
	constructor(props) {
		super(props);
		this.handleToggle = this.handleToggle.bind(this);
	}

	handleToggle(evt) {
		console.log(evt.target);
		console.log('current state is ',this.props.lightsOnStatus);
		this.props.toggleLight(this.props.blockId);

	}

	render() {
		return (
			<div 
				className = 'GridBlock'
				onClick = {this.handleToggle}
				value = {this.props.blockId}
				style = {{ backgroundColor: this.props.lightsOnStatus ? "orangered" : "grey" }}
			>
				<b>{this.props.blockId}</b>
			</div>
		)
	}
}

export default GridBlock