import React from 'react'
import "./Cell.css"

export class Cell extends React.Component {
	constructor(props) {
	    super(props);
	    this.handleClick = this.handleClick.bind(this);
  	}

  	handleClick(evt) {
	    // call up to the board to flip cells around this cell
	    this.props.flipCellsAround();
  	}

	render() {
		let classes = "Cell" + (this.props.isLit ? " Cell-lit" : "");
		return (
			<td className={classes} onClick={this.handleClick} />
		)
	}
}

export default Cell