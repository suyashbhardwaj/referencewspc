import React from 'react'
import './Board.css';
import Cell from "./Cell";

export class Board extends React.Component {

	static defaultProps = {
	    nrows: 5,
	    ncols: 5,
	    chanceLightStartsOn:0.25  
	  }

	constructor(props) {
		super(props);
	    this.createBoard = this.createBoard.bind(this);

	    this.state = {
	      hasWon: false,
	      board: this.createBoard()
	  	}
	}

	createBoard() {
		let board = [];
			for(let y=0; y<this.props.nrows; y++){
				let row = [];
				for(let x=0; x<this.props.ncols; x++){
					row.push(Math.random() < this.props.chanceLightStartsOn);
				}
			board.push(row);
			}
	    return board;
	}

	flipCellsAround(coord) {
		console.log('invoked flipCellsAround with coord: ', coord);
	    let {ncols, nrows} = this.props;
	    let board = this.state.board;
	    let [y, x] = coord.split("-").map(Number);
	    let hasWon = board.every(row => row.every(cell => !cell));

	    function flipCell(y, x) {
		    if (x >= 0 && x < ncols && y >= 0 && y < nrows) {
	        	board[y][x] = !board[y][x];
	    	}
	    }

	    flipCell(y,x);
	    flipCell(y,x-1);
	    flipCell(y,x+1);
	    flipCell(y-1,x);
	    flipCell(y+1,x);

	    this.setState({board: board, hasWon: hasWon});
	}

	render() {
		/*const oneRow = (idx) => { return Array.from({ length: this.props.ncols }).map((y,idy) => {
				return (
					<Cell 
						key = {`${idx}-${idy}`}
						flipCellsAround = {this.flipCellsAround} 
						isLit = {false}
					/>
					)
				})
			};

		const stackOfRows = Array.from({ length: this.props.nrows }).map((x,idx) => {
			return (<tr>{oneRow(idx)}</tr>);
		});*/

		const stackOfRows = [];
		for(let y=0; y<this.props.nrows;y++){
			let row = [];
			for(let x=0; x<this.props.ncols; x++){
				let coord = `${y}-${x}`
				row.push(<Cell key = {coord} flipCellsAround = {() => this.flipCellsAround(coord)} isLit = {this.state.board[y][x]} />)
			}
			stackOfRows.push(<tr>{row}</tr>);
		}
		if (this.state.hasWon) {return <center><h2>YOU WON!</h2></center>}
		return (
			<div>
				<table className = 'Board'>
			        <tbody>
			            {stackOfRows}
			        </tbody>
			    </table>	
			</div>
		)
	}
}

export default Board