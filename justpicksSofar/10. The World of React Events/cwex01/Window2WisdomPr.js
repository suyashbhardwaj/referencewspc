
/*inline binding creates new functions for every bind - leads to hampered performance, although minimal + explicitly tells*/

import React from 'react'
import './Window2Wisdom.css'

export class Window2WisdomPr extends React.Component {
	
	static defaultProps = {
		wisedommessages : [
			'honesty is the best policy',
			'love thy neighbour as thy self',
			'जात की १०० बीघा, किसान की ८ बीघा और माली की २ बीघा - एक बराबर।',
			'consciousness come with the pain',
			'an eye to an eye makes the whole world blind'
			]
	};

	/*resolve context loss of 'this' - using method bind in constructor */
	/*constructor(props) {
		super(props);
		this.handleMouseEnter = this.handleMouseEnter.bind(this);
		this.handleCopy = this.handleCopy.bind(this);
	}*/

	/*handleMouseEnter() {*/
	/*resolve context loss of 'this' - using alternative Binding With Class Properties */
	handleMouseEnter = () => {
		!this ? console.log("I AM THIS, I\'VE LOST CONTEXT!!") : console.log('I am this, I know who I am.. I am ',this);
		let { wisedommessages } = this.props;
		document.getElementById('yoyo').style.backgroundColor= 'grey';
		let randIndex = Math.floor(Math.random()*wisedommessages.length);
		console.log(wisedommessages[randIndex]);
	}

	handleCopy() {
		console.log('लो हो गया सब कापी');
	}

	render() {
		return (
			<div className = 'window'>
			<div id='yoyo' className = 'window-item' onMouseEnter = {this.handleMouseEnter}>  🎃	</div>
			{/*resolve context loss of 'this' - by binding inline: pass the result i.e. returned function (~this has context) of .bind(this)*/}
			{/*<div id='yoyo' className = 'window-item' onMouseEnter = {this.handleMouseEnter.bind(this)}>  🎃	</div>*/}
			{/*resolve context loss of 'this' - by using arrow function*/}
			{/*<div id='yoyo' className = 'window-item' onMouseEnter = {() => this.handleMouseEnter()}>  🎃	</div>*/}

			<textarea onKeyUp = {this.handleMouseEnter}/>
			<section style = {{ width: '300px', backgroundColor: 'ivory'}} onCopy = {this.handleCopy}>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro aut explicabo fugiat deserunt dolorum facilis iure obcaecati nihil. Cumque similique repudiandae temporibus tenetur optio, nulla aut enim rem quia eius.
			</section>
			</div>
		)
	}
}

export default Window2WisdomPr