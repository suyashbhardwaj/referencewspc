import React from 'react'
import './Window2Wisdom.css'

export class Window2Wisdom extends React.Component {
	constructor(props) {
		super(props);
		this.handleMouseEnter = this.handleMouseEnter.bind(this);
		this.handleCopy = this.handleCopy.bind(this);
	}

	handleMouseEnter() {
		document.getElementById('yoyo').style.backgroundColor= 'grey';
		const wisedommessages = [
			'honesty is the best policy',
			'love thy neighbour as thy self',
			'जात की १०० बीघा, किसान की ८ बीघा और माली की २ बीघा - एक बराबर।'
			];

		let randIndex = Math.floor(Math.random()*wisedommessages.length);
		console.log(wisedommessages[randIndex]);
	}

	handleCopy() {
		console.log('लो हो गया सब कापी');
	}

	render() {
		return (
			<div className = 'window'>
			<div id='yoyo' className = 'window-item' onMouseEnter = {this.handleMouseEnter}>  🎃	</div>
			<textarea onKeyUp = {this.handleMouseEnter}/>
			<section style = {{ width: '300px', backgroundColor: 'ivory'}} onCopy = {this.handleCopy}>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro aut explicabo fugiat deserunt dolorum facilis iure obcaecati nihil. Cumque similique repudiandae temporibus tenetur optio, nulla aut enim rem quia eius.
			</section>
			</div>
		)
	}
}

export default Window2Wisdom