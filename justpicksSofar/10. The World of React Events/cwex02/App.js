/*01 what are the components we need
02 how many could we get away with 
03 how much state do we need to store
04 where do we store it*/

import './App.css';
import React from 'react'
import ButtonList from './ButtonList'

export class App extends React.Component {
  render() {
    return (
      <div className = "App">
        <ButtonList />
      </div>
    )
  }
}

export default App