import React from 'react'

export class ButtonList extends React.Component {

	static defaultProps = {
		colors: ['teal', 'grey', 'skyblue', 'yellow']
	}

	constructor(props){
		super(props);
		this.state = { color: 'cyan' };
		// this.handleClick = this.handleClick.bind(this);
	}

	handleClick(newColor){
		console.log('msg received is: ',newColor);
		this.setState({color:newColor});
	}

	render() {
		return (
			<div className = 'ButtonList' style = {{ backgroundColor:this.state.color }}>
				{ this.props.colors.map(c => {
					const colorObj = { backgroundColor: c };
						{/*return <button style = {colorObj} onClick = {() => this.handleClick('hey sexy')}>Pass Arguments</button>;*/}
						{/*return <button style = {colorObj} onClick = {this.handleClick.bind(this,c)}>Pass Arguments</button>;*/}
						return <button style = {colorObj} onClick = {() => this.handleClick(c)}>Pass Arguments</button>;
						{/*इस तरीके से करने मे हर बार हर एक button के लिय नया function बनेगा। Not a good idea!*/}
					}) 
				}
				
			</div>
		)
	}
}

export default ButtonList