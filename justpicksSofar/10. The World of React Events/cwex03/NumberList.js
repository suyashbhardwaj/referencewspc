import React from 'react'
import NumberItem from './NumberItem'

export class NumberList extends React.Component {
	constructor(props){
		super(props);
		this.state = { nums: [1,2,3,4,5] };
	}
	
	remove(arg){
		console.log("yoparent component's remove() invoked with arg as ", arg);
		this.setState(currentState => ({ nums: currentState.nums.filter(n => n!=arg) }));
	}

	render() {
		/*for each react render (i.e. @ each state update), a fresh new temporary function is created because of below inline binding*/
		let nums = this.state.nums.map(x => <NumberItem value = {x} remove = {()=> this.remove(x)}/>);
		return (
			<div>
				<h1>First Number List</h1>
				<ul>{nums}</ul>
			</div>
		)
	}
}

export default NumberList