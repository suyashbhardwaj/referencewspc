/*OUR STATELESS CHILD COMPONENT ~ DOWNWARD DATA FLOW, PARENT'S STATE UPDATE TRIGERRED FROM CHILD*/
import React from 'react'

export class BetterNumberItem extends React.Component {
	constructor(props){
		super(props);
		this.handleRemove = this.handleRemove.bind(this);
	}

	handleRemove(evt){
		console.log("child component's eventhandler invoked.");
		this.props.remove(this.props.value);
	}

	render() {
		return (
			<li>
				{this.props.value}
				{/*the default value that gets passed to an event handler (this.props.remove here) is the event object itself,no need to mention..*/}
				{/*<button onClick = {this.props.remove}>x</button>*/}
				<button onClick = {this.handleRemove}>x</button>
			</li>
		);
	}
}

export default BetterNumberItem