/*OUR STATEFUL PARENT COMPONENT ~ DOWNWARD DATA FLOW*/
import React from 'react'
import NumberItem from './BetterNumberItem'

export class BetterNumberList extends React.Component {
	constructor(props){
		super(props);
		this.state = { nums: [1,2,3,4,5] };
		this.remove = this.remove.bind(this);	//remove() binded just once here
	}
	
	remove(arg){
		console.log("parent component's remove() invoked with arg as ", arg);
		this.setState(currentState => ({ nums: currentState.nums.filter(n => n!=arg) }));
	}

	render() {
		const customkeys = ['*^5GFJ7','FH46d7$','DH#%f','+)(FJ6','#^fjhH'];
		/*for each react render (i.e. @ each state update), a fresh new temporary function is created because of below inline binding*/
		/*let nums = this.state.nums.map(x => <NumberItem value = {x} remove = {()=> this.remove(x)}/>);*/
		/*let nums = this.state.nums.map(x => <NumberItem value = {x} remove = {this.remove}/>);*/	//no more re-creating functions here
		let nums = this.state.nums.map((x,idx) => <NumberItem key = {customkeys[idx]} value = {x} remove = {this.remove}/>); // no more unique key error
		return (
			<div>
				<h1>Better Number List</h1>
				<ul>{nums}</ul>
			</div>
		)
	}
}

export default BetterNumberList