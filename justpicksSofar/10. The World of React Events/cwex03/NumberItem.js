import React from 'react'

export class NumberItem extends React.Component {
	constructor(props){
		super(props);
	}

	render() {
		return (
			<li>
				{this.props.value}
				<button onClick = {this.props.remove}>x</button>
			</li>
		);
	}
}

export default NumberItem