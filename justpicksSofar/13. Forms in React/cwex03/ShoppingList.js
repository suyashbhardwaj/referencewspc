import React from 'react'
import ShoppingListForm from './ShoppingListForm'
import {v4 as uuid} from "uuid"

export class ShoppingList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			items: [
					{ itemname:"spinach", qty:"200gms", id:uuid() },
					{ itemname:"cauliflower", qty:"1 nos", id:uuid() }
				]
			};
		this.renderItems = this.renderItems.bind(this);
		this.addItem = this.addItem.bind(this);
		}

	renderItems(){
		return (
			<ul>
				{ this.state.items.map(item => (<li> {item.itemname} : {item.qty} </li>)) }
			</ul>
		);
	}

	addItem(item) {
		this.setState(curState => {
			let newItem = { ...item, id:uuid() }
			return { items: [...curState.items, newItem]}
		})
	}

	render() {
		return (
			<div>
				<h1>The Shopping List</h1>
				{this.renderItems()}
				<hr />
				<ShoppingListForm addItem = {this.addItem}/>
			</div>
		)
	}
}

export default ShoppingList