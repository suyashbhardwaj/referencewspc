import React from 'react'

export class ShoppingListForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = { itemname:"", qty:"" };
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(evt) {
		this.setState({
			[evt.target.name]:evt.target.value
		});
	}

	handleSubmit(evt) {
		evt.preventDefault();
		console.log(this.state);
		this.props.addItem(this.state);
	}

	render() {
		return (
			<div>
				<form onSubmit = {this.handleSubmit}>
					<input 
						type="text" 
						name="itemname" 
						value={this.state.itemname}
						onChange={this.handleChange}
						placeholder="Item Name"
					/>
					<input 
						type="text" 
						name="qty" 
						value={this.state.qty}
						onChange={this.handleChange}
						placeholder="Quantity"
					/>
					<button>addItem.</button>
				</form>
			</div>
		)
	}
}

export default ShoppingListForm