import React from 'react'

export class FormMultipleInputReactWay extends React.Component {
	constructor(props) {
		super(props);
		this.state = { username: "", email: "", password: "" }
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(evt) {
		this.setState({ [evt.target.name]: evt.target.value });
	}

	handleSubmit(evt){
			evt.preventDefault();
			alert(`you entered ${this.state.username}`);
			this.setState({ username: "" });
		}

	render() {
		return (
			<div className = "main-centerdiv">
				<form onSubmit={this.handleSubmit}>
					<label htmlFor="uname">Username</label>
					<input id="uname" type="text" name="username" value = {this.state.username} onChange = {this.handleChange} placeholder="username" />
					<input type="text" name="email" value = {this.state.email} onChange = {this.handleChange} placeholder="email" />
					<input type="password" name="password" value = {this.state.password} onChange = {this.handleChange} placeholder="password" />
					<button>Submit</button>
				</form>	
			</div>
		)
	}
}

export default FormMultipleInputReactWay