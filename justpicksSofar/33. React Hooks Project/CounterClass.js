import React from 'react'

export class CounterClass extends React.Component {
	constructor(props) {
		super(props);
		this.state = { count: 0 }
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		this.setState({ count: this.state.count+1 })
	}

	render() {
		return (
			<div>
				<h1>The count is {this.state.count}</h1>
				<button onClick = {this.handleClick}>Add one</button>
			</div>
		)
	}
}

export default CounterClass