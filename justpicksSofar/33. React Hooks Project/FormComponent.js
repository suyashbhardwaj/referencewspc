import React from 'react'
import useMyFormHook from './useMyFormHook'

export class FormComponent extends React.Component {
	/*constructor(props) {
		super(props);
		this.state = { uname: "", pass: "" };
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(evt){	this.setState({[evt.target.name]: evt.target.value}); }*/
	const [uname, changeUname, resetUname] = useMyFormHook("");
	const [pass, changePass, resetPass] = useMyFormHook("");
	render() {
		return (
			<div>
				{/*<h4>uname set up as : {this.state.uname} | pass set up as : {this.state.pass}</h4>*/}
				<h4>uname set up as : {uname} | pass set up as : {pass}</h4>
				<hr/>
				<label htmlFor="uname">User ID</label>
				{/*<input type="text" name = "uname" onChange={this.handleChange} />*/}
				<input type="text" name = "uname" onChange={changeUname} />
				<label htmlFor="pass">Password</label>
				{/*<input type="password" name= "pass" onChange={this.handleChange} />*/}
				<input type="password" name= "pass" onChange={changePass} />
			</div>
		)
	}
}

export default FormComponent