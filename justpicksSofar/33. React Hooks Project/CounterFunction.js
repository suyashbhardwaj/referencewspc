import React, { useState } from 'react'

const CounterFunction = () => {
	const [count, setCount] = useState(0);
	return (
		<div>
			<h1>The count is {count}</h1>
			<button onClick = {()=> setCount(count+1)}>Add one</button>	
		</div>
	)
}

export default CounterFunction