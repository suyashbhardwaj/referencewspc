import React, { useState } from 'react'


const useMyTogglerHook = (initialValue = false) => {
	const [state, setState] = useState(initialValue);
	const togglerFn = () => setState(!state);
	return [state, togglerFn];
	
}

export default useMyTogglerHook