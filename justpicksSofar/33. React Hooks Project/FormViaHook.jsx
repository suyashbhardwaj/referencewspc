import React from 'react'
import useMyForm from './useMyForm'

const FormViaHook = () => {
	const [email, setEmail, resetEmail] = useMyForm("");
	const [password, setPassword, resetPassword] = useMyForm("");
	const resetForm = () => { resetEmail(); resetPassword(); }
	return (
		<div>
			<h4>{email} & {password}</h4>
			<hr/>
			<label htmlFor="uname"></label>
			<input type="text" name = "uname" onChange = {setEmail} value = {email}/>
			<label htmlFor="pass"></label>
			<input type="text" name= "pass" onChange = {setPassword} type="password" value = {password}/>
			<button onClick = {resetForm}>reset fields</button>
		</div>
	)
}

export default FormViaHook