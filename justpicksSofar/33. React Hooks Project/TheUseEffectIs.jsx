import React, { useEffect, useState } from 'react'
import axios from 'axios';

const TheUseEffectIs = () => {
	const [count, setCount] = useState(0);
	const [number, setNumber] = useState(1);
	const [movie, setMovie] = useState("");

	useEffect(()=> {
		document.title = `clicked ${count} times`;
	});

	useEffect(()=>{
		async function getData(){
			const response = await axios.get(`https://swapi.dev/api/films/${number}`);
			setMovie(response.data);
		}
		getData();
	},[number]); /*केवल number variable की value change होने पर ही useEffect चलना चाहिय। यह इसकी dependency है।*/  

	return (
		<div>
			<button onClick = {()=>setCount(count+1)}>raisecount{count}</button>
			{/*<button onClick = {()=>setCount(count+1, function(){alert("with every change in count")})}>raisecount{count}</button>*/}

			<h4>you selected {movie.title}</h4>
			<h6>{movie.opening_crawl}</h6>
			<hr/>
			<select name="movie" onChange = {(evt) => setNumber(evt.target.value)}>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
			</select>
		</div>
	)
}

export default TheUseEffectIs