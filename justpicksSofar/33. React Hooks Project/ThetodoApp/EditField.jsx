import React from 'react'
import { TextField } from '@mui/material';
import useInputState from './hooks/useInputState'

const EditField = ({id, task, editTodo, toggleEditMode}) => {
	const [editedTodo, setEditedTodo, resetEditedTodo] = useInputState(task);
	return (
		<div>
			<form style = {{marginLeft: '0.8rem',paddingTop: '0px', paddingBottom: '0px' , width: 'inherit'}} onSubmit = {(e) => {
				e.preventDefault();
				editTodo(id,editedTodo);
				resetEditedTodo();
				toggleEditMode();
			}}>
			<TextField value = {editedTodo} onChange = {setEditedTodo} fullWidth autoFocus/>
			</form>
		</div>
	)
}

export default EditField