import React, { useEffect, useState } from 'react'
import { AppBar, Grid, Paper, Toolbar, Typography } from '@mui/material';
import TodoList from './TodoList';
import TodoForm from './TodoForm'
import useMyTodoHook from './hooks/useMyTodoHook'

const initialTodos = JSON.parse(window.localStorage.getItem('todos') || "[]");

const TodoApp = () => {
	const { todos, toggleCheck, addTodo, removeTodo, editTodo } = useMyTodoHook(initialTodos);
	
	useEffect(() => {
		console.log('you made me render')
		window.localStorage.setItem("todos", JSON.stringify(todos));
	}, [todos])

	return (
		<Paper elevation = {3}
			style = {{
				padding: '0',
				margin: '0',
				height: '100vh',
				backgroundColor: 'darkslategrey'
			}}
			>
			<AppBar color= 'primary' position= 'static' style = {{height: '64px'}}>
				<Toolbar>
					<Typography color = 'inherit'> टू डू का मजा लो </Typography>
				</Toolbar>
			</AppBar>
			
			<Grid container style = {{marginTop: '1rem', justifyContent: 'center'}} >
				<Grid item xs = {11} md = {8} lg = {4}>
					<TodoForm addTodo = {addTodo}/>
					<TodoList todos = {todos} removeTodo = {removeTodo} toggleCheck = {toggleCheck} editTodo = {editTodo}/>
				</Grid>
			</Grid>
		</Paper>
	)
}

export default TodoApp