import React from 'react'
import {useState} from 'react'
import useMyTogglerHook from './useMyTogglerHook'		/*हमारा अपना custom hook*/
import './App.css'
import theimage from './assets/images/PhotoPopOutLBN2.jpg'
import theimage2 from './assets/images/doubleExposureLBN.jpg'

const Toggler = () => {
	/*const [isHappy, setIsHappy] = useState(true);
	const [isHeartBroken, setIsHeartBroken] = useState(false);*/

	/*भाइ, 
		एक तो stateVar दे जिसमे क्या value रखनी है मै बताउंगा और 
		एक दे reference to ऐसा function जो उस की value को तुम्हारे अपने उस standard तरीके से बदले/mutate करे invoke करने पर, एक array बना कर दे दे इन दोनो वस्तुओ का। */
	const [isHappy, toggleIsHappy] = useMyTogglerHook(true);
	const [isHeartBroken, toggleIsHeartBroken] = useMyTogglerHook(false);
	const [showBoobs, toggleShowBoobs] = useMyTogglerHook();

	/*const toggleHappy = () => { setIsHappy(!isHappy); }
	const toggleHeartBroken = () => { setIsHeartBroken(!isHeartBroken); }*/

	return (
		<>
			<div className = "main-centerdiv">
				{/*<button className="hinditext" onClick = {toggleHappy}>{isHappy ? "उदास": "प्रसन्न"}</button>
				<button className="hinditext" onClick = {toggleHeartBroken}>{isHeartBroken ? "दुखियारा": "उजियारा"}</button>*/}
				<button className="hinditext" onClick = {toggleIsHappy}>{isHappy ? "उदास": "प्रसन्न"}</button>
				<button className="hinditext" onClick = {toggleIsHeartBroken}>{isHeartBroken ? "दुखियारा": "उजियारा"}</button>
			</div>
			<div className = "image2toggle">
					<img src = {showBoobs? theimage: theimage2} onClick = {toggleShowBoobs} style = {{width:"inherit"}}/>
			</div>
		</>
	)
}

export default Toggler