import React from 'react'

const useMyFormHook = (initialValue) => {
	const [state, setState] = useState(initialValue);
	const setInput = (evt) => { setState(evt.target.value) }
	const resetState = { setState("") }

	return [state, setInput, resetState];
}

export default useMyFormHook