import React from 'react';
import CounterClass from './CounterClass';
import CounterFunction from './CounterFunction'
import Toggler from "./Toggler"
import FormViaHook from './FormViaHook'
import TheUseEffectIs from './TheUseEffectIs'

export class App extends React.Component {
  render() {

    return (
      <div className = 'App'>        
         <h4>That is it</h4>
         <hr/>
         {/*<CounterClass/>
         <hr/>
         <CounterFunction/>
         <Toggler />
         <FormViaHook />*/}
         <TheUseEffectIs />
      </div>  
    );
  }
}

export default App