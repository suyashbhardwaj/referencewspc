import React, { useState } from 'react'


const useMyForm = (initialValue) => {
	const [state, setState] = useState(initialValue);
	const handleChange = (evt) => { setState(evt.target.value) }
	const resetState = () => { setState("") }

	return [state, handleChange, resetState]
}

export default useMyForm