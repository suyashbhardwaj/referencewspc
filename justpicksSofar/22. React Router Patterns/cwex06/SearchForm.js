import React from 'react'
import { Link } from 'react-router-dom'

export class SearchForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = { skey:'' }
		this.handleChange = this.handleChange.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}

	handleChange(evt) {
		this.setState({ skey:evt.target.value })
	}

	handleClick(evt) {
		console.log('saving to database...');
		/*do something to save to database*/
		console.log('successfully saved to database...');
		alert('successfully saved to database...');
		/*redirect to somewhere else*/
		this.props.history.push('/shop/signs/item/underconstruction');
	}

	render() {
		return (
			<div>
				<input type = 'text' placeholder = 'find an image' onChange = {this.handleChange}/>
				<Link to = {`/item/${this.state.skey}`}> Go </Link>
				<button onClick = {this.handleClick} >SaveToDB</button>
			</div>
		)
	}
}

export default SearchForm