import './App.css';
import React from 'react'
import {Route, Switch} from 'react-router-dom'
import SearchUnsplash from './SearchUnsplash'

export class App extends React.Component {
  render() {
    return (
      <div className = "App">
        <Switch>
          <Route exact path = '/item/:skey' component = {SearchUnsplash} />
          <Route exact path = '/shop/:category/item/:skey' component = {SearchUnsplash} />
          <Route render = {()=><h1>404!! NOT FOUND!</h1>} />
        </Switch>
      </div>
    );
  }
}

export default App