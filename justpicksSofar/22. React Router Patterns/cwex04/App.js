/*01 what are the components we need: <ToDoList />, <ToDoListForm />, <ToDoItem />
02 how many could we get away with 
03 how much state do we need to store: 
  { todolist: [{ id: "", todotask: "", doneStatus: false },{ id: "", todotask: "", doneStatus: false }]}
  { todotask: "", doneStatus: false, isBeingEdited: false}
04 where do we store it*/

import './App.css';
import React from 'react'
import {Route, Switch} from 'react-router-dom'
import SearchUnsplash from './SearchUnsplash'
import SearchForm from './SearchForm'

export class App extends React.Component {
  render() {
    return (
      <div className = "App">
        {/*<h1>।। प्रतिक्रिया परियावरण मे आपका स्वागत है ।।</h1>
        <i className = 'fa fa-4x fa-television' />*/}
        <Switch>
          <Route exact path = '/' component = {SearchForm} />
          <Route exact path = '/item/:skey' component = {SearchUnsplash} />
          <Route exact path = '/shop/:category/item/:skey' component = {SearchUnsplash} />
          <Route render = {()=><h1>404!! NOT FOUND!</h1>} />
        </Switch>
      </div>
    );
  }
}

export default App