import React from 'react'
import { Link } from 'react-router-dom'

export class SearchForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = { skey:'' }
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(evt) {
		this.setState({ skey:evt.target.value })
	}

	render() {
		return (
			<div>
				<input type = 'text' placeholder = 'find an image' onChange = {this.handleChange}/>
				<Link to = {`/item/${this.state.skey}`}> Go </Link>
			</div>
		)
	}
}

export default SearchForm