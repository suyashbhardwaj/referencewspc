import React from 'react'
import './SearchUnsplash.css';

export class SearchUnsplash extends React.Component {
	render() {
		const skey = this.props.match.params.skey;
		const category = this.props.match.params.category;
		const caturl = `https://source.unsplash.com/1600x900/?${category}`;
		const url = `https://source.unsplash.com/1600x900/?${skey}`;
		return (
			<div className = 'searchframe'>
				<section>
					<img src = {caturl} alt = {category}/>
					<img src = {url} alt = {skey}/>
				</section>
				<section>
					<h2>{skey}</h2>
				</section>
			</div>
		)
	}
}

export default SearchUnsplash