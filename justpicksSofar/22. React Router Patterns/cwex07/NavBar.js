import React from 'react'
import './NavBar.css'
import { withRouter } from 'react-router-dom'

export class NavBar extends React.Component {
	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(evt) {
		alert('have logged you in..');
		this.props.history.push('/shop/flags/item/brazil');
	}

	render() {
		return (
			<div className = 'thenavbar'>
				<button onClick = {this.handleClick} >Log In</button>	
			</div>
		)
	}
}

/*इसको बताओ की routerProps साथ मे ले जानी है अपने।*/
export default withRouter(NavBar)