import './App.css';
import React from 'react'
import {Route} from 'react-router-dom'
import SearchUnsplash from './SearchUnsplash'

export class App extends React.Component {
  render() {
    return (
      <div className = "App">
        {/*<Route exact path = '/item/:skey' render = {routeProps => <SearchUnsplash {...routeProps}/>} />*/}
        <Route exact path = '/item/:skey' component = {SearchUnsplash} />
      </div>
    );
  }
}

export default App