import AccessibilityNewIcon from '@mui/icons-material/AccessibilityNew';
import PointOfSaleIcon from '@mui/icons-material/PointOfSale';
import SummarizeIcon from '@mui/icons-material/Summarize';
import SecurityIcon from '@mui/icons-material/Security';
import ToggleOffIcon from '@mui/icons-material/ToggleOff';
import SettingsIcon from '@mui/icons-material/Settings';

export const mainNavbarItems = [
		{
			id:0,
			icon:<PointOfSaleIcon/>,
			label:'Transactions',
			route:'transactions'
		},
		{
			id:1,
			icon:<SummarizeIcon/>,
			label:'Reports',
			route:'reports'
		},
		{
			id:2,
			icon:<SecurityIcon/>,
			label:'Security',
			route:'security'
		},
		{
			id:3,
			icon:<ToggleOffIcon/>,
			label:'Configurations',
			route:'configurations'
		},
		{
			id:4,
			icon:<SettingsIcon/>,
			label:'Admin Settings',
			route:'adminsettings'
		}
	];
