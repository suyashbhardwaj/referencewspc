import React, { useEffect, useState } from 'react'
import BasicModal from '../reusables/BasicModal/BasicModal'
import Box from '@mui/material/Box';
import Input from '@mui/material/Input';
import { TextField } from '@mui/material';
import * as yup from 'yup';
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import {v4 as uuid} from 'uuid'

const defaultValues = {
	userId:'',
	email:'',
	phoneNumber:''
}

const NewUserModalF = ({ open, onClose, addNewUser }) => {
	const [values, setValues] = useState(defaultValues)	
	const modalStyles = {
			inputFields: {
				display:'flex',
				flexDirection:'column',
				marginTop: '20px',
				marginBottom:'15px',
				'.MuiFormControl-root': {
					marginBottom:'20px',
				},
				'.MuiOutlinedInput-notchedOutline:-webkit-autofill': {
					backgroundColor:'skyblue'
				}
			}
		}

	const validationSchema = yup
	  .object()
	  .shape({
	    userId: yup.string().required('user ID has to be there').min(6,'UserId has to be atleast 6 characters long'),
	    email: yup.string().required('email is required').email('entered mail id is invalid'),
	    phoneNumber: yup.string()/*.matches(phoneRegEx,'entered invalid phone number')*/
	  });

	const { register, handleSubmit, formState: {errors} } = useForm({ resolver: yupResolver(validationSchema) });

	const addUser = (data) => {
		data = {...data, key:uuid()}
		addNewUser(data);
	}

	const handleChange = (currentValues) => {
		setValues(currentValues);
	}

	const getContent = () => {
		return (
			<Box sx={modalStyles.inputFields}>
			    <TextField
			    	placeholder = 'UserID'
			    	name = 'userId'
			    	label = 'User ID'
			    	required
			    	error
			    	{...register('userId')}
			    	error = { errors.userId? true: false }
			    	helperText = { errors.userId?.message }
			    	onChange = {(event)=>handleChange({...values, userId:event.target.value})}
			    	value = {values.userId}
			    />
			    <TextField
			    	placeholder = 'Email ID'
			    	name = 'email'
			    	label = 'Email ID'
			    	required
			    	error
			    	{...register('email')}
			    	error = { errors.email? true: false }
			    	helperText = { errors.email?.message }
			    	onChange = {(event)=>handleChange({...values, email:event.target.value})}
			    	value = {values.email}
			    />
			    <TextField
			    	placeholder = 'Phone Number'
			    	name = 'phoneNumber'
			    	label = 'Phone Number'
			    	required
			    	error
			    	{...register('phoneNumber')}
			    	error = { errors.phoneNumber? true: false }
			    	helperText = { errors.phoneNumber?.message }
			    	onChange = {(event)=>handleChange({...values, phoneNumber:event.target.value})}
			    	value = {values.phoneNumber}
			    />
			</Box>
			)
		}

	useEffect(() => {
		if(open) setValues(defaultValues)
	}, [open])
	
	return (
		<BasicModal
			open = {open}
			handleClose = {onClose}
			title = 'New User'
			subtitle = "Fill out inputs and hit the 'submit' button"
			content = {getContent()}
			onSubmit = {handleSubmit(addUser)}
		>
		</BasicModal>
	)
}

export default NewUserModalF