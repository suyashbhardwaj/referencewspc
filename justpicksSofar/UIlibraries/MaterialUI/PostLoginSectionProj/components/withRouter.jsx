import React from 'react'
import { useNavigate } from 'react-router-dom'

const withRouter = WrappedComponent => props => {
	const navigate = useNavigate();
	const navogate = (msg) => {alert(msg)}
	/*और जो कोई भी hooks तुझे किसी भी class component मे use करने हो वे यहां लिख दे। */

	return (
		<WrappedComponent
			{...props}
			{...{navigate, navogate}}
		/>
	);
};

export default withRouter