import * as React from 'react';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import {mainNavbarItems} from './mainNavbarItems';
import { Navigate } from "react-router-dom";
import withRouter from './withRouter'

export class Navbar extends React.Component {
	render() {
		const drawerWidth = 240;
		const {navigate} = this.props;
		return (
			<Drawer
		        sx={{
		          width: drawerWidth,
		          flexShrink: 0,
		          '& .MuiDrawer-paper': {
		            width: drawerWidth,
		            boxSizing: 'border-box',
		            backgroundColor: '#101F33',
		            color:'rgba(255,255,255,0.7)'
		          },
		        }}
		        variant="permanent"
		        anchor="left"
		    >
		        <Toolbar />
		        <Divider />
		        <List>
		          {mainNavbarItems.map((item, index) => (
		            <ListItem key={item.id} disablePadding>
		              <ListItemButton onClick = {()=>navigate(item.route)}>
		                <ListItemIcon sx = {{color:'rgba(255,255,255,0.7)'}}>
		                  {item.icon}
		                </ListItemIcon>
		                <ListItemText primary={item.label} />
		              </ListItemButton>
		            </ListItem>
		          ))}
		        </List>
		        {/*<Divider />*/}
		    </Drawer>
		)
	}
}

export default withRouter(Navbar)