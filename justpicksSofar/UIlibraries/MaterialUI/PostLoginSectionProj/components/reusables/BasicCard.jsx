import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';

export class BasicCard extends React.Component {
	render() {
		const cardStyles = {
			width:'80%',
			position:'absolute',
			/*left:'50%',
			transform:'translateX(-50%)'*/
			borderRadius:'8px'
		}

		return (
			<Card sx={cardStyles}>
				{this.props.header}
		      	<CardContent sx = {{display:'flex', flexDirection: 'column' }}>
		      		{this.props.content}
		      	</CardContent>
		    </Card>
		)
	}
}

export default BasicCard