import React from 'react'
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

export class BasicMenu extends React.Component {
	render() {
		const { anchorEl, handleCloseMenu } = this.props;
		return (
			<div>
			    <Menu
			        id="basic-menu"
			        anchorEl={anchorEl}
			        open={Boolean(anchorEl)}
			        onClose={handleCloseMenu}
			        MenuListProps={{
			          'aria-labelledby': 'basic-button',
			        }}
			      >
			        <MenuItem onClick={handleCloseMenu}>Profile</MenuItem>
			        <MenuItem onClick={handleCloseMenu}>My account</MenuItem>
			        <MenuItem onClick={handleCloseMenu}>Logout</MenuItem>
			    </Menu>	
			</div>
		)
	}
}

export default BasicMenu