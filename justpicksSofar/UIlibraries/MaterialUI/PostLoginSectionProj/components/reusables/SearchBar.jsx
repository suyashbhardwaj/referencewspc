import React from 'react'
import Input from '@mui/material/Input';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';

export class SearchBar extends React.Component {
	render() {
		return (
			<Box sx = {{ display: 'flex', alignItems: 'center' }}>
				<IconButton>
					<SearchIcon sx = {{marginRight: '10px'}}/>
				</IconButton>
				<Input 
					placeholder="Search by email address, phone number, or user ID"
					onChange = {this.props.onChange} 
					sx = {{
						width:this.props.searchBarWidth,
						color: 'rgba(0,0,0,0.6)',
						fontSize:'1.1rem'
					}}
					disableUnderline
					/>
			</Box>
		)
	}
}

export default SearchBar