import React from 'react'
import Grid from '@mui/material/Grid';

export class GridWrapper extends React.Component {
	render() {
		const gridWrapperStyles = {
			display:'flex',
			justifyContent:'center',
			alignItems:'center',
			
			position: 'relative' ,
			padding: '48px 32px' ,
			/*minHeight: 'calc(70vh)',*/ /*कम से कम इतनी जगह तो यह div लेगा ही irrespectice of की content को कितने की आवश्यक्ता है। */
			minHeight: 'calc(100vh - 166px)',
			marginLeft:'240px',
			backgroundColor:'#eaeff1'
		}

		return (
			<Grid item xs={12} sx = {gridWrapperStyles}>
				{this.props.children}
			</Grid>
		)
	}
}

export default GridWrapper