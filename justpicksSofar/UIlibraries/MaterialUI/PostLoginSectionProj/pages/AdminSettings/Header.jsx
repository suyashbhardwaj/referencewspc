import React from 'react'
import Grid from '@mui/material/Grid';
import NotificationBell from '../../components/reusables/NotificationBell'
import MyButton from '../../components/reusables/MyButton'
import Avatar from '@mui/material/Avatar';
import avatarface from '../../../../../assets/images/hazel.jpg'
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import HelpIcon from '@mui/icons-material/Help';

export class Header extends React.Component {
	constructor(props){
		super(props);
		this.state = { anchorEl:null }
		this.handleOpen = this.handleOpen.bind(this);
		this.handleClose = this.handleClose.bind(this);
	}

	handleOpen(evt) {
		this.setState({ anchorEl:evt.target })
	}

	handleClose(evt) {
		this.setState({ anchorEl:null })
	}

	render() {
		const headerStyles = {
			wrapper: {
				width: '100%', 
				display:'flex',
				flexDirection: 'column',
				backgroundColor:'#009be5', 
				padding: '20px', 
			},
			toprow: {
				display:'flex',
				flexDirection:'row',
				justifyContent: 'flex-end',
				alignItems: 'center',
				marginBottom: '20px',
				'*': {
					marginRight: '5px'
				}
			},
			middlerow: {
				display:'flex',
				flexDirection:'row',
				justifyContent: 'space-between',
				alignItems: 'center',
				marginBottom: '20px',
				marginLeft: '320px'
			},
			link: {
				fontWeight:500,
				color:'rgba(255,255,255,0.7)',
				'&:hover': {
					color:'#fff',
					cursor:'pointer'
				}
			},
			webbutton: {
				marginRight:'5px'
			}
		}

		return (
			<Box sx = {headerStyles.wrapper}>
				<Box sx = {headerStyles.toprow}>
					<Typography sx = {headerStyles.link}>Go To Docs</Typography>
					<NotificationBell
						badgeContent = {3}
						iconColor = 'primary'
						anchorEl = {this.state.anchorEl}
						onClick = {this.handleOpen}
						handleClose = {this.handleClose}
						/>
					<Avatar alt="Remy Sharp" src={avatarface} />
				</Box>

				<Box sx = {headerStyles.middlerow}>
					<Typography>{this.props.title}</Typography>
					<MyButton variant = 'outlined' styleoverride = {headerStyles.webbutton}>Web Setup</MyButton>
					<Tooltip title='some question'>
						<IconButton>
							<HelpIcon/>
						</IconButton>
					</Tooltip>
				</Box>

			</Box>
		)
	}
}

export default Header