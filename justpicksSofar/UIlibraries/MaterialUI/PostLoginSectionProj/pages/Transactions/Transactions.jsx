import React from 'react'
import Grid from '@mui/material/Grid';
import MyButton from '../../components/reusables/MyButton'

export class Transactions extends React.Component {
	render() {
		const buttonstyle = { 
			fontSize:'0.875rem', 
			fontWeight:600,
			textTransform:'capitalize',
			borderRadius:2.5, 
			color:'orangered',
			'&:hover':{
				backgroundColor:'pink'
			},

			'&.MuiButton-contained': {
				backgroundColor:'orange',
				'&:hover': {
					backgroundColor:'yellow'
				}
			},
			
			'&.MuiButton-outlined': {
				color:'white',
				backgroundColor:'skyblue',
				'&:hover': {
					backgroundColor:'transparent'
				}
			}
		}

		return (
			<Grid item xs={8} style = {{backgroundColor:'#009BE5', display: 'inline-flex', justifyContent: 'space-evenly' }}>
				This is the transactions page.
				<MyButton variant = 'contained' styleoverride = {buttonstyle}> Add User </MyButton>
				<MyButton variant = 'outlined' styleoverride = {buttonstyle}> web setup </MyButton>
				<MyButton color='primary'> regularbutton </MyButton>
			</Grid>
		)
	}
}

export default Transactions