import React from 'react'
import Navbar from './components/Navbar'
import {Routes, Route} from 'react-router-dom'
import Transactions from './pages/Transactions/Transactions'
import AdminSettings from './pages/AdminSettings/AdminSettings'
import Security from './pages/Security/Security'
import Configurations from './pages/Configurations/Configurations'
import Reports from './pages/Reports/Reports'

import Grid from '@mui/material/Grid';
import { ThemeProvider } from '@mui/material/styles';
import {postloginTheme} from './sitethemes/postloginTheme'

export class PLlandingPage extends React.Component {
	render() {
		return (
			<ThemeProvider theme = {postloginTheme}>
        		<Grid container spacing = {1}>
					<Navbar/>
					<Routes>
			            <Route path = '/transactions' element = {<Transactions/>}/>
			            <Route path = '/reports' element = {<Reports/>}/>
			            <Route path = '/configurations' element = {<Configurations/>}/>
			            <Route path = '/security' element = {<Security/>}/>
			            <Route path = '/adminsettings' element = {<AdminSettings/>}/>
			        </Routes>
				</Grid>
      		</ThemeProvider>		
		)
	}
}

export default PLlandingPage