import React from 'react'

export class Box extends React.Component {

	constructor(props){
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(evt) {
		this.props.removefn(this.props.uid);
	}

	render() {
		const boxstyling = {
			backgroundColor: this.props.bcolor ,
			width: `${this.props.width}px`,
			height: `${this.props.height}px`
		}

		return (
			<>
			<div style = {boxstyling} ></div>
			<button onClick = {this.handleClick}>removeIt</button>
			</>
		)
	}
}

export default Box