import React from 'react'
import Box from './Box'
import {v4 as uuid} from 'uuid'
import BoxListForm from './BoxListForm'

export class BoxList extends React.Component {
	constructor(props){
		super(props);
		this.state = { boxlist: [
				{id: uuid(), bcolor: 'grey', width:100, height:40},
				{id: uuid(), bcolor: 'orange', width:160, height:40}
			]
		}
		this.add2boxes = this.add2boxes.bind(this);
		this.removeMe = this.removeMe.bind(this);
	}

	add2boxes(box){
		this.setState(curState => {
			let idfiedbox = {id:uuid(), ...box}
			return { boxlist: [...curState.boxlist, idfiedbox] }
		})
	}

	removeMe(uid){
		console.log('removing the box', uid);

		this.setState(curState => {
			let newarr = curState.boxlist.filter((obj) => obj.id!==uid);
			return { boxlist: [...newarr] }

			/*let foundAt = curState.boxlist.findIndex((obj)=>obj.id===uid)
			console.log('found at index ',foundAt);
			curState.boxlist.splice(foundAt,1);
			return curState;*/
		})
	}

	render() {
		const boxes = this.state.boxlist.map(bx => {
			return(
				<Box 
					bcolor= {bx.bcolor}
					width={bx.width}
					height={bx.height}
					removefn={this.removeMe}
					key={bx.id}
					uid={bx.id}
				/>
			)
		});

		return (
			<div>
				<h2>The box maker thingy</h2>
				<BoxListForm addabox = {this.add2boxes}/>
				<hr/>
				{boxes}
			</div>
		)
	}
}

export default BoxList