import React from 'react'

export class BoxListForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = { bcolor: '', width:'', height:'' }
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(evt){
		this.setState({
			[evt.target.name]:evt.target.value
		})
	}

	handleSubmit(evt){
		evt.preventDefault();
		this.props.addabox(this.state);
		this.setState({	bcolor: '', width:'', height:''	})
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<input id="bcolor" type="color" name="bcolor" value = {this.state.bcolor} onChange = {this.handleChange} placeholder="background color" />
					<input type="text" name="width" value = {this.state.width} onChange = {this.handleChange} placeholder="width" />
					<input type="text" name="height" value = {this.state.height} onChange = {this.handleChange} placeholder="height" />
					<button>DrawBox</button>
				</form>	
			</div>
		)
	}
}

export default BoxListForm