import React from 'react'
import './Joke.css'
import emojiAngry from '../assets/icons/emojiAngry.svg'
import emojiConfused from '../assets/icons/emojiConfused.svg'
import emojiLaughing from '../assets/icons/emojiLaughing.svg'
import emojiSmiling from '../assets/icons/emojiSmiling.svg'
import emojiRofl from '../assets/icons/emojiRofl.svg'

export class Joke extends React.Component {
	constructor(props) {
		super(props);
		this.getColor = this.getColor.bind(this);
		this.getEmoji = this.getEmoji.bind(this);
	}

	getColor() {
		return this.props.votes > 15 ? '#4CAF50' : (this.props.votes > 12 ? '#8BC34A' : (this.props.votes > 9 ? '#CDDC39' : (this.props.votes > 6 ? '#FFEB3B': (this.props.votes > 3 ? '#FFC107': (this.props.votes > 0 ? '#FF9800' : '#F44336')))));
	}

	getEmoji() {
		return this.props.votes > 12 ? emojiRofl : (this.props.votes > 10 ? emojiLaughing : (this.props.votes > 6 ? emojiSmiling : (this.props.votes > 0 ? emojiConfused: emojiAngry)));
	}

	render() {
		return (
			<div className = 'joke'>
				<div className = 'joke-buttons'>
					<i className = 'fa fa-arrow-up' onClick = {this.props.upvote} />
					<span style = {{borderColor: this.getColor()}} >{this.props.votes}</span>
					<i className = 'fa fa-arrow-down' onClick = {this.props.downvote} />
				</div>
				<div className = 'thejoke'>{this.props.thejoke}</div>
				<div className = 'joke-smiley'>
					<img src = {this.getEmoji()} />
				</div>
			</div>
		)
	}
}

export default Joke