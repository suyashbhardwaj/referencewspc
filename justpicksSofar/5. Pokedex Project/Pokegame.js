import React from 'react'
import Pokedex from './Pokedex'

export class Pokegame extends React.Component {
	
	render() {
		const teamOnepokemons = [];

		const teamTwopokemons = [
			  {id: 4, name: 'Charmander', type: 'fire', base_experience: 62},
			  {id: 7, name: 'Squirtle', type: 'water', base_experience: 63},
			  {id: 11, name: 'Metapod', type: 'bug', base_experience: 72},
			  {id: 12, name: 'Butterfree', type: 'flying', base_experience: 178},
			  {id: 25, name: 'Pikachu', type: 'electric', base_experience: 112},
			  {id: 39, name: 'Jigglypuff', type: 'normal', base_experience: 95},
			  {id: 94, name: 'Gengar', type: 'poison', base_experience: 225},
			  {id: 133, name: 'Eevee', type: 'normal', base_experience: 65}
			];

		/*---------------------------------------------------------------------------------------------------*/
		while(teamOnepokemons.length < teamTwopokemons.length) {
			let randIndex = Math.floor(Math.random()*teamTwopokemons.length);
			let randPokemon = teamTwopokemons.splice(randIndex,1)[0];
			teamOnepokemons.push(randPokemon);
			}
		/*--------------------------------------------------{random setup of teams}--------------------------*/

		// गलत let yoyo = teamOnepokemons.reduce((accumulator, currentValue) => {return accumulator.base_experience + currentValue.base_experience});
		let teamOneTotal = teamOnepokemons.reduce((accumulator, currentValue) => {return accumulator + currentValue.base_experience},0);
		let teamTwoTotal = teamTwopokemons.reduce((accumulator, currentValue) => {return accumulator + currentValue.base_experience},0);
		
		/*let teamOneTotal = 0;
		let teamTwoTotal = 0;
		teamOnepokemons.map(pkmn => {teamOneTotal += pkmn.base_experience});
		teamTwopokemons.map(pkmn => {teamTwoTotal += pkmn.base_experience});*/
		console.log('total for teamOne is ',teamOneTotal);
		console.log('total for teamOne is ',teamTwoTotal);

		return (
			<div>
				This is a Pokegame Component <hr />
				<Pokedex pokemons = {teamOnepokemons} isWinner = {teamOneTotal > teamTwoTotal ? true : false} netExp = {teamOneTotal}/>
				<Pokedex pokemons = {teamTwopokemons} isWinner = {teamOneTotal < teamTwoTotal ? true : false} netExp = {teamTwoTotal}/>
			</div>
		)
	}
}

export default Pokegame