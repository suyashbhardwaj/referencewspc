import React from 'react'
import './Pokecard.css'
import imgsample from './images/lineportraitLBN.png'

export class Pokecard extends React.Component {
	render() {
		let pokeimg = `./images/${this.props.id}.png`;
		// console.log(`./images/${this.props.id}.png`);
		return (
			<div className = 'OnePokecard'>
				<h4 className = 'Pokecard-title'>{this.props.name}</h4>
				<img className = 'Pokecard-images' src = {imgsample} alt = {this.props.name} />
				<h5 className = 'Pokecard-data'>{this.props.type}</h5>
				<h6 className = 'Pokecard-data'>{this.props.exp}</h6>
			</div>
		)
	}
}

export default Pokecard