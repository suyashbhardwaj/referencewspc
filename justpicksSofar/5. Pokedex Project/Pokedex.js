import React from 'react'
import Pokecard from './Pokecard'
import './Pokedex.css'

export class Pokedex extends React.Component {

	static defaultProps = {
		pokemons : [
		  {id: 4, name: 'Charmander', type: 'fire', base_experience: 62},
		  {id: 7, name: 'Squirtle', type: 'water', base_experience: 63},
		  {id: 11, name: 'Metapod', type: 'bug', base_experience: 72},
		  {id: 12, name: 'Butterfree', type: 'flying', base_experience: 178},
		  {id: 25, name: 'Pikachu', type: 'electric', base_experience: 112},
		  {id: 39, name: 'Jigglypuff', type: 'normal', base_experience: 95},
		  {id: 94, name: 'Gengar', type: 'poison', base_experience: 225},
		  {id: 133, name: 'Eevee', type: 'normal', base_experience: 65}
		]
	}

	render() {
		let winlosetag = this.props.isWinner ? 
			<h1 className = 'Pokedex-winnerhandTag'>Winner</h1> 
			: <h1 className = 'Pokedex-loserhandTag'>Loser</h1>;
		return (
			<div className = 'Pokedex-cards'>
				{this.props.pokemons.map( pokemon => 
					<Pokecard 
						id = {this.props.pokemons[0].id} 
						name = {pokemon.name} 
						type = {pokemon.type} 
						exp = {pokemon.base_experience}
					/>
					)}
				<div className = 'Pokedex-WinnerLoserBadge'>
					{winlosetag}
					<h4> Net Experience: {this.props.netExp}</h4>
				</div>

				{/*<Pokecard
					id = {this.props.pokemons[0].id} 
					name = {this.props.pokemons[0].name} 
					type = {this.props.pokemons[0].type} 
					exp =  {this.props.pokemons[0].base_experience}
					/>

				<Pokecard
					id = {this.props.pokemons[1].id} 
					name = {this.props.pokemons[1].name} 
					type = {this.props.pokemons[1].type} 
					exp =  {this.props.pokemons[1].base_experience}
					/>*/}

			</div>
		)
	}
}

export default Pokedex