import React from 'react'
import './NavigateCards.css'
import MorphedImage from './MorphedImage';
import Gokuldham from './Gokuldham'
import Haryana from './Haryana'
import Signature from './Signature'

export class NavigateCards extends React.Component {
	constructor(props) {
		super(props);
		this.changePage = this.changePage.bind(this);
		this.state = { currentPage:"" }
	}

	changePage(page) {
		console.log('request to change page to ', page);
		this.setState( {currentPage: page} )
	}

	render() {
		const renderedPage = this.state.currentPage === 'gokuldham' ? <Gokuldham />
			: (this.state.currentPage === 'haryana' ? <Haryana />
			: (this.state.currentPage === 'signature' ? <Signature />
			: (this.state.currentPage === 'morphedimage' ? <MorphedImage />
			: <h4>WelcomeHome</h4> )));

		return (
			<div className = 'onecard'>
				<section className = 'links'>
					<i className = 'fa fa-heart fa-2x' onClick = {()=>this.changePage('gokuldham')}/>
					<i className = 'fa fa-heart fa-2x' onClick = {()=>this.changePage('haryana')}/>
					<i className = 'fa fa-heart fa-2x' onClick = {()=>this.changePage('signature')}/>
					<i className = 'fa fa-heart fa-2x' onClick = {()=>this.changePage('morphedimage')}/>
				</section>
				<section className = 'thecard'>
					{renderedPage}
				</section>					
			</div>
		)
	}
}

export default NavigateCards