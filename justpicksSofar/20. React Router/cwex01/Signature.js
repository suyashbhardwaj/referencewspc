import React from 'react'
import theimage from './assets/images/paperCutoutSignatureBikeLBN.jpg'
import './thecomponent.css'

export class Signature extends React.Component {
	render() {
		return (
			<div className = 'thecomponent'>
				<img src = {theimage} />
			</div>
		)
	}
}

export default Signature