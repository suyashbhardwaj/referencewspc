import React from 'react'
import theimage from './assets/images/PhotoPopOutLBN2.jpg'
import './thecomponent.css'

export class MorphedImage extends React.Component {
	render() {
		return (
			<div className = 'thecomponent'>
				<h2>This is a morphed image</h2>
				<img src = {theimage} />
			</div>
		)
	}
}

export default MorphedImage