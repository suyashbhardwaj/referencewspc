import React from 'react'
import theimage from './assets/images/BalbhavanExtract.jpg'
import './thecomponent.css'

export class Gokuldham extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className = 'thecomponent'>
				<h4>This गौमाता is {this.props.name}</h4>
				<img src = {theimage} />
			</div>
		)
	}
}

export default Gokuldham