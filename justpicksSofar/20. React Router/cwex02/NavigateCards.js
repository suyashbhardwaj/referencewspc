import React from 'react'
import './NavigateCards.css'
import { Route, Switch, Link, NavLink } from "react-router-dom";
import MorphedImage from './MorphedImage';
import Gokuldham from './Gokuldham'
import Haryana from './Haryana'
import Signature from './Signature'

const WelcomePage = () => <h4>WelcomeHome</h4>;

export class NavigateCards extends React.Component {
	constructor(props) {
		super(props);
		this.changePage = this.changePage.bind(this);
		this.state = { currentPage:"" }
	}

	changePage(page) {
		console.log('request to change page to ', page);
		this.setState( {currentPage: page} )
	}

	render() {
		const renderedPage = this.state.currentPage === 'gokuldham' ? <Gokuldham />
			: (this.state.currentPage === 'haryana' ? <Haryana />
			: (this.state.currentPage === 'signature' ? <Signature />
			: (this.state.currentPage === 'morphedimage' ? <MorphedImage />
			: <h4>WelcomeHome</h4> )));

		return (
			<div className = 'onecard'>
				<section className = 'links'>
					
					{/*<i className = 'fa fa-heart fa-2x' onClick = {()=>this.changePage('gokuldham')}/>
					<i className = 'fa fa-heart fa-2x' onClick = {()=>this.changePage('haryana')}/>
					<i className = 'fa fa-heart fa-2x' onClick = {()=>this.changePage('signature')}/>
					<i className = 'fa fa-heart fa-2x' onClick = {()=>this.changePage('morphedimage')}/>*/}

					{/*<Link to = '/'><i className = 'fa fa-heart fa-2x'/></Link>
					<Link to = '/gokuldham'><i className = 'fa fa-heart fa-2x'/></Link>
					<Link to = '/haryana'><i className = 'fa fa-heart fa-2x'/></Link>
					<Link to = '/morphedimage'><i className = 'fa fa-heart fa-2x'/></Link>
					<Link to = '/signature'><i className = 'fa fa-heart fa-2x'/></Link>*/}

					<NavLink exact activeClassName = 'active-link' to = '/'><i className = 'fa fa-heart fa-2x'/></NavLink>
					<NavLink exact activeClassName = 'active-link' to = '/gokuldham'><i className = 'fa fa-heart fa-2x'/></NavLink>
					<NavLink exact activeClassName = 'active-link' to = '/haryana'><i className = 'fa fa-heart fa-2x'/></NavLink>
					<NavLink exact activeClassName = 'active-link' to = '/morphedimage'><i className = 'fa fa-heart fa-2x'/></NavLink>
					<NavLink exact activeClassName = 'active-link' to = '/signature'><i className = 'fa fa-heart fa-2x'/></NavLink>

				</section>
				<section className = 'thecard'>
					{/*{renderedPage}*/}
					<Switch>  {/*Switch ensures that there is not a follow-through, only one component show up. limits us to only one match*/}
			          <Route exact path = '/' component = {WelcomePage} />
			          {/*<Route exact path = '/gokuldham' component = {() => <Gokuldham name = 'गंगा'/>} />*/}
			          <Route exact path = '/gokuldham' render = {() => <Gokuldham name = 'गंगा'/>} />
			          <Route exact path = '/haryana' component = {Haryana} />
			          <Route exact path = '/signature' component = {Signature} />
			          <Route exact path = '/morphedimage' component = {MorphedImage} />
			        </Switch>
				</section>					
			</div>
		)
	}
}

export default NavigateCards