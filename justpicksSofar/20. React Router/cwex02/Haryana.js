import React from 'react'
import theimage from './assets/images/doubleExposureLBN.jpg'
import './thecomponent.css'

export class Haryana extends React.Component {
	render() {
		return (
			<div className = 'thecomponent'>
				<img src = {theimage} />
			</div>
		)
	}
}

export default Haryana